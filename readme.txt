11803391 CORTINOVIS Wesley
11803622 PLAIGE Christophe
11810341 PROVOST Lucas 

Projet Forge : MonsCard
Project ID: 14787

Ce projet est une version améliorée d'un ancien projet réalisé avec Fournier Lucie (11811511), trouvable ici :
Projet Forge : Lifap4_luluwechri_Projet 
Project ID: 13507

######################################################################################################

Compilation et exécution :

Un makefile complet est fourni. Il permet de compiler à la fois sur Linux et Windows, en version debug ou release.

Il y a trois modes de compilation principaux :
- SDL : crée bin/debug_sdl (mode debug) ou bin/main_sdl (mode release). Il s'agit du jeu lui-même.
- editeur: crée bin/debug_editeur (mode debug) ou bin/main_editeur (mode release). Il permet d'utiliser l'éditeur de monde développé en parallèle.

Un projet Code::Blocks est fourni. Si vous compilez depuis ce projet, vérifiez que vous avez bien les bonnes build targets :
- DebugSDL -> bin/debug_sdl(.exe)
- ReleaseSDL -> bin/main_sdl(.exe)
- DebugTXT -> bin/debug_editeur(.exe)
- ReleaseTXT -> bin/main_editeur(.exe)

Vous pouvez également lancer l'application directement depuis les exécutables présents dans le dossier bin.
/!\ Ne supprimez pas et ne videz pas le dossier bin. Il contient toutes les librairies nécessaires.

######################################################################################################

But du jeu :

Livrez des combats à base de cartes afin de gagner de nouvelles cartes, de l'or et de l'expérience.
Montez en niveau, parlez avec les villageois et réalisez des quêtes pour les aider.

Fonctionnalités :

- Interface graphique via sdl
- Déplacement à travers la map et différents niveaux
- Menu d'édition du deck de cartes
- Sauvegardes et chargements de sauvegardes
- Interaction avec les Personnages Non Joueurs (PNJ)
- Système de quête principale pour poursuivre l'histoire (tutoriel)
- Système de quêtes secondaires données par des PNJ
- Recueil de quête afin de consulter les quêtes en cours/terminées
- Apparitions d'ennemis
- Montée en puissance des ennemis au cours de l'aventure
- Affrontement des ennemis rencontrés
- IA de combat pour les alliés et les adversaires
- IA de déplacement pour les ennemis qui ne sont pas en mode garde (qui se déplacent)
- Gestion des victoires/défaites
- Gain d'expérience, de cartes et de pièces d'or
- Système de coffre, d'inventaire et de récupération d'objets sur les ennemis vaincus

Cette version n'offre pas toutes les fonctionnalités pensées pour le jeu, il ne s'agit encore que d'une Alpha dont le tutoriel permet de comprendre les bases ! 
Le jeu a été pensé de manière à permettre la création d'une longue aventure, et l'ajout facile de nouveaux niveaux, adversaires, pnj, décors, quêtes ou objets.

######################################################################################################

Commandes du jeu :

- Utilisez les touches ZQSD pour déplacer le personnage et naviguer dans les menus.
- Utilisez la touche Echap pour afficher ou fermer le menu, ou refermer une fenêtre ouverte.
- Utilisez la touche Espace ou le clic gauche pour valider dans un menu.
- Utilisez la touche C pour afficher le menu d'édition du deck (gauche : deck ; droite : réserve)
	- clic gauche pour ajouter/enlever du deck, clic droit pour zoomer sur une carte
- En combat :
	- utilisez un clic gauche pour jouer une carte (attention au coût en mana) ou appuyer sur finir le tour ;
	- utilisez un clic gauche pour valider la carte de l'adversaire ;
	- passez la souris sur l'image de l'ennemi ou du joueur pour afficher ses statuts (poison, esquive...) ;
	- utilisez un clic droit sur l'une de vos cartes pour zoomer dessus
- Lors des dialogues :
	- utilisez la touche E pour passer à la phrase suivante. Lorsque le narrateur parle, vous pouvez utiliser la touche Echap deux fois pour le passer.
- Lors des dialogues ou de l'ouverture d'un menu ou d'une fenêtre d'information, votre personnage ne peut plus bouger.
- Entrez en collision avec un ennemi pour déclencher un combat
- Utilisez la touche E pour interagir avec un coffre fermé ou un PNJ souhaitant vous parler
- Accédez à un autre niveau en atteignant l'extrémité sans obstacle d'un niveau (toutes les extrémités sans obstacles ne sont pas nécessairement des sorties)
- Utilisez la touche R pour accéder à votre recueil de quête une fois celui-ci débloqué (première quête acceptée/refusée)
- Utilisez la touche I pour accéder à votre inventaire une fois celui-ci débloqué (premier coffre refermé)

######################################################################################################

Règles du jeu :

- Vous avez un certain nombre de points de vie (PV). S'ils tombent à 0, vous mourrez.
- Si vous mourez, vous pourrez reprendre à votre dernière sauvegarde ou charger une nouvelle partie.
- En combattant des monstres, vous pourrez gagner de l'expérience (XP) : elle vous permettra de gagner des niveaux.
- Plus votre niveau sera élevé et plus vous serez fort. Vous aurez besoin de gagner des niveaux pour battre certains monstres.
- Lorsque vous combattez l'ennemi, prenez garde au coût en mana de vos cartes (souvent vous ne pourrez pas jouer le premier tour).
- Prenez également garde aux statuts : par exemple, si l'ennemi est invulnérable, vous ne lui ferez rien en l'attaquant.
	- Bouclier : le personnage possède un bouclier qui prendra un certain nombre de dégâts à sa place
	- Esquive : le personnage a une certaine chance d'éviter une attaque
	- Invulnérabilité : le personnage ne peut pas prendre de dégâts
	- Poison : le personnage subit quelques dégâts supplémentaires à chaque tour
	- Dernière chance : si les PV du personnage tombent à 0, il a droit à un tour supplémentaire
	- Confusion : le personnage risque de se blesser lui-même s'il attaque
- Les cartes que vous pouvez jouer sont les suivantes :
	- Attaque : vous attaquez l'adversaire. Certaines cartes infligent des effets.
	- Défense : vous vous protégez à l'aide d'un bouclier ou d'un statut
	- Soin : vous recouvrez des PV
	- Bonus : vous gagnez de l'expérience, de l'or, de l'attaque ou de la défense supplémentaire
- Lisez les descriptions des cartes en zoomant dessus pour en savoir plus.
- A l'issue du combat, vous pourrez obtenir une carte. N'oubliez pas de regarder et d'ajuster votre deck en fonction des cartes obtenues.
- Vous devrez parler à certains PNJ pour avancer dans le scénario.
- Si vous refusez une quête, vous n'aurez plus d'occasion de l'accepter.
- N'oubliez pas de sauvegarder régulièrement votre partie (6 slots disponibles) : ainsi, si vous mourrez, vous n'aurez pas à tout
reprendre depuis le début.
- Pour regagner des PV, vous pouvez utiliser des potions. Vous pouvez en acheter auprès de vendeurs (avec un drapeau vert en SDL
et notés 'V' en TXT) et les utilisez depuis votre inventaire (accessible depuis le menu).
- A la fin de chaque combat, vous gagnez de l'or et de l'expérience.
- Après la mort d'un ennemi, il y a une chance que celui-ci lâche des objets, créant une tombe sur la carte avec laquelle vous pourrez interagir (même fonctionnement qu'un coffre)
- Vous pouvez également trouver des objets dans les coffres.
- Les objets ne sont actuellement pas utilisables et l'équipement pas équipable bien que l'inventaire soit fonctionnel, l'or ne peut pas être utilisé pour l'instant (pas encore de vendeur).
- Une fois le tutoriel fini, vous obtenez 3 familiers qui se battront à vos côtés afin de montrer que le combat à plusieurs est fonctionnel.

######################################################################################################

Organisation du projet :

/	 le projet Code::Blocks, le Makefile, le readme et le .gitignore
/bin/    les exécutables et des librairies sdl
/data/    les fichiers textes, les polices et les images 
	/cartes/   les images pour toutes les cartes du jeu et le fichier txt permettant de les charger
	/decors/    les images des décors du terrain
	/dialogues/    les fichiers txt pour charger les dialogues des pnj et du scénario
	/ennemis/    les images des ennemis
	/interactions/	  les images des décors interactifs (pnj, coffres, joueuse, ...)
	/interfaces/    les images des menus, fonds pour les dialogues, sélection etc.
	/musiques/	la musique du jeu
	/niveaux/    les fichiers txt pour charger les informations sur le monde et les différents niveaux
	/objets/    les images et fichiers pour charger les objets du jeu
	/personnages/    les images et fichiers des personnages alliés (familiers...)
	/polices/    les fichiers de police d'écriture
	/quetes/  le fichier txt pour charger les quêtes 
	/sauvegardes/    les fichiers txt et images de sauvegarde
/doc/    Le diagramme des classes UML reprenant la majorité des classes utilisées
/extern/    les librairies sdl
/src/    les fichiers sources du projet
