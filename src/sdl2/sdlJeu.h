#ifndef SDLJEU_H
#define SDLJEU_H

#include <stdlib.h>
#include <vector>
#include "SDL_ttf.h"
#include "SDL_mixer.h"
#include "Jeu.h"
#include "Image.h"

class sdlJeu {
public:
    /**@brief Constructeur de sdlJeu */
    sdlJeu();
    /**@brief Destructeur de sdlJeu */
    ~sdlJeu();
    /**@brief Fait une capture d'ecran ; renvoie true en cas de succes  @param [in] string& nomFichier @param [in] SDL_Window* window @param [in] SDL_Renderer* renderer @return bool*/
    bool captureEcran(const std::string& nomFichier, SDL_Window* window, SDL_Renderer* renderer);
    /**@brief Permet d'affiche du texte a l'ecran @param [in] int x @param [in] int y @param [in] int w @param [in] int h @param string texte @param TTF_Font* police @param SDL_Color couleur @return non*/
    void afficheTexte(int x, int y, int w, int h, std::string texte, TTF_Font* police, SDL_Color couleur, unsigned int transparence=255);
    /**@brief Gere l'affichage sur l'ecran  @param non @return non*/
    void sdlAff();
    /**@brief Gere le fonctionnement du jeu en temps reel (interactions utilisateur) @param non @return non*/
    void sdlBoucle();
    void tourIA();
    unsigned int tailleSansAccents(std::string chaine) const;

private:
    void sdlAffCase(unsigned int i, Terrain* terrain, unsigned int debutTerrainX, unsigned int debutTerrainY, unsigned int& indiceCoffre);
    void sdlAffJoueur(const Joueur* joueur, unsigned int debutTerrainX, unsigned int debutTerrainY);
    void sdlAffEnnemi(const Ennemi* ennemi, bool vivant, unsigned int debutTerrainX, unsigned int debutTerrainY, int numeroEnnemi);
    void partitionPoint(Point T[],  int pivot, unsigned int debut, unsigned int fin, unsigned int& k1, unsigned int& k2);
    void triPartitionPoint(Point T[], unsigned int debut, unsigned int fin);
    void sdlAffEquipier(const Personnage* equipier, unsigned int debutTerrainX, unsigned int debutTerrainY);
    void sdlAffQuete(Quete* queteAffichee, unsigned int debutX, unsigned int debutY, unsigned int niveauProfondeur, unsigned int hauteur);

    SDL_Window * window;
    SDL_Renderer * renderer;
    SDL_Rect camera;

    int DIMX;
    int DIMY;

    // Polices
    TTF_Font* policeTexte;
    SDL_Color couleurPolice;
    SDL_Color couleurPoliceOr;
    TTF_Font* policeDescription;
    TTF_Font* policeMenu;
    SDL_Color couleurPoliceMenu;

    // Musique
    Mix_Music *m_Musique;
    Mix_Music *m_msqScenario;

    Jeu jeu;

    // Images
    Image im_texte;    // Pour afficher du texte
    std::string phrase; // Phrase texte a afficher

    // Personnages
    Image im_joueur;
    SDL_Rect m_spritesJoueur[24];

    Image im_pnjBulle;
    Image im_narrateurBulle;
    Image im_bulle;
    Image im_toucheE;

    // Terrain
    std::vector<Image*> im_premiereCouche;
    // Decors et interactions
    Image im_coffreOuvert;
    std::vector<Image*> im_deuxiemeCouche;
    // PNJ
    std::vector<Image*> im_imagePnj;

    // Ennemis
    std::vector<Image*> im_tabImagesEnnemis;
    Image im_champDeVision;

    // Equipe
    std::vector<Image*> im_tabImagesEquipers;

    // Inventaire
    int m_zoomObjet;
    Image im_cadre;
    Image im_equipement;
    Image im_fondeq;
    Image im_bourse;
    Image im_toucheEbis;
    Image im_coffre;
    Image im_bulleInfo;
    std::vector<Image*> im_tabImagesEquipementVide;

    Image im_tombe;

    // Objets
    std::vector<Image*> im_tabImagesObjets;

    // Cartes
    std::vector<Image*> im_tabImagesReserve;
    Image im_masqueCartes;
    Image im_nombreCartes[4];

    // Personnages
    std::vector<Image*> im_tabImagesPersonnages;

    // Quetes
    unsigned int m_moletteRecueil;
    int m_queteRecueil;
    Image im_fondRecueil;
    Image im_enteteRecueil;
    Image im_piedPageRecueil;
    Image im_barreDefilementRecueil;
    Image im_barreCoulissanteRecueil;
    Image im_boussole;

    // Menus
    Image im_menu;
    Image im_menuPrincipal;
    Image im_quitter;
    Image im_sauvegarde;
    Image im_chargement;
    Image im_confirmSauvegarde;
    Image im_chargementError;
    Image im_sauvegardeOK;
    Image im_sauvegardeError;
    Image im_selectionGrande;
    Image im_selectionPetite;
    Image im_selectionSauvegarde;
    Image im_victoire;
    Image im_levelUp;
    Image im_combat;
    Image im_fondCombat;

    // Autre
    Image im_captures[6];   // Captures d'ecran
    Image im_or;
    Image im_barrePV;
    Image im_barreXP;
    Image im_PV;
    Image im_XP;
    Image im_nouveau;
    Image im_controlesGeneraux;
    Image im_controlesCartes;
    Image im_controlesMenu;
    Image im_echapCombat;
    Image im_controlesCombat;
    Image im_fleche;
    Image im_boutonMenu;
    Image im_boutonRetour;

    // Editeur de deck
    int m_zoomCarte;
    int m_carteSelectionnee;
    Point m_posSouris;
    unsigned int m_hauteurCarteReserve;
    unsigned int m_largeurCarteReserve;
    unsigned int m_nbLignesReserve;
    unsigned int m_hauteurCarteDeck;
    unsigned int m_largeurCarteDeck;
    unsigned int m_nbLignesDeck;

    // Combat
    int m_tailleMainJoueur;
    int m_tailleMainEnnemi;
    int m_hauteurCarteJoueur;
    int m_hauteurCarteEnnemi;
    int m_largeurCarteJoueur;
    int m_largeurCarteEnnemi;
    int m_afficheStatut;
    int m_afficheBulleX;
    int m_afficheBulleY;
    bool m_jouerCarte;
    int m_jeuCarte;
    unsigned int m_dernierActif;
    std::string m_nomDerniereCarte;
    unsigned int m_molette=0;
    bool m_DoitSelection;

    unsigned int m_selectionPerso;
    int m_carteAjouer;

    // Marche noir
    int m_hauteurCarteMarche;
    int m_largeurCarteMarche;

    // Dialogues
    PNJ* m_PNJ;    // lien vers le PNJ qui parle
    double m_animationEchapScenario;
    bool m_afficherEchapScenario;
    double m_animationTransitionNoire;

    // Autre
    char m_faceJoueur; // f = face, b = dos, g = gauche, d = droite
    int m_frame;
    bool m_joueurAvance;
    unsigned int m_tailleCase;
    unsigned int m_tailleCaseOriginale;
};

#endif
