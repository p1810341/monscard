#include "sdlJeu.h"
#include <iostream>
#include <cassert>
#include <fstream>
#include <string.h>
#include <math.h>
#include <unistd.h>

sdlJeu::sdlJeu () {
    DIMX=1600;
    DIMY=900;
    // Initialisation SDL
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        std::cout<<"sdlJeu::sdlJeu : SDL ne peut pas s'initialiser - "<<SDL_GetError()<<std::endl;
        exit(1);
    }
    // Initialisation TTF
    if(TTF_Init() == -1) {
        std::cout << "sdlJeu::sdlJeu : Erreur d'initialisation de TTF_Init - " << TTF_GetError() << std::endl;
        exit(1);
    }
    // Initialisation audio
     //Initialize SDL_mixer
    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 ) {
        std::cout << "sdlJeu::sdlJeu : Erreur d'initialisation de l'audio - " << Mix_GetError() << std::endl;
        exit(1);
    }
    // Creation de la fenetre
    window = SDL_CreateWindow("MonsCard", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                DIMX, DIMY, SDL_WINDOW_FULLSCREEN_DESKTOP); //SDL_WINDOW_OPENGL pour fenetre
    if(window == NULL) {
        std::cout<<"sdlJeu::sdlJeu : La fenetre ne peut pas se creer - "<<SDL_GetError()<<std::endl;
        exit(1);
    }
    SDL_DisplayMode dm;
    if (SDL_GetDesktopDisplayMode(0, &dm) != 0) {
        SDL_Log("SDL_GetDesktopDisplayMode failed: %s", SDL_GetError());
        exit(1);
    }
    DIMX=dm.w;
    DIMY=dm.h;
    m_tailleCaseOriginale=70;             // represente la taille d'affichage voulu en 1600*900
    m_tailleCase=((float)DIMX/1600)*m_tailleCaseOriginale;

    // Renderer
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    // Chargement police
    policeTexte = NULL;
    policeTexte = TTF_OpenFont("data/polices/arial.ttf", 100);
    if (policeTexte == NULL)
    {
        policeTexte = TTF_OpenFont("../data/polices/arial.ttf", 100);
        if(policeTexte == NULL) {
            policeTexte = TTF_OpenFont("../../data/polices/arial.ttf", 100);
            if (policeTexte == NULL) {
                std::cout << "sdlJeu::sdlJeu : Erreur dans le chargement de la police arial.ttf - " << TTF_GetError() << std::endl;
                SDL_Quit();
                assert(policeTexte != NULL);
            }
        }
    }
    couleurPolice.r = 255;
    couleurPolice.g = 255;
    couleurPolice.b = 255;
    couleurPolice.a = 255;
    couleurPoliceOr.r = 0;
    couleurPoliceOr.g = 0;
    couleurPoliceOr.b = 0;
    couleurPoliceOr.a = 255;

    // Chargement police menu
    policeMenu = NULL;
    policeMenu = TTF_OpenFont("data/polices/goudosb.ttf", 100);
    if (policeMenu == NULL)
    {
        policeMenu = TTF_OpenFont("../data/polices/goudosb.ttf", 100);
        if(policeMenu == NULL) {
            policeMenu = TTF_OpenFont("../../data/polices/goudosb.ttf", 100);
            if(policeMenu == NULL) {
                std::cout << "sdlJeu::sdlJeu : Erreur dans le chargement de la police goudosb.ttf - " << TTF_GetError() << std::endl;
                SDL_Quit();
                assert(policeMenu != NULL);
            }
        }
    }
    couleurPoliceMenu.r = 132;
    couleurPoliceMenu.g = 86;
    couleurPoliceMenu.b = 51;
    couleurPoliceMenu.a = 255;

    // Chargement musique
    m_Musique = Mix_LoadMUS( "data/musiques/musique.wav" );
    if(m_Musique == NULL) {
        m_Musique = Mix_LoadMUS( "../data/musiques/musique.wav" );
        if(m_Musique == NULL) {
            m_Musique = Mix_LoadMUS( "../../data/musiques/musique.wav" );
            if(m_Musique == NULL) {
                std::cout << "sdlJeu::sdlJeu : Erreur dans le chargement de la musique musique.wav - " << Mix_GetError() << std::endl;
                // On ne quitte pas : tant pis s'il n'y a pas la musique
            }
        }
    }
    m_msqScenario = Mix_LoadMUS( "data/musiques/msq_scenario.wav" );
    if(m_msqScenario == NULL) {
        m_msqScenario = Mix_LoadMUS( "../data/musiques/msq_scenario.wav" );
        if(m_msqScenario == NULL) {
            m_msqScenario = Mix_LoadMUS( "../../data/musiques/msq_scenario.wav" );
            if(m_msqScenario == NULL) {
                std::cout << "sdlJeu::sdlJeu : Erreur dans le chargement de la musique msq_scenario.wav - " << Mix_GetError() << std::endl;
            }
        }
    }

    // Chargement images
    im_joueur.loadFromFile("data/interactions/joueur2.png", renderer);
    // On charge les sprites du joueur
    for(int i = 0 ; i < 4 ; i++) {
        for(int j = 0 ; j < 6 ; j++) {
            m_spritesJoueur[i*6 + j] = {j*50, i*50, 50, 50};
        }
    }
    im_narrateurBulle.loadFromFile("data/interactions/narrateurBulle.png", renderer);
    im_bulle.loadFromFile("data/interfaces/bulle.png", renderer);
    im_menu.loadFromFile("data/interfaces/menu.png", renderer);
    im_menuPrincipal.loadFromFile("data/interfaces/menuPrincipal.png", renderer);
    im_quitter.loadFromFile("data/interfaces/quitter.png", renderer);
    im_sauvegarde.loadFromFile("data/interfaces/sauvegarde.png", renderer);
    im_chargement.loadFromFile("data/interfaces/chargement.png", renderer);
    im_confirmSauvegarde.loadFromFile("data/interfaces/confirmSauvegarde.png", renderer);
    im_chargementError.loadFromFile("data/interfaces/chargementError.png", renderer);
    im_sauvegardeOK.loadFromFile("data/interfaces/sauvegardeOK.png", renderer);
    im_sauvegardeError.loadFromFile("data/interfaces/sauvegardeError.png", renderer);
    im_selectionGrande.loadFromFile("data/interfaces/selectionGrande.png", renderer);
    im_selectionPetite.loadFromFile("data/interfaces/selectionPetite.png", renderer);
    im_selectionSauvegarde.loadFromFile("data/interfaces/selectionSauvegarde.png", renderer);
    im_victoire.loadFromFile("data/interfaces/victoire.png", renderer);
    im_levelUp.loadFromFile("data/interfaces/levelUp.png", renderer);
    im_nouveau.loadFromFile("data/interfaces/nouveau.png", renderer);
    im_or.loadFromFile("data/interfaces/or.png", renderer);
    im_barrePV.loadFromFile("data/interfaces/barrePV.png", renderer);
    im_barreXP.loadFromFile("data/interfaces/barreXP.png", renderer);
    im_PV.loadFromFile("data/interfaces/PV.png", renderer);
    im_XP.loadFromFile("data/interfaces/XP.png", renderer);
    im_fleche.loadFromFile("data/interfaces/fleche.png", renderer);
    im_nombreCartes[0].loadFromFile("data/cartes/0.png",renderer);
    im_nombreCartes[1].loadFromFile("data/cartes/1.png",renderer);
    im_nombreCartes[2].loadFromFile("data/cartes/2.png",renderer);
    im_nombreCartes[3].loadFromFile("data/cartes/3.png",renderer);
    im_masqueCartes.loadFromFile("data/interfaces/masque_cartes.png", renderer);
    im_controlesGeneraux.loadFromFile("data/interfaces/controlesGeneraux.png", renderer);
    im_controlesCartes.loadFromFile("data/interfaces/controlesCartes.png", renderer);
    im_controlesMenu.loadFromFile("data/interfaces/controlesMenu.png", renderer);
    im_echapCombat.loadFromFile("data/interfaces/echapCombat.png", renderer);
    im_controlesCombat.loadFromFile("data/interfaces/controlesCombat.png", renderer);
    im_boutonMenu.loadFromFile("data/interfaces/boutonMenu.png", renderer);
    im_boutonRetour.loadFromFile("data/interfaces/boutonRetour.png", renderer);
    im_toucheE.loadFromFile("data/interfaces/toucheE.png", renderer);
    im_combat.loadFromFile("data/interfaces/combat.png", renderer);
    im_fondCombat.loadFromFile("data/interfaces/fondCombat.png", renderer);
    im_champDeVision.loadFromFile("data/interactions/champDeVision.png", renderer);

    // Initialisation Ennemis
    std::string nomFichier="data/ennemis/nomsImagesEnnemis.txt";
    std::ifstream nomsImagesEnnemis (nomFichier.c_str());
    if(!nomsImagesEnnemis.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        nomsImagesEnnemis.open(parent);
        if(!nomsImagesEnnemis.is_open()) {
            parent = std::string("../") + parent;
            nomsImagesEnnemis.open(parent);
            if(!nomsImagesEnnemis.is_open()) {
                std::cout << "sdlJeu::sdlJeu : Erreur dans l'ouverture du fichier "<< nomFichier << std::endl;
                SDL_Quit();
                assert(nomsImagesEnnemis.is_open());
            }
        }
    }
    if (nomsImagesEnnemis.is_open()) {
        std::string nomImage;
        unsigned int i=0;
        unsigned int c[4];
        while (!nomsImagesEnnemis.eof()) {
            nomsImagesEnnemis >> nomImage >> c[0] >> c[1] >> c[2] >> c[3];
            std::string chemin = "data/ennemis/";
            chemin += nomImage;
            im_tabImagesEnnemis.push_back(new Image());
            im_tabImagesEnnemis[i]->loadFromFile(chemin.c_str(), renderer);
            i++;
        }
        nomsImagesEnnemis.close();
    }

    //chargement Inventaire
    m_zoomObjet = -1;
    im_cadre.loadFromFile("data/interfaces/cadre.png", renderer);
    im_equipement.loadFromFile("data/interfaces/equipement.png", renderer);
    im_fondeq.loadFromFile("data/interfaces/fondeq.png", renderer);
    im_bourse.loadFromFile("data/interfaces/bourse.png", renderer);
    im_coffre.loadFromFile("data/interfaces/coffre.png", renderer);
    im_bulleInfo.loadFromFile("data/interfaces/bulleInfo.png", renderer);
    im_tabImagesEquipementVide.push_back(new Image());
    im_tabImagesEquipementVide[0]->loadFromFile("data/interfaces/tete.png", renderer);
    im_tabImagesEquipementVide.push_back(new Image());
    im_tabImagesEquipementVide[1]->loadFromFile("data/interfaces/dos.png", renderer);
    im_tabImagesEquipementVide.push_back(new Image());
    im_tabImagesEquipementVide[2]->loadFromFile("data/interfaces/amulette.png", renderer);
    im_tabImagesEquipementVide.push_back(new Image());
    im_tabImagesEquipementVide[3]->loadFromFile("data/interfaces/anneaug.png", renderer);
    im_tabImagesEquipementVide.push_back(new Image());
    im_tabImagesEquipementVide[4]->loadFromFile("data/interfaces/anneaud.png", renderer);
    im_tabImagesEquipementVide.push_back(new Image());
    im_tabImagesEquipementVide[5]->loadFromFile("data/interfaces/botte.png", renderer);
    nomFichier="data/objets/nomsImagesObjets.txt";
    std::ifstream nomsImagesObjets (nomFichier.c_str());
    if(!nomsImagesObjets.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        nomsImagesObjets.open(parent);
        if(!nomsImagesObjets.is_open()) {
            parent = std::string("../") + parent;
            nomsImagesObjets.open(parent);
            if(!nomsImagesObjets.is_open()) {
                std::cout << "sdlEditeur::sdlEditeur : Erreur dans l'ouverture du fichier "<< nomFichier << std::endl;
                SDL_Quit();
                assert(nomsImagesObjets.is_open());
            }
        }
    }
    if(nomsImagesObjets.is_open()) {
        std::string nomImage;
        unsigned int i=0;
        while (!nomsImagesObjets.eof()) {
            nomsImagesObjets >> nomImage;
            std::string chemin = "data/objets/";
            chemin += nomImage + ".png";
            im_tabImagesObjets.push_back(new Image());
            im_tabImagesObjets[i]->loadFromFile(chemin.c_str(), renderer);
            i++;
        }
        nomsImagesObjets.close();
    }

    // Initialisation Cartes
    nomFichier="data/cartes/nomsImagesCartes.txt";
    std::ifstream nomsImagesCartes (nomFichier.c_str());
    if(!nomsImagesCartes.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        nomsImagesCartes.open(parent);
        if(!nomsImagesCartes.is_open()) {
            parent = std::string("../") + parent;
            nomsImagesCartes.open(parent);
            if(!nomsImagesCartes.is_open()) {
                std::cout << "sdlJeu::sdlJeu : Erreur dans l'ouverture du fichier "<< nomFichier << std::endl;
                SDL_Quit();
                assert(nomsImagesCartes.is_open());
            }
        }
    }
    if (nomsImagesCartes.is_open()) {
        std::string nomImage;
        int i=0;
        while (std::getline(nomsImagesCartes,nomImage)) {
            std::string chemin = "data/cartes/";
            chemin += nomImage;
            im_tabImagesReserve.push_back(new Image());
            im_tabImagesReserve[i]->loadFromFile(chemin.c_str(), renderer);
            i++;
        }
        nomsImagesCartes.close();
    }

    // Gestion Cartes
    m_zoomCarte=-1;
    m_carteSelectionnee=-1;
    m_largeurCarteReserve=std::min((float)3.5/10*DIMX/10,(float)400);
    m_hauteurCarteReserve=(float)16/10*m_largeurCarteReserve;
    m_nbLignesReserve=8;
    m_hauteurCarteDeck=0;
    m_largeurCarteDeck=0;
    m_nbLignesDeck=0;
    m_posSouris.x=0;
    m_posSouris.y=0;

    // Gestion combat
    m_tailleMainJoueur=0;
    m_tailleMainEnnemi=0;
    m_largeurCarteJoueur=0;
    m_hauteurCarteJoueur=0;
    m_largeurCarteEnnemi=0;
    m_hauteurCarteEnnemi=0;
    m_afficheStatut=-1;
    m_jouerCarte=false;
    m_jeuCarte=-1;
    m_afficheBulleY=-1;
    m_afficheBulleX=-1;

    // Gestion marche noir
    m_hauteurCarteMarche=DIMY/4;
    m_largeurCarteMarche=10*m_hauteurCarteMarche/16;

    // Dialogues
    m_PNJ = NULL;
    m_animationEchapScenario = 0;
    m_afficherEchapScenario = false;
    m_animationTransitionNoire = 0;

    // Quetes
    m_moletteRecueil=0;
    m_queteRecueil=-1;
    im_fondRecueil.loadFromFile("data/interfaces/fondRecueil.png", renderer);
    im_enteteRecueil.loadFromFile("data/interfaces/enteteRecueil.png", renderer);
    im_piedPageRecueil.loadFromFile("data/interfaces/piedPageRecueil.png", renderer);
    im_barreDefilementRecueil.loadFromFile("data/interfaces/barreDefilementRecueil.png", renderer);
    im_barreCoulissanteRecueil.loadFromFile("data/interfaces/barreCoulissanteRecueil.png", renderer);
    im_boussole.loadFromFile("data/interfaces/boussole.png", renderer);

    // Images de sauvegarde
    for(int i = 0 ; i < 6 ; i++) {
        std::string chemin = "data/sauvegardes/sauvegarde";
        chemin += std::to_string(i) + ".bmp";
        if(!jeu.estFichierVide(i)) im_captures[i].loadFromFile(chemin.c_str(), renderer);
    }

    // Autre
    m_faceJoueur = 'f';
    m_frame = 0;
    m_joueurAvance = false;
    camera.x=0;
    camera.y=0;

    // On recupere les informations sur les decors du jeu (uniquement les images)
    // Terrain
    nomFichier = "data/decors/premiereCouche.txt";
    std::ifstream fichier(nomFichier.c_str());
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"sdlJeu::sdlJeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                SDL_Quit();
                assert(fichier.is_open());
            }
        }
    }
    if (fichier.is_open()) {
        std::string ligne;
        bool marchable;
        unsigned int i=0;
        while (!fichier.eof()) {
            fichier >> ligne >> marchable;
            im_premiereCouche.push_back(new Image());
            ligne="data/decors/"+ligne+".png";
            im_premiereCouche[i]->loadFromFile(ligne.c_str(), renderer);
            i++;
        }
        fichier.close();
    }
    // Decors
    nomFichier = "data/decors/deuxiemeCouche1.txt";
    std::ifstream fichier2(nomFichier.c_str());
    if(!fichier2.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier2.open(parent.c_str());
        if(!fichier2.is_open()) {
            parent = std::string("../") + parent;
            fichier2.open(parent.c_str());
            if(!fichier2.is_open()) {
                std::cout<<"sdlJeu::sdlJeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                SDL_Quit();
                assert(fichier2.is_open());
            }
        }
    }
    if (fichier2.is_open()) {
        std::vector<bool> vectorMarchable;
        bool marchable;
        std::string ligne;
        unsigned int i=0;
        unsigned int c[4];
        while (!fichier2.eof()) {
            fichier2 >> ligne >> marchable;
            im_deuxiemeCouche.push_back(new Image());
            ligne="data/decors/"+ligne+".png";
            im_deuxiemeCouche[i]->loadFromFile(ligne.c_str(), renderer);
            if (!marchable) {
                fichier2 >> c[0] >> c[1] >> c[2] >> c[3];
            }
            i++;
        }
        // Interactions et PNJ
        im_coffreOuvert.loadFromFile("data/interactions/coffreOuvert.png", renderer);
        nomFichier = "data/interactions/deuxiemeCouche2.txt";
        std::ifstream fichier3(nomFichier.c_str());
        if(!fichier3.is_open()) {
            std::string parent = std::string("../") + nomFichier;
            fichier3.open(parent.c_str());
            if(!fichier3.is_open()) {
                parent = std::string("../") + parent;
                fichier3.open(parent.c_str());
                if(!fichier3.is_open()) {
                    std::cout<<"sdlJeu::sdlJeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                    SDL_Quit();
                    assert(fichier3.is_open());
                }
            }
        }
        if (fichier3.is_open()) {
            unsigned int action;
            Point p;
            while (!fichier3.eof()) {
                fichier3 >> ligne >> marchable;
                if (!marchable) {
                    fichier3 >> c[0] >> c[1] >> c[2] >> c[3];
                }
                fichier3 >> action;
                if (action==1) {
                    fichier3 >> p.x >> p.y;
                }
                else if (action==3) {  // PNJ
                    std::string ligne2=ligne+"Bulle";
                    im_imagePnj.push_back(new Image());
                    ligne2="data/interactions/"+ligne2+".png";
                    im_imagePnj[im_imagePnj.size()-1]->loadFromFile(ligne2.c_str(), renderer);
                }
                im_deuxiemeCouche.push_back(new Image());
                ligne="data/interactions/"+ligne+".png";
                im_deuxiemeCouche[i]->loadFromFile(ligne.c_str(), renderer);
                i++;
            }
            fichier3.close();
        }
        fichier2.close();
    }
    im_tombe.loadFromFile("data/interactions/tombe.png", renderer);
    // Personnages
    nomFichier="data/personnages/nomsImagesPersonnages.txt";
    std::ifstream nomsImagesPersonnages (nomFichier.c_str());
    if(!nomsImagesPersonnages.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        nomsImagesPersonnages.open(parent);
        if(!nomsImagesPersonnages.is_open()) {
            parent = std::string("../") + parent;
            nomsImagesPersonnages.open(parent);
            if(!nomsImagesPersonnages.is_open()) {
                std::cout << "sdlEditeur::sdlEditeur : Erreur dans l'ouverture du fichier "<< nomFichier << std::endl;
                SDL_Quit();
                assert(nomsImagesPersonnages.is_open());
            }
        }
    }
    if(nomsImagesPersonnages.is_open()) {
        std::string nomImage;
        unsigned int i=0;
        unsigned int c[4];
        while (!nomsImagesPersonnages.eof()) {
            nomsImagesPersonnages >> nomImage >> c[0] >> c[1] >> c[2] >> c[3];
            std::string chemin = "data/personnages/";
            chemin += nomImage;
            im_tabImagesPersonnages.push_back(new Image());
            im_tabImagesPersonnages[i]->loadFromFile(chemin.c_str(), renderer);
            i++;
        }
        nomsImagesPersonnages.close();
    }
    m_carteAjouer=-1;
    m_dernierActif=-1;
    m_DoitSelection=false;

}

sdlJeu::~sdlJeu() {
    for (unsigned int i=0; i<im_premiereCouche.size(); i++)
        delete im_premiereCouche[i];
    for (unsigned int i=0; i<im_deuxiemeCouche.size(); i++)
        delete im_deuxiemeCouche[i];
    for (unsigned int i=0; i<im_imagePnj.size(); i++)
        delete im_imagePnj[i];
    for (unsigned int i=0; i<im_tabImagesEnnemis.size(); i++)
        delete im_tabImagesEnnemis[i];
    for (unsigned int i=0; i<im_tabImagesReserve.size(); i++)
        delete im_tabImagesReserve[i];
    for (unsigned int i=0; i<im_tabImagesObjets.size(); i++)
        delete im_tabImagesObjets[i];
    for (unsigned int i=0; i<im_tabImagesPersonnages.size(); i++)
        delete im_tabImagesPersonnages[i];
    TTF_CloseFont(policeTexte);
    TTF_CloseFont(policeMenu);
    TTF_Quit();
    Mix_FreeMusic(m_Musique);
    m_Musique = NULL;
    Mix_FreeMusic(m_msqScenario);
    m_msqScenario = NULL;
    Mix_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

unsigned int sdlJeu::tailleSansAccents(std::string chaine) const {
    unsigned int compte = 0, j=chaine.size();
    for(unsigned i = 0 ; i < j ; i++) {
        if(int(chaine[i]) < 0 || int(chaine[i] > 127))
            compte ++;
    }
    return j-compte/2;
}

bool sdlJeu::captureEcran(const std::string& nomFichier, SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Surface* saveSurface = NULL;
    SDL_Surface* infoSurface = NULL;
    infoSurface = SDL_GetWindowSurface(window);
    if (infoSurface == NULL) {
        std::cout << "sdlJeu::captureEcran : Impossible de creer infoSurface depuis la fenetre - " << SDL_GetError() << std::endl;
        return false;
    }
    else {
        unsigned char * pixels = new unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
        if (pixels == 0) {
            std::cout << "sdlJeu::captureEcran : Impossible d'allouer de la memoire pour la capture d'ecran." << std::endl;
            return false;
        } else {
            if (SDL_RenderReadPixels(renderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
                std::cout << "sdlJeu::captureEcran : Impossible de lire les pixels depuis le SDL_Renderer - " << SDL_GetError() << std::endl;
                delete[] pixels;
                return false;
            }
            else {
                saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel,
                                                       infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
                if (saveSurface == NULL) {
                    std::cout << "sdlJeu::captureEcran : Impossible de creer la SDL_Surface depuis les donnees de pixels - " << SDL_GetError() << std::endl;
                    delete[] pixels;
                    return false;
                }
                int res = SDL_SaveBMP(saveSurface, nomFichier.c_str());
                if(res < 0) {
                    std::string parent = std::string("../") + nomFichier;
                    res = SDL_SaveBMP(saveSurface, parent.c_str());
                    if(res < 0) {
                        parent = std::string("../") + nomFichier;
                        res = SDL_SaveBMP(saveSurface, parent.c_str());
                        if(res < 0) {
                            std::cout << "sdlJeu::captureEcran : Impossible de sauvegarder la capture d'ecran " << nomFichier << " - " << SDL_GetError() << std::endl;
                            SDL_FreeSurface(saveSurface);
                            saveSurface = NULL;
                            delete [] pixels;
                            SDL_FreeSurface(infoSurface);
                            infoSurface = NULL;
                            return false;
                        }
                    }
                }
                SDL_FreeSurface(saveSurface);
                saveSurface = NULL;
            }
            delete[] pixels;
        }
        SDL_FreeSurface(infoSurface);
        infoSurface = NULL;
    }
    return true;
}

void sdlJeu::afficheTexte(int x, int y, int w, int h, std::string texte, TTF_Font* police, SDL_Color couleur, unsigned int transparence) {
    im_texte.libereSurface();
    im_texte.setSurface(TTF_RenderUTF8_Blended(police, texte.c_str(), couleur));
    im_texte.loadFromCurrentSurface(renderer);
    SDL_Rect positionPhrase = {x, y, w, h};
    if (transparence!=255) {
        SDL_SetSurfaceAlphaMod(im_texte.getSurface(), transparence);
        im_texte.libereTexture();
        im_texte.loadFromCurrentSurface(renderer);
    }
    SDL_RenderCopy(renderer, im_texte.getTexture(), NULL, &positionPhrase);
    im_texte.libereTexture();
}

void sdlJeu::sdlAffCase(unsigned int i, Terrain* terrain, unsigned int debutTerrainX, unsigned int debutTerrainY, unsigned int& indiceCoffre) {
    unsigned int c=terrain->getDeuxiemeCoucheTerrain(i);
    unsigned int tailleX=terrain->getTailleX();
    if (c<jeu.m_tailleDecors+2) {  // <= car 0 et 1 ne sont pas compris dans m_tailleDecors
        if (c>1) {
            im_deuxiemeCouche[terrain->getDeuxiemeCoucheTerrain(i)-2]->draw(renderer, (debutTerrainX+i%tailleX)*m_tailleCase - camera.x, (debutTerrainY+i/tailleX)*m_tailleCase - camera.y, m_tailleCase, m_tailleCase);   // -2 pour le 0 et 1
       }
    }
    else {
        SDL_Surface* s;
        unsigned int w, h;
        s=im_deuxiemeCouche[c-2]->getSurface();
        w=s->w*(float)m_tailleCaseOriginale/50*(float)m_tailleCase/m_tailleCaseOriginale;
        h=s->h*(float)m_tailleCaseOriginale/50*(float)m_tailleCase/m_tailleCaseOriginale;
        switch (jeu.m_interactionsAction[c-2-jeu.m_tailleDecors]) {
            case 0: { // Coffre (affichage fait plus tard)
                    Point p=jeu.getCoffre(jeu.m_niveau, indiceCoffre)->getPosition();
                    if (jeu.getCoffre(jeu.m_niveau, indiceCoffre)->getOuvert())
                        im_coffreOuvert.draw(renderer, (debutTerrainX+p.x)*m_tailleCase - camera.x, (debutTerrainY+p.y)*m_tailleCase - camera.y, m_tailleCase, m_tailleCase);
                    else
                        im_deuxiemeCouche[c-2]->draw(renderer, (debutTerrainX+p.x)*m_tailleCase - camera.x, (debutTerrainY+p.y)*m_tailleCase - camera.y, m_tailleCase, m_tailleCase);
                    indiceCoffre++;
                    break;
                }
            case 2:  // Point d'apparition
                break;
            case 3:  // PNJ
                im_deuxiemeCouche[c-2]->draw(renderer, (debutTerrainX+i%tailleX)*m_tailleCase - camera.x, (debutTerrainY+i/tailleX)*m_tailleCase - camera.y, m_tailleCase, m_tailleCase);
                break;
            default:
                im_deuxiemeCouche[c-2]->draw(renderer, (debutTerrainX+i%tailleX)*m_tailleCase - camera.x, (debutTerrainY+(int)(i/tailleX-(((h/m_tailleCase)-1)*tailleX)/tailleX))*m_tailleCase-h%m_tailleCase - camera.y, w, h);
                break;
        }
    }
}

void sdlJeu::sdlAffJoueur(const Joueur* joueur, unsigned int debutTerrainX, unsigned int debutTerrainY) {
    // Affichage joueur
    SDL_Rect render = {(int)(debutTerrainX*m_tailleCase)+joueur->getPosition().x*DIMX/1600*(int)m_tailleCaseOriginale/(int)jeu.m_tailleCase-camera.x, (int)(debutTerrainY*m_tailleCase)+joueur->getPosition().y*DIMY/900*(int)m_tailleCaseOriginale/(int)jeu.m_tailleCase-camera.y, (int)m_tailleCase, (int)m_tailleCase};
    switch(m_faceJoueur) {
        case 'f': if(!m_joueurAvance) SDL_RenderCopy(renderer, im_joueur.getTexture(), &m_spritesJoueur[12], &render);
            else SDL_RenderCopy(renderer, im_joueur.getTexture(), &m_spritesJoueur[14 + (m_frame%32)/8], &render);
            break;
        case 'b' : if(!m_joueurAvance) SDL_RenderCopy(renderer, im_joueur.getTexture(), &m_spritesJoueur[18], &render);
            else SDL_RenderCopy(renderer, im_joueur.getTexture(), &m_spritesJoueur[20 + (m_frame%32)/8], &render);
            break;
        case 'g' : if(!m_joueurAvance) SDL_RenderCopy(renderer, im_joueur.getTexture(), &m_spritesJoueur[0], &render);
            else SDL_RenderCopy(renderer, im_joueur.getTexture(), &m_spritesJoueur[2 + (m_frame%32)/8], &render);
            break;
        case 'd' : if(!m_joueurAvance) SDL_RenderCopy(renderer, im_joueur.getTexture(), &m_spritesJoueur[6], &render);
            else SDL_RenderCopy(renderer, im_joueur.getTexture(), &m_spritesJoueur[8 + (m_frame%32)/8], &render);
            break;
        default:
            break;
    }
}

void sdlJeu::sdlAffEnnemi(const Ennemi* ennemi, bool vivant, unsigned int debutTerrainX, unsigned int debutTerrainY, int numeroEnnemi) {  // Affiche l'ennemi
    unsigned int id=ennemi->getEspece()->getIdEspece();  // on recupere l'id de l'espece pour obtenir l'image correspondante et ses proportions
    SDL_Surface* s;
    unsigned int w, h;
    s=im_tabImagesEnnemis[id]->getSurface();
    w=s->w*(float)m_tailleCaseOriginale/50*(float)m_tailleCase/m_tailleCaseOriginale;
    h=s->h*(float)m_tailleCaseOriginale/50*(float)m_tailleCase/m_tailleCaseOriginale;
    if (vivant)
        im_tabImagesEnnemis[id]->draw(renderer, (int)(debutTerrainX*m_tailleCase)+ennemi->getPosition().x*DIMX/1600*(int)m_tailleCaseOriginale/(int)jeu.m_tailleCase-camera.x, (debutTerrainY*m_tailleCase)+ennemi->getPosition().y*DIMY/900*(int)m_tailleCaseOriginale/(int)jeu.m_tailleCase-camera.y, w, h);
    else
        im_tombe.draw(renderer, (int)(debutTerrainX*m_tailleCase)+ennemi->getPosition().x*DIMX/1600*(int)m_tailleCaseOriginale/(int)jeu.m_tailleCase-camera.x, (debutTerrainY*m_tailleCase)+ennemi->getPosition().y*DIMY/900*(int)m_tailleCaseOriginale/(int)jeu.m_tailleCase-camera.y, w, h);
    if (vivant)
        im_champDeVision.drawRotate(renderer, (int)(debutTerrainX*m_tailleCase)+ennemi->getPosition().x*DIMX/1600*(int)m_tailleCaseOriginale/(int)jeu.m_tailleCase-camera.x+w/2-(0.9*CHAMPDEVISION/(100)), (debutTerrainY*m_tailleCase)+ennemi->getPosition().y*DIMY/900*(int)m_tailleCaseOriginale/(int)jeu.m_tailleCase-camera.y+h/2-(0.9*CHAMPDEVISION/(100)), jeu.getChampDeVision(numeroEnnemi)+90, CHAMPDEVISION/57, CHAMPDEVISION/57);
    //tppov
}

void sdlJeu::sdlAffEquipier(const Personnage* equipier, unsigned int debutTerrainX, unsigned int debutTerrainY) {
    SDL_Surface* s;
    unsigned int w, h;
    s=im_tabImagesPersonnages[equipier->getId()]->getSurface();
    w=s->w*(float)m_tailleCaseOriginale/50*(float)m_tailleCase/m_tailleCaseOriginale;
    h=s->h*(float)m_tailleCaseOriginale/50*(float)m_tailleCase/m_tailleCaseOriginale;
    im_tabImagesPersonnages[equipier->getId()]->draw(renderer, (int)(debutTerrainX*m_tailleCase)+equipier->getPosition().x*DIMX/1600*(int)m_tailleCaseOriginale/(int)jeu.m_tailleCase-camera.x, (debutTerrainY*m_tailleCase)+equipier->getPosition().y*DIMY/900*(int)m_tailleCaseOriginale/(int)jeu.m_tailleCase-camera.y, w, h);
}

// Partition sur un tableau de Point
void sdlJeu::partitionPoint(Point T[], int pivot, unsigned int debut, unsigned int fin, unsigned int& k1, unsigned int& k2) {
    k1=fin, k2=debut;
    do {
        while (T[k2].y<pivot) {
            k2++;
        }
        while (T[k1].y>pivot) {
            k1--;
        }
        if (k2<=k1) {
            Point p;
            p.x=T[k1].x;
            p.y=T[k1].y;
            T[k1].x=T[k2].x;
            T[k1].y=T[k2].y;
            T[k2].x=p.x;
            T[k2].y=p.y;
            if (k2<fin)
                k2++;
            if (k1>debut)
                k1--;
        }
    } while (k1>=k2);
}

// Algorithme du tri par partition (tri par pivot) sur un tableau de Point
void sdlJeu::triPartitionPoint(Point T[], unsigned int debut, unsigned int fin) {
    unsigned int k1, k2;
    int pivot=T[(debut+fin)/2].y;
    partitionPoint(T,pivot,debut,fin,k1,k2);
    if (debut<k1)
        triPartitionPoint(T,debut,k1);
    if (k2<fin)
        triPartitionPoint(T,k2,fin);
}

void sdlJeu::sdlAffQuete(Quete* queteAffichee, unsigned int debutX, unsigned int debutY, unsigned int niveauProfondeur, unsigned int hauteur) {
    for (unsigned int i=0; i<queteAffichee->getNbEtapes(); i++) {
        Quete* etape=queteAffichee->getEtape(i);
        std::string typeQuete=etape->getType();
        phrase = typeQuete+" : "+etape->getNom()+" | ";
        unsigned int transparence=255;
        if (etape->getEtat()=='f')
            transparence=100;
        else if (etape->getEtat()=='i')
            transparence=0;
        if (typeQuete=="Collecte") {
            QueteCollecte* quete=static_cast<QueteCollecte*>(etape);
            unsigned int idObjet=quete->getIdObjet();
            unsigned int nbObjet=quete->getNombreDemande();
            if (quete->getValidation())
                phrase+=" Ramenez";
            else
                phrase+=" Collectez";
            phrase+=" "+std::to_string(nbObjet)+"x "+jeu.getObjet(idObjet)->getNom()+" ";
            afficheTexte(debutX+niveauProfondeur*DIMX/50, debutY+hauteur*DIMY/50, phrase.size()*DIMX/200, DIMY/50, phrase, policeMenu, couleurPolice, transparence);
            // On affiche l'image a idObjet+1 car 0 contient l'objet vide
            im_tabImagesObjets[idObjet+1]->draw(renderer, debutX+niveauProfondeur*DIMX/50+phrase.size()*DIMX/200, debutY+hauteur*DIMY/40, 9*DIMX/50/16, DIMY/50, transparence);
            std::string phrase2="";
            int idRegionCible=quete->getRegionCible();
            if (idRegionCible!=-1)
                phrase2+=" à "+jeu.getNomTerrain(idRegionCible);
            phrase2+=" ( "+std::to_string(quete->getNombreObtenu())+"/"+std::to_string(quete->getNombreDemande())+" )";
            afficheTexte(debutX+niveauProfondeur*DIMX/50+phrase.size()*DIMX/200+9*DIMX/50/16, debutY+hauteur*DIMY/50, phrase2.size()*DIMX/200, DIMY/50, phrase2, policeMenu, couleurPolice, transparence);
        }
        else if (typeQuete=="D\u00E9placement") {
            QueteDeplacement* quete=static_cast<QueteDeplacement*>(etape);
            phrase+="Rendez-vous à "+jeu.getNomTerrain(quete->getRegionCible());
            if (quete->getValidation())
                phrase+=" puis revenez me voir.";
            afficheTexte(debutX+niveauProfondeur*DIMX/50, debutY+hauteur*DIMY/50, phrase.size()*DIMX/200, DIMY/50, phrase, policeMenu, couleurPolice, transparence);
        }
        else if (typeQuete=="Dialogue") {
            QueteDialogue* quete=static_cast<QueteDialogue*>(etape);
            phrase+="Allez parler à "+quete->getCible()->getNomPNJ();
            int idRegionCible=quete->getRegionCible();
            if (idRegionCible!=-1)
                phrase+=" à "+jeu.getNomTerrain(idRegionCible);
            if (quete->getValidation())
                phrase+=" puis revenez me voir.";
            afficheTexte(debutX+niveauProfondeur*DIMX/50, debutY+hauteur*DIMY/50, phrase.size()*DIMX/200, DIMY/50, phrase, policeMenu, couleurPolice, transparence);
        }
        else if (typeQuete=="Massacre") {
            QueteMassacre* quete=static_cast<QueteMassacre*>(etape);
            unsigned int idEspece=quete->getIdEspece();
            unsigned int nbEnnemis=quete->getNombreDemande();
            phrase+="Vainquez ces adversaires : ";
            phrase+=std::to_string(nbEnnemis)+"x "+jeu.getEspece(idEspece)->getNomEspece()+" ";
            afficheTexte(debutX+niveauProfondeur*DIMX/50, debutY+hauteur*DIMY/50, phrase.size()*DIMX/200, DIMY/50, phrase, policeMenu, couleurPolice, transparence);
            // On affiche l'image de l'ennemi a vaincre
            im_tabImagesEnnemis[idEspece]->draw(renderer, debutX+niveauProfondeur*DIMX/50+phrase.size()*DIMX/200, debutY+hauteur*DIMY/50, 9*DIMX/50/16, DIMY/50, transparence);
            int idRegionCible=quete->getRegionCible();
            std::string phrase2="";
            if (idRegionCible!=-1)
                phrase2+=" à "+jeu.getNomTerrain(idRegionCible);
            if (quete->getValidation())
                phrase2+=" puis revenez me voir.";
            phrase2+=" ( "+std::to_string(quete->getNombreVaincu())+"/"+std::to_string(quete->getNombreDemande())+" )";
            afficheTexte(debutX+niveauProfondeur*DIMX/50+phrase.size()*DIMX/200+9*DIMX/50/16, debutY+hauteur*DIMY/50, phrase2.size()*DIMX/200, DIMY/50, phrase2, policeMenu, couleurPolice, transparence);
        }
        hauteur++;
        sdlAffQuete(etape, debutX, debutY, niveauProfondeur+1, hauteur);
    }
}

void sdlJeu::sdlAff() {
    const Joueur* joueur = jeu.getJoueurConst();
    SDL_SetRenderDrawColor(renderer, 0, 80, 80, 80);
    SDL_RenderClear(renderer);
    if(!jeu.m_menuPrincipal) {
        if (!jeu.m_scenarioNarrateur) {
            if (jeu.getCombat()==NULL) {
                if (jeu.m_indiceScenario>4 && jeu.m_indiceScenario<12) {  // on centre la camera sur le point d'interet choisi, au niveau de l'entree ouest
                    camera.x=3*m_tailleCase-(DIMX/2);
                    camera.y=8*m_tailleCase-(DIMY/2);
                }
                Terrain* terrain = jeu.getTerrain();
                unsigned int tailleX=terrain->getTailleX();
                unsigned int tailleY=terrain->getTailleY();
                unsigned int debutTerrainX=std::max(0,(int)(DIMX/m_tailleCase-tailleX)/2);
                unsigned int debutTerrainY=std::max(0,(int)(DIMY/m_tailleCase-tailleY)/2);
                // Affichage terrain
                SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
                SDL_RenderClear(renderer);
                unsigned int collisionJoueur[4];
                joueur->getCollisions(collisionJoueur);          // on recupere les collisions du joueur pour le calcul de hauteur lors de l'affichage des decors et personnages
                unsigned int indiceCoffre=0;             // compteur pour savoir de quel coffre il s'agit quand on veut verifier s'il est ouvert ou non, sans avoir a comparer les coordonnees avec tous les coffres
                unsigned int nbEnnemis=jeu.getNombreEnnemis();       // permet de savoir le nombre d'ennemis du niveau. Attention, il s'agit en verite du nombre de points d'apparition, incluant donc les ennemis en reapparition
                unsigned int tabHauteur[nbEnnemis]{0};          // tableau pour stocker la hauteur de collision de chaque ennemi du niveau
                Ennemi* tabEnnemi[nbEnnemis]{NULL};                // un tableau d'ennemis pour faciliter l'access a leurs informations
                bool tabVivant[nbEnnemis]{false};           // un tableau de booleen pour savoir si l'ennemi est vivant (on affiche l'ennemi) ou mort (on affiche une tombe)
                // On initialise tabHauteur et tabEnnemi
                for (unsigned int i=0; i<nbEnnemis; i++) {
                    tabEnnemi[i]=jeu.getEnnemi(i);
                    if (tabEnnemi[i]!=NULL) {
                        unsigned int CollisionEnnemi[4];     // pour stocker la hauteur de collision de l'ennemi
                        tabEnnemi[i]->getCollisions(CollisionEnnemi);
                        tabHauteur[i]=tabEnnemi[i]->getPosition().y+CollisionEnnemi[1];
                        tabVivant[i]=true;
                    }
                    else {
                        tabEnnemi[i]=jeu.getTombe(i);
                        if (tabEnnemi[i]!=NULL) {
                            unsigned int CollisionEnnemi[4];     // pour stocker la hauteur de collision de l'ennemi
                            tabEnnemi[i]->getCollisions(CollisionEnnemi);
                            tabHauteur[i]=0;
                        }
                    }
                }
                // On affiche la premiere couche du terrain
                for (unsigned int i=0; i<tailleX*tailleY; i++) {
                    im_premiereCouche[terrain->getPremiereCoucheTerrain(i)]->draw(renderer, (debutTerrainX+i%tailleX)*m_tailleCase - camera.x, (debutTerrainY+i/tailleX)*m_tailleCase - camera.y, m_tailleCase, m_tailleCase);
                }
                // Stockage des indices et hauteurs de tous les decors et personnages du niveau
                Point tableauAffichage[tailleX*tailleY+nbEnnemis+1+3];
                for (unsigned int i=0; i<tailleX*tailleY; i++) {   // decors
                    unsigned int hauteurCollision=jeu.getHauteurCollisionDecors(terrain->getDeuxiemeCoucheTerrain(i));
                    tableauAffichage[i].x=i;
                    tableauAffichage[i].y=(i/tailleX)*jeu.m_tailleCase+hauteurCollision;
                }
                for (unsigned int i=0; i<nbEnnemis; i++) {    // ennemis
                    tableauAffichage[i+tailleX*tailleY].x=i+tailleX*tailleY;
                    if (tabEnnemi[i]!=NULL)
                        tableauAffichage[i+tailleX*tailleY].y=tabHauteur[i];
                    else
                        tableauAffichage[i+tailleX*tailleY].y=0;
                }
                // joueur
                tableauAffichage[tailleX*tailleY+nbEnnemis].x=tailleX*tailleY+nbEnnemis;
                tableauAffichage[tailleX*tailleY+nbEnnemis].y=joueur->getPosition().y+collisionJoueur[1];
                unsigned int collision[4];
                for (unsigned int i=0; i<3; i++) {
                    Personnage* equipier=jeu.getEquipier(i);
                    tableauAffichage[tailleX*tailleY+nbEnnemis+1+i].x=tailleX*tailleY+nbEnnemis+1+i;
                    if (equipier!=NULL) {
                        equipier->getCollisions(collision);
                        tableauAffichage[tailleX*tailleY+nbEnnemis+1+i].y=equipier->getPosition().y+collision[1];
                    }
                    else {
                        tableauAffichage[tailleX*tailleY+nbEnnemis+1+i].y=0;
                    }
                }
                triPartitionPoint(tableauAffichage,0,tailleX*tailleY+nbEnnemis+3);   // on trie le tableau par hauteur croissante en utilisant le tri par partition
                for (unsigned int i=0; i<tailleX*tailleY+nbEnnemis+1+3; i++) {  // on affiche la deuxieme couche : decors, ennemis et joueur, par hauteur croissante afin de respecter premier plan / deuxieme plan
                    if (tableauAffichage[i].x<(int)(tailleX*tailleY))  // decors
                        sdlAffCase(tableauAffichage[i].x, terrain, debutTerrainX, debutTerrainY, indiceCoffre);
                    else if (tableauAffichage[i].x<(int)(tailleX*tailleY+nbEnnemis)) {  // ennemis
                        if (tabEnnemi[tableauAffichage[i].x-tailleX*tailleY]!=NULL)
                            sdlAffEnnemi(tabEnnemi[tableauAffichage[i].x-tailleX*tailleY],tabVivant[tableauAffichage[i].x-tailleX*tailleY],debutTerrainX,debutTerrainY, tableauAffichage[i].x-tailleX*tailleY);
                    }
                    else if (tableauAffichage[i].x<(int)(tailleX*tailleY+nbEnnemis+1)) // joueur
                        sdlAffJoueur(joueur,debutTerrainX,debutTerrainY);
                    else { // familier
                        if (jeu.getEquipier(tableauAffichage[i].x-(tailleX*tailleY+nbEnnemis+1))!=NULL)
                            sdlAffEquipier(jeu.getEquipier(tableauAffichage[i].x-(tailleX*tailleY+nbEnnemis+1)),debutTerrainX,debutTerrainY);
                    }
                }

                // Affichage touche d'interaction
                if (jeu.m_interactionPossible==1 || jeu.m_interactionPossible==2) {  // coffre ou pnj
                    if ((int)((debutTerrainY-1+jeu.m_interactionIndice/tailleX)*m_tailleCase - camera.y)<0)  // si on est en bord superieur de la fenetre on affiche plus bas
                        im_toucheE.draw(renderer, (debutTerrainX+jeu.m_interactionIndice%tailleX)*m_tailleCase - camera.x, (debutTerrainY+jeu.m_interactionIndice/tailleX)*m_tailleCase - camera.y, m_tailleCase, m_tailleCase);
                    else
                        im_toucheE.draw(renderer, (debutTerrainX+jeu.m_interactionIndice%tailleX)*m_tailleCase - camera.x, (debutTerrainY-1+jeu.m_interactionIndice/tailleX)*m_tailleCase - camera.y, m_tailleCase, m_tailleCase);
                }
                else if (jeu.m_interactionPossible==3) {  // tombe
                    if ((int)((debutTerrainY-1+jeu.m_interactionTombeCoordonnees.y/tailleX)*m_tailleCase - camera.y)<0)  // si on est en bord superieur de la fenetre on affiche plus bas
                        im_toucheE.draw(renderer, (debutTerrainX+jeu.m_interactionTombeCoordonnees.x%tailleX)*m_tailleCase - camera.x, (debutTerrainY+jeu.m_interactionTombeCoordonnees.y)*m_tailleCase - camera.y, m_tailleCase, m_tailleCase);
                    else
                        im_toucheE.draw(renderer, (debutTerrainX+jeu.m_interactionTombeCoordonnees.x%tailleX)*m_tailleCase - camera.x, (debutTerrainY-1+jeu.m_interactionTombeCoordonnees.y)*m_tailleCase - camera.y, m_tailleCase, m_tailleCase);
                }

                // Affichage des dialogues
                if(jeu.m_afficheDialogue) {;
                    m_PNJ = jeu.getPNJ(jeu.m_niveau, jeu.m_interactionIndiceObjet);
                    std::string nom = m_PNJ->getNomPNJ();
                    // On affiche la bulle de dialogue
                    im_bulle.draw(renderer, DIMX/2 - 7*DIMX/16, DIMY/80, 14*DIMX/16, 2*DIMY/9);

                    // On dessine le PNJ
                    unsigned int j=0, c=terrain->getDeuxiemeCoucheTerrain(jeu.m_interactionIndice);
                    for (unsigned int i=0; i<c-jeu.m_tailleDecors-2; i++) {
                        if (jeu.m_interactionsAction[i]==3) {
                            j++;
                        }
                    }

                    im_imagePnj[j]->draw(renderer, DIMX/2 - 7*DIMX/16 + DIMX/105, 2*DIMY/33, 3*DIMX/26, 2*DIMY/11);
                    // On affiche le nom du PNJ
                    if (nom.size()>0) {
                        afficheTexte(DIMX/2 - nom.size()*DIMX/160 + DIMX/17, DIMY/26, nom.size()*DIMX/80, DIMY/22, nom, policeMenu, couleurPoliceMenu);
                    }
                    // On recupere la phrase
                    Dialogue* dialogue = m_PNJ->getDialogue(m_PNJ->getIndiceDialogue());
                    phrase = dialogue->getPhrase(dialogue->getIndicePhrase());
                    // On affiche la phrase
                    if (phrase.size()>0) {
                        afficheTexte(DIMX/2 - tailleSansAccents(phrase)*DIMX/180 + DIMX/20, DIMY/12, tailleSansAccents(phrase)*DIMX/90, DIMY/18, phrase, policeMenu, couleurPoliceMenu);
                    }
                }

                // Inventaire tpinv
                if (jeu.m_afficheInventaire) {

                    im_cadre.draw(renderer,0, 0, DIMX/4, DIMY);
                    im_equipement.draw(renderer,DIMX/220, DIMY/100, DIMX/4+1, 1.06*DIMY); //Onglet de l'inventaire
                    std::string phrase2 = "Équipement";
                    afficheTexte(DIMX * 0.061, DIMY * 0.025, int(phrase2.size())*20*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);

                    //tete
                    if (joueur->getEquipement(0) != NULL)              //tete
                    {                           //decalage et centrage de l'image
                        im_fondeq.draw(renderer,DIMX/8 - DIMX * 0.01953125, DIMY * 0.13, DIMX*0.0390625, DIMY*0.0694444);
                        im_tabImagesObjets[joueur->getEquipement(0)->getIdObjet()+1]->draw(renderer,DIMX/8 - DIMX * 0.01953125, DIMY * 0.13, DIMX*0.0390625, DIMY*0.0694444);
                    }
                    else
                        im_tabImagesEquipementVide[0]->draw(renderer,DIMX/8 - DIMX * 0.01953125, DIMY * 0.13, DIMX*0.0390625, DIMY*0.0694444);


                    //amulette
                    if (joueur->getEquipement(1) != NULL)              //amulette
                        {                       //decalage et centrage de l'image
                        im_fondeq.draw(renderer,DIMX/8 - DIMX * 0.01953125 +  DIMX/20, DIMY * 0.23, DIMX*0.0390625, DIMY*0.0694444);;
                        im_tabImagesObjets[joueur->getEquipement(1)->getIdObjet()+1]->draw(renderer,DIMX/8 - DIMX * 0.01953125 +  DIMX/20, DIMY * 0.23, DIMX*0.0390625, DIMY*0.0694444);
                        }
                    else
                        im_tabImagesEquipementVide[2]->draw(renderer,DIMX/8 - DIMX * 0.01953125 +  DIMX/20, DIMY * 0.23, DIMX*0.0390625, DIMY*0.0694444);

                    //cape
                    if (joueur->getEquipement(2) != NULL)              //cape
                        {                       //decalage et centrage de l'image
                        im_fondeq.draw(renderer,DIMX/8 - (DIMX * 0.01953125 +  DIMX/20), DIMY * 0.23, DIMX*0.0390625, DIMY*0.0694444);
                        im_tabImagesObjets[joueur->getEquipement(2)->getIdObjet()+1]->draw(renderer,DIMX/8 - (DIMX * 0.01953125 +  DIMX/20), DIMY * 0.23, DIMX*0.0390625, DIMY*0.0694444);
                        }
                    else
                        im_tabImagesEquipementVide[1]->draw(renderer,DIMX/8 - (DIMX * 0.01953125 +  DIMX/20), DIMY * 0.23, DIMX*0.0390625, DIMY*0.0694444);

                    //anneaug
                    if (joueur->getEquipement(3) != NULL)              //anneaug
                        {                       //decalage et centrage de l'image
                        im_fondeq.draw(renderer,DIMX/8 - DIMX * 0.01953125 +  DIMX/20, DIMY * 0.33, DIMX*0.0390625, DIMY*0.0694444);
                        im_tabImagesEquipementVide[3]->draw(renderer,DIMX/8 - DIMX * 0.01953125 +  DIMX/20, DIMY * 0.33, DIMX*0.0390625, DIMY*0.0694444);
                        }
                    else
                        im_tabImagesEquipementVide[3]->draw(renderer,DIMX/8 - DIMX * 0.01953125 +  DIMX/20, DIMY * 0.33, DIMX*0.0390625, DIMY*0.0694444);

                    //anneaud
                    if (joueur->getEquipement(4) != NULL)              //anneaud
                        {                       //decalage et centrage de l'image
                        im_fondeq.draw(renderer,DIMX/8 - (DIMX * 0.01953125 +  DIMX/20), DIMY * 0.33, DIMX*0.0390625, DIMY*0.0694444);
                        im_tabImagesEquipementVide[4]->draw(renderer,DIMX/8 - (DIMX * 0.01953125 +  DIMX/20), DIMY * 0.33, DIMX*0.0390625, DIMY*0.0694444);
                        }
                    else
                        im_tabImagesEquipementVide[4]->draw(renderer,DIMX/8 - (DIMX * 0.01953125 +  DIMX/20), DIMY * 0.33, DIMX*0.0390625, DIMY*0.0694444);

                    //botte
                    if (joueur->getEquipement(5) != NULL)              //botte
                        {                       //decalage et centrage de l'image
                        im_fondeq.draw(renderer,DIMX/8 - DIMX * 0.01953125, DIMY * 0.43, DIMX*0.0390625, DIMY*0.0694444);
                        im_tabImagesEquipementVide[5]->draw(renderer,DIMX/8 - DIMX * 0.01953125, DIMY * 0.43, DIMX*0.0390625, DIMY*0.0694444);
                        }
                    else
                        im_tabImagesEquipementVide[5]->draw(renderer,DIMX/8 - DIMX * 0.01953125, DIMY * 0.43, DIMX*0.0390625, DIMY*0.0694444);

                    im_cadre.draw(renderer,DIMX/200, 5.46 * DIMY/10-10, DIMX * 0.244, DIMY * 0.382);
                    for (unsigned int j=0; j<5; j++) {
                        for (unsigned int i=0; i<6; i++) {
                                im_tabImagesObjets[0]->draw(renderer, (DIMX/100) + i*DIMX * 0.04, 5.46 * DIMY/10+j*DIMY * 0.072222, DIMX * 0.04, DIMY * 0.072222);
                                if (joueur->getInventaire()->getObjet(i+6*j) != NULL) {
                                    unsigned int idObjet = joueur->getInventaire()->getObjet(i+6*j)->getIdObjet();
                                    im_tabImagesObjets[idObjet+1]->draw(renderer, (DIMX/100) + i*DIMX * 0.04, 5.46 * DIMY/10+j*DIMY * 0.072222, DIMX * 0.04, DIMY * 0.072222);
                                }
                            }
                    }

                    im_cadre.draw(renderer,DIMX/4, 0, DIMX/4, DIMY);
                    im_equipement.draw(renderer,DIMX/4 + DIMX/220, DIMY/100, DIMX/4+1, 1.06*DIMY); //Onglet de statistiques
                    phrase2 = "Statistiques";
                    afficheTexte(DIMX * 0.31, DIMY * 0.025, int(phrase2.size())*18*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);

                    phrase2 = "Nom :";
                    afficheTexte(DIMX * 0.35, DIMY * 0.11, int(phrase2.size())*20*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                    afficheTexte(DIMX * (0.375-0.0051666*int(joueur->getNom().size())), DIMY * 0.16, int(joueur->getNom().size())*20*DIMX/1600, 40*DIMY/900, joueur->getNom(), policeMenu, couleurPoliceMenu);
                                    //centrage des dalles de textes par rapport au centre de l'onglet de stats
                    phrase2 = "Vie Maximum :";
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.235, int(phrase2.size())*18*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                    phrase2 = std::to_string(joueur->getPointsDeVieMax());
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.285, int(phrase2.size())*20*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);

                    phrase2 = "Mana Maximum :";
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.36, int(phrase2.size())*18*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                    phrase2 = std::to_string(joueur->getPointsDeManaMax());
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.41, int(phrase2.size())*20*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);

                    phrase2 = "Attaque :";
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.485, int(phrase2.size())*18*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                    phrase2 = std::to_string(joueur->getAttaque());
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.535, int(phrase2.size())*20*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);

                    phrase2 = "Défense :";
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.610, int(phrase2.size())*18*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                    phrase2 = std::to_string(joueur->getDefense());
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.66, int(phrase2.size())*20*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);

                    phrase2 = "Niveau Actuel :";
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.735, int(phrase2.size())*17*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                    phrase2 = std::to_string(joueur->getNiveau());
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.785, int(phrase2.size())*20*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);

                    phrase2 = "Prochain Niveau :";
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.860, int(phrase2.size())*16*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                    phrase2 = std::to_string(joueur->getExperience()) + " / " + std::to_string(joueur->getNiveau()*joueur->getNiveau()*25);
                    afficheTexte(DIMX * (0.375-0.0051666*int(phrase2.size())), DIMY * 0.910, int(phrase2.size())*20*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);

                    im_bourse.draw(renderer,DIMX/9 - (DIMX * 0.01953125 +  DIMX/20), DIMY * 0.925, DIMX*0.038, DIMY*0.05);
                    phrase2 = std::to_string(joueur->getOr());
                    afficheTexte(DIMX * (0.125-0.0051666*int(phrase2.size())), DIMY * 0.929, int(phrase2.size())*20*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);

                    im_cadre.draw(renderer,DIMX/2, 0, DIMX/4, DIMY);
                    im_equipement.draw(renderer,DIMX/2 + DIMX/220, DIMY/100, DIMX/4+1, 1.06*DIMY);     //Onglet Equipe
                    phrase2 = "Équipe";
                    afficheTexte(DIMX * 0.585, DIMY * 0.025, int(phrase2.size())*18*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);

                    for (unsigned int n=0; n<jeu.getNbFamiliers(); n++) {
                        if (jeu.getEquipier(n)!=NULL) {
                            phrase2 = jeu.getEquipier(n)->getNom();
                            afficheTexte(DIMX * (0.62-0.0051666*int(phrase2.size())), DIMY * (n*0.3 + 0.125), int(phrase2.size())*18*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                            phrase2 = "Niveau Actuel :";
                            afficheTexte(DIMX * (0.62-0.0051666*int(phrase2.size())), DIMY * (n*0.3 + 0.175), int(phrase2.size())*18*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                            phrase2 = std::to_string(jeu.getEquipier(n)->getNiveau());
                            afficheTexte(DIMX * (0.62-0.0051666*int(phrase2.size())), DIMY * (n*0.3 + 0.225), int(phrase2.size())*18*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                            phrase2 = "Vie :";
                            afficheTexte(DIMX * (0.62-0.0051666*int(phrase2.size())), DIMY * (n*0.3 + 0.275), int(phrase2.size())*18*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                            phrase2 = std::to_string(jeu.getEquipier(n)->getPointsDeVie()) + " / " + std::to_string(jeu.getEquipier(n)->getPointsDeVieMax());
                            afficheTexte(DIMX * (0.62-0.0051666*int(phrase2.size())), DIMY * (n*0.3 + 0.325), int(phrase2.size())*18*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                        }
                        else {
                            phrase2 = "Pas de familiers";
                            afficheTexte(DIMX * (0.62-0.0051666*int(phrase2.size())), DIMY * 0.525, int(phrase2.size())*16*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                        }
                    }
                    if (!jeu.m_afficheInventaireCoffre && !jeu.m_afficheInventaireTombe) {
                        im_cadre.draw(renderer,3*DIMX/4, 0, DIMX/4, DIMY);
                        im_equipement.draw(renderer,3*DIMX/4 + DIMX/220, DIMY/100, DIMX/4+1, 1.06*DIMY);     //Onglet Quete
                        phrase2 = "Qu\u00EAtes";
                        afficheTexte(DIMX * 0.84, DIMY * 0.025, int(phrase2.size())*16*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                        if (jeu.m_indiceQueteSuivie!=-1) {
                            Quete* queteSuivie=jeu.getQuete(jeu.m_indiceQueteSuivie);
                            Quete* Etape=jeu.getQuete(jeu.m_indiceQueteSuivie)->getQueteActive();
                            if (queteSuivie!=NULL) {
                                phrase2 = queteSuivie->getNom();
                                afficheTexte(DIMX * (0.87-0.0025833*int(phrase2.size())), DIMY * 0.125, int(phrase2.size())*8*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                                phrase2 = Etape->getNom();
                                afficheTexte(DIMX * (0.87-0.0025833*int(phrase2.size())), DIMY * 0.325, int(phrase2.size())*8*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                                phrase2 = "Region de la quête :";
                                afficheTexte(DIMX * (0.87-0.0025833*int(phrase2.size())), DIMY * 0.425, int(phrase2.size())*8*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                                phrase2 = "Objectif de la quête en cours :";
                                afficheTexte(DIMX * (0.87-0.0025833*int(phrase2.size())), DIMY * 0.225, int(phrase2.size())*8*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                                if (queteSuivie->getRegionCible()!=-1) {
                                    phrase2 = jeu.getNomTerrain(queteSuivie->getRegionCible());
                                    afficheTexte(DIMX * (0.87-0.0051666*int(phrase2.size())), DIMY * 0.525, int(phrase2.size())*16*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                                }
                                phrase2 = "Récompense :";
                                afficheTexte(DIMX * (0.87-0.0025833*int(phrase2.size())), DIMY * 0.625, int(phrase2.size())*8*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);

                                Inventaire* recompenses=queteSuivie->getRecompenses();
                                for (unsigned int i=0; i<recompenses->getNbObjet(); i++) {
                                    im_tabImagesObjets[0]->draw(renderer, 7*DIMX/8-((int)(recompenses->getNbObjet())-2*(int)i)*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/20, 16*DIMY/20/9);
                                    // On affiche l'image a idObjet+1 car 0 contient l'objet vide
                                    im_tabImagesObjets[recompenses->getIemeObjet(i+1)->getIdObjet()+1]->draw(renderer, 7*DIMX/8-((int)(recompenses->getNbObjet())-2*(int)i)*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/20, 16*DIMY/20/9);
                                }
                                im_tabImagesObjets[0]->draw(renderer, 7*DIMX/8-(recompenses->getNbObjet()+2)*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/20, 16*DIMY/20/9);
                                phrase="XP";
                                afficheTexte(7*DIMX/8-(recompenses->getNbObjet()+2)*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/60, 16*DIMY/60/9, phrase, policeMenu, couleurPoliceOr);
                                phrase=std::to_string(queteSuivie->getExp());
                                afficheTexte(7*DIMX/8-(recompenses->getNbObjet()+2)*DIMX/40+DIMX/40-phrase.size()*DIMX/120, 3*DIMY/4+16*DIMY/40/9-DIMY/40-DIMY/40, phrase.size()*DIMX/60, DIMY/20, phrase, policeMenu, couleurPoliceOr);
                                im_tabImagesObjets[0]->draw(renderer, 7*DIMX/8+recompenses->getNbObjet()*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/20, 16*DIMY/20/9);
                                im_or.draw(renderer, 7*DIMX/8+recompenses->getNbObjet()*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/60, 16*DIMY/60/9);
                                phrase=std::to_string(queteSuivie->getOr());
                                afficheTexte(7*DIMX/8+recompenses->getNbObjet()*DIMX/40+DIMX/40-phrase.size()*DIMX/120, 3*DIMY/4+16*DIMY/40/9-DIMY/40-DIMY/40, phrase.size()*DIMX/60, DIMY/20, phrase, policeMenu, couleurPoliceOr);

                            }
                        }
                        else {
                            phrase2 = "Vous ne suivez aucune quête.";
                            afficheTexte(DIMX * (0.87-0.0025833*int(phrase2.size())), DIMY * 0.425, int(phrase2.size())*8*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                            phrase2 = "(Vous pouvez suivre une quête ";
                            afficheTexte(DIMX * (0.87-0.0025833*int(phrase2.size())), DIMY * 0.525, int(phrase2.size())*8*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                            phrase2 = "en la sélectionnant depuis le recueil)";
                            afficheTexte(DIMX * (0.87-0.0025833*int(phrase2.size())), DIMY * 0.625, int(phrase2.size())*8*DIMX/1600, 40*DIMY/900, phrase2, policeMenu, couleurPoliceMenu);
                        }
                    }

                    if (m_zoomObjet != -1) {
                        if (joueur->getInventaire()->getObjet(m_zoomObjet) != NULL) {
                            int i = 2;
                            im_bulleInfo.draw(renderer, (DIMX/100) + DIMX * 0.04 *(m_zoomObjet%6+0.5), 5.46 * DIMY/10+m_zoomObjet/5+0.5*DIMY * 0.072222, DIMX * 0.1, DIMY * 0.2);
                            afficheTexte((DIMX/100) + DIMX * 0.04 * (m_zoomObjet%6+0.75) + (75- joueur->getInventaire()->getObjet(m_zoomObjet)->getNom().size()*3), 5.46 * DIMY/10+m_zoomObjet/5+0.75*DIMY * 0.072222 , int(joueur->getInventaire()->getObjet(m_zoomObjet)->getNom().size())*5*DIMX/1600, 10*DIMY/900, joueur->getInventaire()->getObjet(m_zoomObjet)->getNom(), policeTexte, {0,0,0});
                            if(joueur->getInventaire()->getObjet(m_zoomObjet)->getTypeObjet() == "\u00C9quipement") {
                                Equipement *EquipementCourant = static_cast<Equipement*>(joueur->getInventaire()->getObjet(m_zoomObjet));

                                phrase2 = " +" + std::to_string(EquipementCourant->getAttaqueObjet()) + " Attaque    +"
                                              + std::to_string(EquipementCourant->getManaObjet()) + " Mana";
                                afficheTexte((DIMX/100) + DIMX * 0.04 * (m_zoomObjet%6+0.75), 5.46 * DIMY/10+m_zoomObjet/5+(0.75+ i * 0.25)*DIMY * 0.072222, int(phrase2.size())*5*DIMX/1600, 10*DIMY/900, phrase2, policeTexte, {0,0,0});
                                i++;
                                phrase2 = " +" + std::to_string(EquipementCourant->getDefenseObjet()) + " Défense    +"
                                              + std::to_string(EquipementCourant->getPvObjet()) + " PV";
                                afficheTexte((DIMX/100) + DIMX * 0.04 * (m_zoomObjet%6+0.75), 5.46 * DIMY/10+m_zoomObjet/5+(0.75+ i * 0.25)*DIMY * 0.072222, int(phrase2.size())*5*DIMX/1600, 10*DIMY/900, phrase2, policeTexte, {0,0,0});
                                i++;
                            }
                            phrase2 = joueur->getInventaire()->getObjet(m_zoomObjet)->getDescriptionObjet();
                            std::string s = phrase2;
                            std::string delimiter = "§";
                            size_t pos = 0;
                            std::string token;

                            while ((pos = s.find(delimiter)) != std::string::npos) {
                                token = s.substr(0, pos);
                                afficheTexte((DIMX/100) + DIMX * 0.04 * (m_zoomObjet%6+0.75), 5.46 * DIMY/10+m_zoomObjet/5+(0.75+ i * 0.25)*DIMY * 0.072222, int(token.size())*5*DIMX/1600, 10*DIMY/900, token, policeTexte, {0,0,0});
                                s.erase(0, pos + delimiter.length());
                                i++;
                            }
                            afficheTexte((DIMX/100) + DIMX * 0.04 * (m_zoomObjet%6+0.75), 5.46 * DIMY/10+m_zoomObjet/5+(0.75+ i * 0.25)*DIMY * 0.072222, int(s.size())*5*DIMX/1600, 10*DIMY/900, s, policeTexte, {0,0,0});


                            phrase2 = std::to_string(joueur->getInventaire()->getObjet(m_zoomObjet)->getValeurObjet());
                            im_or.draw(renderer, (DIMX/100) + DIMX * 0.04 *(m_zoomObjet%6+0.5) + DIMX * 0.01, 5.46 * DIMY/10+m_zoomObjet/5+0.5*DIMY * 0.072222 + DIMY * 0.17, DIMX * 0.01, DIMY * 0.0182);
                            afficheTexte((DIMX/100) + DIMX * 0.04 *(m_zoomObjet%6+0.5) + DIMX * 0.022, 5.46 * DIMY/10+m_zoomObjet/5+0.5*DIMY * 0.072222 + DIMY * 0.17, int(phrase2.size())*8*DIMX/1600, 15*DIMY/900, phrase2, policeTexte, {0,0,0});
                            if(joueur->getInventaire()->getObjet(m_zoomObjet)->getTypeObjet() == "\u00C9quipement") {
                                phrase2 = "Équiper";
                                im_toucheE.draw(renderer, (DIMX/100) + DIMX * 0.04 *(m_zoomObjet%6+0.5) + DIMX * 0.08, 5.46 * DIMY/10+m_zoomObjet/5+0.5*DIMY * 0.072222 + DIMY * 0.153, DIMX * 0.02, DIMY * 0.0364);
                                afficheTexte((DIMX/100) + DIMX * 0.04 *(m_zoomObjet%6+0.5) + DIMX * 0.045, 5.46 * DIMY/10+m_zoomObjet/5+0.5*DIMY * 0.072222 + DIMY * 0.17, int(phrase2.size())*8*DIMX/1600, 15*DIMY/900, phrase2, policeTexte, {0,0,0});
                            }
                        }
                    }
                }

                // Inventaire coffre tpinv tpco
                if (jeu.m_afficheInventaireCoffre) {
                    Coffre *monCoffre = jeu.getCoffre(jeu.m_niveau,jeu.m_interactionIndiceObjet);

                    im_cadre.draw(renderer,3*DIMX/4, 0, DIMX/4, DIMY);
                    im_equipement.draw(renderer,3*DIMX/4 + DIMX/220, DIMY/100, DIMX/4+1, 1.06*DIMY);
                    std::string phrase4 = "Coffre";
                    afficheTexte(DIMX * 0.828, DIMY * 0.025, int(phrase4.size())*20*DIMX/1600, 40*DIMY/900, phrase4, policeMenu, couleurPoliceMenu);
                    im_coffre.draw(renderer, DIMX - DIMX * 0.22, DIMY * 0.16, DIMX/6, DIMY/4);
                    im_cadre.draw(renderer,3*DIMX/4 + DIMX/220, 5.46 * DIMY/10-10, DIMX * 0.244, DIMY * 0.382);
                    for (unsigned int j=0; j<5; j++) {
                        for (unsigned int i=0; i<6; i++) {
                            im_tabImagesObjets[0]->draw(renderer, (DIMX/100) + 3*DIMX/4 + i*DIMX * 0.04, 5.46 * DIMY/10+j*DIMY * 0.072222, DIMX * 0.04, DIMY * 0.072222);
                            if (monCoffre->getButin()->getObjet(i+6*j) != NULL) {
                                unsigned int idObjet = monCoffre->getButin()->getObjet(i+6*j)->getIdObjet();
                                im_tabImagesObjets[idObjet+1]->draw(renderer, (DIMX/100) + 3*DIMX/4 + i*DIMX * 0.04, 5.46 * DIMY/10+j*DIMY * 0.072222, DIMX * 0.04, DIMY * 0.072222);
                            }
                        }
                    }


                    if (m_zoomObjet != -1 && m_zoomObjet >= 30) {
                        m_zoomObjet-=30;
                        if (monCoffre->getButin()->getObjet(m_zoomObjet) != NULL) {
                            int i = 2;
                            im_bulleInfo.draw(renderer, (DIMX/100) + 3*DIMX/4 + DIMX * 0.04*(m_zoomObjet%6+0.5) - DIMX * 0.1, 5.46 * DIMY/10+m_zoomObjet/5+0.5*DIMY * 0.072222, DIMX * 0.1, DIMY * 0.2);
                            afficheTexte((DIMX/100) + 3*DIMX/4 + DIMX * 0.04 * (m_zoomObjet%6+0.75) + (75- monCoffre->getButin()->getObjet(m_zoomObjet)->getNom().size()*3) - DIMX * 0.1,
                                         5.46 * DIMY/10+m_zoomObjet/5+0.75*DIMY * 0.072222 ,int(monCoffre->getButin()->getObjet(m_zoomObjet)->getNom().size())*5*DIMX/1600, 10*DIMY/900,
                                         monCoffre->getButin()->getObjet(m_zoomObjet)->getNom(), policeTexte, {0,0,0});
                            if(monCoffre->getButin()->getObjet(m_zoomObjet)->getTypeObjet() == "\u00C9quipement") {
                                Equipement *EquipementCourant = static_cast<Equipement*>(joueur->getInventaire()->getObjet(m_zoomObjet));

                                phrase4 = " +" + std::to_string(EquipementCourant->getAttaqueObjet()) + " Attaque    +"
                                              + std::to_string(EquipementCourant->getManaObjet()) + " Mana";
                                afficheTexte((DIMX/100) + DIMX * 0.04 * (m_zoomObjet%6+0.75), 5.46 * DIMY/10+m_zoomObjet/5+(0.75+ i * 0.25)*DIMY * 0.072222, int(phrase4.size())*5*DIMX/1600, 10*DIMY/900, phrase4, policeTexte, {0,0,0});
                                i++;
                                phrase4 = " +" + std::to_string(EquipementCourant->getDefenseObjet()) + " Défense    +"
                                              + std::to_string(EquipementCourant->getPvObjet()) + " PV";
                                afficheTexte((DIMX/100) + DIMX * 0.04 * (m_zoomObjet%6+0.75), 5.46 * DIMY/10+m_zoomObjet/5+(0.75+ i * 0.25)*DIMY * 0.072222, int(phrase4.size())*5*DIMX/1600, 10*DIMY/900, phrase4, policeTexte, {0,0,0});
                                i++;
                            }
                            phrase4 = monCoffre->getButin()->getObjet(m_zoomObjet)->getDescriptionObjet();

                            std::string s = phrase4;
                            std::string delimiter = "§";
                            size_t pos = 0;
                            std::string token;

                            while ((pos = s.find(delimiter)) != std::string::npos) {
                                token = s.substr(0, pos);
                                afficheTexte((DIMX/100) + 3*DIMX/4 + DIMX * 0.04 * (m_zoomObjet%6+0.75) - DIMX * 0.1, 5.46 * DIMY/10+m_zoomObjet/5+(0.75+ i * 0.25)*DIMY * 0.072222, int(token.size())*5*DIMX/1600, 10*DIMY/900, token, policeTexte, {0,0,0});
                                s.erase(0, pos + delimiter.length());
                                i++;
                            }
                            afficheTexte((DIMX/100) + 3*DIMX/4 + DIMX * 0.04 * (m_zoomObjet%6+0.75) - DIMX * 0.1, 5.46 * DIMY/10+m_zoomObjet/5+(0.75+ i * 0.25)*DIMY * 0.072222, int(s.size())*5*DIMX/1600, 10*DIMY/900, s, policeTexte, {0,0,0});


                            phrase4 = std::to_string(monCoffre->getButin()->getObjet(m_zoomObjet)->getValeurObjet());
                            im_or.draw(renderer, (DIMX/100) + 3*DIMX/4 + DIMX * 0.04 *(m_zoomObjet%6+0.5)  - DIMX * 0.09, 5.46 * DIMY/10+m_zoomObjet/5+0.5*DIMY * 0.072222 + DIMY * 0.17, DIMX * 0.01, DIMY * 0.0182);
                            afficheTexte((DIMX/100) + 3*DIMX/4 + DIMX * 0.04 *(m_zoomObjet%6+0.5) + DIMX * 0.022  - DIMX * 0.1, 5.46 * DIMY/10+m_zoomObjet/5+0.5*DIMY * 0.072222 + DIMY * 0.17, int(phrase4.size())*8*DIMX/1600, 15*DIMY/900, phrase4, policeTexte, {0,0,0});
                            m_zoomObjet+=30;
                        }
                    }

                }
                // Affichage du butin de la tombe
                else if (jeu.m_afficheInventaireTombe) {
                    im_cadre.draw(renderer,3*DIMX/4, 0, DIMX/4, DIMY);
                    im_equipement.draw(renderer,3*DIMX/4 + DIMX/220, DIMY/100, DIMX/4+1, 1.06*DIMY);
                    std::string phrase4 = "Tombe";
                    afficheTexte(DIMX * 0.828, DIMY * 0.025, int(phrase4.size())*20*DIMX/1600, 40*DIMY/900, phrase4, policeMenu, couleurPoliceMenu);
                    im_tombe.draw(renderer,DIMX - DIMX * 0.22, DIMY * 0.16, DIMX/6, DIMY/4);
                    im_cadre.draw(renderer,3*DIMX/4 + DIMX/220, 5.46 * DIMY/10-10, DIMX * 0.244, DIMY * 0.382);
                    for (unsigned int j=0; j<5; j++) {
                        for (unsigned int i=0; i<6; i++) {
                            im_tabImagesObjets[0]->draw(renderer, (DIMX/100) + 3*DIMX/4 + i*DIMX * 0.04, 5.46 * DIMY/10+j*DIMY * 0.072222, DIMX * 0.04, DIMY * 0.072222);
                            if (jeu.getInventaireTombe(jeu.m_interactionIndiceObjet)->getObjet(i+6*j) != NULL) {
                                unsigned int idObjet = jeu.getInventaireTombe(jeu.m_interactionIndiceObjet)->getObjet(i+6*j)->getIdObjet();
                                im_tabImagesObjets[idObjet+1]->draw(renderer, (DIMX/100) + 3*DIMX/4 + i*DIMX * 0.04, 5.46 * DIMY/10+j*DIMY * 0.072222, DIMX * 0.04, DIMY * 0.072222);
                            }
                        }
                    }
                }

                // Affichage de la proposition de quete
                if (jeu.m_fenetreOuverte) {
                    if (jeu.m_afficheDebutQuete) {
                        im_bulleInfo.draw(renderer, DIMX/5, DIMY/5, 3*DIMX/5, 3*DIMY/5);
                        phrase=jeu.getQuete(jeu.m_indiceQuete)->getNom();
                        if (phrase.size()>0) {
                            afficheTexte(DIMX/2 - phrase.size()*DIMX/160, DIMY/4+DIMY/100, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        }
                        phrase=jeu.getPNJ(jeu.m_niveau,jeu.m_interactionIndiceObjet)->getNomPNJ();
                        if (phrase.size()>0) {
                            afficheTexte(DIMX/2 - phrase.size()*DIMX/160, DIMY/3+DIMY/40, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        }
                        std::string typeQuete=jeu.getQuete(jeu.m_indiceQuete)->getType();
                        phrase="vous propose une quête de "+typeQuete+".";
                        afficheTexte(DIMX/2 - phrase.size()*DIMX/160, DIMY/3+2*DIMY/20, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        if (typeQuete=="Collecte") {
                            QueteCollecte* quete=static_cast<QueteCollecte*>(jeu.getQuete(jeu.m_indiceQuete));
                            unsigned int idObjet=quete->getIdObjet();
                            unsigned int nbObjet=quete->getNombreDemande();
                            phrase="Collectez "+std::to_string(nbObjet)+"x";
                            afficheTexte(DIMX/2-phrase.size()*DIMX/160, DIMY/2+DIMY/20, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                            // On affiche l'image a idObjet+1 car 0 contient l'objet vide
                            im_tabImagesObjets[idObjet+1]->draw(renderer, DIMX/2+phrase.size()*DIMX/160+DIMX/100, DIMY/2+DIMY/40, DIMX/20, 16*DIMY/20/9);
                        }
                        else if (typeQuete=="D\u00E9placement") {
                            QueteDeplacement* quete=static_cast<QueteDeplacement*>(jeu.getQuete(jeu.m_indiceQuete));
                            phrase="Rendez-vous à "+jeu.getNomTerrain(quete->getRegionCible());
                            afficheTexte(DIMX/2-phrase.size()*DIMX/160, DIMY/2+DIMY/20, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        }
                        else if (typeQuete=="Dialogue") {
                            QueteDialogue* quete=static_cast<QueteDialogue*>(jeu.getQuete(jeu.m_indiceQuete));
                            unsigned int indiceImagePnj=quete->getCible()->getIdPNJ();
                            phrase="Allez parler à "+quete->getCible()->getNomPNJ();
                            afficheTexte(DIMX/2-phrase.size()*DIMX/160, DIMY/2+DIMY/20, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                            int idRegionCible=quete->getRegionCible();
                            phrase="";
                            if (idRegionCible!=-1)
                                phrase="Région : "+jeu.getNomTerrain(idRegionCible)+" | ";
                            phrase+=" Cible : ";
                            afficheTexte(DIMX/2-phrase.size()*DIMX/160, DIMY/2+2*DIMY/20, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                            im_deuxiemeCouche[indiceImagePnj+jeu.m_tailleDecors]->draw(renderer, DIMX/2+phrase.size()*DIMX/160+DIMX/100, DIMY/2+DIMY/20+DIMY/40, DIMX/20, 16*DIMY/20/9);
                        }
                        else if (typeQuete=="Massacre") {
                            QueteMassacre* quete=static_cast<QueteMassacre*>(jeu.getQuete(jeu.m_indiceQuete));
                            unsigned int idEspece=quete->getIdEspece();
                            unsigned int nbEnnemis=quete->getNombreDemande();
                            phrase="Vainquez ces adversaires";
                            afficheTexte(DIMX/2-phrase.size()*DIMX/160, DIMY/2+DIMY/20, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                            int idRegionCible=quete->getRegionCible();
                            phrase="";
                            if (idRegionCible!=-1)
                                phrase="Région : "+jeu.getNomTerrain(idRegionCible)+" | ";
                            phrase+=" Cible : "+std::to_string(nbEnnemis)+"x";
                            afficheTexte(DIMX/2-phrase.size()*DIMX/160, DIMY/2+2*DIMY/20, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                            // On affiche l'image de l'ennemi a vaincre
                            im_tabImagesEnnemis[idEspece]->draw(renderer, DIMX/2+phrase.size()*DIMX/160+DIMX/100, DIMY/2+DIMY/20+DIMY/40, DIMX/20, 16*DIMY/20/9);
                        }
                        phrase="Accepter";
                        afficheTexte(DIMX/3 - phrase.size()*DIMX/160, 2*DIMY/3, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        phrase="Refuser";
                        afficheTexte(2*DIMX/3 - phrase.size()*DIMX/160, 2*DIMY/3, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        if(jeu.m_selectBouton)
                            im_selectionPetite.draw(renderer, DIMX/3-DIMX/10, 2*DIMY/3, DIMX/5, DIMY/17);
                        else
                            im_selectionPetite.draw(renderer, 2*DIMX/3 - DIMX/10, 2*DIMY/3, DIMX/5, DIMY/17);
                    }
                    else if (jeu.m_afficheFinQuete && !jeu.m_afficheVictoire && !jeu.m_afficheMonteeNiveau) {
                        im_bulleInfo.draw(renderer, DIMX/5, DIMY/5, 3*DIMX/5, 3*DIMY/5);
                        phrase=jeu.getQuete(jeu.m_indiceFinQuete)->getNom();
                        if (phrase.size()>0) {
                            afficheTexte(DIMX/2 - phrase.size()*DIMX/160, DIMY/4+DIMY/100, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        }
                        std::string typeQuete=jeu.getQuete(jeu.m_indiceFinQuete)->getType();
                        std::string nomPNJ=jeu.getQuete(jeu.m_indiceFinQuete)->getPNJ()->getNomPNJ();
                        phrase="Vous avez une récompense disponible";
                        afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/3+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                        phrase="pour avoir fini la quête";
                        afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/3+2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                        phrase="de "+typeQuete+" donnée par : "+nomPNJ+".";
                        afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/3+3*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                        phrase="OK";
                        afficheTexte(DIMX/2 - phrase.size()*DIMX/140, 2*DIMY/3, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        if (jeu.m_selectBouton)
                            im_selectionPetite.draw(renderer, DIMX/2-DIMX/10, 2*DIMY/3, DIMX/5, DIMY/20);
                    }
                    else if (jeu.m_afficheValidationCollecte) {
                        im_bulleInfo.draw(renderer, DIMX/5, DIMY/5, 3*DIMX/5, 3*DIMY/5);
                        phrase=jeu.getQuete(jeu.m_indiceQuete)->getNom();
                        if (phrase.size()>0) {
                            afficheTexte(DIMX/2 - phrase.size()*DIMX/160, DIMY/4+DIMY/100, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        }
                        phrase=jeu.getPNJ(jeu.m_niveau,jeu.m_interactionIndiceObjet)->getNomPNJ();
                        if (phrase.size()>0) {
                            afficheTexte(DIMX/2 - phrase.size()*DIMX/160, DIMY/3+DIMY/40, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        }
                        std::string typeQuete=jeu.getQuete(jeu.m_indiceQuete)->getType();
                        phrase="vous propose de valider votre";
                        afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/3+2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                        phrase="quête de "+typeQuete+".";
                        afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/3+3*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                        QueteCollecte* quete=static_cast<QueteCollecte*>(jeu.getQuete(jeu.m_indiceQuete));
                        unsigned int idObjet=quete->getIdObjet();
                        unsigned int nbObjet=quete->getNombreDemande();
                        phrase="Donner "+std::to_string(nbObjet)+"x ";
                        afficheTexte(DIMX/2-phrase.size()*DIMX/200-DIMX/50, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                        // On affiche l'image a idObjet+1 car 0 contient l'objet vide
                        im_tabImagesObjets[idObjet+1]->draw(renderer, DIMX/2+phrase.size()*DIMX/200, DIMY/2+DIMY/40, DIMX/25, 16*DIMY/25/9);
                        phrase="Accepter";
                        afficheTexte(DIMX/3 - phrase.size()*DIMX/160, 2*DIMY/3, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        phrase="Refuser";
                        afficheTexte(2*DIMX/3 - phrase.size()*DIMX/160, 2*DIMY/3, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        if(jeu.m_selectBouton)
                            im_selectionPetite.draw(renderer, DIMX/3-DIMX/10, 2*DIMY/3, DIMX/5, DIMY/17);
                        else
                            im_selectionPetite.draw(renderer, 2*DIMX/3 - DIMX/10, 2*DIMY/3, DIMX/5, DIMY/17);
                    }
                }

                // Affichage cartes
                if (jeu.m_afficheCartes) {

                    // Affichage controles
                    im_controlesCartes.draw(renderer, DIMX/10, DIMY/2, DIMX/4, 2*DIMY/7);

                    unsigned int nbCartesLigne, i, j;
                    unsigned int nbCartesDeck = joueur->getDeck()->getNombreCartes();
                    if(nbCartesDeck <= 10) m_nbLignesDeck = 1;
                    else m_nbLignesDeck = 2;

                    // Affichage deck
                    for (j=0; j < m_nbLignesDeck; j++) {
                        if (j == m_nbLignesDeck-1) {
                            nbCartesLigne = nbCartesDeck%10;
                            if (nbCartesLigne == 0 && nbCartesDeck != 0) nbCartesLigne = 10;
                        }
                        else nbCartesLigne = 10;
                        for (i=0; i < nbCartesLigne; i++) {
                            unsigned int idCarte = joueur->getDeck()->getCarte(i+10*j)->getIdCarte();
                            im_tabImagesReserve[idCarte]->draw(renderer,DIMX/10+i*m_largeurCarteDeck,DIMY/10+j*m_hauteurCarteDeck,m_largeurCarteDeck,m_hauteurCarteDeck);
                        }
                    }

                    // Affichage reserve
                    for (j=0; j<m_nbLignesReserve; j++) {
                        if (j==m_nbLignesReserve-1) nbCartesLigne=5;
                        else  nbCartesLigne=10;
                        for (i=0; i<nbCartesLigne; i++) {
                            im_tabImagesReserve[i+10*j]->draw(renderer,5.5*DIMX/10+i*m_largeurCarteReserve,DIMY/10+j*m_hauteurCarteReserve,m_largeurCarteReserve,m_hauteurCarteReserve);
                            im_nombreCartes[jeu.getJoueur()->getCartesReserve()[i+10*j]].draw(renderer,5.5*DIMX/10+(i+1)*m_largeurCarteReserve-m_largeurCarteReserve/5,DIMY/10+(j+1)*m_hauteurCarteReserve-m_hauteurCarteReserve/5,m_largeurCarteReserve/5,m_hauteurCarteReserve/5);
                            if(jeu.getJoueur()->getCartesReserve()[i+10*j] == 0 && !jeu.getJoueur()->getDeck()->estCartePresente(i+10*j))
                                im_masqueCartes.draw(renderer,5.5*DIMX/10+i*m_largeurCarteReserve,DIMY/10+j*m_hauteurCarteReserve,m_largeurCarteReserve,m_hauteurCarteReserve);
                        }
                    }

                    // Zoom sur les cartes
                    if (m_zoomCarte>-1) {
                        int l=std::min((float)DIMX/4,(float)400);
                        int h=(float)16/10*l;
                        if (m_zoomCarte>19)
                        {
                            im_tabImagesReserve[m_zoomCarte-20]->draw(renderer,DIMX/2-l/2,DIMY/2-h/2,l,h);
                            afficheTexte(DIMX/2+l/2-50,DIMY/2+h/2-50,30,30,std::to_string(jeu.getTableauCartes().getCarte(m_zoomCarte-20)->getCout()),policeTexte,couleurPoliceOr);
                        }
                        else {
                            unsigned int id = joueur->getDeck()->getCarte(m_zoomCarte)->getIdCarte();
                            im_tabImagesReserve[id]->draw(renderer,DIMX/2-l/2,DIMY/2-h/2,l,h);
                            afficheTexte(DIMX/2+l/2-50,DIMY/2+h/2-50,30,30,std::to_string(jeu.getTableauCartes().getCarte(id)->getCout()),policeTexte,couleurPoliceOr);
                        }
                    }
                }

                if (!jeu.m_fenetreOuverte && !jeu.m_afficheDialogue && !jeu.m_scenario) {
                    // Affichage PV
                    // On dessine la barre de PV
                    im_barrePV.draw(renderer, 10*DIMX/1600, DIMY-75*DIMY/900, 300*DIMX/1600, 68*DIMY/900);
                    int pv = joueur->getPointsDeVie();
                    int pvMax = joueur->getPointsDeVieMax();
                    // La barre est plus ou moins affichee selon la quantite de PV
                    SDL_Rect quantitePV = {0, 0, 552*DIMX/1600*pv/pvMax, 61*DIMY/900};
                    SDL_Rect posPV = {82*DIMX/1600, DIMY-43*DIMY/900, 175*DIMX/1600*pv/pvMax, 20*DIMY/900};
                    SDL_RenderCopy(renderer, im_PV.getTexture(), &quantitePV, &posPV);
                    // On affiche le nombre de PV
                    phrase = std::to_string(pv) + "/" + std::to_string(pvMax);
                    afficheTexte(220*DIMX/1600 - int(phrase.size())*5*DIMX/1600, DIMY-43*DIMY/900, int(phrase.size())*10*DIMX/1600, 20*DIMY/900, phrase, policeTexte, couleurPoliceOr);

                    // Affichage XP
                    im_barreXP.draw(renderer, DIMX - 310*DIMX/1600, DIMY-75*DIMY/900, 300*DIMX/1600, 68*DIMY/900);
                    int xp = joueur->getExperience();
                    int niveau = joueur->getNiveau();
                    int xpMax = niveau*niveau*25;
                    // La barre est plus ou moins grande selon l'XP qu'on a
                    SDL_Rect quantiteXP = {0, 0, 552*DIMX/1600*xp/xpMax, 61*DIMY/900};
                    SDL_Rect posXP = {DIMX - 260*DIMX/1600, DIMY-43*DIMY/900, 175*DIMX/1600*xp/xpMax, 20*DIMY/900};
                    SDL_RenderCopy(renderer, im_XP.getTexture(), &quantiteXP, &posXP);
                    // On affiche le nombre exact
                    phrase = std::to_string(xp) + "/" + std::to_string(xpMax);
                    afficheTexte(DIMX - 258*DIMX/1600, DIMY - 43*DIMY/900, int(phrase.size())*10*DIMX/1600, 20*DIMY/900, phrase, policeTexte, couleurPoliceOr);
                    // On affiche le niveau
                    phrase = std::to_string(niveau);
                    afficheTexte(DIMX - 50*DIMX/1600 - int(phrase.size())*10*DIMX/1600, DIMY - 61*DIMY/900, int(phrase.size())*20*DIMX/1600, 40*DIMY/900, phrase, policeMenu, couleurPoliceMenu);

                    // Affichage de la boussole pour la quete de deplacement selectionnee
                    if (jeu.m_indiceQueteSuivie!=-1) {
                        Quete* queteSuivie=jeu.getQuete(jeu.m_indiceQueteSuivie)->getQueteActive();
                        if (queteSuivie->getType()=="D\u00E9placement") {
                            QueteDeplacement* queteDeplacement=static_cast<QueteDeplacement*>(queteSuivie);
                            if (queteDeplacement->getEtat()=='a') {  // Si la quete est en cours on indique ou aller
                                // Si on est dans la bonne region, on affiche la boussole, sinon on indique dans quelle region se rendre
                                if (queteDeplacement->getRegionCible()==(int)jeu.getTerrain()->getIdRegion()) {
                                    Point destination=queteDeplacement->getDestination();
                                    Point positionJoueur=joueur->getPosition();
                                    int distanceHorizontale=(positionJoueur.x/jeu.m_tailleCase)-destination.x;
                                    int distanceVerticale=(positionJoueur.y/jeu.m_tailleCase)-destination.y;
                                    float distance=std::sqrt(std::pow(distanceHorizontale,2)+std::pow(distanceVerticale,2));
                                    phrase="Distance : "+std::to_string((int)distance);
                                    afficheTexte(DIMX/2 - phrase.size()*DIMX/160 - DIMX/80, DIMY-DIMY/18, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceOr);
                                    // On calcule le cosinus du triangle forme par la destination, le joueur et un point de coordonnees (destination.x,positionJoueur.y) puis l'angle de rotation
                                    double cosinus=(double)(abs(distanceHorizontale))/distance;
                                    unsigned int rotation=0;
                                    double PI=3.14159265;
                                    unsigned int angleDegres=std::acos(cosinus)*180.0/PI;
                                    if (distanceHorizontale>=0) {
                                        if (distanceVerticale>=0)
                                            rotation=180+angleDegres;
                                        else
                                            rotation=180-angleDegres;
                                    }
                                    else {
                                        if (distanceVerticale>=0)
                                            rotation=360-angleDegres;
                                        else
                                            rotation=angleDegres;
                                    }
                                    SDL_Rect r;
                                    r.x = DIMX/2 + phrase.size()*DIMX/160 - DIMX/160;
                                    r.y = DIMY-DIMY/18;
                                    r.w = DIMX/40;
                                    r.h = DIMY/20;
                                    SDL_RenderCopyEx(renderer, im_boussole.getTexture(), NULL, &r, rotation, NULL, SDL_FLIP_NONE);
                                }
                                else {
                                    phrase="Se rendre à : "+jeu.getNomTerrain(queteDeplacement->getRegionCible());
                                    afficheTexte(DIMX/2 - phrase.size()*DIMX/160, DIMY-DIMY/18, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceOr);
                                }
                            }
                            else { // Sinon elle est en attente de validation, on indique vers quel PNJ retourner
                                phrase="Retourner voir : "+queteDeplacement->getPNJ()->getNomPNJ();
                                afficheTexte(DIMX/2 - phrase.size()*DIMX/160, DIMY-DIMY/18, phrase.size()*DIMX/80, DIMY/20, phrase, policeMenu, couleurPoliceOr);
                            }
                        }
                    }
                }

                // Affichage ecran de victoire
                if(jeu.m_afficheVictoire) {
                    im_victoire.draw(renderer, 3*DIMX/10, DIMY/20, 2*DIMX/5, 9*DIMY/10);

                    im_tabImagesPersonnages[0]->draw(renderer, 4*DIMX/13, 2*DIMY/9, DIMX/20, (float)16/9*DIMY/20);
                    for (unsigned int i=0; i<3; i++) {
                        if (jeu.getEquipier(i)!=NULL) {
                            im_tabImagesPersonnages[jeu.getEquipier(i)->getId()]->draw(renderer, 4*DIMX/13+(i+1)*DIMX/20+DIMX/100, 2*DIMY/9, DIMX/20, (float)16/9*DIMY/20);
                        }
                    }

                    // Affichage XP
                    phrase = "+ ";
                    phrase += std::to_string(jeu.getGainExperience());
                    afficheTexte(DIMX/2-DIMX/9, 2*DIMY/7+DIMY/24, int(phrase.size()*DIMX/70), DIMY/18, phrase, policeMenu, couleurPoliceMenu);

                    // Affichage bouton OK
                    im_selectionPetite.draw(renderer, DIMX/2+DIMX/25, 5*DIMY/16-DIMY/28, DIMX/7, DIMY/14);
                    phrase = "OK";
                    afficheTexte(DIMX/2+DIMX/12, 5*DIMY/16-DIMY/32, int(phrase.size()*DIMX/40), DIMY/16, phrase, policeMenu, couleurPoliceMenu);

                    // Affichage carte obtenue ou non
                    int idCarte = jeu.getIdCarteObtenue();
                    if (idCarte != -1) {
                        phrase = "Carte obtenue :";
                        afficheTexte(DIMX/2 - int(phrase.size())*10, DIMY/2-DIMY/17, int(phrase.size()*20), DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        // On affiche la carte
                        im_tabImagesReserve[idCarte]->draw(renderer, DIMX/2 - DIMX/15, 11*DIMY/21, 2*DIMX/15, 3*DIMY/8);
                        // Si c'est une nouvelle carte, on affiche "nouveau"
                        if (joueur->getCartesObtenues()[idCarte] == 1)
                            im_nouveau.draw(renderer, DIMX/2 + DIMX/18, DIMY/2, DIMX/16, DIMY/30);
                    }
                    else {
                        phrase = "Vous n'avez pas obtenu";
                        afficheTexte(DIMX/2 - int(phrase.size())*DIMX/160, DIMY/2, int(phrase.size()*DIMX/80), DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        phrase = "de nouvelle carte...";
                        afficheTexte(DIMX/2 - int(phrase.size())*DIMX/160, DIMY/2+DIMY/20, int(phrase.size()*DIMX/80), DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    }
                }

                // Affiche ecran de level up
                if(jeu.m_afficheMonteeNiveau) {
                    im_levelUp.draw(renderer, 3*DIMX/10, DIMY/20, 2*DIMX/5, 9*DIMY/10);
                    for (unsigned int i=0; i<4; i++) {
                        if (jeu.getMonteeNiveau(i)) {
                            Personnage* p;
                            if (i==0)
                                p=jeu.getJoueur();
                            else
                                p=jeu.getEquipier(i-1);
                            unsigned int id=p->getId();
                            im_tabImagesPersonnages[i]->draw(renderer, 4*DIMX/13, 2*DIMY/9, DIMX/20, (float)16/9*DIMY/20);
                            SDL_Rect r;
                            r.x = 9*DIMX/13-DIMX/20;
                            r.y = 2*DIMY/9;
                            r.w = DIMX/20;
                            r.h = (float)16/9*DIMY/20;
                            SDL_RenderCopyEx(renderer, im_tabImagesPersonnages[id]->getTexture(), NULL, &r, 0, NULL, SDL_FLIP_HORIZONTAL);
                            // Affichage niveaux
                            unsigned int stat;
                            stat = jeu.getAncienNiveauPersonnage(i);
                            phrase = std::to_string(stat);
                            afficheTexte(2*DIMX/5 - phrase.size()*DIMX/48, DIMY/4-DIMY/20, phrase.size()*DIMX/24, DIMY/7, phrase, policeMenu, couleurPoliceMenu);
                            unsigned int nouvelleStat;
                            nouvelleStat = p->getNiveau();
                            phrase = std::to_string(nouvelleStat);
                            afficheTexte(3*DIMX/5 - phrase.size()*DIMX/48, DIMY/4-DIMY/20, phrase.size()*DIMX/24, DIMY/7, phrase, policeMenu, couleurPoliceMenu);

                            // Affichage PV
                            stat = jeu.getAncienPvPersonnage(i);
                            phrase = std::to_string(stat);
                            afficheTexte(DIMX/2 - phrase.size()*DIMX/96, DIMY/2-DIMY/28, phrase.size()*DIMX/48, DIMY/14, phrase, policeMenu, couleurPoliceMenu);
                            nouvelleStat = p->getPointsDeVieMax();
                            phrase = std::to_string(nouvelleStat);
                            afficheTexte(16*DIMX/25 - phrase.size()*DIMX/96, DIMY/2-DIMY/28, phrase.size()*DIMX/48, DIMY/14, phrase, policeMenu, couleurPoliceMenu);

                            // Affichage MP
                            stat = jeu.getAncienManaPersonnage(i);
                            phrase = std::to_string(stat);
                            afficheTexte(DIMX/2 - phrase.size()*DIMX/96, DIMY/2+DIMY/24, phrase.size()*DIMX/48, DIMY/14, phrase, policeMenu, couleurPoliceMenu);
                            nouvelleStat = p->getPointsDeManaMax();
                            phrase = std::to_string(nouvelleStat);
                            afficheTexte(16*DIMX/25 - phrase.size()*DIMX/96, DIMY/2+DIMY/24, phrase.size()*DIMX/48, DIMY/14, phrase, policeMenu, couleurPoliceMenu);

                            // Affichage Attaque
                            stat = jeu.getAncienneAttaquePersonnage(i);
                            phrase = std::to_string(stat);
                            afficheTexte(DIMX/2 - phrase.size()*DIMX/96, DIMY/2+3*DIMY/24, phrase.size()*DIMX/48, DIMY/14, phrase, policeMenu, couleurPoliceMenu);
                            nouvelleStat = p->getAttaque();
                            phrase = std::to_string(nouvelleStat);
                            afficheTexte(16*DIMX/25 - phrase.size()*DIMX/96, DIMY/2+3*DIMY/24, phrase.size()*DIMX/48, DIMY/14, phrase, policeMenu, couleurPoliceMenu);

                            // Affichage Defense
                            stat = jeu.getAncienneDefensePersonnage(i);
                            phrase = std::to_string(stat);
                            afficheTexte(DIMX/2 - phrase.size()*DIMX/96, DIMY/2+5*DIMY/24, phrase.size()*DIMX/48, DIMY/14, phrase, policeMenu, couleurPoliceMenu);
                            nouvelleStat = p->getDefense();
                            phrase = std::to_string(nouvelleStat);
                            afficheTexte(16*DIMX/25 - phrase.size()*DIMX/96, DIMY/2+5*DIMY/24, phrase.size()*DIMX/48, DIMY/14, phrase, policeMenu, couleurPoliceMenu);

                            // Affichage XP restants
                            if (i==0) {
                                Joueur* j=static_cast<Joueur*>(p);
                                stat=j->getNiveau()*j->getNiveau()*25 - j->getExperience();
                            }
                            else {
                                Familier* f=static_cast<Familier*>(p);
                                stat=f->getNiveau()*f->getNiveau()*15 - f->getExperience();
                            }
                            phrase = std::to_string(stat) + " points !";
                            afficheTexte(18*DIMX/29-phrase.size()*DIMX/240, 5*DIMY/6-DIMY/28, phrase.size()*DIMX/120, DIMY/24, phrase, policeMenu, couleurPoliceMenu);

                            // Affiche OK
                            im_selectionPetite.draw(renderer, DIMX/2 - DIMX/12, 7*DIMY/8-DIMY/32, DIMX/6, DIMY/16);
                            phrase = "OK";
                            afficheTexte(DIMX/2 - phrase.size()*DIMX/120, 7*DIMY/8-DIMY/40, phrase.size()*DIMX/60, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                            break;
                        }
                    }

                }

                if (jeu.m_afficheRecueilQuete) {
                    unsigned int transparence=255;
                    // Affichage de la liste deroulante des quetes
                    im_cadre.draw(renderer, 0, 0, DIMX/3, DIMY);
                    im_fondRecueil.draw(renderer, DIMX/220, DIMY/6, DIMX/3-DIMX/220, 5*DIMY/6-DIMY/12);
                    // Affichage barre de defilement
                    im_barreDefilementRecueil.draw(renderer, DIMX/80, DIMY/6, DIMX/60, 5*DIMY/6-DIMY/12);
                    unsigned int nombreQuetes=jeu.getNbQuetes();
                    // Affichage de la barre de coulissement
                    float taille;  // la taille que fait la barre de coulissement
                    if (5*DIMY/6-DIMY/12>(int)nombreQuetes*DIMY/6)
                        taille=5*DIMY/6-DIMY/12;
                    else
                        taille=(float)(5*DIMY/6-DIMY/12)/(nombreQuetes*DIMY/6)*(5*DIMY/6-DIMY/12);
                    unsigned int hauteurMax=5*DIMY/6-DIMY/12-taille;  // la hauteur maximale a laquelle peut commencer la barre pour ne pas depasser
                    unsigned int valeurMaxMolette=std::max(nombreQuetes*DIMY/6-(DIMY/6+5*DIMY/6-DIMY/12-DIMY/8),(unsigned int)1);   // la valeur maximale que peut prendre m_moletteRecueil, non nulle
                    im_barreCoulissanteRecueil.draw(renderer, DIMX/80, DIMY/6+hauteurMax*((float)m_moletteRecueil/valeurMaxMolette), DIMX/60, taille);  // affiche la barre de coulissement a la bonne hauteur et la bonne taille en fonction du nombre de quetes et de m_moletteRecueil
                    // Affichage de la liste des quetes
                    for (unsigned int i=0; i<nombreQuetes; i++) {
                        phrase = jeu.getQuete(i)->getNom();
                        if (jeu.getQuete(i)->getEtat()=='i') {
                            phrase = "-------";
                            transparence=127;
                        }
                        else if (jeu.getQuete(i)->getEtat()=='r')
                            transparence=127;
                        else
                            transparence=255;
                        if ((int)(DIMY/6+i*DIMY/6-m_moletteRecueil)>DIMY/100) {
                            im_fondeq.draw(renderer, DIMX/20, DIMY/6+i*DIMY/6-m_moletteRecueil, DIMX/4, DIMY/8, transparence);
                            afficheTexte(DIMX/20+DIMX/8-phrase.size()*DIMX/240, DIMY/6+DIMY/16-DIMY/60+i*DIMY/6-m_moletteRecueil, phrase.size()*DIMX/120, DIMY/30, phrase, policeMenu, couleurPolice, transparence);
                        }
                    }
                    im_enteteRecueil.draw(renderer, DIMX/220, DIMY/100, DIMX/3-DIMX/220, DIMY/6-DIMY/100);
                    im_piedPageRecueil.draw(renderer, DIMX/220, 11*DIMY/12, DIMX/3-DIMX/220, DIMY/12);
                    phrase = "Quêtes";
                    afficheTexte(DIMX/6-phrase.size()*DIMX/120, DIMY/40, phrase.size()*DIMX/60, DIMY/20, phrase, policeMenu, couleurPolice);
                    // Affichage des informations de la quete selectionnee
                    im_cadre.draw(renderer, DIMX/3, 0, 2*DIMX/3, DIMY);
                    im_enteteRecueil.draw(renderer, DIMX/3+DIMX/220, DIMY/100, 2*DIMX/3-2*DIMX/220, DIMY/6-DIMY/100);
                    im_fondRecueil.draw(renderer, DIMX/3+DIMX/220, DIMY/6, 2*DIMX/3-2*DIMX/220, 5*DIMY/6-DIMY/12);
                    im_piedPageRecueil.draw(renderer, DIMX/3+DIMX/220, 11*DIMY/12, 2*DIMX/3-2*DIMX/220, DIMY/12);
                    if (m_queteRecueil!=-1) {
                        Quete* queteAffichee=jeu.getQuete(m_queteRecueil);
                        phrase = queteAffichee->getNom();
                        afficheTexte(2*DIMX/3-phrase.size()*DIMX/120, DIMY/40, phrase.size()*DIMX/60, DIMY/20, phrase, policeMenu, couleurPolice);
                        std::string nomPnjLanceur=queteAffichee->getPNJ()->getNomPNJ();
                        afficheTexte(2*DIMX/3-nomPnjLanceur.size()*DIMX/240, DIMY/7, nomPnjLanceur.size()*DIMX/120, DIMY/30, nomPnjLanceur, policeMenu, couleurPolice);
                        std::string typeQueteGlobale=queteAffichee->getType();
                        phrase = "Quête de "+typeQueteGlobale+" :";
                        afficheTexte(2*DIMX/3-phrase.size()*DIMX/240, DIMY/6+DIMY/20, phrase.size()*DIMX/120, DIMY/30, phrase, policeMenu, couleurPolice);
                        // Affichage de la liste des etapes
                        sdlAffQuete(queteAffichee, DIMX/3+DIMX/100, DIMY/4+DIMY/20, 0, 0);
                        // Affichage des recompenses
                        phrase="Récompenses : ";
                        afficheTexte(2*DIMX/3 - phrase.size()*DIMX/240, 2*DIMY/3, phrase.size()*DIMX/120, DIMY/30, phrase, policeMenu, couleurPolice);
                        Inventaire* recompenses=queteAffichee->getRecompenses();
                        for (unsigned int i=0; i<recompenses->getNbObjet(); i++) {
                            im_tabImagesObjets[0]->draw(renderer, 2*DIMX/3-((int)(recompenses->getNbObjet())-2*(int)i)*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/20, 16*DIMY/20/9);
                            // On affiche l'image a idObjet+1 car 0 contient l'objet vide
                            im_tabImagesObjets[recompenses->getIemeObjet(i+1)->getIdObjet()+1]->draw(renderer, 2*DIMX/3-((int)(recompenses->getNbObjet())-2*(int)i)*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/20, 16*DIMY/20/9);
                        }
                        im_tabImagesObjets[0]->draw(renderer, 2*DIMX/3-(recompenses->getNbObjet()+2)*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/20, 16*DIMY/20/9);
                        phrase="XP";
                        afficheTexte(2*DIMX/3-(recompenses->getNbObjet()+2)*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/60, 16*DIMY/60/9, phrase, policeMenu, couleurPoliceOr);
                        phrase=std::to_string(queteAffichee->getExp());
                        afficheTexte(2*DIMX/3-(recompenses->getNbObjet()+2)*DIMX/40+DIMX/40-phrase.size()*DIMX/120, 3*DIMY/4+16*DIMY/40/9-DIMY/40-DIMY/40, phrase.size()*DIMX/60, DIMY/20, phrase, policeMenu, couleurPoliceOr);
                        im_tabImagesObjets[0]->draw(renderer, 2*DIMX/3+recompenses->getNbObjet()*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/20, 16*DIMY/20/9);
                        im_or.draw(renderer, 2*DIMX/3+recompenses->getNbObjet()*DIMX/40, 3*DIMY/4-DIMY/40, DIMX/60, 16*DIMY/60/9);
                        phrase=std::to_string(queteAffichee->getOr());
                        afficheTexte(2*DIMX/3+recompenses->getNbObjet()*DIMX/40+DIMX/40-phrase.size()*DIMX/120, 3*DIMY/4+16*DIMY/40/9-DIMY/40-DIMY/40, phrase.size()*DIMX/60, DIMY/20, phrase, policeMenu, couleurPoliceOr);
                        // Bouton de recuperation des recompenses
                        if (queteAffichee->getEtat()=='f' || queteAffichee->getEtat()=='r') {  // la quete est finie voire deja recuperee
                            transparence=255;
                            if (queteAffichee->getEtat()=='r') // on a deja recupere la recompense
                                transparence=127;
                            im_fondeq.draw(renderer, 2*DIMX/3-DIMX/12, 7*DIMY/8-DIMY/20, DIMX/6, DIMY/12, transparence);
                            phrase = "Récupérer";
                            afficheTexte(2*DIMX/3-phrase.size()*DIMX/240, 7*DIMY/8+DIMY/24-DIMY/60-DIMY/20, phrase.size()*DIMX/120, DIMY/30, phrase, policeMenu, couleurPolice, transparence);
                        }
                        else {
                            transparence=255;
                            if (jeu.m_indiceQueteSuivie==m_queteRecueil)
                                transparence=127;
                            im_fondeq.draw(renderer, 2*DIMX/3-DIMX/12, 7*DIMY/8-DIMY/20, DIMX/6, DIMY/12, transparence);
                            phrase = "Suivre cette quête";
                            afficheTexte(2*DIMX/3-phrase.size()*DIMX/240, 7*DIMY/8+DIMY/24-DIMY/60-DIMY/20, phrase.size()*DIMX/120, DIMY/30, phrase, policeMenu, couleurPolice, transparence);
                        }
                    }
                    if (jeu.m_afficheInventairePlein) {
                        im_bulleInfo.draw(renderer, DIMX/4, DIMY/5, DIMX/2, 3*DIMY/5);
                        phrase="Inventaire plein";
                        afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        phrase="Votre inventaire ne peut pas";
                        afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                        phrase="contenir tous ces objets !";
                        afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                        phrase="OK";
                        afficheTexte(DIMX/2 - phrase.size()*DIMX/140, 2*DIMY/3, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                        if (jeu.m_selectBouton)
                            im_selectionPetite.draw(renderer, DIMX/2-DIMX/10, 2*DIMY/3, DIMX/5, DIMY/20);
                    }
                }
            }
            // Affichage du combat
            else {
                im_fondCombat.draw(renderer, 0, 0, DIMX, DIMY);
                // On recupere les parametres
                m_tailleMainJoueur=jeu.getCombat()->getMainPersonnage(0).getNombreCartes();
               // m_tailleMainEnnemi=jeu.getCombat()->getMainEnnemi().getNombreCartes();
                if (m_tailleMainJoueur>0) m_largeurCarteJoueur = std::min((int)(DIMX*0.06), DIMX/10);
                m_hauteurCarteJoueur=16*m_largeurCarteJoueur/10;
                if(m_afficheBulleX!=-1 && m_afficheBulleY!=-1 ){
                        im_bulleInfo.draw(renderer, m_afficheBulleX, m_afficheBulleY,DIMX/6,DIMY/8);
                        phrase=jeu.getCombat()->getNomNonOrdo(m_afficheStatut);
                        afficheTexte((m_afficheBulleX+DIMX/34),m_afficheBulleY,phrase.size()*DIMX/192, DIMY/70,phrase,policeTexte,couleurPolice);
                        for(int i=0;i<6;i++){
                            if(i==0){
                                if(jeu.getCombat()->getStatutPersonnage(m_afficheStatut,i)==0)
                                    phrase=" Invulnérabilité : non ";
                                else  phrase=" Invulnérabilité : oui ";
                            }
                            if(i==1){
                                if(jeu.getCombat()->getStatutPersonnage(m_afficheStatut,i)==0)
                                    phrase=" Dernière chance : non ";
                                else  phrase=" Dernière chance : oui ";

                            }
                            if(i==2){
                                    phrase=" Tour d'Esquive restant : "+std::to_string(jeu.getCombat()->getStatutPersonnage(m_afficheStatut,i));

                            }
                            if(i==3){
                                    phrase=" Tour de silence restant : "+std::to_string(jeu.getCombat()->getStatutPersonnage(m_afficheStatut,i));

                            }
                            if(i==4){
                                    phrase=" Tour de confusion restant : "+std::to_string(jeu.getCombat()->getStatutPersonnage(m_afficheStatut,i));

                            }
                            if(i==5){
                                    phrase=" Tour de poison restant"+std::to_string(jeu.getCombat()->getStatutPersonnage(m_afficheStatut,i));

                            }
                            afficheTexte(m_afficheBulleX,m_afficheBulleY+(3+i)*DIMY/72,phrase.size()*DIMX/192, DIMY/72,phrase,policeTexte,couleurPolice);
                        }

                }

                // Affichage des deck
               // im_dosCarte.draw(renderer, 8*DIMX/10, DIMY/20, DIMX/10, 16*DIMX/100);
               // im_dosCarte.draw(renderer, 8*DIMX/10, 9.5*DIMY/10-16*DIMX/100, DIMX/10, 16*DIMX/100);

                unsigned int nbEquipier=jeu.getCombat()->getNbEquipier();
                unsigned int nbEnnemi=jeu.getCombat()->getNbEnnemi();
               // Affichage du personnage et de l'ennemi
                for (unsigned int i=0; i<nbEquipier+1; i++) {
                    if(i==0) {
                        im_tabImagesPersonnages[0]->draw(renderer, 0.5*DIMX/10,(0.3)*DIMY/(nbEquipier+1),DIMX/10, (float)16/9*DIMY/10);
                        phrase = std::to_string(jeu.getCombat()->getPointsDeViePersonnage(i)) + " / " + std::to_string(jeu.getCombat()->getPointsDeVieMaxPersonnage(i)) + " PV ";
                        afficheTexte(1.3*DIMX/10,(0.1)*DIMY/(nbEquipier+1)+ (float)16/9*DIMY/10, phrase.size()*DIMX/192, DIMY/72, phrase, policeTexte, couleurPoliceOr);
                         phrase = std::to_string(jeu.getCombat()->getManaPersonnage(i))+ " / " + std::to_string(std::min(jeu.getCombat()->getManaMaxPersonnage(i),jeu.getCombat()->getNbTour())) + " PM ";
                        afficheTexte(1.3*DIMX/10,(0.1)*DIMY/(nbEquipier+1)+ (float)16/9*DIMY/10+DIMY/72, phrase.size()*DIMX/192, DIMY/72, phrase, policeTexte, couleurPoliceOr);
                        if (jeu.getCombat()->getBouclierPersonnage(0)>0.0) {
                            phrase = std::to_string(jeu.getCombat()->getBouclierPersonnage(0)) +" bouclier";
                            afficheTexte(1.3*DIMX/10,(0.1)*DIMY/(nbEquipier+1)+ (float)16/9*DIMY/10+2*DIMY/72, phrase.size()*DIMX/192, DIMY/72, phrase, policeTexte, couleurPoliceOr);
                        }
                    }
                    else {
                        phrase = std::to_string(jeu.getCombat()->getPointsDeViePersonnage(i)) + " / " + std::to_string(jeu.getCombat()->getPointsDeVieMaxPersonnage(i)) + " PV";
                        im_tabImagesPersonnages[jeu.getCombat()->getEquipier(i-1)->getId()]->draw(renderer, 0.5*DIMX/10,(0.2+i)*DIMY/(nbEquipier+1),DIMX/10, (float)16/9*DIMY/10);
                        afficheTexte(1.3*DIMX/10,(0.1+i)*DIMY/(nbEquipier+1)+ (float)16/9*DIMY/10, phrase.size()*DIMX/192, DIMY/72, phrase, policeTexte, couleurPoliceOr);
                    }
                    // Affichage du marqueur d'initiative
                    if (jeu.getCombat()->getPersonnageActuel()==i) {
                        SDL_Rect r;
                        r.x = 2*DIMX/10;
                        r.y = (0.1+i)*DIMY/(nbEquipier+1)+ (float)16/9*DIMY/20;
                        r.w = DIMX/40;
                        r.h = DIMY/20;
                        SDL_RenderCopyEx(renderer, im_boussole.getTexture(), NULL, &r, 180, NULL, SDL_FLIP_NONE);
                    }
                }

                for (unsigned int i=0; i<nbEnnemi; i++) {
                    phrase = std::to_string(jeu.getCombat()->getPointsDeViePersonnage(1+nbEquipier+i)) + " / " + std::to_string(jeu.getCombat()->getPointsDeVieMaxPersonnage(1+nbEquipier+i)) + " PV";
                    if(i==1 || i==3){
                        im_tabImagesEnnemis[jeu.getCombat()->getEnnemi(i)->getEspece()->getIdEspece()]->draw(renderer, 7*DIMX/10,i*DIMY/5,DIMX/10, (float)16/9*DIMY/10);
                        afficheTexte(6*DIMX/10,(0.5+i)*DIMY/5, phrase.size()*DIMX/192, DIMY/72, phrase, policeTexte, couleurPoliceOr);
                        if (jeu.getCombat()->getBouclierPersonnage(i+nbEquipier+1)>0.0) {
                            phrase = std::to_string(jeu.getCombat()->getBouclierPersonnage(i+nbEquipier+1)) +" bouclier";
                            afficheTexte(0.2*DIMX/10, 2.5*DIMY/10, phrase.size()*10, 15, phrase, policeTexte, couleurPoliceOr);
                        }
                        // Affichage du marqueur d'initiative
                        if (jeu.getCombat()->getPersonnageActuel()==i+nbEquipier+1) {
                            im_boussole.draw(renderer, 6.5*DIMX/10-DIMX/40, i*DIMY/5, DIMX/40, DIMY/20);
                        }
                    }
                    else {
                        im_tabImagesEnnemis[jeu.getCombat()->getEnnemi(i)->getEspece()->getIdEspece()]->draw(renderer, 8*DIMX/10,i*DIMY/5,DIMX/10, (float)16/9*DIMY/10);
                        afficheTexte(7*DIMX/10,(0.5+i)*DIMY/5, phrase.size()*DIMX/192, DIMY/72, phrase, policeTexte, couleurPoliceOr);
                        if (jeu.getCombat()->getBouclierPersonnage(i+nbEquipier+1)>0.0) {
                            phrase = std::to_string(jeu.getCombat()->getBouclierPersonnage(i+nbEquipier+1)) +" bouclier";
                            afficheTexte(7*DIMX/10,(0.5+i)*DIMY/5+DIMY/72, phrase.size()*DIMX/192, DIMY/72, phrase, policeTexte, couleurPoliceOr);
                        }
                        if (jeu.getCombat()->getPersonnageActuel()==i+nbEquipier+1) {
                            im_boussole.draw(renderer, 7.5*DIMX/10-DIMX/40, i*DIMY/5, DIMX/40, DIMY/20);
                        }
                    }
                }

                 // Main du joueur
                for (int i=0; i<m_tailleMainJoueur; i++) {
                    unsigned int miseEnValeur=0;
                    if (m_carteAjouer==i)
                        miseEnValeur=DIMY/20;
                    // On affiche les cartes
                    im_tabImagesReserve[jeu.getCombat()->getMainPersonnage(0).getCarte(i)->getIdCarte()]->draw(renderer, 2*DIMX/6+i*m_largeurCarteJoueur, 9.5*DIMY/10-m_hauteurCarteJoueur-miseEnValeur, m_largeurCarteJoueur, m_hauteurCarteJoueur);

                    // Affichage coût en mana
                    phrase = std::to_string(jeu.getTableauCartes().getCarte(jeu.getCombat()->getMainPersonnage(0).getCarte(i)->getIdCarte())->getCout());
                    afficheTexte(2*DIMX/6+(0.9+i)*m_largeurCarteJoueur, 9.5*DIMY/10-1.2*DIMY/72-miseEnValeur, phrase.size()*DIMX/192, DIMY/72, phrase, policeTexte, couleurPoliceOr);
                }

                // Affichage des esquives/invulnerabilites/blessures par confusion
                if (m_jeuCarte > -1 && m_jeuCarte < 5 ) {
                  phrase=jeu.getCombat()->getNomOrdo(m_dernierActif)+" utilise "+m_nomDerniereCarte+" sur "+jeu.getCombat()->getNomNonOrdo(jeu.getCombat()->getCible());
                  jeu.getCombat()->setLogs(phrase);
                    switch (m_jeuCarte) {
                        case 0:
                            break;
                        case 1:
                            phrase = "Mais il se blesse dans sa confusion !";
                            jeu.getCombat()->setLogs(phrase);
                            break;
                         case 2:
                            phrase ="Mais il est confus !";
                            jeu.getCombat()->setLogs(phrase);
                            break;
                        case 3:
                            phrase = " Mais "+jeu.getCombat()->getNomNonOrdo(jeu.getCombat()->getCible())+" est Invuln\u00E9rable !";
                            break;
                        case 4: phrase= " Mais "+jeu.getCombat()->getNomNonOrdo(jeu.getCombat()->getCible())+" Esquive !";jeu.getCombat()->setLogs(phrase);
                            break;
                            break;
                    }
                    m_jeuCarte=-1;
                }

                // Affichage du bouton de fin de tour
                SDL_Rect fondMenu = {4*DIMX/10, 0, DIMX/10, DIMY/10};
                SDL_SetRenderDrawColor(renderer, 151, 109, 63, 255);
                SDL_RenderFillRect(renderer, &fondMenu);
                phrase = "FINIR LE TOUR";
                afficheTexte(4.125*DIMX/10, 0.5*DIMY/10, phrase.size()*DIMX/192, DIMY/72, phrase, policeTexte, couleurPoliceOr);
                if(jeu.getCombat()->getTailleLogs()>4){
                   for(int i=5;i>0;i--){
                        phrase=jeu.getCombat()->getLogs(jeu.getCombat()->getTailleLogs()-i-m_molette);
                        afficheTexte(3*DIMX/9,(16+(5-i))*DIMY/36,phrase.size() * DIMX/206,DIMY/36,phrase, policeTexte, couleurPoliceOr);

                    }
                }
                else {
                    for(int i=jeu.getCombat()->getTailleLogs();i>0;i--){
                       phrase=jeu.getCombat()->getLogs(jeu.getCombat()->getTailleLogs()-i);
                        afficheTexte(3*DIMX/9,(16+(jeu.getCombat()->getTailleLogs()-i))*DIMY/35,phrase.size()*DIMX/206,DIMY/36,phrase, policeTexte, couleurPoliceOr);
                    }
                }
                if (m_carteSelectionnee!=-1) {
                    int l = std::min((float)DIMX/4, (float)400);
                    int h = (float)16/10*l;
                    im_tabImagesReserve[m_carteSelectionnee]->draw(renderer,DIMX/2-l/2,DIMY/2-h/2,l,h);
                    // Affichage coût en mana
                    phrase = std::to_string(jeu.getTableauCartes().getCarte(m_carteSelectionnee)->getCout());
                    afficheTexte(DIMX/2 + l/2 - phrase.size()*DIMX/96*2, DIMY/2 + h/2 - 1.2*DIMY/36, phrase.size()*DIMX/96, DIMY/36, phrase, policeTexte, couleurPoliceOr);
                }

                if (jeu.m_afficheCombat) {
                    im_combat.draw(renderer, 3*DIMX/10, DIMY/20, 2*DIMX/5, 9*DIMY/10);
                    for (unsigned int i=0; i<nbEquipier+1; i++) {
                        if(i==0) {
                            im_tabImagesPersonnages[0]->draw(renderer, 4*DIMX/10-DIMX/40,2*DIMY/10+i*(7*DIMY/10)/(nbEquipier+1), DIMX/20, (float)16/9*DIMY/20);
                            phrase = jeu.getCombat()->getJoueur()->getNom();
                            afficheTexte(4*DIMX/10-phrase.size()*DIMX/256,2*DIMY/10+i*(7*DIMY/10)/(nbEquipier+1)-DIMY/200-DIMY/43, phrase.size()*DIMX/128, DIMY/43, phrase, policeTexte, couleurPoliceOr);
                        }
                        else {
                            im_tabImagesPersonnages[jeu.getCombat()->getEquipier(i-1)->getId()]->draw(renderer, 4*DIMX/10-DIMX/40,2*DIMY/10+i*(7*DIMY/10)/(nbEquipier+1),DIMX/20, (float)16/9*DIMY/20);
                            phrase = jeu.getCombat()->getEquipier(i-1)->getNom();
                            afficheTexte(4*DIMX/10-phrase.size()*DIMX/256,2*DIMY/10+i*(7*DIMY/10)/(nbEquipier+1)-DIMY/200-DIMY/43, phrase.size()*DIMX/128, DIMY/43, phrase, policeTexte, couleurPoliceOr);
                        }

                    }
                    for (unsigned int i=0; i<nbEnnemi; i++) {
                        im_tabImagesEnnemis[jeu.getCombat()->getEnnemi(i)->getEspece()->getIdEspece()]->draw(renderer, 6*DIMX/10-DIMX/40,2*DIMY/10+i*(7*DIMY/10)/(nbEnnemi), DIMX/20, (float)16/9*DIMY/20);
                        phrase = jeu.getCombat()->getEnnemi(i)->getNom();
                        afficheTexte(6*DIMX/10-phrase.size()*DIMX/256,2*DIMY/10+i*(7*DIMY/10)/(nbEnnemi)-DIMY/200-DIMY/43, phrase.size()*DIMX/128, DIMY/43, phrase, policeTexte, couleurPoliceOr);
                    }
                }
            }

            if (jeu.m_afficheTutoriel) {
                im_bulleInfo.draw(renderer, DIMX/4, DIMY/5, DIMX/2, 3*DIMY/5);
                if (jeu.m_afficheTutorielQuete) {
                    phrase="- Apprentissage des quêtes #1 -";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Ce monde regorge d'aventures et de quêtes.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Parlez aux personnages qui vous entourent.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Certains vous proposeront des quêtes,";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="libre à vous d'accepter, ou non";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                }
                else if (jeu.m_afficheTutorielRefusQuete) {
                    phrase="- Une femme peu serviable -";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Ainsi, vous osez refuser une quête...";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Eh bien, libre à vous ! En revanche,";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="vous ne pourrez plus jamais l'accepter";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="et la récompense vous passera sous le nez.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                }
                else if (jeu.m_afficheTutorielRecueil) {
                    phrase="- Apprentissage des quêtes #2 -";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Vous avez accepté votre première quête !";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Vous pouvez consulter la liste des quêtes";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="et récupérer vos récompenses depuis le";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="recueil de quêtes en appuyant sur la touche R.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                }
                else if (jeu.m_afficheTutorielInventaire) {
                    phrase="- Faire l'inventaire -";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Vous pouvez désormais accéder à l'inventaire";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="en appuyant sur la touche I.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Vous y trouverez également vos équipements";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="et diverses informations sur votre situation.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                }
                else if (jeu.m_afficheTutorielDeck) {
                    phrase="- Plusieurs cartes à sa main -";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Vous avez débloqué votre deck de cartes !";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Vous pouvez accéder à l'éditeur de deck";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="afin d'ajouter ou retirer des cartes";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="en appuyant sur la touche C.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                }
                else if (jeu.m_afficheTutorielLancerCombat) {
                    phrase="- BAAAAASTON ! -";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Maintenant que vous possédez un deck,";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="vous avez le pouvoir d'affronter les monstres !";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Pour ce faire, il vous suffit de vous";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="retrouver en contact avec un monstre.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                }
                else if (jeu.m_afficheTutorielCombat) {
                    phrase="- Le combat pour les nulls #1 -";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Vous voici dans votre premier combat !";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Votre équipe se trouve à gauche,";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="vos adversaires sont à droite.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Pour l'instant, vous êtes en un contre un.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                }
                else if (jeu.m_afficheTutorielMain) {
                    phrase="- Le combat pour les nulls #2 -";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Votre main se situe en bas de l'écran.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Un clic gauche permet de jouer une carte,";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="un clic droit de zoomer dessus.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Chaque carte a un coût en mana et un type.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                }
                else if (jeu.m_afficheTutorielMana) {
                    phrase="- Le combat pour les nulls #3 -";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Le mana (PM) vous permet de jouer des cartes.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Vous possédez un mana maximum et un actuel.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Chaque tour, votre mana se régénère et";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="augmente de 1 dans la limite de votre maximum.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                }
                else if (jeu.m_afficheTutorielCarte) {
                    phrase="- Le combat pour les nulls #4 -";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Il existe 4 types de carte différents :";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Attaque, Bonus, Défense et Soin.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Chaque type offre des possibilités différentes";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="et est représenté par un style différent.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                }
                else if (jeu.m_afficheTutorielLourdeau) {
                    phrase="- Toujours s'attaquer aux plus faibles que soi -";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/140, DIMY/5+DIMY/20, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Cet adversaire est plus coriace que ses congénères !";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-2*DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="Vous devriez peut-être commencer par";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2-DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="vous débarasser des trois Vaut-Rien";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                    phrase="qui menacent les autres villageois.";
                    afficheTexte(DIMX/2 - phrase.size()*DIMX/200, DIMY/2+DIMY/20, phrase.size()*DIMX/100, DIMY/25, phrase, policeMenu, couleurPoliceMenu);
                }
                phrase="OK";
                afficheTexte(DIMX/2 - phrase.size()*DIMX/140, 2*DIMY/3, phrase.size()*DIMX/70, DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                if (jeu.m_selectBouton)
                    im_selectionPetite.draw(renderer, DIMX/2-DIMX/10, 2*DIMY/3, DIMX/5, DIMY/20);
            }

            // Affichage bouton accès menu
            if (!jeu.m_fenetreOuverte && !jeu.m_afficheDialogue)
                im_boutonMenu.draw(renderer, DIMX-DIMX/16, 0, DIMX/16, DIMY/18);
            else if (jeu.m_afficheMenu && !jeu.m_afficheDialogue)
                im_boutonRetour.draw(renderer, DIMX-DIMX/16, 0, DIMX/16, DIMY/18);

            // Affichage Menu
            if(jeu.m_afficheMenu) {

                // controles
                im_controlesMenu.draw(renderer, 0, 0, DIMX/6, DIMY/6);

                // Menu
                im_menu.draw(renderer, 3*DIMX/10, DIMY/20, 2*DIMX/5, 9*DIMY/10);
                std::string phrasesMenu [4] = {"Voir l'inventaire", "Sauvegarder la partie", "Charger une partie", "Quitter le jeu"};
                for(int i = 0 ; i < 4 ; i++) {
                    unsigned int transparence=255;
                    if (jeu.m_indiceScenario<14 && i==1)
                        transparence=127;
                    // Surlignage de la ligne selectionnee
                    if(jeu.m_ligneSelectionnee == i)
                        im_selectionGrande.draw(renderer, 12*DIMX/40, DIMY/5 + i*4*DIMY/19-DIMY/16, 16*DIMX/40, DIMY/8, transparence);
                    // Affichage phrase
                    afficheTexte(DIMX/2 - int(phrasesMenu[i].size()*DIMX/150), DIMY/5 + i*4*DIMY/19-DIMY/30, int(phrasesMenu[i].size()*DIMX/75), DIMY/15, phrasesMenu[i], policeMenu, couleurPoliceMenu, transparence);
                }
            }
        }
        else  // Affichage dialogue du narrateur
        {
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            SDL_RenderClear(renderer);
            // On affiche la bulle de dialogue
            im_bulle.draw(renderer, DIMX/2 - 7*DIMX/16, DIMY/80, 14*DIMX/16, 2*DIMY/9);
            // On dessine le PNJ
            im_narrateurBulle.draw(renderer, DIMX/2 - 7*DIMX/16 + DIMX/105, 2*DIMY/33, 3*DIMX/26, 2*DIMY/11);
            // On affiche le nom du PNJ
            phrase = "Narrateur";
            afficheTexte(DIMX/2 - phrase.size()*DIMX/160 + DIMX/17, DIMY/26, phrase.size()*DIMX/80, DIMY/22, phrase, policeMenu, couleurPoliceMenu);
            // On recupere la phrase
            phrase = jeu.m_dialogueNarrateur->getPhrase(jeu.m_dialogueNarrateur->getIndicePhrase());
            // On affiche la phrase
            afficheTexte(DIMX/2 - tailleSansAccents(phrase)*DIMX/180 + DIMX/20, DIMY/12, tailleSansAccents(phrase)*DIMX/90, DIMY/18, phrase, policeMenu, couleurPoliceMenu);
            // On affiche le texte pour passer l'introduction
            if (m_afficherEchapScenario) {
                phrase = "[ECHAP] Passer";
                afficheTexte(DIMX - phrase.size()*DIMX/150, DIMY-DIMY/40, phrase.size()*DIMX/150, DIMY/40, phrase, policeMenu, couleurPolice, (unsigned int)((sin(m_animationEchapScenario)+1)*255/2));
                if (m_animationEchapScenario<2*3.14)
                    m_animationEchapScenario+=0.02;
                else
                    m_animationEchapScenario=0;
            }
        }
    }

    // Menu principal
    else {
        // controles
        im_controlesMenu.draw(renderer, 5*DIMX/6, 5*DIMY/6, DIMX/6, DIMY/6);

        // Fond
        im_menuPrincipal.draw(renderer, 3*DIMX/10, DIMY/20, 2*DIMX/5, 9*DIMY/10);
        std::string phrases[3] = {"Nouvelle partie", "Charger une partie", "Quitter"};
        for(int i = 0 ; i < 3 ; i++) {
            // Surlignage de la ligne selectionnee
            if(jeu.m_ligneSelectionnee == i)
                im_selectionGrande.draw(renderer, 13*DIMX/40, DIMY/2 + i*DIMY/6-DIMY/16, 14*DIMX/40, DIMY/8);
            afficheTexte(DIMX/2 - int(phrases[i].size()*DIMX/150), DIMY/2 + i*DIMY/6-DIMY/30, int(phrases[i].size()*DIMX/75), DIMY/15, phrases[i], policeMenu, couleurPoliceMenu);
        }
    }

    // Affiche les slots de sauvegarde
    if(jeu.m_afficheChargement || jeu.m_afficheSauvegarde) {
        // Fond
        if(jeu.m_afficheChargement)
            im_chargement.draw(renderer, 3*DIMX/10, DIMY/20, 2*DIMX/5, 9*DIMY/10);
        else
            im_sauvegarde.draw(renderer, 3*DIMX/10, DIMY/20, 2*DIMX/5, 9*DIMY/10);
        // Bordures et phrases
        for(int i = 0 ; i < 6 ; i++) {
            // Surlignage de la ligne selectionnee
            if(jeu.m_ligneSelectionnee == i) {
                if(jeu.estFichierVide(i))
                    im_selectionGrande.draw(renderer, 13*DIMX/40, DIMY/6 + i*5*DIMY/36-DIMY/18, 14*DIMX/40, DIMY/9);
                else
                    im_selectionSauvegarde.draw(renderer, 13*DIMX/40, DIMY/6 + i*5*DIMY/36-DIMY/18, 14*DIMX/40, DIMY/9);
            }
            if(jeu.estFichierVide(i)) {
                // On affiche "Partie vide"
                phrase = "Partie vide";
                afficheTexte(DIMX/2 - int(phrase.size()*DIMX/150), DIMY/6 + i*5*DIMY/36-DIMY/32, int(phrase.size()*DIMX/75), DIMY/16, phrase, policeMenu, couleurPoliceMenu);
            }
            else {
                phrase = "Partie ";
                phrase += std::to_string(i+1);
                // Dessin du slot
                im_captures[i].draw(renderer, 15*DIMX/40, DIMY/6 + i*5*DIMY/36-DIMY/24, DIMX/12, DIMY/12);
                // On affiche le nom de la partie
                afficheTexte(15*DIMX/40+DIMX/10, 3*DIMY/19 + i*5*DIMY/36-DIMY/40, int(phrase.size()*DIMX/100), DIMY/20, phrase, policeMenu, couleurPoliceMenu);
                // On affiche sa date
                phrase = jeu.getDateSauvegarde(i);
                afficheTexte(2*DIMX/3 - int(phrase.size()*DIMX/170), 3*DIMY/15 + i*5*DIMY/36-DIMY/80, int(phrase.size()*DIMX/170), DIMY/40, phrase, policeMenu, couleurPoliceMenu);
            }
        }
        // Affichage menu de confirmation de sauvegarde
        if(jeu.m_afficheConfirmSauvegarde) {
            // Fond
            im_confirmSauvegarde.draw(renderer, DIMX/2-DIMX/6, DIMY/2-DIMY/5, 2*DIMX/6, 2*DIMY/5);

            // Surlignage ligne selectionnee
            if(jeu.m_selectBouton)
                im_selectionPetite.draw(renderer, DIMX/2 - 2*DIMX/13, DIMY/2 + DIMY/15, DIMX/6, DIMY/17);
            else
                im_selectionPetite.draw(renderer, DIMX/2 + DIMX/100, DIMY/2 + DIMY/15, DIMX/6, DIMY/17);

            // Affichage phrases
            phrase = "Sauvegarder";
            afficheTexte(DIMX/2 - DIMX/19 - int(phrase.size()*DIMX/200), DIMY/2 + DIMY/15, int(phrase.size()*DIMX/100), DIMY/20, phrase, policeMenu, couleurPoliceMenu);
            phrase = "Annuler";
            afficheTexte(DIMX/2 + DIMX/11 - int(phrase.size()*DIMX/200), DIMY/2 + DIMY/15, int(phrase.size()*DIMX/100), DIMY/20, phrase, policeMenu, couleurPoliceMenu);
        }
    }

    // Affichage sauvegarde effectuee
    if(jeu.m_afficheSauvegardeOK) {
        // Fond
        im_sauvegardeOK.draw(renderer, DIMX/2-DIMX/6, DIMY/2-DIMY/5, 2*DIMX/6, 2*DIMY/5);
        im_selectionPetite.draw(renderer, DIMX/2 - DIMX/13, DIMY/2 + DIMY/13, DIMX/7, DIMY/18);
        phrase = "OK";
        afficheTexte(DIMX/2 - int(phrase.size()*DIMX/100), DIMY/2 + DIMX/22, int(phrase.size()*DIMX/50), DIMX/36, phrase, policeMenu, couleurPoliceMenu);

    }

    // Affichage erreur sauvegarde
    if(jeu.m_afficheSauvegardeError) {
        // Fond
        im_sauvegardeError.draw(renderer, DIMX/2-DIMX/6, DIMY/2-DIMY/5, 2*DIMX/6, 2*DIMY/5);
        im_selectionPetite.draw(renderer, DIMX/2 - DIMX/13, DIMY/2 + DIMY/13, DIMX/7, DIMY/18);
        phrase = "OK";
        afficheTexte(DIMX/2 - int(phrase.size()*DIMX/100), DIMY/2 + DIMX/22, int(phrase.size()*DIMX/50), DIMX/36, phrase, policeMenu, couleurPoliceMenu);
    }

    // Affiche echec chargement
    if(jeu.m_afficheChargementError) {
        // Fond
        im_chargementError.draw(renderer, DIMX/2-DIMX/6, DIMY/2-DIMY/5, 2*DIMX/6, 2*DIMY/5);
        im_selectionPetite.draw(renderer, DIMX/2 - DIMX/13, DIMY/2 + DIMY/13, DIMX/7, DIMY/18);
        phrase = "OK";
        afficheTexte(DIMX/2 - int(phrase.size()*DIMX/100), DIMY/2 + DIMX/22, int(phrase.size()*DIMX/50), DIMX/36, phrase, policeMenu, couleurPoliceMenu);
    }

    // Affichage confirmation quitter
    if(jeu.m_afficheQuitter) {
        // Fond
        im_quitter.draw(renderer, DIMX/2-DIMX/6, DIMY/2-DIMY/5, 2*DIMX/6, 2*DIMY/5);
        // Surlignage ligne selectionnee
        if(jeu.m_selectBouton)
            im_selectionPetite.draw(renderer, DIMX/2 - 2*DIMX/15, DIMY/2 + DIMY/11, DIMX/6, DIMY/18);
        else
            im_selectionPetite.draw(renderer, DIMX/2 - DIMX/100, DIMY/2 + DIMY/11, DIMX/6, DIMY/18);
        phrase = "Quitter";
        afficheTexte(DIMX/2 - DIMX/20 - int(phrase.size()*DIMX/200), DIMY/2 + DIMY/11, int(phrase.size()*DIMX/100), DIMY/20, phrase, policeMenu, couleurPoliceMenu);
        phrase = "Annuler";
        afficheTexte(DIMX/2 + DIMX/13 - int(phrase.size()*DIMX/200), DIMY/2 + DIMY/11, int(phrase.size()*DIMX/100), DIMY/20, phrase, policeMenu, couleurPoliceMenu);
    }

    // Transition
    if (jeu.m_affichageTransitionNoire) {
        if (m_animationTransitionNoire<3.14) {
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, (unsigned int)(sin(m_animationTransitionNoire)*255));
            SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
            SDL_RenderFillRect(renderer, NULL);
            m_animationTransitionNoire+=0.01;
            if ((float)m_animationTransitionNoire==(float)1.57)
                jeu.appliquerTransition();
        }
        else {
            jeu.m_affichageTransitionNoire=false;
            m_animationTransitionNoire=0;
            if (jeu.m_indiceScenario>4 && jeu.m_indiceScenario<11) {
                jeu.m_indiceScenario++;
                jeu.m_affichageTransitionNoire=true;
                sleep(1);
            }
        }
    }
}

void sdlJeu::tourIA() {
    SDL_Event events;
    Point p;
    p.y=0;
    while (p.y!=-1) {
        for (unsigned int i=0; i<jeu.getCombat()->getMainPersonnage(1).getNombreCartes(); i++) {
        }
        int valeur=-999999;
        p=jeu.getCombat()->meilleurCoup(p,valeur);
        if (p.y!=-1) {
            Carte* carte=jeu.getCombat()->getMainPersonnage(jeu.getCombat()->getPersonnageActuel()).getCarte(p.y);
            m_carteSelectionnee=carte->getIdCarte();
            while (!m_jouerCarte)  // On attends confirmation du joueur pour jouer la carte de l'ennemi
            {
                sdlAff();
                SDL_RenderPresent(renderer);
                while(SDL_PollEvent(&events) != 0) {
                    switch(events.type) {
                        case SDL_MOUSEBUTTONDOWN:
                            if (events.button.button==SDL_BUTTON_LEFT)
                            {
                                m_jeuCarte=jeu.getCombat()->jouerCarte(carte,p.y,p.x);
                                m_nomDerniereCarte=carte->getNomCarte();
                                m_carteSelectionnee=-1;
                                m_jouerCarte=true;
                                if (m_jeuCarte==6) // Si l'attaquant est vaincu
                                {
                                    p.y=-1;
                                    break;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                 }
            }
            m_jouerCarte=false;
        }
    }
    unsigned int victoire=jeu.getCombat()->augmenterOrdreActuel();
    if (victoire==1)
    {
        jeu.finirCombat(false);
    }
    else if (victoire==2)
    {
        jeu.finirCombat(true);
    }
}

void sdlJeu::sdlBoucle() {
    // loop flag
    bool keys[322] = {false};
    bool carte_ok = true;   // Pour ne pas repeter le maintien de la touche c quand on ouvre les cartes
    bool inventaire_ok = true;   // Pour ne pas repeter le maintien de la touche i quand on ouvre l'inventaire
    bool menu_ok = true;    // Pour ne pas repeter le maintien de la touche Esc quand on ouvre le menu
    bool change_ligne_ok = true; // Pour ne pas repeter le maintien des touches pour naviguer dans le menu
    bool valider_menu_ok = true; // Pour ne pas repeter le maintien de la touche espace
    bool interaction_ok = true;
    Uint32 time = 0;
    Uint32 tempsDernierTick=0;
    camera.w = DIMX;
    camera.h = DIMY;

    // Event handler
    SDL_Event events;

    // On lance la musique
    Mix_PlayMusic(m_Musique, -1 );

    // Boucle d'evenements
    while(!jeu.m_quitter) {

        // Musiques
        if (jeu.m_lanceMsqScenario)
        {
            Mix_PlayMusic(m_msqScenario, -1 );
            jeu.m_lanceMsqScenario=false;
        }

        // Boucle d'evenements
        while(SDL_PollEvent(&events) != 0) {

            switch(events.type) {
                // Croix pour fermer
                case SDL_QUIT:
                    jeu.m_quitter = true;
                    break;
                // Touche enfoncee
                case SDL_KEYDOWN: {
                    int key = events.key.keysym.sym;
                    if (key > 1000) key -= 1073741881 + 128;
                    keys[key] = true;
                }
                    break;
                // Touche relachee
                case SDL_KEYUP: {
                    int key = events.key.keysym.sym;
                    if (key > 1000) key = key - 1073741881 + 128;
                    keys[key] = false;
                    if(key == SDLK_c) carte_ok = true;
                    if(key == SDLK_i) inventaire_ok = true;
                    if(key == SDLK_ESCAPE) menu_ok = true;
                    if(key == SDLK_s || key == SDLK_z || key == SDLK_d || key == SDLK_q) {
                        change_ligne_ok = true;
                        m_joueurAvance = false;
                    }
                    if(key == SDLK_SPACE) valider_menu_ok = true;
                    if (key == SDLK_e) interaction_ok = true;
                    if (key == SDLK_r) inventaire_ok = true;
                }
                    break;
                // Mouvement de la souris
                case SDL_MOUSEMOTION :
                    SDL_GetMouseState(&m_posSouris.x, &m_posSouris.y);
                    if (jeu.m_afficheChargement || jeu.m_afficheSauvegarde) {
                        if (!jeu.m_afficheConfirmSauvegarde && !jeu.m_afficheChargementError && !jeu.m_afficheSauvegardeError) {
                            for(int i = 0 ; i < 6 ; i++) {
                                if (m_posSouris.x>13*DIMX/40 && m_posSouris.x<27*DIMX/40 && m_posSouris.y>DIMY/6 + i*5*DIMY/36-DIMY/18 && m_posSouris.y<DIMY/6 + i*5*DIMY/36+DIMY/18)
                                    jeu.m_ligneSelectionnee=i;
                            }
                        }
                        else if (jeu.m_afficheConfirmSauvegarde) {
                            if (m_posSouris.x>DIMX/2 - 2*DIMX/13 && m_posSouris.x<DIMX/2 - 2*DIMX/13+DIMX/6 && m_posSouris.y>DIMY/2 + DIMY/15 && m_posSouris.y<DIMY/2 + DIMY/15+DIMY/17)
                                jeu.m_selectBouton=true;
                            else if (m_posSouris.x>DIMX/2 + DIMX/100 && m_posSouris.x<DIMX/2 + DIMX/100+DIMX/6 && m_posSouris.y>DIMY/2 + DIMY/15 && m_posSouris.y<DIMY/2 + DIMY/15+DIMY/17)
                                jeu.m_selectBouton=false;
                        }
                    }
                    else if (jeu.m_afficheQuitter) {
                        if (m_posSouris.x>DIMX/2 - 2*DIMX/15 && m_posSouris.x<DIMX/2 - 2*DIMX/15+DIMX/8 && m_posSouris.y>DIMY/2 + DIMY/11 && m_posSouris.y<DIMY/2 + DIMY/11+DIMY/18)
                            jeu.m_selectBouton=true;
                        else if (m_posSouris.x>DIMX/2 - DIMX/100 && m_posSouris.x<DIMX/2 - DIMX/100+DIMX/8 && m_posSouris.y>DIMY/2 + DIMY/11 && m_posSouris.y<DIMY/2 + DIMY/11+DIMY/18)
                            jeu.m_selectBouton=false;
                    }
                    else if (jeu.m_afficheMenu) {
                        if (jeu.m_afficheInventaire) {
                            for (int i=0; i<7; i++) {
                                if (m_posSouris.x>5*DIMX/16 && m_posSouris.x<DIMX/2 && m_posSouris.y>DIMY/5 + i*DIMY/14-DIMY/30 && m_posSouris.y<DIMY/5 + i*DIMY/14+DIMY/30)
                                    jeu.m_ligneSelectionnee=i;
                            }
                        }
                        else {
                            for (int i=0; i<4; i++) {
                                if (m_posSouris.x>12*DIMX/40 && m_posSouris.x<28*DIMX/40 && m_posSouris.y>DIMY/5 + i*4*DIMY/19-DIMY/16 && m_posSouris.y<DIMY/5 + i*4*DIMY/19+DIMY/16)
                                    jeu.m_ligneSelectionnee=i;
                            }
                        }
                    }
                    else if (jeu.m_menuPrincipal) {
                        for (int i=0; i<3; i++) {
                            if (m_posSouris.x>13*DIMX/40 && m_posSouris.x<27*DIMX/40 && m_posSouris.y>DIMY/2 + i*DIMY/6-DIMY/16 && m_posSouris.y<DIMY/2 + i*DIMY/6+DIMY/16)
                                jeu.m_ligneSelectionnee=i;
                        }
                    }
                    else if (jeu.m_afficheDebutQuete) {
                        if (m_posSouris.x>DIMX/4 + DIMX/100 && m_posSouris.x<DIMX/4 + DIMX/100 + DIMX/5 && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                            jeu.m_selectBouton=true;
                        else if (m_posSouris.x>3*DIMX/4 - DIMX/100 - DIMX/5 && m_posSouris.x<3*DIMX/4 - DIMX/100 && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                            jeu.m_selectBouton=false;
                    }
                    else if (jeu.m_afficheFinQuete) {
                        if (m_posSouris.x>DIMX/2 - DIMX/10 && m_posSouris.x<DIMX/2 + DIMX/10 && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                            jeu.m_selectBouton=true;
                        else
                            jeu.m_selectBouton=false;
                    }
                    else if (jeu.m_afficheValidationCollecte) {
                        if (m_posSouris.x>DIMX/4 + DIMX/100 && m_posSouris.x<DIMX/4 + DIMX/100 + DIMX/5 && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                            jeu.m_selectBouton=true;
                        else if (m_posSouris.x>3*DIMX/4 - DIMX/100 - DIMX/5 && m_posSouris.x<3*DIMX/4 - DIMX/100 && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                            jeu.m_selectBouton=false;
                    }
                    else if (jeu.m_afficheInventairePlein) {
                        if (m_posSouris.x>DIMX/2 - DIMX/10 && m_posSouris.x<DIMX/2 + DIMX/10 && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                            jeu.m_selectBouton=true;
                        else
                            jeu.m_selectBouton=false;
                    }
                    else if (jeu.m_afficheTutoriel) {
                        if (m_posSouris.x>DIMX/2 - DIMX/10 && m_posSouris.x<DIMX/2 + DIMX/10 && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                            jeu.m_selectBouton=true;
                        else
                            jeu.m_selectBouton=false;
                    }
                    else if(jeu.getCombat()!=NULL){
                        ////AFFICHAGE BULLE

                                        for(unsigned int i=0;i<jeu.getCombat()->getNbEnnemi()+jeu.getCombat()->getNbEquipier()+1;i++){

                                            if( i==0){
                                                if(m_posSouris.x>0.5*DIMX/10 &&  m_posSouris.x<1.5*DIMX/10 && m_posSouris.y>(0.3)*DIMY/(jeu.getCombat()->getNbEquipier()+1) && m_posSouris.y<(0.3)*DIMY/(jeu.getCombat()->getNbEquipier()+1)+(float)16/9*DIMY/10){
                                                    ////Si on clique sur la position du joueur
                                                    m_afficheBulleX=1.5*DIMX/10;
                                                    m_afficheBulleY=(0.3)*DIMY/(jeu.getCombat()->getNbEquipier()+1);
                                                    m_afficheStatut=0;
                                                    break;

                                                }
                                                else {
                                                    m_afficheBulleX=-1;
                                                    m_afficheBulleY=-1;

                                                }
                                            }

                                            else if(i<jeu.getCombat()->getNbEquipier()+1){
                                                    if(jeu.getCombat()->getNbEquipier()>0 && m_posSouris.x>0.5*DIMX/10 &&  m_posSouris.x<1.5*DIMX/10 && m_posSouris.y>(0.2+i)*DIMY/(jeu.getCombat()->getNbEquipier()+1) && m_posSouris.y<(0.2+i)*DIMY/(jeu.getCombat()->getNbEquipier()+1)+(float)16/9*DIMY/10){
                                                        if(jeu.getCombat()->getVivant(i)==true){
                                                                m_afficheBulleX=2*DIMX/10;
                                                                m_afficheBulleY=(0.2+i)*DIMY/(jeu.getCombat()->getNbEquipier()+1);
                                                                m_afficheStatut=i;
                                                                break;
                                                            }


                                                    }
                                                    else {
                                                    m_afficheBulleX=-1;
                                                    m_afficheBulleY=-1;

                                                }
                                            }
                                            if(i>=jeu.getCombat()->getNbEquipier()+1){
                                            if(m_posSouris.x>7*DIMX/10 && m_posSouris.x<9*DIMX/10 && m_posSouris.y>((int)(i-1-jeu.getCombat()->getNbEquipier())*DIMY/5)&& m_posSouris.y<(((i-1-jeu.getCombat()->getNbEquipier())*DIMY/5)+(float)16/9*DIMY/10)){  // On regarde si on le cible
                                                 m_afficheBulleX=6*DIMX/10;
                                                 m_afficheBulleY=(i-1-jeu.getCombat()->getNbEquipier())*DIMY/5;
                                                 m_afficheStatut=i;
                                                 }
                                                else {
                                                    m_afficheBulleX=-1;
                                                    m_afficheBulleY=-1;

                                                }


                                            }


                                        }
                    }



                    break;
                case SDL_MOUSEWHEEL:
                    if(jeu.getCombat()!=NULL && !jeu.m_affichageTransitionNoire){
                        if ((events.wheel.y>0))
                            {
                               if(jeu.getCombat()->getTailleLogs()-5> 1+m_molette)
                                    m_molette++;
                            }
                        if ((events.wheel.y<0))
                            {
                               if(m_molette>0)
                                    m_molette--;
                            }
                    }
                    else {
                        if (jeu.m_afficheRecueilQuete && !jeu.m_afficheInventairePlein) {
                            m_posSouris.x=events.button.x;
                            if (m_posSouris.x<DIMX/3)  {// Si on est dans la zone de la liste des quetes
                                if (events.wheel.y<0) {
                                    m_moletteRecueil+=DIMY/40;
                                    if ((int)m_moletteRecueil>std::max((int)jeu.getNbQuetes()*DIMY/6-(DIMY/6+5*DIMY/6-DIMY/12-DIMY/8),0))
                                        m_moletteRecueil=std::max((int)jeu.getNbQuetes()*DIMY/6-(DIMY/6+5*DIMY/6-DIMY/12-DIMY/8),0);
                                }
                                else {
                                    if (m_moletteRecueil<(unsigned int)DIMY/40)
                                        m_moletteRecueil=0;
                                    else
                                        m_moletteRecueil-=DIMY/40;
                                }
                            }
                        }
                    }
                   break;
                // Clic
                case SDL_MOUSEBUTTONDOWN:
                    if (!jeu.m_scenario && !jeu.m_affichageTransitionNoire) {
                        m_posSouris.x=events.button.x;
                        m_posSouris.y=events.button.y;
                        // Bouton pour ouvrir le menu
                        if (!jeu.m_menuPrincipal) {
                            if (m_posSouris.x>DIMX-DIMX/16 && m_posSouris.x<DIMX && m_posSouris.y>0 && m_posSouris.y<DIMY/18) {
                                if (jeu.m_afficheMenu)
                                    jeu.actionClavier('p',0);
                                else if (!jeu.m_fenetreOuverte) {
                                    jeu.m_afficheMenu=true;
                                    jeu.m_fenetreOuverte=true;
                                }
                            }
                        }
                        if (jeu.getCombat()!=NULL) {
                            if (jeu.m_afficheCombat) {
                                if (events.button.button==SDL_BUTTON_LEFT && m_posSouris.x>DIMX/2-DIMX/20 && m_posSouris.x<DIMX/2+DIMX/20 && m_posSouris.y>17*DIMY/20 && m_posSouris.y<17*DIMY/20+DIMY/18)
                                    jeu.actionClavier('p',0);
                            }
                            else {
                                    if (jeu.getCombat()->getPersonnageActuel()==0) { // Tour du joueur
                                    m_dernierActif=jeu.getCombat()->getOrdreActuel();
                                    if(m_carteAjouer!=-1){ // si on a deja selectionne une carte
                                        int nbEnnemi=jeu.getCombat()->getNbEnnemi();
                                        int nbEquipier=jeu.getCombat()->getNbEquipier();
                                        for(int i=0;i<nbEnnemi;i++){
                                            if(jeu.getCombat()->getVivant(1+nbEquipier+i)==true){ // on verifie que cet ennemi est vivant
                                                if(m_posSouris.x>7*DIMX/10 &&  m_posSouris.x<9*DIMX/10 && m_posSouris.y>i*DIMY/5 && m_posSouris.y<((i*DIMY/5)+(float)16/9*DIMY/10)){  // On regarde si on le cible
                                                    if(events.button.button == SDL_BUTTON_LEFT){
                                                         if(m_DoitSelection){// Si on a deja selectionne la carte a jouer
                                                        if(jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer)->getTypeCarte()=="Attaque"){
                                                            m_selectionPerso=i+1+nbEquipier;  // l'indice du personnage selectionne dans le combat, ici un ennemi
                                                            m_nomDerniereCarte=jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer)->getNomCarte();
                                                            m_jeuCarte=jeu.getCombat()->jouerCarte(jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer), m_carteAjouer, m_selectionPerso);
                                                            m_carteAjouer=-1;
                                                            m_DoitSelection=false;
                                                            if (m_jeuCarte==5 && !jeu.getCombat()->ennemiRestant()) // Si l'ennemi est vaincu
                                                            {
                                                                jeu.finirCombat(true);
                                                                break;  // on quitte le for
                                                            }
                                                            else if (m_jeuCarte==6) // Si le joueur est vaincu
                                                            {
                                                                jeu.finirCombat(false);
                                                                break;  // on quitte le for
                                                            }
                                                        }
                                                    }

                                                    }

                                                }
                                            }
                                        }
                                        for(int i=0;i<nbEquipier+1;i++){ // Ici on gere l'interaction avec l'equipe (le soin)
                                            if(i==0){
                                                if(m_posSouris.x>0.5*DIMX/10 &&  m_posSouris.x<1.5*DIMX/10 && m_posSouris.y>(0.3)*DIMY/(jeu.getCombat()->getNbEquipier()+1) && m_posSouris.y<(0.3)*DIMY/(jeu.getCombat()->getNbEquipier()+1)+(float)16/9*DIMY/10){
                                                    ////Si on clique sur la position du joueur
                                                    if(jeu.getCombat()->getVivant(0)==true){
                                                        if(m_DoitSelection){// SI on a déjà selctionne la carte a jouer
                                                            if(events.button.button == SDL_BUTTON_LEFT){
                                                                if(jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer)->getTypeCarte()=="Soin"){
                                                                    m_selectionPerso=i;
                                                                    m_nomDerniereCarte=jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer)->getNomCarte();
                                                                    m_jeuCarte=jeu.getCombat()-> jouerCarte(jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer), m_carteAjouer, m_selectionPerso);
                                                                    m_carteAjouer=-1;
                                                                    m_DoitSelection=false;
                                                                }
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                            else{
                                                if(m_posSouris.x>0.5*DIMX/10 &&  m_posSouris.x<1.5*DIMX/10 && m_posSouris.y>(0.2+i)*DIMY/(jeu.getCombat()->getNbEquipier()+1) && m_posSouris.y<(0.2+i)*DIMY/(jeu.getCombat()->getNbEquipier()+1)+(float)16/9*DIMY/10){
                                                    ////Si on clique sur la position d'un allié
                                                    if(jeu.getCombat()->getVivant(i)==true){
                                                        if(m_DoitSelection){
                                                            if(jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer)->getTypeCarte()=="Soin"){
                                                                m_selectionPerso=i;
                                                                m_nomDerniereCarte=jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer)->getNomCarte();
                                                                m_jeuCarte=jeu.getCombat()-> jouerCarte(jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer), m_carteAjouer, m_selectionPerso);
                                                                m_carteAjouer=-1;
                                                                m_DoitSelection=false;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    ////Selection de la carte dans la main
                                    if (m_posSouris.x>2*DIMX/6 && m_posSouris.x<2*DIMX/6+m_tailleMainJoueur*m_largeurCarteJoueur && m_posSouris.y>9.5*DIMY/10-m_hauteurCarteJoueur && m_posSouris.y<9.5*DIMY/10){
                                        m_carteAjouer=(m_posSouris.x-2*DIMX/6)/m_largeurCarteJoueur;
                                        if (events.button.button==SDL_BUTTON_LEFT){
                                            if (jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer)->getCout()<=jeu.getCombat()->getManaPersonnage(0)){
                                                if(jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer)->getTypeCarte()=="Soin" || jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer)->getTypeCarte()=="Attaque"){
                                                    ////Si on peux jouer la carte Alors on dopit selectiooner une cible ( et si cette carte est de type soin ou attaque)
                                                    m_DoitSelection=true;
                                                }
                                                else {////Si on peux jouer la carte de type autre que les deux au dessus alors on la joue sur nous
                                                    m_dernierActif=jeu.getCombat()->getOrdreActuel();
                                                    m_nomDerniereCarte=jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer)->getNomCarte();
                                                    m_jeuCarte=jeu.getCombat()->jouerCarte(jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer),m_carteAjouer,0);
                                                    m_DoitSelection=false;
                                                    m_carteAjouer=-1;
                                                }
                                            }
                                            else  // si on ne peut pas jouer la carte par manque de mana on n'affiche pas qu'on l'a selectionnee
                                                m_carteAjouer=-1;
                                        }
                                        else if (events.button.button==SDL_BUTTON_RIGHT) {  //affichage de la carte en grand
                                            m_carteSelectionnee=jeu.getCombat()->getMainPersonnage(0).getCarte(m_carteAjouer)->getIdCarte();
                                            m_carteAjouer=-1;
                                        }
                                    }
                                    else if (events.button.button==SDL_BUTTON_LEFT && m_posSouris.x>4*DIMX/10 && m_posSouris.x<5*DIMX/10 && m_posSouris.y>0 && m_posSouris.y<DIMY/10){
                                        m_jeuCarte=-1;
                                        unsigned int victoire=jeu.getCombat()->augmenterOrdreActuel();
                                        if (victoire==1)
                                        {
                                            jeu.finirCombat(false);
                                        }
                                        else if (victoire==2)
                                        {
                                            jeu.finirCombat(true);
                                        }
                                    }
                                    else{
                                        m_carteSelectionnee=-1;
                                        m_carteAjouer=-1;
                                    }
                                }
                            }
                        }
                        // Menus
                        if (jeu.m_afficheChargement || jeu.m_afficheSauvegarde) {
                            if (jeu.m_afficheConfirmSauvegarde) {
                                if (((m_posSouris.x>DIMX/2 - 2*DIMX/13 && m_posSouris.x<DIMX/2 - 2*DIMX/13+DIMX/6) || (m_posSouris.x>DIMX/2 + DIMX/100 && m_posSouris.x<DIMX/2 + DIMX/100+DIMX/6))
                                     && m_posSouris.y>DIMY/2 + DIMY/15 && m_posSouris.y<DIMY/2 + DIMY/15+DIMY/17) {
                                    // chemin de la capture
                                    std::string chemin = "data/sauvegardes/sauvegarde";
                                    std::string extension = ".bmp";
                                    chemin = chemin + std::to_string(jeu.m_ligneSelectionnee) + extension;
                                    // on ferme les fenetres de menu pour la capture
                                    jeu.m_afficheSauvegarde = false;
                                    jeu.m_afficheMenu = false;
                                    jeu.m_afficheConfirmSauvegarde = false;
                                    sdlAff();
                                    // On fait la capture
                                    bool res = captureEcran(chemin, window, renderer);
                                    if(!res) std::cout << "sdlJeu::sdlBoucle : Probleme avec la capture d'ecran." << std::endl;
                                    // On remet les fenetres de menu
                                    jeu.m_afficheMenu = true;
                                    jeu.m_afficheSauvegarde = true;
                                    jeu.m_afficheConfirmSauvegarde = true;
                                    sdlAff();
                                    // On change l'image de sauvegarde
                                    im_captures[jeu.m_ligneSelectionnee].libereSurface();
                                    im_captures[jeu.m_ligneSelectionnee].libereTexture();
                                    im_captures[jeu.m_ligneSelectionnee].loadFromFile(chemin.c_str(), renderer);
                                    jeu.actionClavier('v',0);
                                }
                            }
                            else if (jeu.m_afficheSauvegardeError || jeu.m_afficheChargementError) {
                                if (m_posSouris.x>DIMX/2 - DIMX/13 && m_posSouris.x<DIMX/2 - DIMX/13+DIMX/7 && m_posSouris.y>DIMY/2 + DIMY/13 && m_posSouris.y<DIMY/2 + DIMY/13+DIMY/18)
                                    jeu.actionClavier('v',0);
                            }
                            else {
                                for(int i = 0 ; i < 6 ; i++) {
                                    if (m_posSouris.x>13*DIMX/40 && m_posSouris.x<27*DIMX/40 && m_posSouris.y>DIMY/6 + i*5*DIMY/36-DIMY/18 && m_posSouris.y<DIMY/6 + i*5*DIMY/36+DIMY/18)
                                        jeu.actionClavier('v',0);
                                }
                            }
                        }
                        else if (jeu.m_afficheQuitter) {
                            if (((m_posSouris.x>DIMX/2 - 2*DIMX/15 && m_posSouris.x<DIMX/2 - 2*DIMX/15+DIMX/8) || (m_posSouris.x>DIMX/2 - DIMX/100 && m_posSouris.x<DIMX/2 - DIMX/100+DIMX/8))
                                 && m_posSouris.y>DIMY/2 + DIMY/11 && m_posSouris.y<DIMY/2 + DIMY/11+DIMY/18)
                                jeu.actionClavier('v',0);
                        }
                        else if (jeu.m_afficheMenu) {
                            if (jeu.m_afficheInventaire) {
                                if (m_posSouris.x>22*DIMX/40 && m_posSouris.x<26*DIMX/40 && m_posSouris.y>DIMY/7 + 11*DIMY/30-DIMY/36 && m_posSouris.y<DIMY/7 + 11*DIMY/30+DIMY/36)
                                    jeu.actionClavier('v',0);
                                else if (m_posSouris.x>24*DIMX/40-14*DIMX/280 && m_posSouris.x<24*DIMX/40-4*DIMX/280 && m_posSouris.y>DIMY/7 + 8*DIMY/30 && m_posSouris.y<DIMY/7 + 10*DIMY/30)
                                    jeu.actionClavier('g', 0);
                                else if (m_posSouris.x>24*DIMX/40+4*DIMX/280 && m_posSouris.x<24*DIMX/40+14*DIMX/280 && m_posSouris.y>DIMY/7 + 8*DIMY/30 && m_posSouris.y<DIMY/7 + 10*DIMY/30)
                                    jeu.actionClavier('d', 0);
                            }
                            else {
                                for (int i=0; i<4; i++) {
                                    if (m_posSouris.x>12*DIMX/40 && m_posSouris.x<28*DIMX/40 && m_posSouris.y>DIMY/5 + i*4*DIMY/19-DIMY/16 && m_posSouris.y<DIMY/5 + i*4*DIMY/19+DIMY/16)
                                        jeu.actionClavier('v',0);
                                }
                            }
                        }
                        else if (jeu.m_menuPrincipal) {
                            for (int i=0; i<3; i++) {
                                if (m_posSouris.x>13*DIMX/40 && m_posSouris.x<27*DIMX/40 && m_posSouris.y>DIMY/2 + i*DIMY/6-DIMY/16 && m_posSouris.y<DIMY/2 + i*DIMY/6+DIMY/16)
                                    jeu.actionClavier('v',0);
                            }
                        }
                        else if (jeu.m_afficheSauvegardeOK) {
                            if (m_posSouris.x>DIMX/2 - DIMX/13 && m_posSouris.x<DIMX/2 - DIMX/13+DIMX/7 && m_posSouris.y>DIMY/2 + DIMY/13 && m_posSouris.y<DIMY/2 + DIMY/13+DIMY/18)
                                jeu.actionClavier('v',0);
                        }
                        else if (jeu.m_afficheVictoire) {
                            if (m_posSouris.x>DIMX/2+DIMX/14 && m_posSouris.x<DIMX/2+DIMX/12+DIMX/15 && m_posSouris.y>9*DIMY/32 && m_posSouris.y<11*DIMY/32)
                                jeu.actionClavier('p',0);
                        }
                        else if (jeu.m_afficheMonteeNiveau) {
                            if (m_posSouris.x>DIMX/2 - DIMX/20 && m_posSouris.x<DIMX/2 + DIMX/20 && m_posSouris.y>7*DIMY/8-DIMY/32 && m_posSouris.y<7*DIMY/8+DIMY/32)
                                jeu.actionClavier('p',0);
                        }
                        else if (jeu.m_afficheDebutQuete) {
                            if (((m_posSouris.x>DIMX/4 + DIMX/100 && m_posSouris.x<DIMX/4 + DIMX/100 + DIMX/5) || (m_posSouris.x>3*DIMX/4 - DIMX/100 - DIMX/5 && m_posSouris.x<3*DIMX/4 - DIMX/100))
                                && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                                jeu.actionClavier('v',0);
                        }
                        else if (jeu.m_afficheFinQuete) {
                            if (m_posSouris.x>DIMX/2 - DIMX/10 && m_posSouris.x<DIMX/2 + DIMX/10 && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                                jeu.actionClavier('p',0);
                        }
                        else if (jeu.m_afficheValidationCollecte) {
                            if (((m_posSouris.x>DIMX/4 + DIMX/100 && m_posSouris.x<DIMX/4 + DIMX/100 + DIMX/5) || (m_posSouris.x>3*DIMX/4 - DIMX/100 - DIMX/5 && m_posSouris.x<3*DIMX/4 - DIMX/100))
                                && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                                jeu.actionClavier('v',0);
                        }
                        else if (jeu.m_afficheTutoriel) {
                            if (m_posSouris.x>DIMX/2 - DIMX/10 && m_posSouris.x<DIMX/2 + DIMX/10 && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                                jeu.actionClavier('v',0);
                        }

                        //Cartes
                        // Si les cartes sont affichees clic droit pour zoom, clic gauche pour ajouter ou retirer au deck
                        else if (jeu.m_afficheCartes && (events.button.button==SDL_BUTTON_RIGHT || events.button.button==SDL_BUTTON_LEFT))
                        {
                            unsigned int ligne, colonne;

                            // Si on clique dans la zone du deck
                            unsigned int maxl;
                            if (m_nbLignesDeck==1)
                                maxl=jeu.getJoueurConst()->getDeck()->getNombreCartes()*m_largeurCarteDeck;
                            else
                                maxl=10*m_largeurCarteDeck;
                            if (m_posSouris.x>DIMX/10 && (unsigned int)m_posSouris.x<(unsigned int)DIMX/10+maxl && m_posSouris.y>DIMY/10 && (unsigned int)m_posSouris.y<(unsigned int)DIMY/10+m_nbLignesDeck*m_hauteurCarteDeck)
                            {
                                ligne=(m_posSouris.y-DIMY/10)/m_hauteurCarteDeck;
                                colonne=(m_posSouris.x-DIMX/10)/m_largeurCarteDeck;
                                if (events.button.button==SDL_BUTTON_RIGHT)
                                {
                                        m_zoomCarte=ligne*10+colonne;
                                }
                                else if (events.button.button==SDL_BUTTON_LEFT)
                                {

                                    if (m_zoomCarte!=-1)
                                        m_zoomCarte=-1;
                                    else if (jeu.getJoueurConst()->getDeck()->getNombreCartes() > jeu.getJoueur()->getDeck()->getNombreCartesMin())
                                    {
                                        if (ligne==m_nbLignesDeck-1 && colonne>jeu.getJoueurConst()->getDeck()->getNombreCartes() && jeu.getJoueurConst()->getDeck()->getNombreCartes()%10!=0)
                                        {
                                            m_carteSelectionnee=-1;
                                        }
                                        else
                                        {
                                            m_carteSelectionnee=ligne*10+colonne;
                                            jeu.getJoueur()->getCartesReserve()[jeu.getJoueur()->getDeck()->getCarte(m_carteSelectionnee)->getIdCarte()]++;
                                            jeu.getJoueur()->getDeck()->retirerCarte(m_carteSelectionnee);
                                        }
                                    }
                                }
                            }
                            // Sinon si on clique dans la zone de la reserve
                            else if (m_posSouris.x>5.5*DIMX/10 && m_posSouris.x<5.5*DIMX/10+10*(int)m_largeurCarteReserve && m_posSouris.y>DIMY/10 && m_posSouris.y<DIMY/10+(int)(m_nbLignesReserve*m_hauteurCarteReserve))
                            {
                                ligne=(m_posSouris.y-DIMY/10)/m_hauteurCarteReserve;
                                colonne=(m_posSouris.x-5.5*DIMX/10)/m_largeurCarteReserve;
                                if (events.button.button==SDL_BUTTON_RIGHT)
                                {
                                    m_zoomCarte=ligne*10+colonne+20;
                                }
                                else if (events.button.button==SDL_BUTTON_LEFT)
                                {
                                    if (m_zoomCarte!=-1)
                                        m_zoomCarte=-1;
                                    else if (jeu.getJoueurConst()->getDeck()->getNombreCartes() < jeu.getJoueur()->getDeck()->getNombreCartesMax())
                                    {
                                        if (ligne==m_nbLignesReserve-1 && colonne>5)
                                            m_carteSelectionnee=-1;
                                        else
                                        {
                                            m_carteSelectionnee=ligne*10+colonne;
                                            if (jeu.getJoueur()->getCartesReserve()[m_carteSelectionnee]>0)
                                            {
                                                jeu.getJoueur()->getDeck()->ajouterCarte(jeu.getTableauCartes().getCarte(m_carteSelectionnee));
                                                jeu.getJoueur()->getCartesReserve()[m_carteSelectionnee]--;
                                            }
                                            else
                                                m_carteSelectionnee=-1;
                                        }
                                    }
                                }
                            }
                            else
                                m_zoomCarte=-1;
                            m_nbLignesDeck=(jeu.getJoueurConst()->getDeck()->getNombreCartes()-1)/10+1;
                            if (m_nbLignesDeck==1)
                            {
                                m_largeurCarteDeck=std::min((float)3.5/10*DIMX/jeu.getJoueurConst()->getDeck()->getNombreCartes(),(float)400);
                            }
                            else
                            {
                                m_largeurCarteDeck=std::min((float)3.5/10*DIMX/10,(float)400);
                            }
                            m_hauteurCarteDeck=(float)16/10*m_largeurCarteDeck;
                        }
                        else if (jeu.m_afficheInventaire) { //tpco tpinv
                            m_zoomObjet = -1;
                            if (events.button.button==SDL_BUTTON_RIGHT) {
                                for (unsigned int j=0; j<5; j++) {
                                    for (unsigned int i=0; i<6; i++) {
                                        if (m_posSouris.x> (DIMX/100) + i*DIMX * 0.04 &&
                                            m_posSouris.x< (DIMX/100) + i*DIMX * 0.04 + DIMX * 0.04 &&
                                            m_posSouris.y>5.46 * DIMY/10+j*DIMY * 0.072222 &&
                                            m_posSouris.y<5.46 * DIMY/10+j*DIMY * 0.072222+DIMY * 0.0741)
                                            m_zoomObjet=i+j*6;
                                    }
                                }
                                if (jeu.m_afficheInventaireCoffre) {
                                    for (unsigned int j=0; j<5; j++) {
                                        for (unsigned int i=0; i<6; i++) {
                                            if (m_posSouris.x>(DIMX/100) + DIMX - DIMX * 0.26 + i*DIMX * 0.04 &&
                                                m_posSouris.x<(DIMX/100) + DIMX - DIMX * 0.26 + i*DIMX * 0.04+DIMX * 0.04 &&
                                                m_posSouris.y>5.46 * DIMY/10+j*DIMY * 0.072222 &&
                                                m_posSouris.y<5.46 * DIMY/10+j*DIMY * 0.072222+DIMY * 0.0741)
                                                m_zoomObjet=30+i+j*6;
                                        }
                                    }
                                }
                                //zoom sur les cases d'eq, pas encore op
                                if (m_posSouris.x> DIMX/8 - DIMX * 0.01953125 &&
                                    m_posSouris.x< DIMX/8 - DIMX * 0.01953125 + DIMX*0.0390625 &&
                                    m_posSouris.y>DIMY * 0.13 &&
                                    m_posSouris.y<DIMY * 0.13+DIMY*0.0694444)
                                            m_zoomObjet=3;
                            }
                            else if (events.button.button==SDL_BUTTON_LEFT) {
                                if (jeu.m_afficheInventaireCoffre || jeu.m_afficheInventaireTombe) {
                                    for (unsigned int j=0; j<5; j++) {
                                        for (unsigned int i=0; i<6; i++) {
                                            if (m_posSouris.x>(DIMX/100) + DIMX - DIMX * 0.26 + i*DIMX * 0.04 && m_posSouris.x<(DIMX/100) + DIMX - DIMX * 0.26 + i*DIMX * 0.04+DIMX * 0.0417
                                                && m_posSouris.y>5.46 * DIMY/10+j*DIMY * 0.072222 && m_posSouris.y<5.46 * DIMY/10+j*DIMY * 0.072222+DIMY * 0.0741) {
                                                if (jeu.m_afficheInventaireCoffre)
                                                    jeu.recupererObjet(i+j*6,0);
                                                else if (jeu.m_afficheInventaireTombe)
                                                    jeu.recupererObjet(i+j*6,1);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (jeu.m_afficheRecueilQuete && events.button.button==SDL_BUTTON_LEFT) {
                            if (jeu.m_afficheInventairePlein) {
                                if (m_posSouris.x>DIMX/2 - DIMX/10 && m_posSouris.x<DIMX/2 + DIMX/10 && m_posSouris.y>2*DIMY/3 && m_posSouris.y<2*DIMY/3+DIMY/20)
                                    jeu.actionClavier('p',0);
                            }
                            else {
                                for (unsigned int i=0; i<jeu.getNbQuetes(); i++) {
                                    if (m_posSouris.x>DIMX/20 && m_posSouris.x<DIMX/20 + DIMX/4 && m_posSouris.y>(int)(DIMY/6+i*DIMY/6-m_moletteRecueil) && m_posSouris.y<(int)(DIMY/6+i*DIMY/6-m_moletteRecueil + DIMY/8)) {
                                        m_queteRecueil=i;
                                        if (jeu.getQuete(i)->getEtat()=='i')
                                            m_queteRecueil=-1;
                                    }

                                }
                                if (m_posSouris.x>2*DIMX/3-DIMX/12 && m_posSouris.x<2*DIMX/3+DIMX/12 && m_posSouris.y>7*DIMY/8-DIMY/20 && m_posSouris.y<7*DIMY/8-DIMY/20 + DIMY/12)
                                    jeu.actionClavier('v', m_queteRecueil);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        if (jeu.getCombat()!=NULL && !jeu.m_afficheCombat && jeu.getCombat()->getPersonnageActuel()!=0 && !jeu.m_afficheTutoriel) {
            m_dernierActif=jeu.getCombat()->getOrdreActuel();
            tourIA();
        }

        // Deplacement du personnage
        // Les deplacements ne representent pas le nombre de pixels de deplacement a l'affichage, mais ceux dans le jeu, considere avec une taille de case de m_taileCaseOriginale*m_tailleCaseOriginale
        if(jeu.getCombat()==NULL && !jeu.m_scenario && !jeu.m_affichageTransitionNoire && (tempsDernierTick = SDL_GetTicks() - time) > 15) {
            time = SDL_GetTicks();
            if(keys[SDLK_z]) {
                if((jeu.m_menuPrincipal || jeu.m_afficheMenu) && change_ligne_ok) { // On verifie si des menus sont ouverts pour prendre ou non en compte le relachement de la touche
                    change_ligne_ok = false;
                    jeu.actionClavier('h', 0);
                }
                else if(!jeu.m_fenetreOuverte && !jeu.m_afficheDialogue) {
                    m_faceJoueur = 'b';     // Joueur de dos
                    m_joueurAvance = true;
                    unsigned int deplacement=jeu.m_tailleCase*2.5/1000*tempsDernierTick;
                    if (keys[SDLK_q] || keys[SDLK_d]) {
                        jeu.m_deplacementDiagonale=true;
                        jeu.actionClavier('h', sqrt(2*pow(deplacement,2))/2);
                    }
                    else
                        jeu.actionClavier('h', deplacement);
                };
            }
            if(keys[SDLK_s]) {
                if((jeu.m_menuPrincipal || jeu.m_afficheMenu) && change_ligne_ok) {
                    change_ligne_ok = false;
                    jeu.actionClavier('b', 0);
                }
                else if(!jeu.m_fenetreOuverte && !jeu.m_afficheDialogue) {
                    m_faceJoueur = 'f';     // Joueur de face
                    m_joueurAvance = true;
                    unsigned int deplacement=jeu.m_tailleCase*2.5/1000*tempsDernierTick;
                    if (keys[SDLK_q] || keys[SDLK_d]) {
                        jeu.m_deplacementDiagonale=true;
                        jeu.actionClavier('b', sqrt(2*pow(deplacement,2))/2);
                    }
                    else
                        jeu.actionClavier('b', deplacement);
                }
            }
            if(keys[SDLK_q]) {
                if((jeu.m_menuPrincipal || jeu.m_afficheMenu) && change_ligne_ok) {
                    change_ligne_ok = false;
                    jeu.actionClavier('g', 0);
                }
                else if(!jeu.m_fenetreOuverte && !jeu.m_afficheDialogue) {
                    m_faceJoueur = 'g';     // Joueur vers la gauche
                    m_joueurAvance = true;
                    unsigned int deplacement=jeu.m_tailleCase*2.5/1000*tempsDernierTick;
                    if (keys[SDLK_z] || keys[SDLK_s])
                            jeu.actionClavier('g', sqrt(2*pow(deplacement,2))/2);
                    else
                        jeu.actionClavier('g', deplacement);
                }
            }
            if(keys[SDLK_d]) {
                if((jeu.m_menuPrincipal || jeu.m_afficheMenu) && change_ligne_ok) {
                    change_ligne_ok = false;
                    jeu.actionClavier('d', 0);
                }
                else if(!jeu.m_fenetreOuverte && !jeu.m_afficheDialogue) {
                    m_faceJoueur = 'd';     // Joueur vers la droite
                    m_joueurAvance = true;
                    unsigned int deplacement=jeu.m_tailleCase*2.5/1000*tempsDernierTick;
                    if (keys[SDLK_z] || keys[SDLK_s])
                            jeu.actionClavier('d', sqrt(2*pow(deplacement,2))/2);
                    else
                        jeu.actionClavier('d', deplacement);
                }
            }
            m_frame ++;
            if(m_frame > 256) m_frame = 0;

            // Gestion des ennemis
            if (jeu.getTerrain()->getIdRegion()!=0)
                jeu.gestionEnnemis(jeu.m_tailleCase*2.5/1000*tempsDernierTick);
        }

        // Ouverture et fermeture de l'affichage des cartes
        if(keys[SDLK_c] && carte_ok && !jeu.m_scenario && !jeu.m_affichageTransitionNoire) {
            m_nbLignesDeck=(jeu.getJoueurConst()->getDeck()->getNombreCartes()-1)/10+1;
            if (m_nbLignesDeck==1) m_largeurCarteDeck=std::min((float)3.5/10*DIMX/jeu.getJoueurConst()->getDeck()->getNombreCartes(),(float)400);
            else m_largeurCarteDeck=std::min((float)3.5/10*DIMX/10,(float)400);
            m_hauteurCarteDeck=(float)16/10*m_largeurCarteDeck;
            carte_ok = false;
            m_zoomCarte = -1;
            m_carteSelectionnee=-1;
            jeu.actionClavier('c', 0);
        }

        // Ouverture et fermeture de l'inventaire tpinv
        if(keys[SDLK_i] && inventaire_ok && !jeu.m_scenario && !jeu.m_affichageTransitionNoire) {
            inventaire_ok = false;
            jeu.actionClavier('i', 0);
        }

        // Ouverture et fermeture du recueil de quete
        if(keys[SDLK_r] && inventaire_ok && !jeu.m_scenario && !jeu.m_affichageTransitionNoire) {
            inventaire_ok = false;
            jeu.actionClavier('r', 0);
            if (jeu.m_indiceQueteSuivie!=-1)
                m_queteRecueil=jeu.m_indiceQueteSuivie;
            else
                m_queteRecueil=-1;
        }

        // Echap : ouverture et fermeture des menus
        if(keys[SDLK_ESCAPE] && menu_ok && !jeu.m_affichageTransitionNoire) {
            menu_ok = false;
            if (jeu.m_scenarioNarrateur && m_afficherEchapScenario) {
                Mix_PlayMusic(m_Musique, -1 );
                jeu.actionClavier('p',0);
            }
            if (jeu.m_scenarioNarrateur)
                m_afficherEchapScenario=true;
            else
                jeu.actionClavier('p', 0);
        }

        // Espace : valider dans les menus
        if(keys[SDLK_SPACE] && valider_menu_ok && !jeu.m_scenario && !jeu.m_affichageTransitionNoire) {
            // Si on etait en train de confirmer une sauvegarde, on fait la capture d'ecran
            if(jeu.m_afficheSauvegarde && jeu.m_afficheConfirmSauvegarde && jeu.m_selectBouton) {
                // chemin de la capture
                std::string chemin = "data/sauvegardes/sauvegarde";
                std::string extension = ".bmp";
                chemin = chemin + std::to_string(jeu.m_ligneSelectionnee) + extension;
                // on ferme les fenetres de menu pour la capture
                jeu.m_afficheSauvegarde = false;
                jeu.m_afficheMenu = false;
                jeu.m_afficheConfirmSauvegarde = false;
                sdlAff();
                // On fait la capture
                bool res = captureEcran(chemin, window, renderer);
                if(!res) std::cout << "sdlJeu::sdlBoucle : Probleme avec la capture d'ecran." << std::endl;
                // On remet les fenetres de menu
                jeu.m_afficheMenu = true;
                jeu.m_afficheSauvegarde = true;
                jeu.m_afficheConfirmSauvegarde = true;
                sdlAff();
                // On change l'image de sauvegarde
                im_captures[jeu.m_ligneSelectionnee].libereSurface();
                im_captures[jeu.m_ligneSelectionnee].libereTexture();
                im_captures[jeu.m_ligneSelectionnee].loadFromFile(chemin.c_str(), renderer);
            }
            valider_menu_ok = false;
            jeu.actionClavier('v', 0);
        }

        // E : touche d'interaction
        if (keys[SDLK_e] && interaction_ok && !jeu.m_affichageTransitionNoire) {
            // Dialogues
            if (jeu.m_afficheDialogue) {
                if (jeu.m_scenarioNarrateur) {
                    // On ne veut pas que le dialogue boucle, car lors de la transition on se retrouverait avec de nouveau la premiere phrase affichee
                    // On n'utilise donc pas directement augmenterIndice() sans verification
                    if (jeu.m_dialogueNarrateur->getIndicePhrase()<jeu.m_dialogueNarrateur->getIndiceMax()) {
                        jeu.m_dialogueNarrateur->augmenterIndice();
                    }
                    else {
                        Mix_PlayMusic(m_Musique, -1 );
                        jeu.actionClavier('p',0);
                    }
                }
                else if (interaction_ok) {
                    // On recupere le dialogue en cours et on passe a la phrase suivante
                    if(m_PNJ->getDialogue(m_PNJ->getIndiceDialogue())->augmenterIndice()) { // Le dialogue est fini
                        m_PNJ->augmenterIndiceDialogue();
                        jeu.m_afficheDialogue = false;
                        if (jeu.m_afficheDebutQuete || jeu.m_afficheFinQuete || jeu.m_afficheValidationCollecte)
                            jeu.m_fenetreOuverte=true;
                        m_PNJ->setParle();
                        jeu.finDialogueScenario();
                    }
                }
            }
            // Autres interactions
            else if (jeu.m_interactionPossible) {
                if(m_zoomObjet==-1)
                    jeu.actionClavier('e',0);
                else
                    jeu.actionClavier('e',1);
            }
            interaction_ok = false;
        }

        if (!jeu.m_scenario) {
            // Camera
            unsigned int collision[4];
            jeu.getJoueur()->getCollisions(collision);
            camera.x = (jeu.getJoueur()->getPosition().x+collision[0])*m_tailleCase/jeu.m_tailleCase-(DIMX/2);   // On la centre sur le joueur
            camera.y = (jeu.getJoueur()->getPosition().y+collision[1])*m_tailleCase/jeu.m_tailleCase-(DIMY/2);
        }

        // Pour eviter que la camera ne deborde
        if(camera.x < 0) camera.x = 0;
        if(camera.x > std::max((int)(jeu.getTerrain()->getTailleX()*m_tailleCase) - camera.w - 1, 0)) camera.x =  std::max((int)(jeu.getTerrain()->getTailleX()*m_tailleCase) - camera.w - 1, 0);
        if(camera.y < 0) camera.y = 0;
        if(camera.y > std::max((int)(jeu.getTerrain()->getTailleY()*m_tailleCase) - camera.h - 1, 0)) camera.y = std::max((int)(jeu.getTerrain()->getTailleY()*m_tailleCase) - camera.h - 1, 0);

        // Affichage
        sdlAff();
        SDL_RenderPresent(renderer);
    }
}

