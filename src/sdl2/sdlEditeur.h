#ifndef SDLEDITEUR_H_INCLUDED
#define SDLEDITEUR_H_INCLUDED

#include <stdlib.h>
#include <vector>
#include "SDL_ttf.h"
#include "Terrain.h"
#include "Coffre.h"
#include "Personnage.h"
#include "PNJ.h"
#include "Image.h"
#include "PointApparition.h"

class sdlEditeur {
public:
    /**@brief Constructeur de sdlJeu */
    sdlEditeur();
    /**@brief Destructeur de sdlJeu */
    ~sdlEditeur();
    /**@brief Permet d'affiche du texte a l'ecran @param [in] int x @param [in] int y @param [in] int w @param [in] int h @param string texte @param TTF_Font* police @param SDL_Color couleur @return non*/
    void afficheTexte(int x, int y, int w, int h, std::string texte, TTF_Font* police, SDL_Color couleur, unsigned int transparence=255);
    /**@brief Gere l'affichage sur l'ecran  @param non @return non*/
    void sdlAff();
    /**@brief Gere le fonctionnement du jeu en temps reel (interactions utilisateur) @param non @return non*/
    void sdlBoucle();;


private:
    unsigned int tailleSansAccents(std::string chaine) const;
    void saisieTexte(int key);
    unsigned int indiceSansAccents(std::string chaine, float j) const;
    void setSorties();
    void setSortie(Point sortie, std::vector<Point> copieSorties, std::vector<Point> copieDestinations);
    void sauvegarderNiveau();
    bool chargerNiveau(std::string nom);
    void sauvegarderMonde();
    bool chargerMonde(std::string nom);

    SDL_Window * window;
    SDL_Renderer * renderer;
    SDL_Rect camera;
    SDL_Rect cameraMonde;
    int DIMX;
    int DIMY;

    // Polices
    TTF_Font* policeTexte;
    SDL_Color couleurPolice;
    SDL_Color couleurPoliceOr;
    TTF_Font* policeMenu;
    SDL_Color couleurPoliceMenu;

    // Images
    Image im_texte;    // Pour afficher du texte
    std::string phrase; // Phrase texte a afficher

    // Terrain
    unsigned int tailleX;
    unsigned int tailleY;
    int tailleCase;
    unsigned int* m_premiereCoucheTerrain;
    unsigned int* m_deuxiemeCoucheTerrain;
        /*
            # vide
            a arbre
            c coffre
            l debut maison
            m maison
            n fin maison
            o obstacle
            p pnj
            r rocher
            s point d'apparition
        */
    Image im_quadrillage;
    std::vector<bool> m_marchablePremiereCouche;
    std::vector<bool> m_marchableDeuxiemeCouche;
    unsigned int m_tailleDecors;
    std::vector<unsigned int> m_interactionsAction;
    std::vector<unsigned int*> m_collisionsDecors;
    std::vector<unsigned int*> m_positionsPortes;    // valeur du terrain indiquant le type de maison et positions (x1,x2) des portes par rapport a la maison
    std::vector<unsigned int*> m_coordonneesPortes;   // coordonnees des portes dans le niveau (x1,y,x2)
    // Ennemis
    std::vector<PointApparition*> m_pointApparition;
    PointApparition* m_pointApparitionSelectionne;
    std::vector<Image*> m_listeEnnemis;
    unsigned int m_ennemiSelectionne;
    std::vector<Espece*> m_tableauEspeces;

    // PNJ
    std::vector<PNJ*> m_pnj;
    PNJ* m_pnjSelectionne;

    // Coffres
    std::vector<Objet*> m_tableauObjets;
    std::vector<Image*> m_listeObjets;
    std::vector<Coffre*> m_coffre;
    unsigned int m_objetSelectionne;
    Coffre* m_coffreSelectionne;

    // Niveau
    std::vector<Point> m_niveauCoordonnees;
    std::vector<std::string> m_niveauNom;
    std::vector<Point> m_niveauTaille;
    unsigned int m_niveauSelectionne;
    bool m_niveauSauvegarde;
    std::vector<std::vector<Point>> m_niveauSorties;     // point : x indice debut sortie, y indice fin sortie
    bool m_afficheSorties;
    std::vector<std::vector<Point>> m_niveauDestinationsSorties;     // point : x indice niveau destination, y indice sortie destination
    bool m_niveauClic;
    Image im_rouge;
    Image im_bleu;
    unsigned int m_sortieSelectionnee;
    bool m_lienSortie;
    std::vector<unsigned int> m_niveauVersion;    // indice du niveau dont c'est une version (lui-meme si ce n'est pas une version d'un autre niveau)
    bool m_lienNiveau;

    // Interface
    Image im_fondEdition;
    Image im_barreOutil;
    Image im_boutonDescendre;
    Image im_boutonMonter;
    Image im_boutonGauche;
    Image im_boutonDroite;
    Image im_plus;
    Image im_moins;
    Image im_bouton;
    Image im_boutonRetour;
    Image im_barreSaisie;
    Image im_curseurSaisie;
    unsigned int m_curseurSaisie;
    double m_animationSaisie;
    char* m_nomSauvegarde;
    unsigned int m_tailleNomSauvegarde;
    std::string m_nomNiveauSauvegarde;
    unsigned int m_tailleNomNiveauSauvegarde;
    std::string m_nomPnj;
    unsigned int m_tailleNomPnj;
    std::string m_phrasePnj;
    unsigned int m_taillePhrasePnj;
    unsigned int m_ennemiEspece;
    unsigned int m_ennemiPageEspece;
    unsigned int m_ennemiPv;
    unsigned int m_ennemiMana;
    unsigned int m_ennemiAttaque;
    unsigned int m_ennemiDefense;
    unsigned int m_ennemiExperience;
    unsigned int m_apparitionTemps;
    bool m_garde;
    Image im_coche;
    unsigned int m_coffreObjet;
    unsigned int m_coffrePageObjet;
    unsigned int m_coffreNbExemplaireMateriau;

    Image im_menu;
    Image im_quitter;
    Image im_selectionPetite;
    Image im_selectionGrande;
    Image im_selectionCarre;
    Image im_outilDeplacement;
    Image im_outilSelection;
    Image im_outilGomme;
    Image im_outilParametre;
    Image im_outilDoubleFleche;
    Image im_outilVersion;
    unsigned int m_outilSelectionne;
        /*
            0 deplacement
            1 selection
            2 gomme
            3 parametre
            ...
        */
    unsigned int m_page;
    unsigned int m_editionSelectionnee;
    std::vector<Image*>* m_listeEdition;
    std::vector<Image*> m_listeEdition1;
    std::vector<Image*> m_listeEdition2;
    std::vector<Image*> m_listeEdition3;
    unsigned int m_ongletEdition;

    Point m_posSouris;
    bool m_afficheMenuMonde;
    bool m_afficheMenu;
    bool m_saisieTailleX;
    bool m_saisieTailleY;
    bool m_afficheQuitter;
    bool m_afficheMonde;
    bool m_afficheSauvegarde;
    bool m_afficheChargement;
    bool m_erreurChargement;
    bool m_saisieSauvegarde;
    bool m_saisieNiveauSauvegarde;
    bool m_afficheParametrePnj;
    bool m_saisieNomPnj;
    bool m_saisiePhrasePnj;
    bool m_afficheParametreEnnemi;
    bool m_saisiePvEnnemi;
    bool m_saisieManaEnnemi;
    bool m_saisieAttaqueEnnemi;
    bool m_saisieDefenseEnnemi;
    bool m_saisieExperienceEnnemi;
    bool m_saisieTempsApparition;
    bool m_afficheParametreCoffre;
    bool m_saisieNbExemplaires;
    bool m_quitter;
    bool m_clic;
    bool m_majuscule;
    bool m_altGr;
    bool m_alt;
    std::string m_combinaisonAlt;
    bool m_accentGrave;
    bool m_accentCirconflexe;
    bool m_selectBouton;
    unsigned int m_ligneSelectionnee;
};

#endif
