#include "sdlEditeur.h"
#include <iostream>
#include <cassert>
#include <fstream>
#include <string.h>
#include <math.h>

sdlEditeur::sdlEditeur () {
    DIMX=1600;
    DIMY=900;
    // Initialisation SDL
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        std::cout<<"sdlEditeur::sdlEditeur : SDL ne peut pas s'initialiser - "<<SDL_GetError()<<std::endl;
        exit(1);
    }
    // Initialisation TTF
    if(TTF_Init() == -1) {
        std::cout << "sdlEditeur::sdlEditeur : Erreur d'initialisation de TTF_Init - " << TTF_GetError() << std::endl;
        exit(1);
    }

    // Creation de la fenetre
    window = SDL_CreateWindow   ("Editeur MonsCard", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                DIMX, DIMY, SDL_WINDOW_FULLSCREEN_DESKTOP);
    if(window == NULL) {
        std::cout<<"sdlEditeur::sdlEditeur : La fenetre ne peut pas se creer - "<<SDL_GetError()<<std::endl;
        exit(1);
    }
    SDL_DisplayMode dm;
    if (SDL_GetDesktopDisplayMode(0, &dm) != 0) {
        SDL_Log("SDL_GetDesktopDisplayMode failed: %s", SDL_GetError());
        exit(1);
    }
    DIMX=dm.w;
    DIMY=dm.h;

    // Renderer
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    // Chargement police
    policeTexte = NULL;
    policeTexte = TTF_OpenFont("data/polices/arial.ttf", 100);
    if (policeTexte == NULL)
    {
        policeTexte = TTF_OpenFont("../data/polices/arial.ttf", 100);
        if(policeTexte == NULL) {
            policeTexte = TTF_OpenFont("../../data/polices/arial.ttf", 100);
            if (policeTexte == NULL) {
                std::cout << "sdlEditeur::sdlEditeur : Erreur dans le chargement de la police arial.ttf - " << TTF_GetError() << std::endl;
                SDL_Quit();
                assert(policeTexte != NULL);
            }
        }
    }
    couleurPolice.r = 255;
    couleurPolice.g = 255;
    couleurPolice.b = 255;
    couleurPolice.a = 255;
    couleurPoliceOr.r = 0;
    couleurPoliceOr.g = 0;
    couleurPoliceOr.b = 0;
    couleurPoliceOr.a = 255;

    // Chargement police menu
    policeMenu = NULL;
    policeMenu = TTF_OpenFont("data/polices/goudosb.ttf", 100);
    if (policeMenu == NULL)
    {
        policeMenu = TTF_OpenFont("../data/polices/goudosb.ttf", 100);
        if(policeMenu == NULL) {
            policeMenu = TTF_OpenFont("../../data/polices/goudosb.ttf", 100);
            if(policeMenu == NULL) {
                std::cout << "sdlEditeur::sdlEditeur : Erreur dans le chargement de la police goudosb.ttf - " << TTF_GetError() << std::endl;
                SDL_Quit();
                assert(policeMenu != NULL);
            }
        }
    }
    couleurPoliceMenu.r = 132;
    couleurPoliceMenu.g = 86;
    couleurPoliceMenu.b = 51;
    couleurPoliceMenu.a = 255;

    // Carte
    tailleX=0;
    tailleY=0;
    tailleCase=DIMX/16;
    im_quadrillage.loadFromFile("data/decors/quadrillage.png", renderer);
    // Terrain
    std::string nomFichier = "data/decors/premiereCouche.txt";
    std::ifstream fichier(nomFichier.c_str());
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"sdlEditeur::sdlEditeur : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier.is_open());
            }
        }
    }
    if (fichier.is_open()) {
        std::string ligne;
        bool marchable;
        unsigned int i=0;
        while (!fichier.eof()) {
            fichier >> ligne >> marchable;
            m_listeEdition1.push_back(new Image());
            ligne="data/decors/"+ligne+".png";
            m_listeEdition1[i]->loadFromFile(ligne.c_str(), renderer);
            m_marchablePremiereCouche.push_back(marchable);
            i++;
        }
        fichier.close();
    }
    // Decors
    nomFichier = "data/decors/deuxiemeCouche1.txt";
    std::ifstream fichier2(nomFichier.c_str());
    if(!fichier2.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier2.open(parent.c_str());
        if(!fichier2.is_open()) {
            parent = std::string("../") + parent;
            fichier2.open(parent.c_str());
            if(!fichier2.is_open()) {
                std::cout<<"sdlEditeur::sdlEditeur : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier2.is_open());
            }
        }
    }
    if (fichier2.is_open()) {
        m_marchableDeuxiemeCouche.push_back(1);
        m_marchableDeuxiemeCouche.push_back(0);
        unsigned int* c;
        for (unsigned int i=0; i<2; i++) {
            c=new unsigned int[4]{0,0,50,50};
            m_collisionsDecors.push_back(c);
        }
        std::string ligne;
        bool marchable;
        unsigned int i=0;
        while (!fichier2.eof()) {
            c=new unsigned int[4]{0,0,0,0};
            fichier2 >> ligne >> marchable;
            m_listeEdition2.push_back(new Image());
            ligne="data/decors/"+ligne+".png";
            m_listeEdition2[i]->loadFromFile(ligne.c_str(), renderer);
            m_marchableDeuxiemeCouche.push_back(marchable);
            if (!marchable) {
                fichier2 >> c[0] >> c[1] >> c[2] >> c[3];
            }
            m_collisionsDecors.push_back(c);
            i++;
        }
        m_tailleDecors=i;
        // Interactions et PNJ
        nomFichier = "data/interactions/deuxiemeCouche2.txt";
        std::ifstream fichier3(nomFichier.c_str());
        if(!fichier3.is_open()) {
            std::string parent = std::string("../") + nomFichier;
            fichier3.open(parent.c_str());
            if(!fichier3.is_open()) {
                parent = std::string("../") + parent;
                fichier3.open(parent.c_str());
                if(!fichier3.is_open()) {
                    std::cout<<"sdlEditeur::sdlEditeur : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                    assert(fichier3.is_open());
                }
            }
        }
        if (fichier3.is_open()) {
            unsigned int action;
            unsigned int* p;
            while (!fichier3.eof()) {
                c=new unsigned int[4]{0,0,0,0};
                fichier3 >> ligne >> marchable;
                if (!marchable) {
                    fichier3 >> c[0] >> c[1] >> c[2] >> c[3];
                }
                m_collisionsDecors.push_back(c);
                fichier3 >> action;
                if (action==1) {
                    p=new unsigned int[3]{i+2,0,0};
                    fichier3 >> p[1] >> p[2];
                    m_positionsPortes.push_back(p);
                }
                m_listeEdition3.push_back(new Image());
                ligne="data/interactions/"+ligne+".png";
                m_listeEdition3[i-m_tailleDecors]->loadFromFile(ligne.c_str(), renderer);
                m_marchableDeuxiemeCouche.push_back(marchable);
                m_interactionsAction.push_back(action);
                i++;
            }
            fichier3.close();
        }
        fichier2.close();
    }
    m_pnjSelectionne=NULL;
    // Objets
    nomFichier="data/objets/nomsImagesObjets.txt";
    std::ifstream nomsImagesObjets (nomFichier.c_str());
    if(!nomsImagesObjets.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        nomsImagesObjets.open(parent);
        if(!nomsImagesObjets.is_open()) {
            parent = std::string("../") + parent;
            nomsImagesObjets.open(parent);
            if(!nomsImagesObjets.is_open()) {
                std::cout << "sdlEditeur::sdlEditeur : Erreur dans l'ouverture du fichier "<< nomFichier << std::endl;
                SDL_Quit();
                assert(nomsImagesObjets.is_open());
            }
        }
    }
    if(nomsImagesObjets.is_open()) {
        std::string nomImage;
        unsigned int i=0;
        while (!nomsImagesObjets.eof()) {
            nomsImagesObjets >> nomImage;
            std::string chemin = "data/objets/";
            chemin += nomImage + ".png";
            m_listeObjets.push_back(new Image());
            m_listeObjets[i]->loadFromFile(chemin.c_str(), renderer);
            i++;
        }
        nomsImagesObjets.close();
    }
    // On ajoute tous les objets
    nomFichier = "data/objets/objets.txt";
    std::ifstream objets(nomFichier.c_str());
    if(!objets.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        objets.open(parent.c_str());
        if(!objets.is_open()) {
            parent = std::string("../") + parent;
            objets.open(parent.c_str());
            if(!objets.is_open()) {
                std::cout<<"sdlEditeur::sdlEditeur : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(objets.is_open());
            }
        }
    }
    if (objets.is_open()) {
        std::vector<std::string> listeObjets;
        std::string parametre;
        unsigned int i=0,j=0;
        while (std::getline(objets,parametre,';')) {
            listeObjets.push_back(parametre);
        }
        while (i<listeObjets.size()) {
            std::string type=listeObjets[i];
            std::string nom=listeObjets[++i];
            std::string description=listeObjets[++i];
            unsigned int valeur=std::stoi(listeObjets[++i]);
            if (type=="Mat\u00E9riau") {
                unsigned int nbMax=std::stoi(listeObjets[++i]);
                m_tableauObjets.push_back(new Materiau(j,nom,description,valeur,1,nbMax));
            }
            else if (type=="\u00C9quipement") {
                unsigned int pv=std::stoi(listeObjets[++i]);
                unsigned int mana=std::stoi(listeObjets[++i]);
                unsigned int defense=std::stoi(listeObjets[++i]);
                unsigned int attaque=std::stoi(listeObjets[++i]);
                unsigned int exp=std::stoi(listeObjets[++i]);
                unsigned int emplacement=std::stoi(listeObjets[++i]);
                m_tableauObjets.push_back(new Equipement(j,nom,description,valeur,pv,mana,defense,attaque,exp,emplacement));
            }
            else if (type=="Consommable") {
                unsigned int pv=std::stoi(listeObjets[++i]);
                unsigned int mana=std::stoi(listeObjets[++i]);
                unsigned int defense=std::stoi(listeObjets[++i]);
                unsigned int attaque=std::stoi(listeObjets[++i]);
                unsigned int exp=std::stoi(listeObjets[++i]);
                m_tableauObjets.push_back(new Consommable(j,nom,description,valeur,pv,mana,defense,attaque,exp));

            }
            else if (type=="Sp\u00E9cial") {
                m_tableauObjets.push_back(new ObjetSpecial(j,nom,description,valeur));
            }
            i++;
            j++;
        }
        objets.close();
    }
    m_coffreSelectionne=NULL;

    // Ennemis
    nomFichier="data/ennemis/nomsImagesEnnemis.txt";
    std::ifstream nomsImagesEnnemis (nomFichier.c_str());
    if(!nomsImagesEnnemis.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        nomsImagesEnnemis.open(parent);
        if(!nomsImagesEnnemis.is_open()) {
            parent = std::string("../") + parent;
            nomsImagesEnnemis.open(parent);
            if(!nomsImagesEnnemis.is_open()) {
                std::cout << "sdlEditeur::sdlEditeur : Erreur dans l'ouverture du fichier "<< nomFichier << std::endl;
                SDL_Quit();
                assert(nomsImagesEnnemis.is_open());
            }
        }
    }
    if(nomsImagesEnnemis.is_open()) {
        std::string nomImage;
        unsigned int i=0;
        unsigned int c[4];
        while (!nomsImagesEnnemis.eof()) {
            nomsImagesEnnemis >> nomImage >> c[0] >> c[1] >> c[2] >> c[3];
            std::string chemin = "data/ennemis/";
            chemin += nomImage;
            m_listeEnnemis.push_back(new Image());
            m_listeEnnemis[i]->loadFromFile(chemin.c_str(), renderer);
            i++;
        }
        nomsImagesEnnemis.close();
    }
    m_pointApparitionSelectionne=NULL;

    // On cree les differentes especes
    nomFichier = "data/ennemis/ennemis.txt";
    std::ifstream ennemis(nomFichier.c_str());
    if(!ennemis.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        ennemis.open(parent.c_str());
        if(!ennemis.is_open()) {
            parent = std::string("../") + parent;
            ennemis.open(parent.c_str());
            if(!ennemis.is_open()) {
                std::cout<<"Jeu::Jeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(ennemis.is_open());
            }
        }
    }
    if (ennemis.is_open()) {
        std::vector<std::string> listeEnnemis;
        std::string parametre;
        unsigned int i=0,j=0;
        while (std::getline(ennemis,parametre,';')) {
            listeEnnemis.push_back(parametre);
        }
        while (i<listeEnnemis.size()) {
            std::string nomEspece=listeEnnemis[i];
            unsigned int butinOr=std::stoi(listeEnnemis[++i]);
            unsigned int vitesse=std::stoi(listeEnnemis[++i]);
            Espece* espece=new Espece(j,nomEspece,butinOr,vitesse);
            m_tableauEspeces.push_back(espece);
            unsigned int nbObjet=std::stoi(listeEnnemis[++i]);
            for (unsigned int k=0; k<nbObjet; k++) {
                unsigned int idObjet=std::stoi(listeEnnemis[++i]);
                Objet* objet=m_tableauObjets[idObjet];
                std::string type=m_tableauObjets[idObjet]->getTypeObjet();
                unsigned int chance;
                if (type=="Mat\u00E9riau") {
                    unsigned int nbExemplaire=std::stoi(listeEnnemis[++i]);
                    Materiau* m=static_cast<Materiau*>(objet);
                    Materiau* materiau=new Materiau(m);
                    materiau->modifierNombreExemplaires(-materiau->getNombreExemplaires()+nbExemplaire);
                    chance=std::stoi(listeEnnemis[++i]);
                    espece->ajouterButin(materiau,chance);
                }
                else if (type=="\u00C9quipement") {
                    Equipement* e=static_cast<Equipement*>(objet);
                    Equipement* equipement=new Equipement(e);
                    chance=std::stoi(listeEnnemis[++i]);
                    espece->ajouterButin(equipement,chance);
                }
                else if (type=="Consommable") {
                    Consommable* c=static_cast<Consommable*>(objet);
                    Consommable* consommable=new Consommable(c);
                    chance=std::stoi(listeEnnemis[++i]);
                    espece->ajouterButin(consommable,chance);

                }
                else if (type=="Sp\u00E9cial") {
                    ObjetSpecial* s=static_cast<ObjetSpecial*>(objet);
                  ObjetSpecial* objetSpecial=new ObjetSpecial(s);
                    chance=std::stoi(listeEnnemis[++i]);
                    espece->ajouterButin(objetSpecial,chance);
                }
            }
            i++;
            j++;
        }
        ennemis.close();
    }

    im_coche.loadFromFile("data/interfaces/coche.png", renderer);

    // Interface
    im_barreOutil.loadFromFile("data/interfaces/barreOutil.png", renderer);
    im_selectionCarre.loadFromFile("data/interfaces/selectionCarre.png", renderer);
    // Outils
    im_outilDeplacement.loadFromFile("data/interfaces/outilDeplacement.png", renderer);
    im_outilSelection.loadFromFile("data/interfaces/outilSelection.png", renderer);
    im_outilGomme.loadFromFile("data/interfaces/outilGomme.png", renderer);
    im_outilParametre.loadFromFile("data/interfaces/outilParametre.png", renderer);
    im_outilDoubleFleche.loadFromFile("data/interfaces/outilDoubleFleche.png", renderer);
    im_outilVersion.loadFromFile("data/interfaces/outilVersion.png", renderer);
    // Edition
    im_fondEdition.loadFromFile("data/interfaces/fondEdition.png", renderer);
    im_boutonDescendre.loadFromFile("data/interfaces/boutonDescendre.png", renderer);
    im_boutonMonter.loadFromFile("data/interfaces/boutonMonter.png", renderer);
    im_bouton.loadFromFile("data/interfaces/boutonMenu.png", renderer);
    // Sauvegarde
    im_boutonRetour.loadFromFile("data/interfaces/boutonRetour.png", renderer);
    im_barreSaisie.loadFromFile("data/interfaces/barreSaisie.png", renderer);
    im_curseurSaisie.loadFromFile("data/interfaces/curseurSaisie.png", renderer);
    // Parametre PNJ
    im_boutonGauche.loadFromFile("data/interfaces/boutonGauche.png", renderer);
    im_boutonDroite.loadFromFile("data/interfaces/boutonDroite.png", renderer);
    im_plus.loadFromFile("data/interfaces/plus.png", renderer);
    im_moins.loadFromFile("data/interfaces/moins.png", renderer);
    // Menu taille
    im_menu.loadFromFile("data/interfaces/menu.png", renderer);
    im_selectionGrande.loadFromFile("data/interfaces/selectionGrande.png", renderer);
    // Quitter
    im_quitter.loadFromFile("data/interfaces/quitter.png", renderer);
    im_selectionPetite.loadFromFile("data/interfaces/selectionPetite.png", renderer);
    m_selectBouton=true;

    m_nomSauvegarde=new char[20];
    m_tailleNomSauvegarde=0;
    m_nomNiveauSauvegarde="";
    m_tailleNomNiveauSauvegarde=0;
    m_tailleNomPnj=0;
    m_taillePhrasePnj=0;
    m_ennemiSelectionne=0;
    m_animationSaisie=0;
    m_curseurSaisie=m_taillePhrasePnj;

    m_outilSelectionne=0;
    m_page=1;
    m_ennemiPageEspece=1;
    m_coffrePageObjet=1;
    m_editionSelectionnee=0;
    m_ongletEdition=1;
    m_listeEdition=&m_listeEdition1;

    m_posSouris.x=0;
    m_posSouris.y=0;
    camera.x=0;
    camera.y=DIMY/5;
    cameraMonde.x=0;
    cameraMonde.y=DIMX/5;
    m_afficheMenuMonde=true;
    m_afficheMenu=false;
    m_saisieTailleX=false;
    m_saisieTailleY=false;
    m_afficheQuitter=false;
    m_afficheMonde=false;
    m_afficheSauvegarde=false;
    m_afficheChargement=false;
    m_erreurChargement=false;
    m_saisieSauvegarde=false;
    m_saisieNiveauSauvegarde=false;
    m_afficheParametrePnj=false;
    m_saisieNomPnj=false;
    m_saisiePhrasePnj=false;
    m_afficheParametreEnnemi=false;
    m_saisiePvEnnemi=false;
    m_saisieManaEnnemi=false;
    m_saisieAttaqueEnnemi=false;
    m_saisieDefenseEnnemi=false;
    m_saisieExperienceEnnemi=false;
    m_saisieTempsApparition=false;
    m_afficheParametreCoffre=false;
    m_saisieNbExemplaires=false;
    m_quitter=false;
    m_clic=false;
    m_majuscule=false;
    if (SDL_GetModState()==KMOD_CAPS)
        m_majuscule=true;
    m_altGr=false;
    if (SDL_GetModState()==KMOD_RALT)
        m_altGr=true;
    m_alt=false;
    m_combinaisonAlt="";
    m_accentGrave=false;
    m_accentCirconflexe=false;
    m_ligneSelectionnee=0;
    m_premiereCoucheTerrain=NULL;
    m_deuxiemeCoucheTerrain=NULL;
    m_niveauClic=false;
    im_rouge.loadFromFile("data/interfaces/rouge.png", renderer);
    im_bleu.loadFromFile("data/interfaces/bleu.png", renderer);
    m_lienSortie=false;
}

sdlEditeur::~sdlEditeur() {
    TTF_CloseFont(policeTexte);
    TTF_CloseFont(policeMenu);
    TTF_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    if (m_premiereCoucheTerrain!=NULL)
        delete [] m_premiereCoucheTerrain;
    if (m_deuxiemeCoucheTerrain!=NULL)
        delete [] m_deuxiemeCoucheTerrain;
    delete [] m_nomSauvegarde;
    for (unsigned int i=0; i<m_pnj.size(); i++)
        delete m_pnj[i];
    for (unsigned int i=0; i<m_pointApparition.size(); i++)
        delete m_pointApparition[i];
    for (unsigned int i=0; i<m_listeObjets.size(); i++)
        delete m_listeObjets[i];
    for (unsigned int i=0; i<m_coffre.size(); i++)
        delete m_coffre[i];
    for (unsigned int i=0; i<m_tableauObjets.size(); i++)
        delete m_tableauObjets[i];
    for (unsigned int i=0; i<m_tableauEspeces.size(); i++)
        delete m_tableauEspeces[i];
    for (unsigned int i=0; i<m_listeEdition1.size(); i++)
        delete m_listeEdition1[i];
    for (unsigned int i=0; i<m_listeEdition2.size(); i++)
        delete m_listeEdition2[i];
    for (unsigned int i=0; i<m_listeEdition3.size(); i++)
        delete m_listeEdition3[i];
    for (unsigned int i=0; i<m_listeEnnemis.size(); i++)
        delete m_listeEnnemis[i];
    for (unsigned int i=0; i<m_collisionsDecors.size(); i++)
        delete m_collisionsDecors[i];
    for (unsigned int i=0; i<m_positionsPortes.size(); i++)
        delete m_positionsPortes[i];
    for (unsigned int i=0; i<m_coordonneesPortes.size(); i++)
        delete m_coordonneesPortes[i];
}

unsigned int sdlEditeur::tailleSansAccents(std::string chaine) const {
    unsigned int compte = 0, j=chaine.size();
    for (unsigned i = 0 ; i < j ; i++) {
        if (int(chaine[i]) < 0 || int(chaine[i] > 127))
            compte ++;
    }
    return j-compte/2;
}

unsigned int sdlEditeur::indiceSansAccents(std::string chaine, float j) const {
    unsigned int compte = 0;
    for (unsigned int i = 0 ; i < j ; i++) {
        if (int(chaine[i]) < 0 || int(chaine[i] > 127))
            j+=0.5;
        compte++;
    }
    return compte;
}

void sdlEditeur::afficheTexte(int x, int y, int w, int h, std::string texte, TTF_Font* police, SDL_Color couleur, unsigned int transparence) {
    im_texte.setSurface(TTF_RenderUTF8_Blended(police, texte.c_str(), couleur));
    im_texte.loadFromCurrentSurface(renderer);
    SDL_Rect positionPhrase = {x, y, w, h};
    if (transparence!=255) {
        SDL_SetSurfaceAlphaMod(im_texte.getSurface(), transparence);
        im_texte.libereTexture();
        im_texte.loadFromCurrentSurface(renderer);
    }
    SDL_RenderCopy(renderer, im_texte.getTexture(), NULL, &positionPhrase);
    im_texte.libereSurface();
    im_texte.libereTexture();
}


void sdlEditeur::setSortie(Point sortie, std::vector<Point> copieSorties, std::vector<Point> copieDestinations) {
    Point destination;
    destination.x=-1;
    destination.y=-1;
    for (unsigned int j=0; j<copieSorties.size(); j++) {
        if (sortie.x==copieSorties[j].x && sortie.y==copieSorties[j].y) {
            destination.x=copieDestinations[j].x;
            destination.y=copieDestinations[j].y;
        }
    }
    m_niveauSorties[m_niveauSelectionne].push_back(sortie);
    m_niveauDestinationsSorties[m_niveauSelectionne].push_back(destination);
    if (destination.x!=-1) {
        Point destination2;
        destination2.x=m_niveauSelectionne;
        destination2.y=m_niveauSorties[m_niveauSelectionne].size()-1;
        m_niveauDestinationsSorties[destination.x][destination.y]=destination2;
    }
}

void sdlEditeur::setSorties() {
    std::vector<Point> copieSorties=m_niveauSorties[m_niveauSelectionne];
    std::vector<Point> copieDestinations=m_niveauDestinationsSorties[m_niveauSelectionne];
    std::vector<Point>().swap(m_niveauSorties[m_niveauSelectionne]);
    std::vector<Point>().swap(m_niveauDestinationsSorties[m_niveauSelectionne]);
    unsigned int c1, c2;
    for (unsigned int i=0; i<tailleX; i++) {
        c1=m_premiereCoucheTerrain[i];
        c2=m_deuxiemeCoucheTerrain[i];
        if (m_marchablePremiereCouche[c1] && m_marchableDeuxiemeCouche[c2]) {
            unsigned int j=i;
            while (j<(i+1)*tailleX && m_marchablePremiereCouche[c1] && m_marchableDeuxiemeCouche[c2]) {
                j++;
                c1=m_premiereCoucheTerrain[j];
                c2=m_deuxiemeCoucheTerrain[j];
            }
            j--;
            Point sortie;
            sortie.x=i;
            sortie.y=j;
            setSortie(sortie, copieSorties, copieDestinations);
            i=j;
        }
    }
    for (unsigned int i=(tailleX-1)*tailleY; i<tailleX*tailleY; i++) {
        c1=m_premiereCoucheTerrain[i];
        c2=m_deuxiemeCoucheTerrain[i];
        if (m_marchablePremiereCouche[c1] && m_marchableDeuxiemeCouche[c2]) {
            unsigned int j=i;
            while (j<tailleX*tailleY && m_marchablePremiereCouche[c1] && m_marchableDeuxiemeCouche[c2]) {
                j++;
                c1=m_premiereCoucheTerrain[j];
                c2=m_deuxiemeCoucheTerrain[j];
            }
            j--;
            Point sortie;
            sortie.x=i;
            sortie.y=j;
            setSortie(sortie, copieSorties, copieDestinations);
            i=j;
        }
    }
    for (unsigned int i=tailleX; i<tailleX*tailleY; i+=tailleX) {
        c1=m_premiereCoucheTerrain[i];
        c2=m_deuxiemeCoucheTerrain[i];
        if (m_marchablePremiereCouche[c1] && m_marchableDeuxiemeCouche[c2]) {
            unsigned int j=i;
            while (j<tailleX*tailleY && m_marchablePremiereCouche[c1] && m_marchableDeuxiemeCouche[c2]) {
                j+=tailleX;
                c1=m_premiereCoucheTerrain[j];
                c2=m_deuxiemeCoucheTerrain[j];
            }
            j-=tailleX;
            Point sortie;
            sortie.x=i;
            sortie.y=j;
            setSortie(sortie, copieSorties, copieDestinations);
            i=j;
        }
    }
    for (unsigned int i=2*tailleX-1; i<tailleY*tailleX; i+=tailleX) {
        c1=m_premiereCoucheTerrain[i];
        c2=m_deuxiemeCoucheTerrain[i];
        if (m_marchablePremiereCouche[c1] && m_marchableDeuxiemeCouche[c2]) {
            unsigned int j=i;
            while (j<tailleX*tailleY && m_marchablePremiereCouche[c1] && m_marchableDeuxiemeCouche[c2]) {
                j+=tailleX;
                c1=m_premiereCoucheTerrain[j];
                c2=m_deuxiemeCoucheTerrain[j];
            }
            j-=tailleX;
            Point sortie;
            sortie.x=i;
            sortie.y=j;
            setSortie(sortie, copieSorties, copieDestinations);
            i=j;
        }
    }
}

void sdlEditeur::sdlAff() {
    SDL_SetRenderDrawColor(renderer, 0, 80, 80, 80);
    SDL_RenderClear(renderer);
    if (m_afficheMenuMonde) {
        im_menu.draw(renderer, 3*DIMX/10, DIMY/20, 2*DIMX/5, 9*DIMY/10);
        std::string phrasesMenu [2] = {"Nouveau monde", "Charger monde"};
        for(unsigned int i = 0 ; i < 2 ; i++) {
            // Surlignage de la ligne selectionnee
            if (m_ligneSelectionnee == i)
                im_selectionGrande.draw(renderer, 12*DIMX/40, DIMY/5 + i*4*DIMY/19-DIMY/16, 16*DIMX/40, DIMY/8);
            // Affichage phrase
            afficheTexte(DIMX/2 - int(phrasesMenu[i].size()*DIMX/150), DIMY/5 + i*4*DIMY/19-DIMY/26, int(phrasesMenu[i].size()*DIMX/75), DIMY/13, phrasesMenu[i], policeMenu, couleurPoliceMenu);
        }
    }
    else if (m_afficheMenu) {
        im_fondEdition.draw(renderer, DIMX/8, DIMY/16, 6*DIMX/8, 14*DIMY/16);
        std::string phrase="Taille X :";
        afficheTexte(DIMX/2-DIMX/24-phrase.size()*DIMX/100, DIMY/2+DIMY/200, phrase.size()*DIMX/100, 8*DIMY/20/10, phrase, policeTexte, couleurPoliceOr);
        im_barreSaisie.draw(renderer, DIMX/2-DIMX/24, DIMY/2, DIMX/12, DIMY/20);
        afficheTexte(DIMX/2-DIMX/24+DIMX/80, DIMY/2+DIMY/200, std::to_string(tailleX).size()*DIMX/100, 8*DIMY/20/10, std::to_string(tailleX), policeTexte, couleurPoliceOr);
        if (m_saisieTailleX)
            im_curseurSaisie.draw(renderer, DIMX/2-DIMX/24+DIMX/80+std::to_string(tailleX).size()*DIMX/100, DIMY/2+DIMY/200, DIMX/100, 8*DIMY/20/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
        if (m_animationSaisie<2*3.14)
            m_animationSaisie+=0.07;
        else
            m_animationSaisie=0;
        phrase="Taille Y :";
        afficheTexte(DIMX/2-DIMX/24-phrase.size()*DIMX/100, 3*DIMY/5+DIMY/200, phrase.size()*DIMX/100, 8*DIMY/20/10, phrase, policeTexte, couleurPoliceOr);
        im_barreSaisie.draw(renderer, DIMX/2-DIMX/24, 3*DIMY/5, DIMX/12, DIMY/20);
        afficheTexte(DIMX/2-DIMX/24+DIMX/80, 3*DIMY/5+DIMY/200, std::to_string(tailleY).size()*DIMX/100, 8*DIMY/20/10, std::to_string(tailleY), policeTexte, couleurPoliceOr);
        if (m_saisieTailleY)
            im_curseurSaisie.draw(renderer, DIMX/2-DIMX/24+DIMX/80+std::to_string(tailleY).size()*DIMX/100, 3*DIMY/5+DIMY/200, DIMX/100, 8*DIMY/20/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
        im_boutonRetour.draw(renderer, DIMX/2-DIMX/16, 4*DIMY/5, DIMX/8, DIMY/10);
    }
    else if (m_afficheMonde) {
        for (unsigned int i=0; i<m_niveauCoordonnees.size(); i++) {
            im_barreSaisie.draw(renderer, (m_niveauCoordonnees[i].x-m_niveauTaille[i].x/2*6+1)*tailleCase/120 - cameraMonde.x*tailleCase/120, DIMY/5+(m_niveauCoordonnees[i].y-m_niveauTaille[i].y/2*6+1)*tailleCase/120 - cameraMonde.y*tailleCase/120, m_niveauTaille[i].x*tailleCase/20, m_niveauTaille[i].y*tailleCase/20);
            Point tailles=m_niveauTaille[i];
            for (unsigned int j=0; j<m_niveauSorties[i].size(); j++) {
                Point p=m_niveauSorties[i][j];
                if ((unsigned int)p.y/tailles.x==(unsigned int)p.x/tailles.x) {
                    if (m_lienSortie && m_niveauSelectionne==i && m_sortieSelectionnee==j)
                        im_bleu.draw(renderer, (m_niveauCoordonnees[i].x-tailles.x/2*6+1+6*(p.x%tailles.x))*tailleCase/120 - cameraMonde.x*tailleCase/120, DIMY/5+(m_niveauCoordonnees[i].y-tailles.y/2*6+1+6*(unsigned int)(p.x/tailles.x))*tailleCase/120 - cameraMonde.y*tailleCase/120, (p.y-p.x+1)*6*tailleCase/120, 6*tailleCase/120);
                    else
                        im_rouge.draw(renderer, (m_niveauCoordonnees[i].x-tailles.x/2*6+1+6*(p.x%tailles.x))*tailleCase/120 - cameraMonde.x*tailleCase/120, DIMY/5+(m_niveauCoordonnees[i].y-tailles.y/2*6+1+6*(unsigned int)(p.x/tailles.x))*tailleCase/120 - cameraMonde.y*tailleCase/120, (p.y-p.x+1)*6*tailleCase/120, 6*tailleCase/120);
                }
                else {
                    if (m_lienSortie && m_niveauSelectionne==i && m_sortieSelectionnee==j)
                        im_bleu.draw(renderer, (m_niveauCoordonnees[i].x-tailles.x/2*6+1+6*(p.x%tailles.x))*tailleCase/120 - cameraMonde.x*tailleCase/120, DIMY/5+(m_niveauCoordonnees[i].y-tailles.y/2*6+1+6*(unsigned int)(p.x/tailles.x))*tailleCase/120 - cameraMonde.y*tailleCase/120, 6*tailleCase/120, ((p.y-p.x)/tailles.x+1)*6*tailleCase/120);
                    else
                        im_rouge.draw(renderer, (m_niveauCoordonnees[i].x-tailles.x/2*6+1+6*(p.x%tailles.x))*tailleCase/120 - cameraMonde.x*tailleCase/120, DIMY/5+(m_niveauCoordonnees[i].y-tailles.y/2*6+1+6*(unsigned int)(p.x/tailles.x))*tailleCase/120 - cameraMonde.y*tailleCase/120, 6*tailleCase/120, ((p.y-p.x)/tailles.x+1)*6*tailleCase/120);
                }
                Point p2=m_niveauDestinationsSorties[i][j];
                if (p2.x!=-1) {
                    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                    SDL_RenderDrawLine(renderer,(m_niveauCoordonnees[i].x-tailles.x/2*6+1+6*(p.x%tailles.x))*tailleCase/120 - cameraMonde.x*tailleCase/120, DIMY/5+(m_niveauCoordonnees[i].y-tailles.y/2*6+1+6*(unsigned int)(p.x/tailles.x))*tailleCase/120 - cameraMonde.y*tailleCase/120,
                                       (m_niveauCoordonnees[p2.x].x-m_niveauTaille[p2.x].x/2*6+1+6*(m_niveauSorties[p2.x][p2.y].x%m_niveauTaille[p2.x].x))*tailleCase/120 - cameraMonde.x*tailleCase/120, DIMY/5+(m_niveauCoordonnees[p2.x].y-m_niveauTaille[p2.x].y/2*6+1+6*(unsigned int)(m_niveauSorties[p2.x][p2.y].x/m_niveauTaille[p2.x].x))*tailleCase/120 - cameraMonde.y*tailleCase/120);
                }
            }
            if (m_niveauVersion[i]!=i) {
                unsigned int niv=m_niveauVersion[i];
                SDL_SetRenderDrawColor(renderer, 0, 255, 0, SDL_ALPHA_OPAQUE);
                    SDL_RenderDrawLine(renderer, m_niveauCoordonnees[i].x*tailleCase/120 - cameraMonde.x*tailleCase/120, DIMY/5+m_niveauCoordonnees[i].y*tailleCase/120 - cameraMonde.y*tailleCase/120, m_niveauCoordonnees[niv].x*tailleCase/120 - cameraMonde.x*tailleCase/120, DIMY/5+m_niveauCoordonnees[niv].y*tailleCase/120 - cameraMonde.y*tailleCase/120);
            }
        }
        im_barreOutil.draw(renderer, 0, 0, DIMX, DIMY/5);
        // Sauvegarde
        im_bouton.draw(renderer, 0, 0, DIMX/16, DIMY/20);
        // Chargement
        im_bouton.draw(renderer, DIMX/16, 0, DIMX/16, DIMY/20);
        // Outils
        im_outilDeplacement.draw(renderer, DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        im_plus.draw(renderer, 2*DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        im_outilGomme.draw(renderer, 3*DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        im_outilParametre.draw(renderer, 4*DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        im_outilSelection.draw(renderer, 5*DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        im_outilDoubleFleche.draw(renderer, 6*DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        im_outilVersion.draw(renderer, 7*DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        im_selectionCarre.draw(renderer, DIMX/10+m_outilSelectionne*DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
    }
    else {
        SDL_Surface* s;
        unsigned int w, h;
        for (unsigned int i=0; i<tailleX*tailleY; i++) {
            m_listeEdition1[m_premiereCoucheTerrain[i]]->draw(renderer, i%tailleX*tailleCase - camera.x, DIMY/5+(i/tailleX)*tailleCase - camera.y, tailleCase, tailleCase);
        }
        for (unsigned int i=0; i<tailleX*tailleY; i++) {
            if (m_deuxiemeCoucheTerrain[i]<m_tailleDecors+2) {  // <= car 0 et 1 ne sont pas compris dans m_tailleDecors
                if (m_deuxiemeCoucheTerrain[i]>1) {
                    m_listeEdition2[m_deuxiemeCoucheTerrain[i]-2]->draw(renderer, i%tailleX*tailleCase - camera.x, DIMY/5+(i/tailleX)*tailleCase - camera.y, tailleCase, tailleCase);   // -2 pour le 0 et 1
               }
            }
            else {
                s=m_listeEdition3[m_deuxiemeCoucheTerrain[i]-2-m_tailleDecors]->getSurface();
                w=s->w*tailleCase/50;
                h=s->h*tailleCase/50;
                switch (m_interactionsAction[m_deuxiemeCoucheTerrain[i]-2-m_tailleDecors]) {
                    case 3:
                        m_listeEdition3[m_deuxiemeCoucheTerrain[i]-2-m_tailleDecors]->draw(renderer, i%tailleX*tailleCase - camera.x, DIMY/5+(i/tailleX)*tailleCase - camera.y, tailleCase, tailleCase);
                    break;
                    default:
                        m_listeEdition3[m_deuxiemeCoucheTerrain[i]-2-m_tailleDecors]->draw(renderer, i%tailleX*tailleCase - camera.x, DIMY/5+((int)(i/tailleX-(((h/tailleCase)-1)*tailleX)/tailleX)*tailleCase-h%tailleCase) - camera.y, w, h);
                        break;
                }
            }
            im_quadrillage.draw(renderer, i%tailleX*tailleCase - camera.x, DIMY/5+(i/tailleX)*tailleCase - camera.y, tailleCase, tailleCase);
        }
        im_barreOutil.draw(renderer, 0, 0, DIMX, DIMY/5);
        // Sauvegarde
        im_bouton.draw(renderer, 0, 0, DIMX/16, DIMY/20);
        // Chargement
        im_bouton.draw(renderer, DIMX/16, 0, DIMX/16, DIMY/20);
        // Outils
        im_outilDeplacement.draw(renderer, DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        im_outilSelection.draw(renderer, 2*DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        im_outilGomme.draw(renderer, 3*DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        im_outilParametre.draw(renderer, 4*DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        im_selectionCarre.draw(renderer, DIMX/10+m_outilSelectionne*DIMX/10, DIMY/10-DIMY/30, DIMY/15, DIMY/15);
        // Edition
        im_fondEdition.draw(renderer, 4*DIMX/5, DIMY/5, DIMX/4, 4*DIMY/5);
        im_boutonDescendre.draw(renderer, 4*DIMX/5+DIMX/16, DIMY-DIMY/19-DIMY/13.2, DIMX/13.3, DIMY/13.2);
        im_boutonMonter.draw(renderer, 4*DIMX/5+DIMX/16, DIMY/5+DIMY/19, DIMX/13.3, DIMY/13.2);
        // Onglets edition
        im_bouton.draw(renderer, 4*DIMX/5+1, DIMY/5, DIMX/15, DIMY/20);
        im_bouton.draw(renderer, 4*DIMX/5+1+DIMX/15, DIMY/5, DIMX/15, DIMY/20);
        im_bouton.draw(renderer, 4*DIMX/5+2*DIMX/15, DIMY/5, DIMX/15, DIMY/20);
        for (unsigned int i=0; i<8; i++) {
            if (i+(m_page-1)*2<m_listeEdition->size()) {
                (*m_listeEdition)[i+(m_page-1)*2]->draw(renderer, 4*DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20), 11*DIMY/30+i/2*(DIMY/22.5+DIMY/11.25), DIMX/20, DIMY/11.25);
                if (m_editionSelectionnee==i+(m_page-1)*2)
                    im_selectionCarre.draw(renderer, 4*DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20), 11*DIMY/30+i/2*(DIMY/22.5+DIMY/11.25), DIMX/20, DIMY/11.25);
            }
        }
        if (m_afficheParametrePnj) {
            im_fondEdition.draw(renderer, DIMX/8, DIMY/16, 6*DIMX/8, 14*DIMY/16);
            im_boutonRetour.draw(renderer, 4*DIMX/5-DIMX/16, DIMY/12, DIMX/16, DIMY/10);
            im_barreSaisie.draw(renderer, DIMX/8, DIMY/8, DIMX/3, DIMY/20);
            if (m_tailleNomPnj>0)
                afficheTexte(DIMX/8+DIMX/80, DIMY/8+DIMY/200, m_tailleNomPnj*DIMX/100, 8*DIMY/20/10, m_nomPnj, policeTexte, couleurPoliceOr);
            if (m_saisieNomPnj)
                im_curseurSaisie.draw(renderer, DIMX/8+DIMX/80+m_tailleNomPnj*DIMX/100, DIMY/8+DIMY/200, DIMX/100, 8*DIMY/20/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
            if (m_animationSaisie<2*3.14)
                    m_animationSaisie+=0.07;
                else
                    m_animationSaisie=0;
            std::string phrase="Nom du PNJ";
            afficheTexte(DIMX/8+DIMX/80+35*DIMX/100, DIMY/8+DIMY/200, phrase.size()*DIMX/100, 8*DIMY/20/10, phrase, policeTexte, couleurPoliceOr);
            phrase="Dialogue ";
            if (m_pnjSelectionne->getADialogue()) {
                im_barreSaisie.draw(renderer, DIMX/8, DIMY/5, 6*DIMX/8, 3*DIMY/5);
                Dialogue* d=m_pnjSelectionne->getDialogue(m_pnjSelectionne->getIndiceDialogue());
                std::string phrasePnj;
                unsigned int taillePhrase;
                if (d->getIndicePhrase()>0) {
                    im_barreSaisie.draw(renderer, DIMX/8+DIMX/100, DIMY/5+3*DIMY/100, DIMX/2, 14*DIMY/100);
                    phrasePnj=d->getPhrase(d->getIndicePhrase()-1);
                    taillePhrase=tailleSansAccents(phrasePnj);
                    if (taillePhrase>0)
                        afficheTexte(DIMX/8+DIMX/100+DIMX/40, 3*DIMY/10-4*DIMY/20/15, taillePhrase*DIMX/150, 8*DIMY/20/15, phrasePnj, policeTexte, couleurPoliceOr);
                    im_boutonMonter.draw(renderer, 12*DIMX/16, DIMY/5+3*DIMY/100+7*DIMY/200, 9*7*DIMX/100/16, 7*DIMY/100);
                }
                im_plus.draw(renderer, 11*DIMX/16, DIMY/5+3*DIMY/100+7*DIMY/200, 9*7*DIMX/100/16, 7*DIMY/100);
                if (d->getIndiceMax()>0)
                    im_moins.draw(renderer, 25*DIMX/32, 2*DIMY/5+DIMY/100+9*DIMY/200, 9*9*DIMX/100/16, 9*DIMY/100);
                im_barreSaisie.draw(renderer, DIMX/8+DIMX/100, 2*DIMY/5+DIMY/100, 5*DIMX/8, 18*DIMY/100);
                if (m_taillePhrasePnj>0)
                    afficheTexte(DIMX/8+DIMX/100+DIMX/40, DIMY/2-4*DIMY/20/10, m_taillePhrasePnj*DIMX/100, 8*DIMY/20/10, m_phrasePnj, policeTexte, couleurPoliceOr);
                if (m_saisiePhrasePnj)
                    im_curseurSaisie.draw(renderer, DIMX/8+DIMX/100+DIMX/40+m_curseurSaisie*DIMX/100-DIMX/200, DIMY/2-4*DIMY/20/10, DIMX/100, 8*DIMY/20/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
                if (d->getIndicePhrase()<d->getIndiceMax()) {
                    im_barreSaisie.draw(renderer, DIMX/8+DIMX/100, 3*DIMY/5+3*DIMY/100, DIMX/2, 14*DIMY/100);
                    phrasePnj=d->getPhrase(d->getIndicePhrase()+1);
                    taillePhrase=tailleSansAccents(phrasePnj);
                    if (taillePhrase>0)
                        afficheTexte(DIMX/8+DIMX/100+DIMX/40, 7*DIMY/10-4*DIMY/20/15, taillePhrase*DIMX/150, 8*DIMY/20/15, phrasePnj, policeTexte, couleurPoliceOr);
                    im_boutonDescendre.draw(renderer, 12*DIMX/16, 3*DIMY/5+3*DIMY/100+7*DIMY/200, 9*7*DIMX/100/16, 7*DIMY/100);
                }
                im_plus.draw(renderer, 11*DIMX/16, 3*DIMY/5+3*DIMY/100+7*DIMY/200, 9*7*DIMX/100/16, 7*DIMY/100);
                if (m_pnjSelectionne->getIndiceDialogue()>0)
                    im_boutonGauche.draw(renderer, DIMX/5, 41*DIMY/50, DIMX/25, 47*DIMY/400-DIMY/50);
                im_plus.draw(renderer, DIMX/5+2*DIMX/25, 41*DIMY/50, 9*(47*DIMX/400-DIMX/50)/16, 47*DIMY/400-DIMY/50);
                im_moins.draw(renderer, DIMX/2-9*9*DIMX/300/16, 45*DIMY/50-9*DIMY/300, 9*9*DIMX/150/16, 9*DIMY/150);
                if (m_pnjSelectionne->getIndiceDialogue()<m_pnjSelectionne->getIndiceMax())
                    im_boutonDroite.draw(renderer, 4*DIMX/5-DIMX/25, 41*DIMY/50, DIMX/25, 47*DIMY/400-DIMY/50);
                im_plus.draw(renderer, 4*DIMX/5-3*DIMX/25, 41*DIMY/50, 9*(47*DIMX/400-DIMX/50)/16, 47*DIMY/400-DIMY/50);
                phrase+=std::to_string(m_pnjSelectionne->getIndiceDialogue()+1);
            }
            else {
                im_plus.draw(renderer, DIMX/2-DIMX/40, DIMY/2-16*DIMY/40/9, DIMX/20, 16*DIMY/20/9);
            }
            afficheTexte(DIMX/2-phrase.size()/2*DIMX/100, 41*DIMY/50, phrase.size()*DIMX/100, 8*DIMY/20/10, phrase, policeTexte, couleurPoliceOr);
        }
        else if (m_afficheParametreEnnemi) {
            im_fondEdition.draw(renderer, DIMX/8, DIMY/16, 6*DIMX/8, 14*DIMY/16);
            im_boutonRetour.draw(renderer, 4*DIMX/5-DIMX/16, DIMY/12, DIMX/16, DIMY/10);
            std::string phrase="Ennemi ";
            if (m_animationSaisie<2*3.14)
                    m_animationSaisie+=0.07;
            else
                m_animationSaisie=0;
            if (m_pointApparitionSelectionne->getTailleListe()>0) {
                im_fondEdition.draw(renderer, DIMX/5, DIMY/4, DIMX/8, 2*DIMY/5);
                im_barreSaisie.draw(renderer, DIMX/5, DIMY/10, DIMX/5, DIMY/20);
                std::string stat;
                // Espece
                stat=m_tableauEspeces[m_ennemiEspece]->getNomEspece();
                afficheTexte(DIMX/5+DIMX/80, DIMY/10+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                im_boutonMonter.draw(renderer, DIMX/5+2*DIMX/40+DIMX/20-DIMX/52, DIMY/4-DIMY/20-DIMY/26, DIMX/26, DIMY/26);
                for (unsigned int i=0; i<6; i++) {
                    if (i+(m_ennemiPageEspece-1)*2<m_listeEnnemis.size()) {
                        m_listeEnnemis[i+(m_ennemiPageEspece-1)*2]->draw(renderer, DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20), DIMY/4+i/2*(DIMY/22.5+DIMY/11.25), DIMX/20, DIMY/11.25);
                        if (m_ennemiEspece==i+(m_ennemiPageEspece-1)*2)
                            im_selectionCarre.draw(renderer, DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20), DIMY/4+i/2*(DIMY/22.5+DIMY/11.25), DIMX/20, DIMY/11.25);
                        im_quadrillage.draw(renderer, DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20), DIMY/4+i/2*(DIMY/22.5+DIMY/11.25), DIMX/20, DIMY/11.25);
                    }
                }
                im_boutonDescendre.draw(renderer, DIMX/5+2*DIMX/40+DIMX/20-DIMX/52, DIMY/4+2*DIMY/5+DIMY/20-DIMY/26, DIMX/26, DIMY/26);
                // Pv
                im_barreSaisie.draw(renderer, 8*DIMX/20, DIMY/4, DIMX/6, DIMY/20);
                afficheTexte(8*DIMX/20+DIMX/6-DIMX/80-std::to_string(m_ennemiPv).size()*DIMX/100, DIMY/4+DIMY/200, std::to_string(m_ennemiPv).size()*DIMX/100, 8*DIMY/20/10, std::to_string(m_ennemiPv), policeTexte, couleurPoliceOr);
                if (m_saisiePvEnnemi)
                    im_curseurSaisie.draw(renderer, 8*DIMX/20+DIMX/6-DIMX/80, DIMY/4+DIMY/200, DIMX/100, 8*DIMY/20/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
                stat="PV";
                afficheTexte(8*DIMX/20+DIMX/6, DIMY/4+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                // Mana
                im_barreSaisie.draw(renderer, 13*DIMX/20, DIMY/4, DIMX/6, DIMY/20);
                afficheTexte(13*DIMX/20+DIMX/6-DIMX/80-std::to_string(m_ennemiMana).size()*DIMX/100, DIMY/4+DIMY/200, std::to_string(m_ennemiMana).size()*DIMX/100, 8*DIMY/20/10, std::to_string(m_ennemiMana), policeTexte, couleurPoliceOr);
                if (m_saisieManaEnnemi)
                    im_curseurSaisie.draw(renderer, 13*DIMX/20+DIMX/6-DIMX/80, DIMY/4+DIMY/200, DIMX/100, 8*DIMY/20/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
                stat="Mana";
                afficheTexte(13*DIMX/20+DIMX/6, DIMY/4+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                // Attaque
                im_barreSaisie.draw(renderer, 8*DIMX/20, DIMY/4+DIMY/15, DIMX/6, DIMY/20);
                afficheTexte(8*DIMX/20+DIMX/6-DIMX/80-std::to_string(m_ennemiAttaque).size()*DIMX/100, DIMY/4+DIMY/15+DIMY/200, std::to_string(m_ennemiAttaque).size()*DIMX/100, 8*DIMY/20/10, std::to_string(m_ennemiAttaque), policeTexte, couleurPoliceOr);
                if (m_saisieAttaqueEnnemi)
                    im_curseurSaisie.draw(renderer, 8*DIMX/20+DIMX/6-DIMX/80, DIMY/4+DIMY/15+DIMY/200, DIMX/100, 8*DIMY/20/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
                stat="Att";
                afficheTexte(8*DIMX/20+DIMX/6, DIMY/4+DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                // Defense
                im_barreSaisie.draw(renderer, 13*DIMX/20, DIMY/4+DIMY/15, DIMX/6, DIMY/20);
                afficheTexte(13*DIMX/20+DIMX/6-DIMX/80-std::to_string(m_ennemiDefense).size()*DIMX/100, DIMY/4+DIMY/15+DIMY/200, std::to_string(m_ennemiDefense).size()*DIMX/100, 8*DIMY/20/10, std::to_string(m_ennemiDefense), policeTexte, couleurPoliceOr);
                if (m_saisieDefenseEnnemi)
                    im_curseurSaisie.draw(renderer, 13*DIMX/20+DIMX/6-DIMX/80, DIMY/4+DIMY/15+DIMY/200, DIMX/100, 8*DIMY/20/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
                stat="Def";
                afficheTexte(13*DIMX/20+DIMX/6, DIMY/4+DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                // Experience
                im_barreSaisie.draw(renderer, 21*DIMX/40, DIMY/4+2*DIMY/15, DIMX/6, DIMY/20);
                afficheTexte(21*DIMX/40+DIMX/6-DIMX/80-std::to_string(m_ennemiExperience).size()*DIMX/100, DIMY/4+2*DIMY/15+DIMY/200, std::to_string(m_ennemiExperience).size()*DIMX/100, 8*DIMY/20/10, std::to_string(m_ennemiExperience), policeTexte, couleurPoliceOr);
                if (m_saisieExperienceEnnemi)
                    im_curseurSaisie.draw(renderer, 21*DIMX/40+DIMX/6-DIMX/80, DIMY/4+2*DIMY/15+DIMY/200, DIMX/100, 8*DIMY/20/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
                stat="Exp";
                afficheTexte(21*DIMX/40+DIMX/6, DIMY/4+2*DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                if (m_ennemiSelectionne>1)
                    im_boutonGauche.draw(renderer, DIMX/5, 41*DIMY/50, DIMX/25, 47*DIMY/400-DIMY/50);
                im_moins.draw(renderer, DIMX/2-2*9*9*DIMX/300/16-DIMX/100, 45*DIMY/50-9*DIMY/300, 9*9*DIMX/150/16, 9*DIMY/150);
                im_plus.draw(renderer, DIMX/2+DIMX/100, 45*DIMY/50-9*DIMY/300, 9*9*DIMX/150/16, 9*DIMY/150);
                if (m_ennemiSelectionne<m_pointApparitionSelectionne->getTailleListe())
                    im_boutonDroite.draw(renderer, 4*DIMX/5-DIMX/25, 41*DIMY/50, DIMX/25, 47*DIMY/400-DIMY/50);
                phrase+=std::to_string(m_ennemiSelectionne);
            }
            else {
                im_plus.draw(renderer, DIMX/2-DIMX/40, DIMY/2-16*DIMY/40/9, DIMX/20, 16*DIMY/20/9);
            }
            afficheTexte(DIMX/2-phrase.size()/2*DIMX/100, 41*DIMY/50, phrase.size()*DIMX/100, 8*DIMY/20/10, phrase, policeTexte, couleurPoliceOr);
            // Garde
            phrase="Garde";
            afficheTexte(DIMX/2-9*DIMX/40/16-DIMX/80-phrase.size()*DIMX/100, 35*DIMY/50, phrase.size()*DIMX/100, 8*DIMY/20/10, phrase, policeTexte, couleurPoliceOr);
            im_barreSaisie.draw(renderer, DIMX/2-9*DIMX/40/16, 35*DIMY/50, 9*DIMX/20/16, DIMY/20);
            if (m_garde)
                im_coche.draw(renderer, DIMX/2-9*DIMX/40/16, 35*DIMY/50, 9*DIMX/20/16, DIMY/20);
            // Temps
            phrase="R\u00E9apparition";
            afficheTexte(DIMX/2-DIMX/12-DIMX/80-tailleSansAccents(phrase)*DIMX/100, 38*DIMY/50, tailleSansAccents(phrase)*DIMX/100, 8*DIMY/20/10, phrase, policeTexte, couleurPoliceOr);
            im_barreSaisie.draw(renderer, DIMX/2-DIMX/12, 38*DIMY/50, DIMX/6, DIMY/20);
            afficheTexte(DIMX/2+DIMX/12-DIMX/80-std::to_string(m_apparitionTemps).size()*DIMX/100, 38*DIMY/50+DIMY/200, std::to_string(m_apparitionTemps).size()*DIMX/100, 8*DIMY/20/10, std::to_string(m_apparitionTemps), policeTexte, couleurPoliceOr);
            if (m_saisieTempsApparition)
                im_curseurSaisie.draw(renderer, DIMX/2+DIMX/12-DIMX/80, 38*DIMY/50+DIMY/200, DIMX/100, 8*DIMY/20/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
            phrase="s";
            afficheTexte(DIMX/2+DIMX/12, 38*DIMY/50+DIMY/200, phrase.size()*DIMX/100, 8*DIMY/20/10, phrase, policeTexte, couleurPoliceOr);
        }
        else if (m_afficheParametreCoffre) {
            im_fondEdition.draw(renderer, DIMX/8, DIMY/16, 6*DIMX/8, 14*DIMY/16);
            im_boutonRetour.draw(renderer, 4*DIMX/5-DIMX/16, DIMY/12, DIMX/16, DIMY/10);
            std::string phrase="Objet ";
            if (m_animationSaisie<2*3.14)
                    m_animationSaisie+=0.07;
            else
                m_animationSaisie=0;
            if (m_objetSelectionne>0) {
                im_fondEdition.draw(renderer, DIMX/5, DIMY/4, DIMX/8, 2*DIMY/5);
                std::string stat;
                // Nom objet
                stat=m_tableauObjets[m_coffreObjet]->getNom();
                im_barreSaisie.draw(renderer, DIMX/5, DIMY/10, DIMX/2, DIMY/20);
                afficheTexte(DIMX/5+DIMX/80, DIMY/10+DIMY/200, tailleSansAccents(stat)*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                im_boutonMonter.draw(renderer, DIMX/5+2*DIMX/40+DIMX/20-DIMX/52, DIMY/4-DIMY/20-DIMY/26, DIMX/26, DIMY/26);
                for (unsigned int i=0; i<6; i++) {
                    if (i+(m_coffrePageObjet-1)*2<m_listeObjets.size()) {
                        m_listeObjets[i+(m_coffrePageObjet-1)*2+1]->draw(renderer, DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20), DIMY/4+i/2*(DIMY/22.5+DIMY/11.25), DIMX/20, DIMY/11.25);
                        if (m_coffreObjet==i+(m_coffrePageObjet-1)*2)
                            im_selectionCarre.draw(renderer, DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20), DIMY/4+i/2*(DIMY/22.5+DIMY/11.25), DIMX/20, DIMY/11.25);
                        im_quadrillage.draw(renderer, DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20), DIMY/4+i/2*(DIMY/22.5+DIMY/11.25), DIMX/20, DIMY/11.25);
                    }
                }
                im_boutonDescendre.draw(renderer, DIMX/5+2*DIMX/40+DIMX/20-DIMX/52, DIMY/4+2*DIMY/5+DIMY/20-DIMY/26, DIMX/26, DIMY/26);
                Objet* objet=m_tableauObjets[m_coffreObjet];
                // Type
                im_barreSaisie.draw(renderer, 8*DIMX/20, DIMY/6, DIMX/6, DIMY/20);
                afficheTexte(8*DIMX/20+DIMX/6-DIMX/80-tailleSansAccents(objet->getTypeObjet())*DIMX/100, DIMY/6+DIMY/200, tailleSansAccents(objet->getTypeObjet())*DIMX/100, 8*DIMY/20/10, objet->getTypeObjet(), policeTexte, couleurPoliceOr);
                // Valeur
                im_barreSaisie.draw(renderer, 21*DIMX/40, DIMY/4, DIMX/6, DIMY/20);
                afficheTexte(21*DIMX/40+DIMX/6-DIMX/80-std::to_string(objet->getValeurObjet()).size()*DIMX/100, DIMY/4+DIMY/200, std::to_string(objet->getValeurObjet()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(objet->getValeurObjet()), policeTexte, couleurPoliceOr);
                stat="PO";
                afficheTexte(21*DIMX/40+DIMX/6, DIMY/4+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                if (objet->getTypeObjet()=="Mat\u00E9riau") {
                    Materiau* m=static_cast<Materiau*>(objet);
                    // Nombre d'exemplaires
                    im_barreSaisie.draw(renderer, 21*DIMX/40, DIMY/4+DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(21*DIMX/40+DIMX/6-DIMX/80-std::to_string(m_coffreNbExemplaireMateriau).size()*DIMX/100, DIMY/4+DIMY/15+DIMY/200, std::to_string(m_coffreNbExemplaireMateriau).size()*DIMX/100, 8*DIMY/20/10, std::to_string(m_coffreNbExemplaireMateriau), policeTexte, couleurPoliceOr);
                    if (m_saisieNbExemplaires)
                        im_curseurSaisie.draw(renderer, 21*DIMX/40+DIMX/6-DIMX/80, DIMY/4+DIMY/15+DIMY/200, DIMX/100, 8*DIMY/20/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
                    stat="Exemplaires";
                    afficheTexte(21*DIMX/40+DIMX/6, DIMY/4+DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                    im_barreSaisie.draw(renderer, 21*DIMX/40, DIMY/4+2*DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(21*DIMX/40+DIMX/6-DIMX/80-std::to_string(m->getNombreMax()).size()*DIMX/100, DIMY/4+2*DIMY/15+DIMY/200, std::to_string(m->getNombreMax()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(m->getNombreMax()), policeTexte, couleurPoliceOr);
                    stat="Max";
                    afficheTexte(21*DIMX/40+DIMX/6, DIMY/4+2*DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                }
                else if (objet->getTypeObjet()=="\u00C9quipement") {
                    Equipement* e=static_cast<Equipement*>(objet);
                    // Pv
                    im_barreSaisie.draw(renderer, 8*DIMX/20, DIMY/4+DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(8*DIMX/20+DIMX/6-DIMX/80-std::to_string(e->getPvObjet()).size()*DIMX/100, DIMY/4+DIMY/15+DIMY/200, std::to_string(e->getPvObjet()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(e->getPvObjet()), policeTexte, couleurPoliceOr);
                    stat="PV";
                    afficheTexte(8*DIMX/20+DIMX/6, DIMY/4+DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                    // Mana
                    im_barreSaisie.draw(renderer, 13*DIMX/20, DIMY/4+DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(13*DIMX/20+DIMX/6-DIMX/80-std::to_string(e->getManaObjet()).size()*DIMX/100, DIMY/4+DIMY/15+DIMY/200, std::to_string(e->getManaObjet()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(e->getManaObjet()), policeTexte, couleurPoliceOr);
                    stat="Mana";
                    afficheTexte(13*DIMX/20+DIMX/6, DIMY/4+DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                    // Defense
                    im_barreSaisie.draw(renderer, 8*DIMX/20, DIMY/4+2*DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(8*DIMX/20+DIMX/6-DIMX/80-std::to_string(e->getDefenseObjet()).size()*DIMX/100, DIMY/4+2*DIMY/15+DIMY/200, std::to_string(e->getDefenseObjet()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(e->getDefenseObjet()), policeTexte, couleurPoliceOr);
                    stat="Def";
                    afficheTexte(8*DIMX/20+DIMX/6, DIMY/4+2*DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                    // Attaque
                    im_barreSaisie.draw(renderer, 13*DIMX/20, DIMY/4+2*DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(13*DIMX/20+DIMX/6-DIMX/80-std::to_string(e->getAttaqueObjet()).size()*DIMX/100, DIMY/4+2*DIMY/15+DIMY/200, std::to_string(e->getAttaqueObjet()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(e->getAttaqueObjet()), policeTexte, couleurPoliceOr);
                    stat="Att";
                    afficheTexte(13*DIMX/20+DIMX/6, DIMY/4+2*DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                    // Experience
                    im_barreSaisie.draw(renderer, 21*DIMX/40, DIMY/4+3*DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(21*DIMX/40+DIMX/6-DIMX/80-std::to_string(e->getMultiplicateurExp()).size()*DIMX/100, DIMY/4+3*DIMY/15+DIMY/200, std::to_string(e->getMultiplicateurExp()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(e->getMultiplicateurExp()), policeTexte, couleurPoliceOr);
                    stat="% Exp";
                    afficheTexte(21*DIMX/40+DIMX/6, DIMY/4+3*DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                }
                else if (objet->getTypeObjet()=="Consommable") {
                    Consommable* c=static_cast<Consommable*>(objet);
                    // Pv
                    im_barreSaisie.draw(renderer, 8*DIMX/20, DIMY/4+DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(8*DIMX/20+DIMX/6-DIMX/80-std::to_string(c->getPvObjet()).size()*DIMX/100, DIMY/4+DIMY/15+DIMY/200, std::to_string(c->getPvObjet()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(c->getPvObjet()), policeTexte, couleurPoliceOr);
                    stat="PV";
                    afficheTexte(8*DIMX/20+DIMX/6, DIMY/4+DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                    // Mana
                    im_barreSaisie.draw(renderer, 13*DIMX/20, DIMY/4+DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(13*DIMX/20+DIMX/6-DIMX/80-std::to_string(c->getManaObjet()).size()*DIMX/100, DIMY/4+DIMY/15+DIMY/200, std::to_string(c->getManaObjet()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(c->getManaObjet()), policeTexte, couleurPoliceOr);
                    stat="Mana";
                    afficheTexte(13*DIMX/20+DIMX/6, DIMY/4+DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                    // Defense
                    im_barreSaisie.draw(renderer, 8*DIMX/20, DIMY/4+2*DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(8*DIMX/20+DIMX/6-DIMX/80-std::to_string(c->getDefenseObjet()).size()*DIMX/100, DIMY/4+2*DIMY/15+DIMY/200, std::to_string(c->getDefenseObjet()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(c->getDefenseObjet()), policeTexte, couleurPoliceOr);
                    stat="Def";
                    afficheTexte(8*DIMX/20+DIMX/6, DIMY/4+2*DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                    // Attaque
                    im_barreSaisie.draw(renderer, 13*DIMX/20, DIMY/4+2*DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(13*DIMX/20+DIMX/6-DIMX/80-std::to_string(c->getAttaqueObjet()).size()*DIMX/100, DIMY/4+2*DIMY/15+DIMY/200, std::to_string(c->getAttaqueObjet()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(c->getAttaqueObjet()), policeTexte, couleurPoliceOr);
                    stat="Att";
                    afficheTexte(13*DIMX/20+DIMX/6, DIMY/4+2*DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                    // Experience
                    im_barreSaisie.draw(renderer, 21*DIMX/40, DIMY/4+3*DIMY/15, DIMX/6, DIMY/20);
                    afficheTexte(21*DIMX/40+DIMX/6-DIMX/80-std::to_string(c->getExpBonus()).size()*DIMX/100, DIMY/4+3*DIMY/15+DIMY/200, std::to_string(c->getExpBonus()).size()*DIMX/100, 8*DIMY/20/10, std::to_string(c->getExpBonus()), policeTexte, couleurPoliceOr);
                    stat="% Exp";
                    afficheTexte(21*DIMX/40+DIMX/6, DIMY/4+3*DIMY/15+DIMY/200, stat.size()*DIMX/100, 8*DIMY/20/10, stat, policeTexte, couleurPoliceOr);
                }
                if (m_objetSelectionne>1)
                    im_boutonGauche.draw(renderer, DIMX/5, 41*DIMY/50, DIMX/25, 47*DIMY/400-DIMY/50);
                im_moins.draw(renderer, DIMX/2-2*9*9*DIMX/300/16-DIMX/100, 45*DIMY/50-9*DIMY/300, 9*9*DIMX/150/16, 9*DIMY/150);
                if (m_coffreSelectionne->getNbButin()+1<m_coffreSelectionne->getButin()->getLimite())
                    im_plus.draw(renderer, DIMX/2+DIMX/100, 45*DIMY/50-9*DIMY/300, 9*9*DIMX/150/16, 9*DIMY/150);
                if (m_objetSelectionne<m_coffreSelectionne->getNbButin())
                    im_boutonDroite.draw(renderer, 4*DIMX/5-DIMX/25, 41*DIMY/50, DIMX/25, 47*DIMY/400-DIMY/50);
                phrase+=std::to_string(m_objetSelectionne)+" / "+std::to_string(m_coffreSelectionne->getButin()->getLimite());
            }
            else {
                im_plus.draw(renderer, DIMX/2-DIMX/40, DIMY/2-16*DIMY/40/9, DIMX/20, 16*DIMY/20/9);
            }
            afficheTexte(DIMX/2-phrase.size()/2*DIMX/100, 41*DIMY/50, phrase.size()*DIMX/100, 8*DIMY/20/10, phrase, policeTexte, couleurPoliceOr);
        }
    }
    if (m_afficheSauvegarde || m_afficheChargement) {
        std::string phrase;
        im_fondEdition.draw(renderer, DIMX/5, DIMY/8, 3*DIMX/5, 6*DIMY/8);
        im_boutonRetour.draw(renderer, 4*DIMX/5-DIMX/16, DIMY/8, DIMX/16, DIMY/10);
        if (m_afficheSauvegarde)
            phrase="Sauvegarde";
        else
            phrase="Chargement";
        afficheTexte(DIMX/2-phrase.length()*DIMX/200, DIMY/3, phrase.length()*DIMX/100, 8*DIMY/10/10, phrase, policeTexte, couleurPoliceOr);
        phrase="Fichier :";
        afficheTexte(DIMX/2-DIMX/8-DIMX/100-phrase.length()*DIMX/100, DIMY/2+DIMY/20-8*DIMY/10/20, phrase.length()*DIMX/100, 8*DIMY/10/10, phrase, policeTexte, couleurPoliceOr);
        im_barreSaisie.draw(renderer, DIMX/2-DIMX/8, DIMY/2, DIMX/4, DIMY/10);
        if (m_tailleNomSauvegarde>0)
            afficheTexte(DIMX/2-9*DIMX/8/10, DIMY/2+DIMY/100, m_tailleNomSauvegarde*DIMX/100, 8*DIMY/10/10, m_nomSauvegarde, policeTexte, couleurPoliceOr);
        if (m_saisieSauvegarde)
            im_curseurSaisie.draw(renderer, DIMX/2-9*DIMX/8/10+m_tailleNomSauvegarde*DIMX/100, DIMY/2+DIMY/100, DIMX/100, 8*DIMY/10/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
        if (m_animationSaisie<2*3.14)
            m_animationSaisie+=0.07;
        else
            m_animationSaisie=0;
        if (m_afficheSauvegarde && !m_afficheMonde) {
            phrase="Nom r\u00E9gion :";
            afficheTexte(DIMX/2-DIMX/8-DIMX/100-phrase.length()*DIMX/100, DIMY/2+DIMY/10+DIMY/100+DIMY/20-8*DIMY/10/20, phrase.length()*DIMX/100, 8*DIMY/10/10, phrase, policeTexte, couleurPoliceOr);
            im_barreSaisie.draw(renderer, DIMX/2-DIMX/8, DIMY/2+DIMY/10+DIMY/100, DIMX/4, DIMY/10);
            if (m_tailleNomNiveauSauvegarde>0)
                afficheTexte(DIMX/2-9*DIMX/8/10, DIMY/2+DIMY/10+DIMY/100, m_tailleNomNiveauSauvegarde*DIMX/100, 8*DIMY/10/10, m_nomNiveauSauvegarde, policeTexte, couleurPoliceOr);
            if (m_saisieNiveauSauvegarde)
                im_curseurSaisie.draw(renderer, DIMX/2-9*DIMX/8/10+m_tailleNomNiveauSauvegarde*DIMX/100, DIMY/2+DIMY/10+2*DIMY/100, DIMX/100, 8*DIMY/10/10, (unsigned int)((sin(m_animationSaisie)+1)*255/2));
        }
        if (m_erreurChargement) {
            phrase="Nom de fichier incorrect";
            afficheTexte(DIMX/2-phrase.length()*DIMX/200, 2*DIMY/3, phrase.length()*DIMX/100, 8*DIMY/10/10, phrase, policeTexte, couleurPoliceOr);
        }
        im_bouton.draw(renderer, DIMX/2-DIMX/20, 6*DIMY/8, DIMX/10, DIMY/10);
    }
    if(m_afficheQuitter) {
        // Fond
        im_quitter.draw(renderer, DIMX/2-DIMX/6, DIMY/2-DIMY/5, 2*DIMX/6, 2*DIMY/5);
        // Surlignage ligne selectionnee
        if(m_selectBouton)
            im_selectionPetite.draw(renderer, DIMX/2 - 2*DIMX/15, DIMY/2 + DIMY/11, DIMX/6, DIMY/18);
        else
            im_selectionPetite.draw(renderer, DIMX/2 - DIMX/100, DIMY/2 + DIMY/11, DIMX/6, DIMY/18);
        phrase = "Quitter";
        afficheTexte(DIMX/2 - DIMX/20 - int(phrase.size()*DIMX/200), DIMY/2 + DIMY/11, int(phrase.size()*DIMX/100), DIMY/20, phrase, policeMenu, couleurPoliceMenu);
        phrase = "Annuler";
        afficheTexte(DIMX/2 + DIMX/13 - int(phrase.size()*DIMX/200), DIMY/2 + DIMY/11, int(phrase.size()*DIMX/100), DIMY/20, phrase, policeMenu, couleurPoliceMenu);
    }
}

void sdlEditeur::sauvegarderNiveau() {
    std::string nomFichier = "data/niveaux/";
    std::string extension = ".txt";
    nomFichier = nomFichier + m_nomSauvegarde + extension;
    std::ofstream fichier(nomFichier.c_str());
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"sdlEditeur::sauvegarderNiveau : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier.is_open());
            }
        }
    }
    if (fichier.is_open()) {  // terrain
        fichier << m_nomNiveauSauvegarde << std::endl
        << tailleX << " " << tailleY << std::endl;
        for (unsigned int j=0; j<tailleY; j++) {
            for (unsigned int i=0; i<tailleX; i++)
            {
                fichier << m_premiereCoucheTerrain[i+j*tailleX] << " " << m_deuxiemeCoucheTerrain[i+j*tailleX] << "  ";
            }
            fichier << std::endl;
        }  // pnj
        fichier << std::endl
        << m_pnj.size() << std::endl;
        for (unsigned int i=0; i<m_pnj.size(); i++) {
            fichier << m_pnj[i]->getIdPNJ() << std::endl
            << m_pnj[i]->getNomPNJ() << std::endl
            << m_pnj[i]->getPosition().x << " " << m_pnj[i]->getPosition().y << std::endl
            << m_pnj[i]->getADialogue() << " " << m_pnj[i]->getIndiceMax()+1 << std::endl;
            if (m_pnj[i]->getADialogue()) {
                for (unsigned int j=0; j<=m_pnj[i]->getIndiceMax(); j++) {
                    Dialogue* d=m_pnj[i]->getDialogue(j);
                    fichier << d->getIndiceMax()+1 << std::endl;
                    for (unsigned int k=0; k<=d->getIndiceMax(); k++) {
                        fichier << d->getPhrase(k) << std::endl;
                    }
                    fichier << std::endl;
                }
            }
        }  // points d'apparition
        fichier << std::endl
        << m_pointApparition.size() << std::endl;
        for (unsigned int i=0; i<m_pointApparition.size(); i++) {
            fichier << m_pointApparition[i]->getPosition().x << " " << m_pointApparition[i]->getPosition().y << " " << m_pointApparition[i]->getGarde()
            << " " << m_pointApparition[i]->getTempsReapparition() << " " << m_pointApparition[i]->getTailleListe() << std::endl;
            for (unsigned int j=0; j<m_pointApparition[i]->getTailleListe(); j++) {
                Ennemi* e=m_pointApparition[i]->getEnnemi(j);
                Espece* es=e->getEspece();
                fichier << es->getIdEspece() << std::endl
                << e->getNom() << std::endl
                << e->getPointsDeVieMax() << " " << e->getPointsDeManaMax() << " " << e->getAttaque() << " " << e->getDefense() << " " << e->getExp() << std::endl;
                fichier << std::endl;
            }
        }  // coffres
        fichier << std::endl
        << m_coffre.size() << std::endl;
        for (unsigned int i=0; i<m_coffre.size(); i++) {
            fichier << m_coffre[i]->getPosition().x << " " << m_coffre[i]->getPosition().y << " " << m_coffre[i]->getNbButin() << std::endl;
            Inventaire* inventaire=m_coffre[i]->getButin();
            for (unsigned int j=0; j<m_coffre[i]->getNbButin(); j++) {
                Objet* objet=inventaire->getIemeObjet(j+1);
                std::string type=objet->getTypeObjet();
                fichier << objet->getIdObjet();
                if (type=="Mat\u00E9riau") {
                    Materiau* m=static_cast<Materiau*>(objet);
                    fichier << " " << m->getNombreExemplaires();
                }
                fichier << std::endl;
            }

        }
        fichier << std::endl;
        /*
        m_coordonneesPortes.size() << std::endl;
        for (unsigned int i=0; i<m_coordonneesPortes.size(); i++) {
            fichier << m_coordonneesPortes[i][0] << " " << m_coordonneesPortes[i][1] << " " << m_coordonneesPortes[i][2] << std::endl;
        }
        fichier.close();*/
    }
}

bool sdlEditeur::chargerNiveau(std::string nom) {
    std::string nomFichier = "data/niveaux/";
    std::string extension = ".txt";
    nomFichier = nomFichier + nom + extension;
    std::ifstream fichier(nomFichier.c_str());
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                return false;
            }
        }
    }
    if (fichier.is_open()) {
        delete [] m_premiereCoucheTerrain;
        delete [] m_deuxiemeCoucheTerrain;
        std::string nom, phrase;
        getline(fichier,nom);  // sauter une ligne pour le nom du terrain
        fichier >> tailleX >> tailleY;
        m_premiereCoucheTerrain = new unsigned int[tailleX*tailleY];
        m_deuxiemeCoucheTerrain = new unsigned int[tailleX*tailleY];
        for (unsigned int j=0; j<tailleY; j++) {
            for (unsigned int i=0; i<tailleX; i++)
            {
                fichier >> m_premiereCoucheTerrain[i+j*tailleX] >> m_deuxiemeCoucheTerrain[i+j*tailleX];
            }
        }
        std::vector<PNJ*>().swap(m_pnj);
        unsigned int x, y;
        unsigned int nbPnj, id, nbDialogue, nbPhrase;
        bool aDialogue;
        fichier >> nbPnj;
        for (unsigned int i; i<nbPnj; i++) {
            getline(fichier,nom);  // sauter une ligne
            fichier >> id;
            getline(fichier,nom);  // ligne suivante
            getline(fichier,nom);  // recuperation nom
            fichier >> x >> y;
            PNJ* pnj=new PNJ(x,y,id,nom);
            fichier >> aDialogue >> nbDialogue;
            if (aDialogue) {
                for (unsigned int j=0; j<nbDialogue; j++) {
                    Dialogue* d=new Dialogue();
                    fichier >> nbPhrase;
                    getline(fichier,phrase);  // sauter une ligne
                    for (unsigned int k=0; k<nbPhrase; k++) {
                        getline(fichier,phrase);
                        d->ajouterPhrase(k,phrase);
                    }
                    pnj->ajouterDialogue(j,d);
                }
            }
            m_pnj.push_back(pnj);
        }
        std::vector<PointApparition*>().swap(m_pointApparition);
        unsigned int nbPoint, temps, nbEnnemi, pv, mana, att, def, exp;
        bool garde;
        fichier >> nbPoint;
        for (unsigned int i=0; i<nbPoint; i++) {
            fichier >> x >> y;
            PointApparition* point=new PointApparition(x,y);
            fichier >> garde >> temps >> nbEnnemi;
            point->setGarde(garde);
            point->setTempsReapparition(temps);
            for (unsigned int j=0; j<nbEnnemi; j++) {
                Ennemi* e = new Ennemi();
                fichier >> id;
                e->setEspece(m_tableauEspeces[id]);
                getline(fichier, nom); // sauter une ligne
                getline(fichier, nom); // recuperation du nom
                e->setNom(nom);
                fichier >> pv >> mana >> att >> def >> exp;
                e->setPointsDeVieMax(pv);
                e->setPointsDeManaMax(mana);
                e->setAttaque(att);
                e->setDefense(def);
                e->setExp(exp);
                point->ajouterEnnemi(e);
            }
            m_pointApparition.push_back(point);
        }
        std::vector<Coffre*>().swap(m_coffre);
        unsigned int nbCoffre, nbObjets, nbExemplaire;
        fichier >> nbCoffre;
        for (unsigned int i=0; i<nbCoffre; i++) {
            fichier >> x >> y;
            Coffre* coffre=new Coffre(x,y);
            Inventaire* inventaire=coffre->getButin();
            fichier >> nbObjets;
            for (unsigned int j=0; j<nbObjets; j++) {
                fichier >> id;
                std::string type=m_tableauObjets[id]->getTypeObjet();
                if (type=="Mat\u00E9riau") {
                    fichier >> nbExemplaire;
                    Materiau* m=static_cast<Materiau*>(m_tableauObjets[id]);
                    Materiau* materiau=new Materiau(m);
                    materiau->modifierNombreExemplaires(-materiau->getNombreExemplaires()+nbExemplaire);
                    inventaire->ajouterObjet(materiau);
                }
                else if (type=="\u00C9quipement") {
                    Equipement* e=static_cast<Equipement*>(m_tableauObjets[id]);
                    Equipement* equipement=new Equipement(e);
                    inventaire->ajouterObjet(equipement);
                }
                else if (type=="Consommable") {
                    Consommable* c=static_cast<Consommable*>(m_tableauObjets[id]);
                    Consommable* consommable=new Consommable(c);
                    inventaire->ajouterObjet(consommable);
                }
                else if (type=="Sp\u00E9cial") {
                    ObjetSpecial* s=static_cast<ObjetSpecial*>(m_tableauObjets[id]);
                    ObjetSpecial* special=new ObjetSpecial(s);
                    inventaire->ajouterObjet(special);
                }
            }
            m_coffre.push_back(coffre);
        }
        fichier.close();
    }
    return true;
}

void sdlEditeur::sauvegarderMonde() {
    std::string nomFichier = "data/niveaux/";
    std::string extension = ".txt";
    nomFichier = nomFichier + m_nomSauvegarde + extension;
    std::ofstream fichier(nomFichier.c_str());
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"sdlEditeur::sauvegarderMonde : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier.is_open());
            }
        }
    }
    if (fichier.is_open()) {
        fichier << m_niveauCoordonnees.size() << std::endl;
        for (unsigned int i=0; i<m_niveauCoordonnees.size(); i++) {
            fichier<<m_niveauNom[i]<<" "<<m_niveauVersion[i]<<" "<<m_niveauCoordonnees[i].x<<" "<<m_niveauCoordonnees[i].y<<" "<<m_niveauTaille[i].x<<" "<<m_niveauTaille[i].y<<" "<<m_niveauSorties[i].size();
            for (unsigned int j=0; j<m_niveauSorties[i].size(); j++) {
                fichier<<" "<<m_niveauSorties[i][j].x<<" "<<m_niveauSorties[i][j].y<<" "<<m_niveauDestinationsSorties[i][j].x<<" "<<m_niveauDestinationsSorties[i][j].y;
            }
            fichier<<std::endl;
        }
        fichier.close();
    }
}

bool sdlEditeur::chargerMonde(std::string nom) {
    std::string nomFichier = "data/niveaux/";
    std::string extension = ".txt";
    nomFichier = nomFichier + nom + extension;
    std::ifstream fichier(nomFichier.c_str());
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                return false;
            }
        }
    }
    if (fichier.is_open()) {
        std::vector<Point>().swap(m_niveauCoordonnees);
        std::vector<std::string>().swap(m_niveauNom);
        std::vector<std::vector<Point>>().swap(m_niveauSorties);
        std::vector<std::vector<Point>>().swap(m_niveauDestinationsSorties);
        std::vector<unsigned int>().swap(m_niveauVersion);
        unsigned int nb, version, nbSorties;
        fichier >> nb;
        std::string nom;
        Point p, p2;
        for (unsigned int i=0; i<nb; i++) {
            fichier >> nom >> version >> p.x >> p.y >> p2.x >> p2.y >> nbSorties;
            m_niveauVersion.push_back(version);
            m_niveauNom.push_back(nom);
            m_niveauCoordonnees.push_back(p);
            std::vector<Point> sorties, destinations;
            Point sortie, destination;
            for (unsigned int j=0; j<nbSorties; j++) {
                fichier >> sortie.x >> sortie.y >> destination.x >> destination.y;
                sorties.push_back(sortie);
                destinations.push_back(destination);
            }
            m_niveauSorties.push_back(sorties);
            m_niveauDestinationsSorties.push_back(destinations);
            m_niveauTaille.push_back(p2);
        }
    }
    return true;
}

void sdlEditeur::saisieTexte(int key) {
    std::string s="";
    bool doubleAjout=false;
    if (key>=SDLK_0 && key<=SDLK_9) {
        if (m_accentGrave) {
            s="\u0060";
            m_accentGrave=false;
        }
        if (m_accentCirconflexe) {
            s="\u005E";
            m_accentCirconflexe=false;
        }
        if (s!="") {
            if (m_saisieNomPnj) {
                m_nomPnj+=s;
                m_tailleNomPnj++;
            }
            else if (m_saisiePhrasePnj) {
                if (m_curseurSaisie==m_taillePhrasePnj)
                    m_phrasePnj+=s;
                else
                    m_phrasePnj.insert(indiceSansAccents(m_phrasePnj,m_curseurSaisie),s);
                m_curseurSaisie++;
                m_taillePhrasePnj++;
            }
        }
        s="";
        switch (key) {
            case SDLK_0:
                if (m_majuscule)
                    s+="0";
                else if (m_altGr)
                    s+="\u0040";
                else
                    s+="\u00E0";
                break;
            case SDLK_1:
                if (m_majuscule)
                    s+="1";
                else if (m_altGr)
                    s+="";
                else
                    s+="\u0026";
                break;
            case SDLK_2:
                if (m_majuscule)
                    s+="2";
                else if (m_altGr)
                    s+="\u007E";
                else
                    s+="\u00E9";
                break;
            case SDLK_3:
                if (m_majuscule)
                    s+="3";
                else if (m_altGr)
                    s+="\u0023";
                else
                    s+="\u0022";
                break;
            case SDLK_4:
                if (m_majuscule)
                    s+="4";
                else if (m_altGr)
                    s+="\u007B";
                else
                    s+="\u0027";
                break;
            case SDLK_5:
                if (m_majuscule)
                    s+="5";
                else if (m_altGr)
                    s+="\u005B";
                else
                    s+="\u0028";
                break;
            case SDLK_6:
                if (m_majuscule)
                    s+="6";
                else if (m_altGr)
                    s+="\u007C";
                else
                    s+="\u002D";
                break;
            case SDLK_7:
                if (m_majuscule)
                    s+="7";
                else if (m_altGr)
                    m_accentGrave=true;
                else
                    s+="\u00E8";
                break;
            case SDLK_8:
                if (m_majuscule)
                    s+="8";
                else if (m_altGr)
                    s+="\u005C";
                else
                    s+="\u005F";
                break;
            case SDLK_9:
                if (m_majuscule)
                    s+="9";
                else if (m_altGr)
                    m_accentCirconflexe=true;
                else
                    s+="\u00E7";
                break;
        }
    }
    else if (key>=SDLK_a && key<=SDLK_z) {
        if (m_accentGrave) {
            switch (key) {
                case 'a' :
                    if (m_majuscule)
                        s="\u00C0";
                    else
                        s="\u00E0";
                    break;
                case 'e' :
                    if (m_majuscule)
                        s="\u00C8";
                    else
                        s="\u00E8";
                    break;
                case 'i' :
                    if (m_majuscule)
                        s="\u00CC";
                    else
                        s="\u00EC";
                    break;
                case 'o' :
                    if (m_majuscule)
                        s="\u00D2";
                    else
                        s="\u00F2";
                    break;
                case 'u' :
                    if (m_majuscule)
                        s="\u00D9";
                    else
                        s="\u00F9";
                    break;
                case 'y' :
                    if (m_majuscule)
                        s="\u1EF1";
                    else
                        s="\u1EF2";
                    break;
                default :
                    doubleAjout=true;
                    s="\u0060";
                    if (m_saisieNomPnj)
                        m_tailleNomPnj++;
                    else if (m_saisiePhrasePnj)
                        m_taillePhrasePnj++;
                    if (m_majuscule)
                        s+=(char)key-32;
                    else
                        s+=(char)key;
            }
            m_accentGrave=false;
        }
        else if (m_accentCirconflexe) {
            switch (key) {
                case 'a' :
                    if (m_majuscule)
                        s="\u00C2";
                    else
                        s="\u00E2";
                    break;
                case 'e' :
                    if (m_majuscule)
                        s="\u00CA";
                    else
                        s="\u00EA";
                    break;
                case 'i' :
                    if (m_majuscule)
                        s="\u00CE";
                    else
                        s="\u00EE";
                    break;
                case 'o' :
                    if (m_majuscule)
                        s="\u00D4";
                    else
                        s="\u00F4";
                    break;
                case 'u' :
                    if (m_majuscule)
                        s="\u00DB";
                    else
                        s="\u00FB";
                    break;
                case 'y' :
                    if (m_majuscule)
                        s="\u0176";
                    else
                        s="\u0177";
                    break;
                default :
                    doubleAjout=true;
                    s="\u005E";
                    if (m_saisieNomPnj)
                        m_tailleNomPnj++;
                    else if (m_saisiePhrasePnj)
                        m_taillePhrasePnj++;
                    if (m_majuscule)
                        s+=(char)key-32;
                    else
                        s+=(char)key;
            }
            m_accentCirconflexe=false;
        }
        else if (m_majuscule) {
            s = (char)key-32;
        }
        else
            s = (char)key;
    }
    else if (key>=SDLK_KP_1 && key<=SDLK_KP_0) {
        if (m_alt) {
            if (key==SDLK_KP_0)
                m_combinaisonAlt+="0";
            else
                m_combinaisonAlt+=(char)49+key-SDLK_KP_1;
        }
        else {
            if (key==SDLK_KP_0)
                key=48;
            else
                key=49+key-SDLK_KP_1;
            s = (char)key;
        }
    }
    else {
        if (m_majuscule) {
            if (key==SDLK_SEMICOLON)
                key=46;
            if (key==SDLK_COMMA)
                key=63;
        }
        s = (char)key;
    }
    if (s!="") {
        if (m_saisieNomPnj) {
            m_nomPnj+=s;
            m_tailleNomPnj++;
        }
        else if (m_saisiePhrasePnj) {
            if (m_curseurSaisie==m_taillePhrasePnj)
                m_phrasePnj+=s;
            else
                m_phrasePnj.insert(indiceSansAccents(m_phrasePnj,m_curseurSaisie),s);
            m_curseurSaisie++;
            if (doubleAjout)
                m_curseurSaisie++;
            m_taillePhrasePnj++;
        }
        else if (m_saisieNiveauSauvegarde) {
            m_nomNiveauSauvegarde+=s;
            m_tailleNomNiveauSauvegarde++;
        }
    }
}

void sdlEditeur::sdlBoucle() {
    // Event handler
    SDL_Event events;
    // Boucle d'evenements
    while(!m_quitter) {
        if (m_afficheMonde) {
            cameraMonde.w = DIMX;
            cameraMonde.h = 4*DIMY/5;
            if(cameraMonde.x < 0) cameraMonde.x = 0;
            if(cameraMonde.x*tailleCase/120 > 10*DIMX*tailleCase/120 - cameraMonde.w) cameraMonde.x = std::max((10*DIMX*tailleCase/120 - cameraMonde.w)*120/tailleCase,0);
            if(cameraMonde.y < 0) cameraMonde.y = 0;
            if(cameraMonde.y*tailleCase/120 > 10*16*DIMY/9*tailleCase/120 - cameraMonde.h) cameraMonde.y = std::max((10*16*DIMY/9*tailleCase/120 - cameraMonde.h)*120/tailleCase,0);
        }
        else {
            camera.w = 4*DIMX/5;
            camera.h = 4*DIMY/5;
            // Pour eviter qu'elle ne deborde du terrain
            if(camera.x < 0) camera.x = 0;
            if(camera.x > (int)tailleX*tailleCase-camera.w) camera.x = std::max((int)tailleX*tailleCase-camera.w, 0);
            if(camera.y < 0) camera.y = 0;
            if(camera.y > (int)tailleY*tailleCase - camera.h) camera.y = std::max((int)tailleY*tailleCase - camera.h, 0);
        }

        // Affichage
        sdlAff();
        SDL_RenderPresent(renderer);
        while(SDL_PollEvent(&events) != 0) {

            switch(events.type) {
                // Touche enfoncee
                case SDL_KEYDOWN: {
                    switch (events.key.keysym.sym)
                    {
                        case SDLK_RETURN:
                            m_saisieSauvegarde=false;
                            m_saisieNomPnj=false;
                            break;
                        default:
                            int key = events.key.keysym.sym;
                            if (key == SDLK_ESCAPE && !m_afficheMenu)                           // ECHAP
                            {
                                m_afficheQuitter=!m_afficheQuitter;
                                m_selectBouton=true;
                            }
                            if (!m_majuscule && (key == SDLK_LSHIFT || key == SDLK_RSHIFT))              // MAJUSCULE
                                m_majuscule=true;
                            if (key==SDLK_CAPSLOCK)                         // CAPS LOCK
                                m_majuscule=!m_majuscule;
                            if (key == SDLK_RALT)                       // ALT GR
                                m_altGr=true;
                            if (key == SDLK_LALT)                       // ALT
                                m_alt=true;
                            if (m_saisieSauvegarde) {
                                if (key == SDLK_BACKSPACE && m_tailleNomSauvegarde>0) {
                                    m_tailleNomSauvegarde--;
                                    m_nomSauvegarde[m_tailleNomSauvegarde] = '\0';
                                }
                                else if (m_tailleNomSauvegarde<20 && ((key>=SDLK_0 && key<=SDLK_9) || (key>=SDLK_KP_1 && key<=SDLK_KP_0) || (key>=SDLK_a && key<=SDLK_z))) {
                                    if (key>=SDLK_a && key<=SDLK_z) {
                                        if (m_majuscule)
                                            m_nomSauvegarde[m_tailleNomSauvegarde] = (char)key-32;
                                        else
                                            m_nomSauvegarde[m_tailleNomSauvegarde] = (char)key;
                                    }
                                    else {
                                        if (key==SDLK_KP_0)
                                            key=48;
                                        if (key>=SDLK_KP_1)
                                            key=49+key-SDLK_KP_1;
                                        m_nomSauvegarde[m_tailleNomSauvegarde] = (char)key;
                                    }
                                    m_nomSauvegarde[m_tailleNomSauvegarde+1] = '\0';
                                    m_tailleNomSauvegarde++;
                                }
                            }
                            else if (m_saisieNiveauSauvegarde) {
                                if (key == SDLK_BACKSPACE && m_tailleNomNiveauSauvegarde>0) {
                                    m_tailleNomNiveauSauvegarde--;
                                    if (m_nomNiveauSauvegarde.size()>0 && (m_nomNiveauSauvegarde[m_nomNiveauSauvegarde.size()-1]<0 || m_nomNiveauSauvegarde[m_nomNiveauSauvegarde.size()-1]>127))
                                        m_nomNiveauSauvegarde=m_nomNiveauSauvegarde.substr(0,m_nomNiveauSauvegarde.size()-1);
                                    m_nomNiveauSauvegarde=m_nomNiveauSauvegarde.substr(0,m_nomNiveauSauvegarde.size()-1);
                                }
                                else if (m_tailleNomNiveauSauvegarde<30 && ((key>=SDLK_0 && key<=SDLK_9) || (key>=SDLK_KP_1 && key<=SDLK_KP_0) || (key>=SDLK_a && key<=SDLK_z) || key==SDLK_SPACE || key==SDLK_SEMICOLON || key==SDLK_COMMA || key==SDLK_COLON || key==SDLK_EXCLAIM)) {
                                    saisieTexte(key);
                                }
                            }
                            else if (m_saisieNomPnj) {
                                if (key == SDLK_BACKSPACE && m_tailleNomPnj>0) {
                                    m_tailleNomPnj--;
                                    if (m_nomPnj.size()>0 && (m_nomPnj[m_nomPnj.size()-1]<0 || m_nomPnj[m_nomPnj.size()-1]>127))
                                        m_nomPnj=m_nomPnj.substr(0,m_nomPnj.size()-1);
                                    m_nomPnj=m_nomPnj.substr(0,m_nomPnj.size()-1);
                                }
                                else if (m_tailleNomPnj<30 && ((key>=SDLK_0 && key<=SDLK_9) || (key>=SDLK_KP_1 && key<=SDLK_KP_0) || (key>=SDLK_a && key<=SDLK_z) || key==SDLK_SPACE || key==SDLK_SEMICOLON || key==SDLK_COMMA || key==SDLK_COLON || key==SDLK_EXCLAIM)) {
                                    saisieTexte(key);
                                }
                            }
                            else if (m_saisiePhrasePnj) {
                                if (key == SDLK_BACKSPACE && m_curseurSaisie>0) {
                                    m_curseurSaisie--;
                                    m_taillePhrasePnj--;
                                    unsigned int i=indiceSansAccents(m_phrasePnj,m_curseurSaisie);
                                    if (m_curseurSaisie>0 && (m_phrasePnj[i]<0 || m_phrasePnj[i]>127))
                                        m_phrasePnj=m_phrasePnj.erase(i,1);
                                    m_phrasePnj=m_phrasePnj.erase(i,1);
                                }
                                else if (m_taillePhrasePnj<55 && ((key>=SDLK_0 && key<=SDLK_9) || (key>=SDLK_KP_1 && key<=SDLK_KP_0) || (key>=SDLK_a && key<=SDLK_z) || key==SDLK_SPACE || key==SDLK_SEMICOLON || key==SDLK_COMMA || key==SDLK_COLON || key==SDLK_EXCLAIM)) {
                                    saisieTexte(key);
                                }
                            }
                            else if (m_saisiePvEnnemi) {
                                std::string pv=std::to_string(m_ennemiPv);
                                if (key == SDLK_BACKSPACE && pv.size()>0) {
                                    pv=pv.substr(0,pv.size()-1);
                                    if (pv.size()==0)
                                        m_ennemiPv=0;
                                    else
                                        m_ennemiPv=std::stoi(pv);
                                }
                                else if (m_ennemiPv*10<1000000 && key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                    if (key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                        if (key==SDLK_KP_0)
                                            key=48;
                                        else
                                            key=49+key-SDLK_KP_1;
                                        if (m_ennemiPv==0)
                                            pv = (char)key;
                                        else
                                            pv += (char)key;
                                        m_ennemiPv=std::stoi(pv);
                                    }
                                }
                            }
                            else if (m_saisieManaEnnemi) {
                                std::string mana=std::to_string(m_ennemiMana);
                                if (key == SDLK_BACKSPACE && mana.size()>0) {
                                    mana=mana.substr(0,mana.size()-1);
                                    if (mana.size()==0)
                                        m_ennemiMana=0;
                                    else
                                        m_ennemiMana=std::stoi(mana);
                                }
                                else if (m_ennemiMana*10<100 && key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                    if (key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                        if (key==SDLK_KP_0)
                                            key=48;
                                        else
                                            key=49+key-SDLK_KP_1;
                                        if (m_ennemiMana==0)
                                            mana = (char)key;
                                        else
                                            mana += (char)key;
                                        m_ennemiMana=std::stoi(mana);
                                    }
                                }
                            }
                            else if (m_saisieAttaqueEnnemi) {
                                std::string attaque=std::to_string(m_ennemiAttaque);
                                if (key == SDLK_BACKSPACE && attaque.size()>0) {
                                    attaque=attaque.substr(0,attaque.size()-1);
                                    if (attaque.size()==0)
                                        m_ennemiAttaque=0;
                                    else
                                        m_ennemiAttaque=std::stoi(attaque);
                                }
                                else if (m_ennemiAttaque*10<1000 && key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                    if (key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                        if (key==SDLK_KP_0)
                                            key=48;
                                        else
                                            key=49+key-SDLK_KP_1;
                                        if (m_ennemiAttaque==0)
                                            attaque = (char)key;
                                        else
                                            attaque += (char)key;
                                        m_ennemiAttaque=std::stoi(attaque);
                                    }
                                }
                            }
                            else if (m_saisieDefenseEnnemi) {
                                std::string defense=std::to_string(m_ennemiDefense);
                                if (key == SDLK_BACKSPACE && defense.size()>0) {
                                    defense=defense.substr(0,defense.size()-1);
                                    if (defense.size()==0)
                                        m_ennemiDefense=0;
                                    else
                                        m_ennemiDefense=std::stoi(defense);
                                }
                                else if (m_ennemiDefense*10<1000 && key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                    if (key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                        if (key==SDLK_KP_0)
                                            key=48;
                                        else
                                            key=49+key-SDLK_KP_1;
                                        if (m_ennemiDefense==0)
                                            defense = (char)key;
                                        else
                                            defense += (char)key;
                                        m_ennemiDefense=std::stoi(defense);
                                    }
                                }
                            }
                            else if (m_saisieExperienceEnnemi) {
                                std::string experience=std::to_string(m_ennemiExperience);
                                if (key == SDLK_BACKSPACE && experience.size()>0) {
                                    experience=experience.substr(0,experience.size()-1);
                                    if (experience.size()==0)
                                        m_ennemiExperience=0;
                                    else
                                        m_ennemiExperience=std::stoi(experience);
                                }
                                else if (m_ennemiExperience*10<1000000 && key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                    if (key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                        if (key==SDLK_KP_0)
                                            key=48;
                                        else
                                            key=49+key-SDLK_KP_1;
                                        if (m_ennemiExperience==0)
                                            experience = (char)key;
                                        else
                                            experience += (char)key;
                                        m_ennemiExperience=std::stoi(experience);
                                    }
                                }
                            }
                            else if (m_saisieTempsApparition) {
                                std::string temps=std::to_string(m_apparitionTemps);
                                if (key == SDLK_BACKSPACE && temps.size()>0) {
                                    temps=temps.substr(0,temps.size()-1);
                                    if (temps.size()==0)
                                        m_apparitionTemps=0;
                                    else
                                        m_apparitionTemps=std::stoi(temps);
                                }
                                else if (m_apparitionTemps*10<1000 && key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                    if (key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                        if (key==SDLK_KP_0)
                                            key=48;
                                        else
                                            key=49+key-SDLK_KP_1;
                                        if (m_apparitionTemps==0)
                                            temps = (char)key;
                                        else
                                            temps += (char)key;
                                        m_apparitionTemps=std::stoi(temps);
                                    }
                                }
                            }
                            else if (m_saisieTailleX) {
                                std::string x=std::to_string(tailleX);
                                if (key == SDLK_BACKSPACE && x.size()>0) {
                                    x=x.substr(0,x.size()-1);
                                    if (x.size()==0)
                                        tailleX=0;
                                    else
                                        tailleX=std::stoi(x);
                                }
                                else if (tailleX*10<101 && key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                    if (key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                        if (key==SDLK_KP_0)
                                            key=48;
                                        else
                                            key=49+key-SDLK_KP_1;
                                        if (tailleX==0)
                                            x = (char)key;
                                        else if (tailleX!=10 || key==48)
                                            x += (char)key;
                                        tailleX=std::stoi(x);
                                    }
                                }
                            }
                            else if (m_saisieTailleY) {
                                std::string y=std::to_string(tailleY);
                                if (key == SDLK_BACKSPACE && y.size()>0) {
                                    y=y.substr(0,y.size()-1);
                                    if (y.size()==0)
                                        tailleY=0;
                                    else
                                        tailleY=std::stoi(y);
                                }
                                else if (tailleY*10<101 && key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                    if (key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                        if (key==SDLK_KP_0)
                                            key=48;
                                        else
                                            key=49+key-SDLK_KP_1;
                                        if (tailleY==0)
                                            y = (char)key;
                                        else if (tailleY!=10 || key==48)
                                            y += (char)key;
                                        tailleY=std::stoi(y);
                                    }
                                }
                            }
                            else if (m_saisieNbExemplaires) {
                                std::string nb=std::to_string(m_coffreNbExemplaireMateriau);
                                Materiau* m=static_cast<Materiau*>(m_tableauObjets[m_coffreObjet]);
                                if (key == SDLK_BACKSPACE && nb.size()>0) {
                                    nb=nb.substr(0,nb.size()-1);
                                    if (nb.size()==0)
                                        m_coffreNbExemplaireMateriau=0;
                                    else
                                        m_coffreNbExemplaireMateriau=std::stoi(nb);
                                }
                                else if (m_coffreNbExemplaireMateriau*10<m->getNombreMax()+1 && key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                    if (key>=SDLK_KP_1 && key<=SDLK_KP_0) {
                                        if (key==SDLK_KP_0)
                                            key=48;
                                        else
                                            key=49+key-SDLK_KP_1;
                                        if (m_coffreNbExemplaireMateriau==0)
                                            nb = (char)key;
                                        else if (m_coffreNbExemplaireMateriau!=10 || key==48)
                                            nb += (char)key;
                                        m_coffreNbExemplaireMateriau=std::stoi(nb);
                                    }
                                }
                            }
                         break;
                     }
                }
                    break;
                case SDL_KEYUP: {
                    switch (events.key.keysym.sym)
                    {
                        case SDLK_LSHIFT:      // MAJ GAUCHE
                            m_majuscule=false;
                            break;
                        case SDLK_RSHIFT:      // MAJ DROITE
                            m_majuscule=false;
                            break;
                        case SDLK_RALT:               // ALT GR
                            m_altGr=false;
                            break;
                        case SDLK_LALT:             // ALT
                        if (m_alt && m_combinaisonAlt!="") {
                                switch (std::stoi(m_combinaisonAlt)) {
                                    case 144:
                                        if (m_saisieNomPnj && m_tailleNomPnj<30) {
                                            m_nomPnj+="\u00C9";
                                            m_tailleNomPnj++;
                                        }
                                        else if (m_saisiePhrasePnj && m_taillePhrasePnj<55) {
                                            m_phrasePnj+="\u00C9";
                                            m_taillePhrasePnj++;
                                        }
                                        break;
                                    case 128:
                                        if (m_saisieNomPnj && m_tailleNomPnj<30) {
                                            m_nomPnj+="\u00C7";
                                            m_tailleNomPnj++;
                                        }
                                        else if (m_saisiePhrasePnj && m_taillePhrasePnj<55) {
                                            m_phrasePnj+="\u00C7";
                                            m_taillePhrasePnj++;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                m_combinaisonAlt="";
                            }
                            m_alt=false;
                            break;
                        default:
                            break;
                     }
                }
                    break;
                // Mouvement de la souris
                case SDL_MOUSEMOTION :
                    if (m_clic && m_outilSelectionne==0) {
                        Point anciennePos;
                        anciennePos.x=m_posSouris.x;
                        anciennePos.y=m_posSouris.y;
                        SDL_GetMouseState(&m_posSouris.x, &m_posSouris.y);
                        if (m_afficheMonde) {
                            cameraMonde.x+=(anciennePos.x-m_posSouris.x)*120/tailleCase;
                            cameraMonde.y+=(anciennePos.y-m_posSouris.y)*120/tailleCase;
                        }
                        camera.x+=anciennePos.x-m_posSouris.x;
                        camera.y+=anciennePos.y-m_posSouris.y;
                    }
                    else if (m_niveauClic && m_outilSelectionne==4) {
                        SDL_GetMouseState(&m_posSouris.x, &m_posSouris.y);
                        int x=m_posSouris.x*120/tailleCase+cameraMonde.x;
                        int y=(m_posSouris.y-DIMY/5)*120/tailleCase+cameraMonde.y;
                        bool ok=true;
                        if (x-300>0 && x+300<10*DIMX && y-300>0 && y+300<10*16*DIMY/9) {
                            for (unsigned int i=0; i<m_niveauCoordonnees.size(); i++)
                            {
                                if (m_niveauSelectionne!=i) {
                                    int x2=m_niveauCoordonnees[i].x;
                                    int y2=m_niveauCoordonnees[i].y;
                                    if (!(x<x2-700 || x>x2+700 || y<y2-700 || y>y2+700)) {
                                        ok=false;
                                    }
                                }
                            }
                            if (ok) {
                                m_niveauCoordonnees[m_niveauSelectionne].x=x;
                                m_niveauCoordonnees[m_niveauSelectionne].y=y;
                            }
                        }

                    }
                    SDL_GetMouseState(&m_posSouris.x, &m_posSouris.y);
                    if ((m_afficheMenu || m_afficheMenuMonde) && !m_afficheQuitter) {
                        if (m_afficheMenu) {
                            for (int i=0; i<4; i++) {
                                if (m_posSouris.x>12*DIMX/40 && m_posSouris.x<28*DIMX/40 && m_posSouris.y>DIMY/5 + i*4*DIMY/19-DIMY/16 && m_posSouris.y<DIMY/5 + i*4*DIMY/19+DIMY/16)
                                    m_ligneSelectionnee=i;
                            }
                        }
                        else {
                            for (int i=0; i<2; i++) {
                                if (m_posSouris.x>12*DIMX/40 && m_posSouris.x<28*DIMX/40 && m_posSouris.y>DIMY/5 + i*4*DIMY/19-DIMY/16 && m_posSouris.y<DIMY/5 + i*4*DIMY/19+DIMY/16)
                                    m_ligneSelectionnee=i;
                            }
                        }
                    }
                    else if (m_afficheQuitter) {
                        if (m_posSouris.x>DIMX/2 - 2*DIMX/15 && m_posSouris.x<DIMX/2 - 2*DIMX/15+DIMX/8 && m_posSouris.y>DIMY/2 + DIMY/11 && m_posSouris.y<DIMY/2 + DIMY/11+DIMY/18)
                            m_selectBouton=true;
                        else if (m_posSouris.x>DIMX/2 - DIMX/100 && m_posSouris.x<DIMX/2 - DIMX/100+DIMX/8 && m_posSouris.y>DIMY/2 + DIMY/11 && m_posSouris.y<DIMY/2 + DIMY/11+DIMY/18)
                            m_selectBouton=false;
                    }
                // Clic
                case SDL_MOUSEBUTTONDOWN :
                    if (events.button.button==SDL_BUTTON_LEFT) {
                        if ((m_afficheMenu || m_afficheMenuMonde) && !m_afficheQuitter) {
                            if (m_afficheMenu) {
                                if (m_posSouris.x>DIMX/2-DIMX/24 && m_posSouris.x<DIMX/2+DIMX/24 && m_posSouris.y>DIMY/2 && m_posSouris.y<DIMY/2+DIMY/20)
                                    m_saisieTailleX=true;
                                else
                                    m_saisieTailleX=false;
                                if (m_posSouris.x>DIMX/2-DIMX/16 && m_posSouris.x<DIMX/2+DIMX/16 && m_posSouris.y>3*DIMY/5 && m_posSouris.y<3*DIMY/5+DIMY/10)
                                    m_saisieTailleY=true;
                                else
                                    m_saisieTailleY=false;
                                if (m_posSouris.x>DIMX/2-DIMX/16 && m_posSouris.x<DIMX/2+DIMX/16 && m_posSouris.y>4*DIMY/5 && m_posSouris.y<4*DIMY/5+DIMY/10)    {
                                    if (tailleX>=3 && tailleY>=3) {
                                        m_premiereCoucheTerrain = new unsigned int[tailleX*tailleY];
                                        m_deuxiemeCoucheTerrain = new unsigned int[tailleX*tailleY];
                                        for (unsigned int i=0; i<tailleX*tailleY; i++)
                                        {
                                            m_premiereCoucheTerrain[i]=0;
                                            m_deuxiemeCoucheTerrain[i]=0;
                                        }
                                        Point p;
                                        p.x=tailleX;
                                        p.y=tailleY;
                                        m_niveauTaille[m_niveauSelectionne]=p;
                                        m_afficheMenu=false;
                                    }
                                }
                            }
                            else {
                                for (int i=0; i<2; i++) {
                                    if (m_posSouris.x>12*DIMX/40 && m_posSouris.x<28*DIMX/40 && m_posSouris.y>DIMY/5 + i*4*DIMY/19-DIMY/16 && m_posSouris.y<DIMY/5 + i*4*DIMY/19+DIMY/16) {
                                        switch (m_ligneSelectionnee) {
                                            case 0:
                                                m_afficheMonde=true;
                                                break;
                                            case 1:
                                                m_afficheChargement=true;
                                                break;
                                            default:
                                                break;
                                        }
                                        m_afficheMenuMonde=false;
                                        m_afficheMonde=true;
                                    }
                                }
                            }
                        }
                        else {
                            if (m_afficheQuitter) {
                                if (((m_posSouris.x>DIMX/2 - 2*DIMX/15 && m_posSouris.x<DIMX/2 - 2*DIMX/15+DIMX/8) || (m_posSouris.x>DIMX/2 - DIMX/100 && m_posSouris.x<DIMX/2 - DIMX/100+DIMX/8))
                                 && m_posSouris.y>DIMY/2 + DIMY/11 && m_posSouris.y<DIMY/2 + DIMY/11+DIMY/18) {
                                    if (m_selectBouton) {
                                        if (m_afficheMonde || m_afficheMenuMonde)
                                            m_quitter=true;
                                        else {
                                            m_afficheSauvegarde=false;
                                            m_afficheChargement=false;
                                            m_afficheParametrePnj=false;
                                            m_afficheParametreEnnemi=false;
                                            m_afficheParametreCoffre=false;
                                            m_afficheMonde=true;
                                            m_lienSortie=false;
                                            m_lienNiveau=false;
                                            m_afficheQuitter=false;
                                            m_selectBouton=true;
                                            if (!m_niveauSauvegarde)
                                            {
                                                m_niveauCoordonnees.erase(m_niveauCoordonnees.begin()+m_niveauSelectionne);
                                                m_niveauNom.erase(m_niveauNom.begin()+m_niveauSelectionne);
                                                m_niveauSorties.erase(m_niveauSorties.begin()+m_niveauSelectionne);
                                                m_niveauDestinationsSorties.erase(m_niveauDestinationsSorties.begin()+m_niveauSelectionne);
                                                m_niveauTaille.erase(m_niveauTaille.begin()+m_niveauSelectionne);
                                                m_niveauVersion.erase(m_niveauVersion.begin()+m_niveauSelectionne);
                                            }
                                        }
                                    }
                                    else {
                                        m_afficheQuitter=false;
                                        m_selectBouton=true;
                                    }
                                 }
                            }
                            else if (!m_afficheSauvegarde && !m_afficheChargement && !m_afficheParametrePnj && !m_afficheParametreEnnemi && !m_afficheParametreCoffre) {
                                if (m_posSouris.x>0 && m_posSouris.x<DIMX/16 && m_posSouris.y>0 && m_posSouris.y<DIMY/20)
                                    m_afficheSauvegarde=true;
                                if (m_posSouris.x>DIMX/16 && m_posSouris.x<2*DIMX/16 && m_posSouris.y>0 && m_posSouris.y<DIMY/20)
                                     m_afficheChargement=true;
                                for (int i=0; i<4; i++) {
                                    if (m_posSouris.x>(i+1)*DIMX/10 && m_posSouris.x<(i+1)*DIMX/10+DIMY/15 && m_posSouris.y>DIMY/10-DIMY/30 && m_posSouris.y<DIMY/10+DIMY/30)
                                        m_outilSelectionne=i;
                                }
                                if (m_afficheMonde) {
                                    if (m_posSouris.x>5*DIMX/10 && m_posSouris.x<5*DIMX/10+DIMY/15 && m_posSouris.y>DIMY/10-DIMY/30 && m_posSouris.y<DIMY/10+DIMY/30)
                                        m_outilSelectionne=4;
                                    else if (m_posSouris.x>6*DIMX/10 && m_posSouris.x<6*DIMX/10+DIMY/15 && m_posSouris.y>DIMY/10-DIMY/30 && m_posSouris.y<DIMY/10+DIMY/30) {
                                        m_lienSortie=false;
                                        m_outilSelectionne=5;
                                    }
                                    else if (m_posSouris.x>7*DIMX/10 && m_posSouris.x<7*DIMX/10+DIMY/15 && m_posSouris.y>DIMY/10-DIMY/30 && m_posSouris.y<DIMY/10+DIMY/30) {
                                        m_lienNiveau=false;
                                        m_outilSelectionne=6;
                                    }
                                    if (m_posSouris.x>0 && m_posSouris.x<DIMX && m_posSouris.y>DIMY/5 && m_posSouris.y<DIMY) {
                                        unsigned int x=m_posSouris.x*120/tailleCase+cameraMonde.x;
                                        unsigned int y=(m_posSouris.y-DIMY/5)*120/tailleCase+cameraMonde.y;
                                        if (m_outilSelectionne==1) {
                                            bool ok=true;
                                            if (x>300 && x+300<10*(unsigned int)DIMX && y>300 && y+300<10*16*(unsigned int)DIMY/9) {
                                                for (unsigned int i=0; i<m_niveauCoordonnees.size(); i++)
                                                {
                                                    unsigned int x2=m_niveauCoordonnees[i].x;
                                                    unsigned int y2=m_niveauCoordonnees[i].y;
                                                    if (!(x+700<x2 || x>x2+700 || y+700<y2 || y>y2+700)) {
                                                        ok=false;
                                                    }
                                                }
                                                if (ok) {
                                                    Point p;
                                                    p.x=x;
                                                    p.y=y;
                                                    m_niveauCoordonnees.push_back(p);
                                                    m_niveauNom.push_back("");
                                                    m_niveauSelectionne=m_niveauCoordonnees.size()-1;
                                                    std::vector<Point> sorties;
                                                    m_niveauSorties.push_back(sorties);
                                                    m_niveauDestinationsSorties.push_back(sorties);
                                                    Point p2;
                                                    m_niveauTaille.push_back(p2);
                                                    m_niveauVersion.push_back(m_niveauVersion.size());
                                                    tailleX=0;
                                                    tailleY=0;
                                                    std::vector<PNJ*>().swap(m_pnj);
                                                    std::vector<PointApparition*>().swap(m_pointApparition);
                                                    std::vector<Coffre*>().swap(m_coffre);
                                                    m_niveauSauvegarde=false;
                                                    m_afficheMonde=false;
                                                    m_afficheMenu=true;
                                                }
                                            }
                                        }
                                        else if (m_outilSelectionne==2) {
                                            int suppression=-1;
                                            for (unsigned int i=0; i<m_niveauCoordonnees.size(); i++)
                                            {
                                                unsigned int x2=m_niveauCoordonnees[i].x;
                                                unsigned int y2=m_niveauCoordonnees[i].y;
                                                if (x>x2-m_niveauTaille[i].x/2*6+1 && x<x2+m_niveauTaille[i].x/2*6 && y>y2-m_niveauTaille[i].y/2*6+1 && y<y2+m_niveauTaille[i].y/2*6) {
                                                    suppression=i;
                                                }
                                            }
                                            if (suppression>=0) {
                                                m_niveauCoordonnees.erase(m_niveauCoordonnees.begin()+suppression);
                                                m_niveauNom.erase(m_niveauNom.begin()+suppression);
                                                m_niveauSorties.erase(m_niveauSorties.begin()+suppression);
                                                for (unsigned int i=0; i<m_niveauDestinationsSorties[suppression].size(); i++) {
                                                    Point destination=m_niveauDestinationsSorties[suppression][i];
                                                    if (destination.x!=-1) {
                                                        m_niveauDestinationsSorties[destination.x][destination.y].x=-1;
                                                        m_niveauDestinationsSorties[destination.x][destination.y].y=-1;
                                                    }
                                                }
                                                m_niveauDestinationsSorties.erase(m_niveauDestinationsSorties.begin()+suppression);
                                                m_niveauTaille.erase(m_niveauTaille.begin()+suppression);
                                                m_niveauVersion.erase(m_niveauVersion.begin()+suppression);
                                                for (unsigned int i=0; i<m_niveauVersion.size(); i++) {
                                                    if (m_niveauVersion[i]==(unsigned int)suppression)
                                                        m_niveauVersion[i]=i;
                                                    else if (m_niveauVersion[i]>(unsigned int)suppression)
                                                        m_niveauVersion[i]--;
                                                }
                                            }
                                        }
                                        else if (m_outilSelectionne==3) {
                                            for (unsigned int i=0; i<m_niveauCoordonnees.size(); i++)
                                            {
                                                unsigned int x2=m_niveauCoordonnees[i].x;
                                                unsigned int y2=m_niveauCoordonnees[i].y;
                                                if (x>x2-m_niveauTaille[i].x/2*6+1 && x<x2+m_niveauTaille[i].x/2*6 && y>y2-m_niveauTaille[i].y/2*6+1 && y<y2+m_niveauTaille[i].y/2*6) {
                                                    m_niveauSelectionne=i;
                                                    m_niveauSauvegarde=true;
                                                    chargerNiveau(m_niveauNom[i]);
                                                    m_afficheMonde=false;
                                                }
                                            }
                                        }
                                        else if (m_outilSelectionne==4) {
                                            for (unsigned int i=0; i<m_niveauCoordonnees.size(); i++)
                                            {
                                                unsigned int x2=m_niveauCoordonnees[i].x;
                                                unsigned int y2=m_niveauCoordonnees[i].y;
                                                if (x>x2-m_niveauTaille[i].x/2*6+1 && x<x2+m_niveauTaille[i].x/2*6 && y>y2-m_niveauTaille[i].y/2*6+1 && y<y2+m_niveauTaille[i].y/2*6) {
                                                    m_niveauSelectionne=i;
                                                    m_niveauClic=true;
                                                }
                                            }
                                        }
                                        else if (m_outilSelectionne==5) {
                                            bool lien=false;
                                            for (unsigned int i=0; i<m_niveauCoordonnees.size(); i++)
                                            {
                                                unsigned int x2=m_niveauCoordonnees[i].x;
                                                unsigned int y2=m_niveauCoordonnees[i].y;
                                                if (x>x2-m_niveauTaille[i].x/2*6+1 && x<x2+m_niveauTaille[i].x/2*6 && y>y2-m_niveauTaille[i].y/2*6+1 && y<y2+m_niveauTaille[i].y/2*6) {
                                                    Point tailles=m_niveauTaille[i];
                                                    for (unsigned int j=0; j<m_niveauSorties[i].size(); j++) {
                                                        Point p=m_niveauSorties[i][j];
                                                        if ((unsigned int)p.y/tailles.x==(unsigned int)p.x/tailles.x) {
                                                            if (x>=x2-tailles.x/2*6+1+6*(p.x%tailles.x) && x<=(x2-tailles.x/2*6+1+6*(p.x%tailles.x))+(p.y-p.x+1)*6 && y>=(y2-tailles.y/2*6+1+6*(unsigned int)(p.x/tailles.x)) && y<=(y2-tailles.y/2*6+1+6*(unsigned int)(p.x/tailles.x))+6) {
                                                                if (m_lienSortie && m_niveauSelectionne!=i)
                                                                {
                                                                    Point ancienneDestination = m_niveauDestinationsSorties[m_niveauSelectionne][m_sortieSelectionnee];
                                                                    if (ancienneDestination.x!=-1) {
                                                                        m_niveauDestinationsSorties[ancienneDestination.x][ancienneDestination.y].x=-1;
                                                                        m_niveauDestinationsSorties[ancienneDestination.x][ancienneDestination.y].y=-1;
                                                                        m_niveauDestinationsSorties[m_niveauSelectionne][m_sortieSelectionnee].x=-1;
                                                                        m_niveauDestinationsSorties[m_niveauSelectionne][m_sortieSelectionnee].y=-1;
                                                                    }
                                                                    Point destination;
                                                                    destination.x=i;
                                                                    destination.y=j;
                                                                    Point destination2;
                                                                    destination2.x=m_niveauSelectionne;
                                                                    destination2.y=m_sortieSelectionnee;
                                                                    Point destination3=m_niveauDestinationsSorties[destination.x][destination.y];
                                                                    if (destination3.x!=-1) {
                                                                        m_niveauDestinationsSorties[destination3.x][destination3.y].x=-1;
                                                                        m_niveauDestinationsSorties[destination3.x][destination3.y].y=-1;
                                                                    }
                                                                    m_niveauDestinationsSorties[destination.x][destination.y]=destination2;
                                                                    m_niveauDestinationsSorties[m_niveauSelectionne][m_sortieSelectionnee]=destination;
                                                                }
                                                                else {
                                                                    m_niveauSelectionne=i;
                                                                    m_sortieSelectionnee=j;
                                                                    lien=true;
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            if (x>=(x2-tailles.x/2*6+1+6*(p.x%tailles.x)) && x<=(x2-tailles.x/2*6+1+6*(p.x%tailles.x))+6 && y>=(y2-tailles.y/2*6+1+6*(unsigned int)(p.x/tailles.x)) && y<=(y2-tailles.y/2*6+1+6*(unsigned int)(p.x/tailles.x))+((p.y-p.x)/tailles.x+1)*6) {
                                                                if (m_lienSortie && m_niveauSelectionne!=i)
                                                                {
                                                                    Point ancienneDestination = m_niveauDestinationsSorties[m_niveauSelectionne][m_sortieSelectionnee];
                                                                    if (ancienneDestination.x!=-1) {
                                                                        m_niveauDestinationsSorties[ancienneDestination.x][ancienneDestination.y].x=-1;
                                                                        m_niveauDestinationsSorties[ancienneDestination.x][ancienneDestination.y].y=-1;
                                                                        m_niveauDestinationsSorties[m_niveauSelectionne][m_sortieSelectionnee].x=-1;
                                                                        m_niveauDestinationsSorties[m_niveauSelectionne][m_sortieSelectionnee].y=-1;
                                                                    }
                                                                    Point destination;
                                                                    destination.x=i;
                                                                    destination.y=j;
                                                                    Point destination2;
                                                                    destination2.x=m_niveauSelectionne;
                                                                    destination2.y=m_sortieSelectionnee;
                                                                    Point destination3=m_niveauDestinationsSorties[destination.x][destination.y];
                                                                    if (destination3.x!=-1) {
                                                                        m_niveauDestinationsSorties[destination3.x][destination3.y].x=-1;
                                                                        m_niveauDestinationsSorties[destination3.x][destination3.y].y=-1;
                                                                    }
                                                                    m_niveauDestinationsSorties[destination.x][destination.y]=destination2;
                                                                    m_niveauDestinationsSorties[m_niveauSelectionne][m_sortieSelectionnee]=destination;
                                                                }
                                                                else {
                                                                    m_niveauSelectionne=i;
                                                                    m_sortieSelectionnee=j;
                                                                    lien=true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            m_lienSortie=lien;
                                        }
                                        else if (m_outilSelectionne==6) {
                                            bool lien=false;
                                            for (unsigned int i=0; i<m_niveauCoordonnees.size(); i++)
                                            {
                                                unsigned int x2=m_niveauCoordonnees[i].x;
                                                unsigned int y2=m_niveauCoordonnees[i].y;
                                                if (x>x2-m_niveauTaille[i].x/2*6+1 && x<x2+m_niveauTaille[i].x/2*6 && y>y2-m_niveauTaille[i].y/2*6+1 && y<y2+m_niveauTaille[i].y/2*6) {
                                                    if (m_lienNiveau) {
                                                        while (m_niveauVersion[i]!=i){
                                                            i=m_niveauVersion[i];
                                                        }
                                                        m_niveauVersion[m_niveauSelectionne]=i;
                                                    }
                                                    else {
                                                        m_niveauSelectionne=i;
                                                        lien=true;
                                                    }
                                                    break;
                                                }
                                            }
                                            m_lienNiveau=lien;
                                        }
                                    }
                                }
                                else {
                                    if (m_listeEdition->size()>8+(m_page-1)*2 && m_posSouris.x>4*DIMX/5+DIMX/16 && m_posSouris.x<4*DIMX/5+DIMX/16+DIMX/13.3 && m_posSouris.y>DIMY-DIMY/20-DIMY/13.2 && m_posSouris.y<DIMY-DIMY/20)
                                        m_page++;
                                    if (m_page>1 && m_posSouris.x>4*DIMX/5+DIMX/16 && m_posSouris.x<4*DIMX/5+DIMX/16+DIMX/13.3 && m_posSouris.y>DIMY/5+DIMY/20 && m_posSouris.y<DIMY/5+DIMY/20+DIMY/13.2)
                                        m_page--;
                                    for (int i=0; i<8; i++) {
                                        if (i+(m_page-1)*2<m_listeEdition->size() && m_posSouris.x>4*DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20) && m_posSouris.x<4*DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20)+DIMX/20
                                            && m_posSouris.y>11*DIMY/30+i/2*(DIMY/22.5+DIMY/11.25) && m_posSouris.y<11*DIMY/30+i/2*(DIMY/22.5+DIMY/11.25)+DIMY/11.25)
                                            m_editionSelectionnee=i+(m_page-1)*2;
                                    }
                                    if (m_outilSelectionne==2 && m_posSouris.x>0 && m_posSouris.x<std::min(4*DIMX/5,(int)tailleX*tailleCase) && m_posSouris.y>DIMY/5 && m_posSouris.y<std::min(DIMY,DIMY/5+(int)tailleY*tailleCase)) {
                                        int caseSelectionnee=((m_posSouris.x+camera.x)/tailleCase)+((m_posSouris.y+camera.y-DIMY/5)/tailleCase)*tailleX;
                                        unsigned int c=m_deuxiemeCoucheTerrain[caseSelectionnee];
                                        if (c>m_tailleDecors+1) {
                                            int i;
                                            switch(m_interactionsAction[c-2-m_tailleDecors]) {
                                                case 0: {
                                                    i=0;
                                                    while (!((unsigned int)m_coffre[i]->getPosition().x==caseSelectionnee%tailleX && (unsigned int)m_coffre[i]->getPosition().y==caseSelectionnee/tailleX)) {
                                                        i++;
                                                    }
                                                    m_coffre.erase(m_coffre.begin()+i);
                                                    m_deuxiemeCoucheTerrain[caseSelectionnee]=0;
                                                    break;
                                                }
                                                case 1: { // Maison
                                                    SDL_Surface* s;
                                                    s=m_listeEdition3[c-2-m_tailleDecors]->getSurface();
                                                    unsigned int x, y, w,h;
                                                    unsigned int collision[4];
                                                    for (unsigned int k=0; k<4; k++)
                                                        collision[k]=m_collisionsDecors[c][k];
                                                    w=collision[2]/50;
                                                    if (collision[2]%50>0)
                                                        w++;
                                                    h=collision[3]/50;
                                                    if (collision[3]%50>0)
                                                        h++;
                                                    x=collision[0]/50;
                                                    y=s->h/50-collision[1]/50-h;
                                                    for (unsigned int k=0; k<w; k++) {
                                                        for (unsigned int l=0; l<h; l++) {
                                                            m_deuxiemeCoucheTerrain[caseSelectionnee+(k+x)-(l+y)*tailleX]=0;
                                                        }
                                                    }
                                                    unsigned int indice=0;
                                                    for (unsigned int k=0; k<m_positionsPortes.size(); k++) {
                                                        if (c==m_positionsPortes[k][0])
                                                            indice=k;
                                                    }
                                                    unsigned int p[3];
                                                    p[0]=(caseSelectionnee%tailleX)*50+m_positionsPortes[indice][1];
                                                    p[1]=caseSelectionnee/tailleX-y;   // donne la case ou se situe la porte
                                                    p[2]=(caseSelectionnee%tailleX)*50+m_positionsPortes[indice][2];
                                                    for (unsigned int k=0; k<m_coordonneesPortes.size(); k++) {
                                                        if (m_coordonneesPortes[k][0]==p[0] && m_coordonneesPortes[k][1]==p[1] && m_coordonneesPortes[k][2]==p[2])
                                                            m_coordonneesPortes.erase(m_coordonneesPortes.begin()+k);
                                                    }
                                                    break;
                                                }
                                                case 2:  // Point d'apparition
                                                    i=0;
                                                    while (!((unsigned int)m_pointApparition[i]->getPosition().x==caseSelectionnee%tailleX && (unsigned int)m_pointApparition[i]->getPosition().y==caseSelectionnee/tailleX)) {
                                                        i++;
                                                    }
                                                    m_pointApparition.erase(m_pointApparition.begin()+i);
                                                    m_deuxiemeCoucheTerrain[caseSelectionnee]=0;
                                                    break;
                                                case 3:  // PNJ
                                                    i=0;
                                                    while (!((unsigned int)m_pnj[i]->getPosition().x==caseSelectionnee%tailleX && (unsigned int)m_pnj[i]->getPosition().y==caseSelectionnee/tailleX)) {
                                                        i++;
                                                    }
                                                    m_pnj.erase(m_pnj.begin()+i);
                                                    m_deuxiemeCoucheTerrain[caseSelectionnee]=0;
                                                    break;
                                                default:
                                                    m_deuxiemeCoucheTerrain[caseSelectionnee]=0;
                                                    break;
                                            }
                                        }
                                        else
                                            m_deuxiemeCoucheTerrain[caseSelectionnee]=0;
                                    }
                                    if (m_ongletEdition!=1 && m_posSouris.x>4*DIMX/5+1 && m_posSouris.x<4*DIMX/5+DIMX/15 && m_posSouris.y>DIMY/5 && m_posSouris.y<DIMY/5+DIMY/20) {
                                        m_ongletEdition=1;
                                        m_page=1;
                                        m_editionSelectionnee=0;
                                        m_listeEdition=&m_listeEdition1;
                                    }
                                    else if (m_ongletEdition!=2 && m_posSouris.x>4*DIMX/5+1+DIMX/15 && m_posSouris.x<4*DIMX/5+1+2*DIMX/15 && m_posSouris.y>DIMY/5 && m_posSouris.y<DIMY/5+DIMY/20) {
                                        m_ongletEdition=2;
                                        m_page=1;
                                        m_editionSelectionnee=0;
                                        m_listeEdition=&m_listeEdition2;
                                    }
                                    else if (m_ongletEdition!=3 && m_posSouris.x>4*DIMX/5+2*DIMX/15 && m_posSouris.x<DIMX-1 && m_posSouris.y>DIMY/5 && m_posSouris.y<DIMY/5+DIMY/20) {
                                        m_ongletEdition=3;
                                        m_page=1;
                                        m_editionSelectionnee=0;
                                        m_listeEdition=&m_listeEdition3;
                                    }
                                }
                                m_clic=true;
                            }
                            else if (m_afficheSauvegarde) {
                                if (m_posSouris.x>4*DIMX/5-DIMX/16 && m_posSouris.x<4*DIMX/5 && m_posSouris.y>DIMY/8 && m_posSouris.y<DIMY/8+DIMY/10)
                                {
                                    m_afficheSauvegarde=false;
                                    m_nomSauvegarde[0]='\0';
                                    m_tailleNomSauvegarde=0;
                                    m_nomNiveauSauvegarde="";
                                    m_tailleNomNiveauSauvegarde=0;
                                }
                                if (m_posSouris.x>DIMX/2-DIMX/8 && m_posSouris.x<DIMX/2+DIMX/8 && m_posSouris.y>DIMY/2 && m_posSouris.y<DIMY/2+DIMY/10)
                                    m_saisieSauvegarde=true;
                                else
                                    m_saisieSauvegarde=false;
                                if (m_posSouris.x>DIMX/2-DIMX/8 && m_posSouris.x<DIMX/2+DIMX/8 && m_posSouris.y>DIMY/2+DIMY/10+DIMY/100 && m_posSouris.y<DIMY/2+DIMY/10+DIMY/100+DIMY/10)
                                    m_saisieNiveauSauvegarde=true;
                                else
                                    m_saisieNiveauSauvegarde=false;
                                if (m_posSouris.x>DIMX/2-DIMX/20 && m_posSouris.x<DIMX/2+DIMX/20 && m_posSouris.y>6*DIMY/8 && m_posSouris.y<6*DIMY/8+DIMY/10) {
                                    if (m_tailleNomSauvegarde>0) {
                                        if (m_afficheMonde) {
                                            sauvegarderMonde();
                                        }
                                        else
                                        {
                                            sauvegarderNiveau();
                                            m_niveauSauvegarde=true;
                                            m_niveauNom[m_niveauSelectionne]=m_nomSauvegarde;
                                            setSorties();
                                        }
                                        m_afficheSauvegarde=false;
                                        m_nomSauvegarde[0]='\0';
                                        m_tailleNomSauvegarde=0;
                                        m_nomNiveauSauvegarde="";
                                        m_tailleNomNiveauSauvegarde=0;
                                    }
                                }
                            }
                            else if (m_afficheChargement) {
                                if (m_posSouris.x>4*DIMX/5-DIMX/16 && m_posSouris.x<4*DIMX/5 && m_posSouris.y>DIMY/8 && m_posSouris.y<DIMY/8+DIMY/10)
                                {
                                    m_afficheChargement=false;
                                    m_nomSauvegarde[0]='\0';
                                    m_tailleNomSauvegarde=0;
                                    m_erreurChargement=false;
                                }
                                if (m_posSouris.x>DIMX/2-DIMX/8 && m_posSouris.x<DIMX/2+DIMX/8 && m_posSouris.y>DIMY/2 && m_posSouris.y<DIMY/2+DIMY/10)
                                    m_saisieSauvegarde=true;
                                else
                                    m_saisieSauvegarde=false;
                                if (m_posSouris.x>DIMX/2-DIMX/20 && m_posSouris.x<DIMX/2+DIMX/20 && m_posSouris.y>6*DIMY/8 && m_posSouris.y<6*DIMY/8+DIMY/10) {
                                    if (m_tailleNomSauvegarde>0) {
                                        m_erreurChargement=false;
                                        if (m_afficheMonde) {
                                            if (chargerMonde(m_nomSauvegarde)) {
                                                m_afficheChargement=false;
                                            }
                                            else
                                                m_erreurChargement=true;
                                        }
                                        else
                                        {
                                            if (chargerNiveau(m_nomSauvegarde)) {
                                                m_niveauSauvegarde=true;
                                                m_niveauNom[m_niveauSelectionne]=m_nomSauvegarde;
                                                Point p;
                                                p.x=tailleX;
                                                p.y=tailleY;
                                                m_niveauTaille[m_niveauSelectionne]=p;
                                                m_afficheChargement=false;
                                                setSorties();
                                            }
                                            else
                                                m_erreurChargement=true;
                                        }
                                        m_nomSauvegarde[0]='\0';
                                        m_tailleNomSauvegarde=0;
                                    }
                                }
                            }
                            else if (m_afficheParametrePnj) {
                                    // Quitter la fenetre
                                if (m_posSouris.x>4*DIMX/5-DIMX/16 && m_posSouris.x<4*DIMX/5 && m_posSouris.y>DIMY/8 && m_posSouris.y<DIMY/8+DIMY/10)
                                {
                                    m_pnjSelectionne->setNom(m_nomPnj);
                                    if (m_pnjSelectionne->getADialogue()) {
                                        Dialogue* d=m_pnjSelectionne->getDialogue(m_pnjSelectionne->getIndiceDialogue());
                                        d->setPhrase(d->getIndicePhrase(),m_phrasePnj);
                                    }
                                    m_afficheParametrePnj=false;
                                }   // Clic dans la zone texte du nom
                                if (m_posSouris.x>DIMX/8 && m_posSouris.x<DIMX/8+DIMX/3 && m_posSouris.y>DIMY/8 && m_posSouris.y<DIMY/8+DIMY/10)
                                    m_saisieNomPnj=true;
                                else
                                    m_saisieNomPnj=false;
                                    // S'il y a un dialogue au moins
                                if (m_pnjSelectionne->getADialogue()) {
                                        // Placement du curseur de saisie
                                    if (m_posSouris.x>DIMX/8+DIMX/100+DIMX/40-DIMX/100 && m_posSouris.x<DIMX/8+DIMX/100+DIMX/40+(int)m_taillePhrasePnj*DIMX/100 && m_posSouris.y>DIMY/2-4*DIMY/20/10 && m_posSouris.y<DIMY/2+4*DIMY/20/10) {
                                        m_curseurSaisie=(m_posSouris.x-(DIMX/8+DIMX/100+DIMX/40)+DIMX/200)/(DIMX/100);
                                    }   // clic dans la zone texte
                                    else if (m_posSouris.x>DIMX/8+DIMX/100 && m_posSouris.x<DIMX/8+DIMX/100+5*DIMX/8 && m_posSouris.y>2*DIMY/5+DIMY/100 && m_posSouris.y<2*DIMY/5+DIMY/100+18*DIMY/100)
                                    {
                                        m_saisiePhrasePnj=true;
                                        m_curseurSaisie=m_taillePhrasePnj;
                                    }
                                    else
                                        m_saisiePhrasePnj=false;
                                    Dialogue* d=m_pnjSelectionne->getDialogue(m_pnjSelectionne->getIndiceDialogue());
                                    if (d->getIndicePhrase()>0) {
                                            // Clic sur le bouton monter des phrases
                                        if (m_posSouris.x>12*DIMX/16 && m_posSouris.x<12*DIMX/16+9*7*DIMX/100/16 && m_posSouris.y>DIMY/5+3*DIMY/100+7*DIMY/200 && m_posSouris.y<DIMY/5+3*DIMY/100+7*DIMY/200+7*DIMY/100)
                                        {
                                            d->setPhrase(d->getIndicePhrase(),m_phrasePnj);
                                            d->reduireIndice();
                                            m_phrasePnj=d->getPhrase(d->getIndicePhrase());
                                            m_taillePhrasePnj=m_phrasePnj.length();
                                        }
                                    }       // Clic sur le bouton + haut des phrases
                                    if (m_posSouris.x>11*DIMX/16 && m_posSouris.x<11*DIMX/16+9*7*DIMX/100/16 && m_posSouris.y>DIMY/5+3*DIMY/100+7*DIMY/200 && m_posSouris.y<DIMY/5+3*DIMY/100+7*DIMY/200+7*DIMY/100)
                                    {
                                        unsigned int i=d->getIndicePhrase();
                                        if (i>0)
                                            i--;
                                        d->ajouterPhrase(d->getIndicePhrase(),"");
                                        d->augmenterIndice();
                                    }
                                    if (d->getIndiceMax()>0) {
                                            // Clic sur le bouton suppression des phrases
                                        if (m_posSouris.x>25*DIMX/32 && m_posSouris.x<25*DIMX/32+9*9*DIMX/100/16 && m_posSouris.y>2*DIMY/5+DIMY/100+9*DIMY/200 && m_posSouris.y<2*DIMY/5+DIMY/100+9*DIMY/200+9*DIMY/100)
                                        {
                                            d->deletePhrase(d->getIndicePhrase());
                                            m_phrasePnj=d->getPhrase(d->getIndicePhrase());
                                            m_taillePhrasePnj=m_phrasePnj.length();
                                        }
                                    }
                                    if (d->getIndicePhrase()<d->getIndiceMax()) {
                                            // Clic sur le bouton descendre des phrases
                                        if (m_posSouris.x>12*DIMX/16 && m_posSouris.x<12*DIMX/16+9*7*DIMX/100/16 && m_posSouris.y>3*DIMY/5+3*DIMY/100+7*DIMY/200 && m_posSouris.y<3*DIMY/5+3*DIMY/100+7*DIMY/200+7*DIMY/100)
                                        {
                                            d->setPhrase(d->getIndicePhrase(),m_phrasePnj);
                                            d->augmenterIndice();
                                            m_phrasePnj=d->getPhrase(d->getIndicePhrase());
                                            m_taillePhrasePnj=m_phrasePnj.length();
                                        }
                                    }       // Clic sur le bouton + bas des phrases
                                    if (m_posSouris.x>11*DIMX/16 && m_posSouris.x<11*DIMX/16+9*7*DIMX/100/16 && m_posSouris.y>3*DIMY/5+3*DIMY/100+7*DIMY/200 && m_posSouris.y<3*DIMY/5+3*DIMY/100+7*DIMY/200+7*DIMY/100)
                                            d->ajouterPhrase(d->getIndicePhrase()+1,"");
                                            // bouton gauche dialogue
                                    if (m_pnjSelectionne->getIndiceDialogue()>0) {
                                        if (m_posSouris.x>DIMX/5 && m_posSouris.x<DIMX/5+DIMX/25 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                            d->setPhrase(d->getIndicePhrase(),m_phrasePnj);
                                            m_pnjSelectionne->reduireIndiceDialogue();
                                            d=m_pnjSelectionne->getDialogue(m_pnjSelectionne->getIndiceDialogue());
                                            m_phrasePnj=d->getPhrase(d->getIndicePhrase());
                                            m_taillePhrasePnj=m_phrasePnj.length();
                                        }
                                    }   // bouton + dialogue gauche
                                    if (m_posSouris.x>DIMX/5+2*DIMX/25 && m_posSouris.x<DIMX/5+2*DIMX/25+9*(47*DIMX/400-DIMX/50)/16 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                        Dialogue* d2=new Dialogue("", false);
                                        unsigned int i=m_pnjSelectionne->getIndiceDialogue();
                                        if (i>0)
                                            i--;
                                        m_pnjSelectionne->ajouterDialogue(i,d2);
                                        m_pnjSelectionne->augmenterIndiceDialogue();
                                    }   // bouton suppression dialogue
                                    if (m_posSouris.x>DIMX/2-9*9*DIMX/300/16 && m_posSouris.x<DIMX/2+9*9*DIMX/300/16 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                        m_pnjSelectionne->retirerDialogue(m_pnjSelectionne->getIndiceDialogue());
                                        if (m_pnjSelectionne->getADialogue()) {
                                            d=m_pnjSelectionne->getDialogue(m_pnjSelectionne->getIndiceDialogue());
                                            m_phrasePnj=d->getPhrase(d->getIndicePhrase());
                                            m_taillePhrasePnj=m_phrasePnj.length();
                                        }
                                    }   // bouton droit dialogue
                                    if (m_pnjSelectionne->getIndiceDialogue()<m_pnjSelectionne->getIndiceMax()) {
                                        if (m_posSouris.x>4*DIMX/5-DIMX/25 && m_posSouris.x<4*DIMX/5 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                            d->setPhrase(d->getIndicePhrase(),m_phrasePnj);
                                            m_pnjSelectionne->augmenterIndiceDialogue();
                                            d=m_pnjSelectionne->getDialogue(m_pnjSelectionne->getIndiceDialogue());
                                            m_phrasePnj=d->getPhrase(d->getIndicePhrase());
                                            m_taillePhrasePnj=m_phrasePnj.length();
                                        }
                                    }   // bouton + dialogue droit
                                    if (m_posSouris.x>4*DIMX/5-3*DIMX/25 && m_posSouris.x<4*DIMX/5-3*DIMX/25+9*(47*DIMX/400-DIMX/50)/16 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                        Dialogue* d2=new Dialogue("", false);
                                        m_pnjSelectionne->ajouterDialogue(m_pnjSelectionne->getIndiceDialogue()+1,d2);
                                    }
                                }       // bouton + premier dialogue
                                else if (m_posSouris.x>DIMX/2-DIMX/40 && m_posSouris.x<DIMX/2+DIMX/40 && m_posSouris.y>DIMY/2-16*DIMY/40/9 && m_posSouris.y<DIMY/2+16*DIMY/40/9) {
                                    Dialogue* d=new Dialogue("", false);
                                    m_pnjSelectionne->ajouterDialogue(0,d);
                                }
                            }
                            else if (m_afficheParametreEnnemi) {
                                    // Quitter la fenetre
                                if (m_posSouris.x>4*DIMX/5-DIMX/16 && m_posSouris.x<4*DIMX/5 && m_posSouris.y>DIMY/8 && m_posSouris.y<DIMY/8+DIMY/10)
                                {
                                    if (m_pointApparitionSelectionne->getTailleListe()>0) {
                                        Ennemi* e=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1);
                                        e->setEspece(m_tableauEspeces[m_ennemiEspece]);
                                        e->setNom(m_tableauEspeces[m_ennemiEspece]->getNomEspece());
                                        e->setPointsDeVieMax(m_ennemiPv);
                                        e->setPointsDeManaMax(m_ennemiMana);
                                        e->setAttaque(m_ennemiAttaque);
                                        e->setDefense(m_ennemiDefense);
                                        e->setExp(m_ennemiExperience);
                                    }
                                    m_pointApparitionSelectionne->setTempsReapparition(m_apparitionTemps);
                                    m_pointApparitionSelectionne->setGarde(m_garde);
                                    m_afficheParametreEnnemi=false;
                                }
                                if (m_pointApparitionSelectionne->getTailleListe()>0) {
                                    // Espece
                                    if (m_listeEnnemis.size()>6+(m_ennemiPageEspece-1)*2 && m_posSouris.x>DIMX/5+2*DIMX/40+DIMX/20-DIMX/52 && m_posSouris.x<DIMX/5+2*DIMX/40+DIMX/20+DIMX/52 && m_posSouris.y>DIMY/4+2*DIMY/5+DIMY/20-DIMY/26 && m_posSouris.y<DIMY/4+2*DIMY/5+DIMY/20)
                                        m_ennemiPageEspece++;
                                    if (m_ennemiPageEspece>1 && m_posSouris.x>DIMX/5+2*DIMX/40+DIMX/20-DIMX/52 && m_posSouris.x<DIMX/5+2*DIMX/40+DIMX/20+DIMX/52 && m_posSouris.y>DIMY/4-DIMY/20-DIMY/26 && m_posSouris.y<DIMY/4-DIMY/20)
                                        m_ennemiPageEspece--;
                                    for (int i=0; i<6; i++) {
                                        if (i+(m_ennemiPageEspece-1)*2<m_listeEnnemis.size() && m_posSouris.x>DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20) && m_posSouris.x<DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20)+DIMX/20
                                            && m_posSouris.y>DIMY/4+i/2*(DIMY/22.5+DIMY/11.25) && m_posSouris.y<DIMY/4+i/2*(DIMY/22.5+DIMY/11.25)+DIMY/11.25) {
                                            m_ennemiEspece=i+(m_ennemiPageEspece-1)*2;
                                        }
                                    }
                                    // Pv
                                    if (m_posSouris.x>8*DIMX/20 && m_posSouris.x<8*DIMX/20+DIMX/6 && m_posSouris.y>DIMY/4 && m_posSouris.y<DIMY/4+DIMY/20)
                                    {
                                        m_saisiePvEnnemi=true;
                                    }
                                    else
                                        m_saisiePvEnnemi=false;
                                    // Mana
                                    if (m_posSouris.x>13*DIMX/20 && m_posSouris.x<13*DIMX/20+DIMX/6 && m_posSouris.y>DIMY/4 && m_posSouris.y<DIMY/4+DIMY/20)
                                    {
                                        m_saisieManaEnnemi=true;
                                    }
                                    else
                                        m_saisieManaEnnemi=false;
                                    // Attaque
                                    if (m_posSouris.x>8*DIMX/20 && m_posSouris.x<8*DIMX/20+DIMX/6 && m_posSouris.y>DIMY/4+DIMY/15 && m_posSouris.y<DIMY/4+DIMY/15+DIMY/20)
                                    {
                                        m_saisieAttaqueEnnemi=true;
                                    }
                                    else
                                        m_saisieAttaqueEnnemi=false;
                                    // Defense
                                    if (m_posSouris.x>13*DIMX/20 && m_posSouris.x<13*DIMX/20+DIMX/6 && m_posSouris.y>DIMY/4+DIMY/15 && m_posSouris.y<DIMY/4+DIMY/15+DIMY/20)
                                    {
                                        m_saisieDefenseEnnemi=true;
                                    }
                                    else
                                        m_saisieDefenseEnnemi=false;
                                    // Experience
                                    if (m_posSouris.x>21*DIMX/40 && m_posSouris.x<21*DIMX/40+DIMX/6 && m_posSouris.y>DIMY/4+2*DIMY/15 && m_posSouris.y<DIMY/4+2*DIMY/15+DIMY/20)
                                    {
                                        m_saisieExperienceEnnemi=true;
                                    }
                                    else
                                        m_saisieExperienceEnnemi=false;
                                    // bouton gauche ennemi
                                    if (m_ennemiSelectionne>1) {
                                        if (m_posSouris.x>DIMX/5 && m_posSouris.x<DIMX/5+DIMX/25 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                            Ennemi* e=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1);
                                            e->setEspece(m_tableauEspeces[m_ennemiEspece]);
                                            e->setNom(m_tableauEspeces[m_ennemiEspece]->getNomEspece());
                                            e->setPointsDeVieMax(m_ennemiPv);
                                            e->setPointsDeManaMax(m_ennemiMana);
                                            e->setAttaque(m_ennemiAttaque);
                                            e->setDefense(m_ennemiDefense);
                                            e->setExp(m_ennemiExperience);
                                            m_ennemiSelectionne--;
                                            m_ennemiEspece=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getEspece()->getIdEspece();
                                            m_ennemiPv=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getPointsDeVieMax();
                                            m_ennemiMana=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getPointsDeManaMax();
                                            m_ennemiAttaque=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getAttaque();
                                            m_ennemiDefense=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getDefense();
                                            m_ennemiExperience=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getExp();
                                        }
                                    }
                                    // bouton suppression ennemi
                                    if (m_posSouris.x>DIMX/2-9*9*DIMX/150/16-DIMX/100 && m_posSouris.x<DIMX/2-DIMX/100 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                        m_pointApparitionSelectionne->supprimerEnnemi(m_ennemiSelectionne-1);
                                        if (m_ennemiSelectionne>m_pointApparitionSelectionne->getTailleListe())
                                            m_ennemiSelectionne--;
                                        if (m_ennemiSelectionne>0) {
                                            m_ennemiEspece=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getEspece()->getIdEspece();
                                            m_ennemiPv=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getPointsDeVieMax();
                                            m_ennemiMana=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getPointsDeManaMax();
                                            m_ennemiAttaque=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getAttaque();
                                            m_ennemiDefense=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getDefense();
                                            m_ennemiExperience=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getExp();
                                        }
                                    }
                                    // bouton + ennemi
                                    if (m_posSouris.x>DIMX/2+DIMX/100 && m_posSouris.x<DIMX/2+DIMX/100+9*9*DIMX/150/16 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                        Ennemi* e=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1);
                                        e->setEspece(m_tableauEspeces[m_ennemiEspece]);
                                        e->setNom(m_tableauEspeces[m_ennemiEspece]->getNomEspece());
                                        e->setPointsDeVieMax(m_ennemiPv);
                                        e->setPointsDeManaMax(m_ennemiMana);
                                        e->setAttaque(m_ennemiAttaque);
                                        e->setDefense(m_ennemiDefense);
                                        e->setExp(m_ennemiExperience);
                                        m_pointApparitionSelectionne->ajouterEnnemi(new Ennemi());
                                        m_ennemiSelectionne=m_pointApparitionSelectionne->getTailleListe();
                                        m_ennemiEspece=0;
                                        m_ennemiPv=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getPointsDeVieMax();
                                        m_ennemiMana=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getPointsDeManaMax();
                                        m_ennemiAttaque=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getAttaque();
                                        m_ennemiDefense=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getDefense();
                                        m_ennemiExperience=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getExp();
                                    }
                                    // bouton droit ennemi
                                    if (m_ennemiSelectionne<m_pointApparitionSelectionne->getTailleListe()) {
                                        if (m_posSouris.x>4*DIMX/5-DIMX/25 && m_posSouris.x<4*DIMX/5 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                            Ennemi* e=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1);
                                            e->setEspece(m_tableauEspeces[m_ennemiEspece]);
                                            e->setNom(m_tableauEspeces[m_ennemiEspece]->getNomEspece());
                                            e->setPointsDeVieMax(m_ennemiPv);
                                            e->setPointsDeManaMax(m_ennemiMana);
                                            e->setAttaque(m_ennemiAttaque);
                                            e->setDefense(m_ennemiDefense);
                                            e->setExp(m_ennemiExperience);
                                            m_ennemiSelectionne++;
                                            m_ennemiEspece=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getEspece()->getIdEspece();
                                            m_ennemiPv=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getPointsDeVieMax();
                                            m_ennemiMana=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getPointsDeManaMax();
                                            m_ennemiAttaque=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getAttaque();
                                            m_ennemiDefense=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getDefense();
                                            m_ennemiExperience=m_pointApparitionSelectionne->getEnnemi(m_ennemiSelectionne-1)->getExp();
                                        }
                                    }
                                } // bouton + premier ennemi
                                else if (m_posSouris.x>DIMX/2-DIMX/40 && m_posSouris.x<DIMX/2+DIMX/40 && m_posSouris.y>DIMY/2-16*DIMY/40/9 && m_posSouris.y<DIMY/2+16*DIMY/40/9) {
                                    m_pointApparitionSelectionne->ajouterEnnemi(new Ennemi());
                                    m_ennemiSelectionne=1;
                                    m_ennemiEspece=0;
                                    m_ennemiPv=m_pointApparitionSelectionne->getEnnemi(0)->getPointsDeVieMax();
                                    m_ennemiMana=m_pointApparitionSelectionne->getEnnemi(0)->getPointsDeManaMax();
                                    m_ennemiAttaque=m_pointApparitionSelectionne->getEnnemi(0)->getAttaque();
                                    m_ennemiDefense=m_pointApparitionSelectionne->getEnnemi(0)->getDefense();
                                    m_ennemiExperience=m_pointApparitionSelectionne->getEnnemi(0)->getExp();
                                } // garde
                                if (m_posSouris.x>DIMX/2-9*DIMX/40/16 && m_posSouris.x<DIMX/2+9*DIMX/40/16 && m_posSouris.y>35*DIMY/50 && m_posSouris.y<35*DIMY/50+DIMY/20)
                                {
                                    m_garde=!m_garde;
                                }
                                // temps
                                if (m_posSouris.x>DIMX/2-DIMX/12 && m_posSouris.x<DIMX/2+DIMX/12 && m_posSouris.y>38*DIMY/50 && m_posSouris.y<38*DIMY/50+DIMY/20)
                                {
                                    m_saisieTempsApparition=true;
                                }
                                else
                                    m_saisieTempsApparition=false;
                            }
                            else if (m_afficheParametreCoffre) {
                                    // Quitter la fenetre
                                if (m_posSouris.x>4*DIMX/5-DIMX/16 && m_posSouris.x<4*DIMX/5 && m_posSouris.y>DIMY/8 && m_posSouris.y<DIMY/8+DIMY/10)
                                {
                                    if (m_objetSelectionne>0) {
                                        Inventaire* inventaire=m_coffreSelectionne->getButin();
                                        Objet* objet=inventaire->getIemeObjet(m_objetSelectionne);
                                        if (objet!=NULL) {
                                            delete objet;
                                            inventaire->retirerIemeObjet(m_objetSelectionne);
                                        }
                                        std::string typeObjet=m_tableauObjets[m_coffreObjet]->getTypeObjet();
                                        if (typeObjet=="Mat\u00E9riau") {
                                            Materiau* m=static_cast<Materiau*>(m_tableauObjets[m_coffreObjet]);
                                            Materiau* materiau=new Materiau(m);
                                            materiau->modifierNombreExemplaires(-materiau->getNombreExemplaires()+m_coffreNbExemplaireMateriau);
                                            inventaire->ajouterObjet(materiau);
                                        }
                                        else if (typeObjet=="\u00C9quipement") {
                                            Equipement* e=static_cast<Equipement*>(m_tableauObjets[m_coffreObjet]);
                                            Equipement* equipement=new Equipement(e);
                                            inventaire->ajouterObjet(equipement);
                                        }
                                        else if (typeObjet=="Consommable") {
                                            Consommable* c=static_cast<Consommable*>(m_tableauObjets[m_coffreObjet]);
                                            Consommable* consommable=new Consommable(c);
                                            inventaire->ajouterObjet(consommable);
                                        }
                                        else if (typeObjet=="Sp\u00E9cial") {
                                            ObjetSpecial* s=static_cast<ObjetSpecial*>(m_tableauObjets[m_coffreObjet]);
                                            ObjetSpecial* special=new ObjetSpecial(s);
                                            inventaire->ajouterObjet(special);
                                        }
                                    }
                                    m_afficheParametreCoffre=false;
                                }
                                if (m_objetSelectionne>0) {
                                    // Choix objet
                                    if (m_listeObjets.size()>6+(m_coffrePageObjet-1)*2 && m_posSouris.x>DIMX/5+2*DIMX/40+DIMX/20-DIMX/52 && m_posSouris.x<DIMX/5+2*DIMX/40+DIMX/20+DIMX/52 && m_posSouris.y>DIMY/4+2*DIMY/5+DIMY/20-DIMY/26 && m_posSouris.y<DIMY/4+2*DIMY/5+DIMY/20)
                                        m_coffrePageObjet++;
                                    if (m_coffrePageObjet>1 && m_posSouris.x>DIMX/5+2*DIMX/40+DIMX/20-DIMX/52 && m_posSouris.x<DIMX/5+2*DIMX/40+DIMX/20+DIMX/52 && m_posSouris.y>DIMY/4-DIMY/20-DIMY/26 && m_posSouris.y<DIMY/4-DIMY/20)
                                        m_coffrePageObjet--;
                                    for (int i=0; i<6; i++) {
                                        if (i+(m_coffrePageObjet-1)*2<m_listeObjets.size() && m_posSouris.x>DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20) && m_posSouris.x<DIMX/5+DIMX/40+i%2*(DIMX/20+DIMX/20)+DIMX/20
                                            && m_posSouris.y>DIMY/4+i/2*(DIMY/22.5+DIMY/11.25) && m_posSouris.y<DIMY/4+i/2*(DIMY/22.5+DIMY/11.25)+DIMY/11.25) {
                                            m_coffreObjet=i+(m_coffrePageObjet-1)*2;
                                            m_coffreNbExemplaireMateriau=1;
                                        }
                                    }
                                    // Nombre exemplaire
                                    if (m_posSouris.x>21*DIMX/40 && m_posSouris.x<21*DIMX/40+DIMX/6 && m_posSouris.y>DIMY/4+DIMY/15 && m_posSouris.y<DIMY/4+DIMY/15+DIMY/20)
                                    {
                                        m_saisieNbExemplaires=true;
                                    }
                                    else
                                        m_saisieNbExemplaires=false;
                                    // bouton gauche objet
                                    if (m_objetSelectionne>1) {
                                        if (m_posSouris.x>DIMX/5 && m_posSouris.x<DIMX/5+DIMX/25 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                            Inventaire* inventaire=m_coffreSelectionne->getButin();
                                            Objet* objet=inventaire->getIemeObjet(m_objetSelectionne);
                                            if (objet!=NULL) {
                                                delete objet;
                                                inventaire->retirerIemeObjet(m_objetSelectionne);
                                            }
                                            std::string typeObjet=m_tableauObjets[m_coffreObjet]->getTypeObjet();
                                            if (typeObjet=="Mat\u00E9riau") {
                                                Materiau* m=static_cast<Materiau*>(m_tableauObjets[m_coffreObjet]);
                                                Materiau* materiau=new Materiau(m);
                                                materiau->modifierNombreExemplaires(-materiau->getNombreExemplaires()+m_coffreNbExemplaireMateriau);
                                                inventaire->ajouterObjet(materiau);
                                            }
                                            else if (typeObjet=="\u00C9quipement") {
                                                Equipement* e=static_cast<Equipement*>(m_tableauObjets[m_coffreObjet]);
                                                Equipement* equipement=new Equipement(e);
                                                inventaire->ajouterObjet(equipement);
                                            }
                                            else if (typeObjet=="Consommable") {
                                                Consommable* c=static_cast<Consommable*>(m_tableauObjets[m_coffreObjet]);
                                                Consommable* consommable=new Consommable(c);
                                                inventaire->ajouterObjet(consommable);
                                            }
                                            else if (typeObjet=="Sp\u00E9cial") {
                                                ObjetSpecial* s=static_cast<ObjetSpecial*>(m_tableauObjets[m_coffreObjet]);
                                                ObjetSpecial* special=new ObjetSpecial(s);
                                                inventaire->ajouterObjet(special);
                                            }
                                            m_objetSelectionne--;
                                            m_coffreObjet=inventaire->getIemeObjet(m_objetSelectionne)->getIdObjet();
                                            typeObjet=m_tableauObjets[m_coffreObjet]->getTypeObjet();
                                            if (typeObjet=="Mat\u00E9riau") {
                                                Materiau* m=static_cast<Materiau*>(inventaire->getIemeObjet(m_objetSelectionne));
                                                m_coffreNbExemplaireMateriau=m->getNombreExemplaires();
                                            }
                                        }
                                    }
                                    // bouton suppression objet
                                    if (m_posSouris.x>DIMX/2-9*9*DIMX/150/16-DIMX/100 && m_posSouris.x<DIMX/2-DIMX/100 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                        Inventaire* inventaire=m_coffreSelectionne->getButin();
                                        Objet* objet=inventaire->getIemeObjet(m_objetSelectionne);
                                        if (objet!=NULL) {
                                            delete objet;
                                            inventaire->retirerIemeObjet(m_objetSelectionne);
                                        }
                                        if (m_objetSelectionne>m_coffreSelectionne->getNbButin())
                                            m_objetSelectionne--;
                                        if (m_objetSelectionne>0) {
                                            m_coffreObjet=inventaire->getIemeObjet(m_objetSelectionne)->getIdObjet();
                                            std::string typeObjet=m_tableauObjets[m_coffreObjet]->getTypeObjet();
                                            if (typeObjet=="Mat\u00E9riau") {
                                                Materiau* m=static_cast<Materiau*>(inventaire->getIemeObjet(m_objetSelectionne));
                                                m_coffreNbExemplaireMateriau=m->getNombreExemplaires();
                                            }
                                        }
                                    }
                                    // bouton + objet
                                    if (m_coffreSelectionne->getNbButin()+1<m_coffreSelectionne->getButin()->getLimite() && m_posSouris.x>DIMX/2+DIMX/100 && m_posSouris.x<DIMX/2+DIMX/100+9*9*DIMX/150/16 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                        Inventaire* inventaire=m_coffreSelectionne->getButin();
                                        Objet* objet=inventaire->getIemeObjet(m_objetSelectionne);
                                        if (objet!=NULL) {
                                            delete objet;
                                            inventaire->retirerIemeObjet(m_objetSelectionne);
                                        }
                                        std::string typeObjet=m_tableauObjets[m_coffreObjet]->getTypeObjet();
                                        if (typeObjet=="Mat\u00E9riau") {
                                            Materiau* m=static_cast<Materiau*>(m_tableauObjets[m_coffreObjet]);
                                            Materiau* materiau=new Materiau(m);
                                            materiau->modifierNombreExemplaires(-materiau->getNombreExemplaires()+m_coffreNbExemplaireMateriau);
                                            inventaire->ajouterObjet(materiau);
                                        }
                                        else if (typeObjet=="\u00C9quipement") {
                                            Equipement* e=static_cast<Equipement*>(m_tableauObjets[m_coffreObjet]);
                                            Equipement* equipement=new Equipement(e);
                                            inventaire->ajouterObjet(equipement);
                                        }
                                        else if (typeObjet=="Consommable") {
                                            Consommable* c=static_cast<Consommable*>(m_tableauObjets[m_coffreObjet]);
                                            Consommable* consommable=new Consommable(c);
                                            inventaire->ajouterObjet(consommable);
                                        }
                                        else if (typeObjet=="Sp\u00E9cial") {
                                            ObjetSpecial* s=static_cast<ObjetSpecial*>(m_tableauObjets[m_coffreObjet]);
                                            ObjetSpecial* special=new ObjetSpecial(s);
                                            inventaire->ajouterObjet(special);
                                        }
                                        m_objetSelectionne=inventaire->getNbObjet()+1;
                                        m_coffreObjet=0;
                                        m_coffreNbExemplaireMateriau=1;
                                    }
                                    // bouton droit objet
                                    if (m_objetSelectionne<m_coffreSelectionne->getNbButin()) {
                                        if (m_posSouris.x>4*DIMX/5-DIMX/25 && m_posSouris.x<4*DIMX/5 && m_posSouris.y>41*DIMY/50 && m_posSouris.y<41*DIMY/50+47*DIMY/400-DIMY/50) {
                                            Inventaire* inventaire=m_coffreSelectionne->getButin();
                                            Objet* objet=inventaire->getIemeObjet(m_objetSelectionne);
                                            if (objet!=NULL) {
                                                delete objet;
                                                inventaire->retirerIemeObjet(m_objetSelectionne);
                                            }
                                            std::string typeObjet=m_tableauObjets[m_coffreObjet]->getTypeObjet();
                                            if (typeObjet=="Mat\u00E9riau") {
                                                Materiau* m=static_cast<Materiau*>(m_tableauObjets[m_coffreObjet]);
                                                Materiau* materiau=new Materiau(m);
                                                materiau->modifierNombreExemplaires(-materiau->getNombreExemplaires()+m_coffreNbExemplaireMateriau);
                                                inventaire->ajouterObjet(materiau);
                                            }
                                            else if (typeObjet=="\u00C9quipement") {
                                                Equipement* e=static_cast<Equipement*>(m_tableauObjets[m_coffreObjet]);
                                                Equipement* equipement=new Equipement(e);
                                                inventaire->ajouterObjet(equipement);
                                            }
                                            else if (typeObjet=="Consommable") {
                                                Consommable* c=static_cast<Consommable*>(m_tableauObjets[m_coffreObjet]);
                                                Consommable* consommable=new Consommable(c);
                                                inventaire->ajouterObjet(consommable);
                                            }
                                            else if (typeObjet=="Sp\u00E9cial") {
                                                ObjetSpecial* s=static_cast<ObjetSpecial*>(m_tableauObjets[m_coffreObjet]);
                                                ObjetSpecial* special=new ObjetSpecial(s);
                                                inventaire->ajouterObjet(special);
                                            }
                                            m_objetSelectionne++;
                                            m_coffreObjet=inventaire->getIemeObjet(m_objetSelectionne)->getIdObjet();
                                            typeObjet=m_tableauObjets[m_coffreObjet]->getTypeObjet();
                                            if (typeObjet=="Mat\u00E9riau") {
                                                Materiau* m=static_cast<Materiau*>(inventaire->getIemeObjet(m_objetSelectionne));
                                                m_coffreNbExemplaireMateriau=m->getNombreExemplaires();
                                            }
                                        }
                                    }
                                } // bouton + premier objet
                                else if (m_posSouris.x>DIMX/2-DIMX/40 && m_posSouris.x<DIMX/2+DIMX/40 && m_posSouris.y>DIMY/2-16*DIMY/40/9 && m_posSouris.y<DIMY/2+16*DIMY/40/9) {
                                    m_objetSelectionne=1;
                                    m_coffreObjet=0;
                                    m_coffreNbExemplaireMateriau=1;
                                }
                            }
                        }
                        if (!m_afficheMenu && !m_afficheMenuMonde  && !m_afficheSauvegarde && !m_afficheChargement && !m_afficheParametrePnj && !m_afficheParametreEnnemi && !m_afficheParametreCoffre) {
                            if (m_outilSelectionne==1 && m_posSouris.x>0 && m_posSouris.x<std::min(4*DIMX/5,(int)tailleX*tailleCase) && m_posSouris.y>DIMY/5 && m_posSouris.y<std::min(DIMY,DIMY/5+(int)tailleY*tailleCase)) {
                                unsigned int caseSelectionnee=((m_posSouris.x+camera.x)/tailleCase)+((m_posSouris.y+camera.y-DIMY/5)/tailleCase)*tailleX;
                                switch (m_ongletEdition) {
                                    case 1:
                                        m_premiereCoucheTerrain[caseSelectionnee]=m_editionSelectionnee;
                                        break;
                                    case 2:
                                        if (m_deuxiemeCoucheTerrain[caseSelectionnee]==0) {
                                            m_deuxiemeCoucheTerrain[caseSelectionnee]=m_editionSelectionnee+2;   // le 0 et le 1 sont pris pour le vide
                                        }
                                        break;
                                    case 3: {
                                        bool libre=true;
                                        if (m_deuxiemeCoucheTerrain[caseSelectionnee]==0) {
                                            switch (m_interactionsAction[m_editionSelectionnee]) {
                                                case 0: { // Coffre
                                                    m_deuxiemeCoucheTerrain[caseSelectionnee]=m_editionSelectionnee+2+m_tailleDecors;
                                                    m_coffre.push_back(new Coffre(caseSelectionnee%tailleX, caseSelectionnee/tailleX));
                                                    break;
                                                }
                                                case 1: {  // Maison
                                                    SDL_Surface* s;
                                                    s=m_listeEdition3[m_editionSelectionnee]->getSurface();
                                                    unsigned int x, y, w,h;
                                                    unsigned int collision[4];
                                                    for (unsigned int k=0; k<4; k++)
                                                        collision[k]=m_collisionsDecors[m_editionSelectionnee+m_tailleDecors+2][k];
                                                    w=collision[2]/50;
                                                    if (collision[2]%50>0)
                                                        w++;
                                                    h=collision[3]/50;
                                                    if (collision[3]%50>0)
                                                        h++;
                                                    x=collision[0]/50;
                                                    y=s->h/50-collision[1]/50-h;
                                                    if (caseSelectionnee<(h-1)*tailleX  || (caseSelectionnee+w-1)%tailleX<=caseSelectionnee%tailleX || caseSelectionnee+w>tailleX*tailleY)  // Depassement haut/ligne/terrain
                                                        libre=false;
                                                    if (libre) {
                                                         for (unsigned int i=0; i<w; i++) {
                                                            for (unsigned int j=0; j<h; j++) {
                                                                if (m_premiereCoucheTerrain[caseSelectionnee+(i+x)-(j+y)*tailleX]==0 || m_deuxiemeCoucheTerrain[caseSelectionnee+(i+x)-(j+y)*tailleX]!=0)
                                                                    libre=false;
                                                            }
                                                        }
                                                    }
                                                    if (libre) {
                                                        for (unsigned int i=0; i<w; i++) {
                                                            for (unsigned int j=0; j<h; j++) {
                                                                m_deuxiemeCoucheTerrain[caseSelectionnee+(i+x)-(j+y)*tailleX]=1;
                                                            }
                                                        }
                                                        m_deuxiemeCoucheTerrain[caseSelectionnee]=m_editionSelectionnee+2+m_tailleDecors;
                                                    }
                                                    unsigned int indice=0;
                                                    for (unsigned int i=0; i<m_positionsPortes.size(); i++) {
                                                        if (m_editionSelectionnee+m_tailleDecors+2==m_positionsPortes[i][0])
                                                            indice=i;
                                                    }
                                                    unsigned int* c=new unsigned int[3];
                                                    c[0]=(caseSelectionnee%tailleX)*50+m_positionsPortes[indice][1];
                                                    c[1]=caseSelectionnee/tailleX-y;   // donne la case ou se situe la porte
                                                    c[2]=(caseSelectionnee%tailleX)*50+m_positionsPortes[indice][2];
                                                    m_coordonneesPortes.push_back(c);
                                                    break;
                                                }
                                                case 2:   // Point d'apparition
                                                    m_pointApparition.push_back(new PointApparition(caseSelectionnee%tailleX, caseSelectionnee/tailleX));
                                                    m_deuxiemeCoucheTerrain[caseSelectionnee]=m_editionSelectionnee+2+m_tailleDecors;
                                                    break;
                                                case 3:   // PNJ
                                                    m_pnj.push_back(new PNJ(caseSelectionnee%tailleX, caseSelectionnee/tailleX, m_editionSelectionnee));
                                                    m_deuxiemeCoucheTerrain[caseSelectionnee]=m_editionSelectionnee+2+m_tailleDecors;
                                                    break;
                                                default:
                                                    m_deuxiemeCoucheTerrain[caseSelectionnee]=m_editionSelectionnee+2+m_tailleDecors;   // le 0 est pris pour le vide
                                                    break;
                                            }
                                        }
                                        break;
                                    }
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                    break;
                case SDL_MOUSEBUTTONUP :
                    if (events.button.button==SDL_BUTTON_LEFT) {
                        m_clic=false;
                        m_niveauClic=false;
                        if (!m_afficheMenu && !m_afficheMenuMonde  && !m_afficheSauvegarde && !m_afficheChargement && !m_afficheParametrePnj && !m_afficheParametreEnnemi && !m_afficheParametreCoffre) {
                            if (m_outilSelectionne==3 && m_posSouris.x>0 && m_posSouris.x<std::min(4*DIMX/5,(int)tailleX*tailleCase) && m_posSouris.y>DIMY/5 && m_posSouris.y<std::min(DIMY,DIMY/5+(int)tailleY*tailleCase)) {
                                int caseSelectionnee=((m_posSouris.x+camera.x)/tailleCase)+((m_posSouris.y+camera.y-DIMY/5)/tailleCase)*tailleX;
                                unsigned int c=m_deuxiemeCoucheTerrain[caseSelectionnee];
                                if (c>m_tailleDecors+1 && m_interactionsAction[c-2-m_tailleDecors]==3) {  // on v�rifie qu'on a une case d'interaction et que cette interaction est celle des pnj (7)
                                    m_afficheParametrePnj=true;
                                    unsigned int i=0;
                                    while (!((unsigned int)m_pnj[i]->getPosition().x==caseSelectionnee%tailleX && (unsigned int)m_pnj[i]->getPosition().y==caseSelectionnee/tailleX)) {
                                        i++;
                                    }
                                    m_pnjSelectionne=m_pnj[i];
                                    m_nomPnj=m_pnjSelectionne->getNomPNJ();
                                    m_tailleNomPnj=m_nomPnj.length();
                                    if (m_pnjSelectionne->getADialogue()) {
                                        Dialogue* d=m_pnjSelectionne->getDialogue(m_pnjSelectionne->getIndiceDialogue());
                                        m_phrasePnj=d->getPhrase(d->getIndicePhrase());
                                        m_taillePhrasePnj=m_phrasePnj.length();
                                    }
                                    else {
                                        m_phrasePnj="";
                                        m_taillePhrasePnj=0;
                                    }
                                }
                                else if (c>m_tailleDecors+1 && m_interactionsAction[c-2-m_tailleDecors]==2) {  // case de point d'apparition
                                    m_afficheParametreEnnemi=true;
                                    unsigned int i=0;
                                    while (!((unsigned int)m_pointApparition[i]->getPosition().x==caseSelectionnee%tailleX && (unsigned int)m_pointApparition[i]->getPosition().y==caseSelectionnee/tailleX)) {
                                        i++;
                                    }
                                    m_pointApparitionSelectionne=m_pointApparition[i];
                                    if (m_pointApparitionSelectionne->getTailleListe()>0) {
                                        m_ennemiSelectionne=1;
                                        m_ennemiEspece=m_pointApparitionSelectionne->getEnnemi(0)->getEspece()->getIdEspece();
                                        m_ennemiPv=m_pointApparitionSelectionne->getEnnemi(0)->getPointsDeVieMax();
                                        m_ennemiMana=m_pointApparitionSelectionne->getEnnemi(0)->getPointsDeManaMax();
                                        m_ennemiAttaque=m_pointApparitionSelectionne->getEnnemi(0)->getAttaque();
                                        m_ennemiDefense=m_pointApparitionSelectionne->getEnnemi(0)->getDefense();
                                        m_ennemiExperience=m_pointApparitionSelectionne->getEnnemi(0)->getExp();
                                    }
                                    m_apparitionTemps=m_pointApparitionSelectionne->getTempsReapparition();
                                    m_garde=m_pointApparitionSelectionne->getGarde();
                                    m_ennemiPageEspece=1;
                                }
                                else if (c>m_tailleDecors+1 && m_interactionsAction[c-2-m_tailleDecors]==0) {  // case de coffre
                                    m_afficheParametreCoffre=true;
                                    unsigned int i=0;
                                    while (!((unsigned int)m_coffre[i]->getPosition().x==caseSelectionnee%tailleX && (unsigned int)m_coffre[i]->getPosition().y==caseSelectionnee/tailleX)) {
                                        i++;
                                    }
                                    m_coffreSelectionne=m_coffre[i];
                                    m_objetSelectionne=0;
                                    if (m_coffreSelectionne->getNbButin()>0) {
                                        m_objetSelectionne=1;
                                        Objet* objet=m_coffreSelectionne->getObjet(0);
                                        if (objet->getTypeObjet()=="Mat\u00E9riau") {
                                            Materiau* m=static_cast<Materiau*>(objet);
                                            m_coffreNbExemplaireMateriau=m->getNombreExemplaires();
                                        }
                                        m_coffreObjet=m_coffreSelectionne->getObjet(0)->getIdObjet();
                                    }
                                    m_coffrePageObjet=1;
                                }
                            }
                        }
                    }
                    break;
                case SDL_MOUSEWHEEL :
                    if (!m_afficheMenu && !m_afficheMenuMonde && !m_afficheSauvegarde && !m_afficheChargement && !m_afficheParametrePnj && !m_afficheParametreEnnemi && !m_afficheParametreCoffre) {
                        if(events.wheel.y > 0)
                        {
                            if (tailleCase<DIMX/8)
                                tailleCase*=2;
                        }
                        else if(events.wheel.y < 0)
                        {
                            if (tailleCase>DIMX/64)
                                tailleCase/=2;
                        }
                    }
                    break;
                default :
                    break;
            }
        }
    }
}
