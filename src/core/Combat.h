#ifndef COMBAT_H
#define COMBAT_H

#include "Personnage.h"
#include "TableauCartes.h"

class Combat {
    public:
    Combat(Joueur* joueur, Personnage* equipe[3], Ennemi* ennemi[5], unsigned int vitesse[5]);
    Combat(const Combat* c);   // utilise pour alphabeta, ne copie pas m_joueur, m_equipe et m_ennemi
    ~Combat();
    Joueur* getJoueur();
    unsigned int getNbEquipier();
    Personnage* getEquipier(unsigned int i);
    unsigned int getNbEnnemi();
    Ennemi* getEnnemi(unsigned int i);
    bool getVivant(unsigned int i);
    unsigned int getOrdre(unsigned int i);      // Renvoie l'indice du personnage qui joue en position i
    unsigned int getOrdreActuel();              // Renvoie l'indice indiquant la position du personnage qui joue actuellement
    unsigned int augmenterOrdreActuel();             // Increment m_ordreActuel, appelle finirTour() si on a fini le tour, renvoie 0 sinon
    unsigned int getNbTour();
    unsigned int getStatutPersonnage(unsigned int i, unsigned int statut);
    unsigned int getPointsDeViePersonnage(unsigned int i);
    unsigned int getPointsDeVieMaxPersonnage(unsigned int i);
    unsigned int getManaPersonnage(unsigned int i);
    unsigned int getManaMaxPersonnage(unsigned int i);
    unsigned int getAttaquePersonnage(unsigned int i);
    unsigned int getDefensePersonnage(unsigned int i);
    unsigned int getInitiativePersonnage(unsigned int i);
    unsigned int getBouclierPersonnage(unsigned int i);
    unsigned int getPoisonPersonnage(unsigned int i);
    unsigned int getEsquivePersonnage(unsigned int i);
    unsigned int getPersonnageActuel();
    Main getMainPersonnage(unsigned int i);
    Deck getDeckPersonnage(unsigned int i);
    TableauCartes getDefaussePersonnage(unsigned int i);
    void piocherCarte(unsigned int i);
    unsigned int jouerCarte(Carte* carte, unsigned int positionCarte, unsigned int personnageCible);
    void finirTour();
    bool ennemiRestant();
    void ordonnerJeu();
    unsigned int getGainExperience();
    Carte* finirCombat();   // Renvoie la carte gagnee lors du combat
    Point meilleurCoup(Point mc, int &valeur);    // Calcul du meilleur coup, valeur correspond a la valeur de la meilleure situation atteignable, et mc a la cible et a l'indice de la carte a jouer en premier dans la main pour l'atteindre
    int valeur();   // On evalue la situation du point de vue de l'ennemi
    void coupsPossibles(std::vector<Point>& coups);   // On initialise les coups possibles
    std::string getNomOrdo(unsigned int i) const;
    std::string getNomNonOrdo(unsigned int i) const;
    std::string getLogs(unsigned int i) const;
    void setLogs(std::string s);
    unsigned int getTailleLogs() const;
    ///////////////
    unsigned int getCible() const;

private:
    void partitionPoint(Point T[],  int pivot, unsigned int debut, unsigned int fin, unsigned int& k1, unsigned int& k2);
    void triPartitionPoint(Point T[], unsigned int debut, unsigned int fin);

    Joueur* m_joueur;               // Pointeur vers le joueur en combat
    Personnage** m_equipe;        // Pointeur vers les equipiers en combat
    unsigned int m_nbEquipier;    // le nombre d'equipiers
    Ennemi** m_ennemi;               // Pointeur vers les ennemis en combat
    unsigned int m_nbEnnemi;     // le nombre d'ennemis
    bool* m_vivant;   // vrai si le personnage est vivant
    Point* m_ordre;    // indice des personnages dans l'ordre de jeu (.x : indice du personnage     .y : vitesse du personnage, utilisee pour trier)
    unsigned int m_ordreActuel;    // indice actuel dans le tableau d'ordre de jeu
    unsigned int m_nbTour;          // Numero de tour
    unsigned int** m_statutPersonnage;          // Status du personnage : [0]: Invulnerable, [1]: Derniere chance, [2]: Esquive, [3]: Silence, [4]: Confusion, [5]: Poison
    unsigned int* m_pointsDeViePersonnage;      // Points de vie du personnage en combat
    unsigned int* m_pointsDeVieMaxPersonnage;  // Points de vie maximum du personnage
    unsigned int* m_manaPersonnage;      // Points de mana du personnage
    unsigned int* m_manaMaxPersonnage;   // Points de mana maximum du personnage
    unsigned int* m_attaquePersonnage;          // Attaque du personnage en combat
    unsigned int* m_defensePersonnage;          // Defense du personnage en combat
    unsigned int* m_initiativePersonnage;          // Initiative du personnage
    unsigned int* m_bouclierPersonnage;         // Points de bouclier du personnage
    unsigned int* m_poisonPersonnage;           // Poison inflige au personnage
    unsigned int* m_esquivePersonnage;            // Esquive du personnage
    Main* m_mainPersonnage;              // Main du personnage (cartes jouables)
    Deck* m_deckPersonnage;              // Deck du personnage (cartes piochables)
    TableauCartes* m_defaussePersonnage; // Defausse du personnage (cartes joueees)
    unsigned int m_personnageActuel;   // Indice du personnage qui joue
    unsigned int m_gainExperience;    // Experience gagnee lors du combat
    std::string * m_tabNomOrdo;
    std::string * m_tabNomNonOrdo;
    std::vector<std::string> m_logs;
    unsigned int m_derniereCible;
};

#endif // COMBAT_H
