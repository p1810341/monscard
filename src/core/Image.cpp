#include "Image.h"
#include <iostream>
#include <cassert>

Image::Image () {
    surface = NULL;
    texture = NULL;
}

Image::~Image() {
    libereSurface();
}

void Image::loadFromFile(const char* filename, SDL_Renderer* renderer) {
    // Chargement surface
    surface = IMG_Load(filename);
    if(surface == NULL) {
        std::string parent = std::string("../") + filename;
        surface = IMG_Load(parent.c_str());
        if(surface == NULL) {
            parent = std::string("../") + parent;
            surface = IMG_Load(parent.c_str());
            if(surface == NULL) {
                std::cout << "Image::loadFromFile : Erreur dans l'ouverture de l'image " << filename << " - " << SDL_GetError()<<std::endl;
                exit(1);
            }
        }
    }

    SDL_Surface * surfaceCorrectPixelFormat = SDL_ConvertSurfaceFormat(surface,SDL_PIXELFORMAT_ARGB8888,0);
    SDL_FreeSurface(surface);
    surface = surfaceCorrectPixelFormat;

    // Chargement texture
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    if(texture == NULL) {
        std::cout << "Image::loadFromFile : Erreur dans la creation de la texture de " << filename << " - " << SDL_GetError()<<std::endl;
        exit(1);
    }
}

void Image::loadFromCurrentSurface (SDL_Renderer * renderer) {
    texture = SDL_CreateTextureFromSurface(renderer,surface);
    if (texture == NULL) {
        std::cout << "Image::loadFromCurrentSurface : Impossible de creer la structure depuis la surface - " << SDL_GetError() << std::endl;
        exit(1);
    }
}

void Image::setSurface(SDL_Surface * surf) {
    surface = surf;
}

SDL_Surface* Image::getSurface() const {
    return surface;
}

SDL_Texture* Image::getTexture() const {
    return texture;
}

void Image::libereSurface() {
    SDL_FreeSurface(surface);
}

void Image::libereTexture() {
    SDL_DestroyTexture(texture);
}

void Image::draw (SDL_Renderer * renderer, int x, int y, int w, int h, unsigned int transparence) {
    int ok;
    SDL_Rect r;
    r.x = x;
    r.y = y;
    r.w = (w<0)?surface->w:w;
    r.h = (h<0)?surface->h:h;
    SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
    SDL_SetTextureAlphaMod(texture, transparence);

    ok = SDL_RenderCopy(renderer,texture,NULL,&r);
    if (ok<0)
        std::cout<<"Image::draw : Impossible de dessiner l'image - "<<SDL_GetError();
    assert(ok == 0);
}

void Image::drawRotate (SDL_Renderer * renderer, int x, int y, int angle, int w, int h, unsigned int transparence) {
    int ok;
    SDL_Rect r;
    r.x = x;
    r.y = y;
    r.w = (w<0)?surface->w:w;
    r.h = (h<0)?surface->h:h;
    if (transparence!=255) {
        SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
        SDL_SetTextureAlphaMod(texture, transparence);
    }

    /*if (has_changed) {
        ok = SDL_UpdateTexture(texture,NULL,surface->pixels,surface->pitch);
        assert(ok == 0);
        has_changed = false;
    }*/

    ok = SDL_RenderCopyEx(renderer, texture, NULL, &r, angle, NULL, SDL_FLIP_NONE);
    if (ok<0)
        std::cout<<"Image::draw : Impossible de dessiner l'image - "<<SDL_GetError();
    assert(ok == 0);
}
