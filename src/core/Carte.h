#ifndef CARTE_H_INCLUDED
#define CARTE_H_INCLUDED

#include <string>

class Carte {
public:
    /**@brief Constructeur par defaut de Carte @param non*/
    Carte();
    /**@brief Constructeur par parametre de Carte @param [in] int idCarte @param [in] int coutEnMana @param [in] const std::string& nomCarte
    @param [in] const std::string& descriptionCarte @return Carte  @warning  Faire attention a l'ordre des parametres*/
    Carte(unsigned int idCarte, unsigned int coutEnMana, const std::string& nomCarte, const std::string& descriptionCarte);
    virtual ~Carte();
    /**@brief Recupere l'ID (m_idCarte) de la carte @param non @return unsigned int @warning  Ne modifie pas de valeur*/
    unsigned int getIdCarte() const;
    /**@brief Recupere le cout (m_coutEnMana) de la carte @param non @return unsigned int @warning  Ne modifie pas de valeur*/
    unsigned int getCout() const;
    /**@brief Recupere le nom (m_nomCarte) de la carte @param non @return string @warning  Ne modifie pas de valeur*/
    std::string getNomCarte() const;
    /**@brief Recupere le type de la carte (m_typeCarte) @param non @return string @warning  Ne modifie pas de valeur*/
    virtual std::string getTypeCarte() const =0;
    /**@brief Recupere la description (m_descriptionCarte) de la carte @param non @return string @warning  Ne modifie pas de valeur*/
    std::string getDescription() const;
    /**@brief Procedure qui permet de tester si la classe Carte fonctionne correctement */
    void testRegression();


protected:
    unsigned int m_idCarte;             // id de la carte
    unsigned int m_coutEnMana;          // son cout en manaas en combat
    std::string m_nomCarte;             // son nom
    std::string m_descriptionCarte;     // sa description
};

class Soin: public Carte {
public:
    /**@brief Constructeur par defaut de Soin @param non*/
    Soin();
    /**@brief Constructeur par parametre de Soin @param [in] int idCarte @param [in] int coutEnMana @param [in] const std::string& nomCarte
    @param [in] const std::string& descriptionCarte @param [in] float pointsDeVie @return Soin  @warning  Faire attention � l'ordre des parametres*/
    Soin(unsigned int idCarte, unsigned int coutEnMana, const std::string& nomCarte, const std::string& descriptionCarte,
         float pointsDeVie);
    ~Soin();
    /**@brief Recupere le type de la carte (m_typeCarte) @param non @return string @warning  Ne modifie pas de valeur*/
    virtual std::string getTypeCarte() const;
    /**@brief Recupere les points de vie (m_nombrePointsDeVie) rendus par la carte de type Soin @param non @return float @warning  Ne modifie pas de valeur*/
    float getPointsDeVie() const;
    /**@brief Procedure qui permet de tester si la classe Soin fonctionne correctement */
    void testRegressionSoin();

private:
    float m_nombrePointsDeVie;  // nombre de points de vie que rend la carte
};

class Bonus: public Carte {
public:
    /**@brief Constructeur par defaut de Bonus @param non*/
    Bonus();
    /**@brief Constructeur par parametre de Bonus @param [in] int idCarte @param [in] int coutEnMana @param [in] const std::string& nomCarte
    @param [in] const std::string& descriptionCarte @param [in] float attaque @param [in] float defense @param [in] float exp @return Carte
    @warning  Faire attention � l'ordre des param�tres*/
    Bonus(unsigned int idCarte, unsigned int coutEnMana, const std::string& nomCarte, const std::string& descriptionCarte,
    float attaque, float defense, float exp);
    ~Bonus();
    /**@brief Recupere le type de la carte (m_typeCarte) @param non @return string @warning  Ne modifie pas de valeur*/
    virtual std::string getTypeCarte() const;
    /**@brief Recupere le bonus d'attaque (m_nombrePointsBonusAttaque) de la carte de type Bonus @param non @return float @warning  Ne modifie pas de valeur*/
    float getAttaque() const;
    /**@brief Recupere le bonus de defense (m_nombrePointsBonusDefense) de la carte de type Bonus @param non @return float @warning  Ne modifie pas de valeur*/
    float getDefense() const;
    /**@brief Recupere le bonus d'experience et d'or (m_nombrePointsBonusExperience) de la carte de type Bonus @param non @return float @warning  Ne modifie pas de valeur*/
    float getExp() const;
    /**@brief Procedure qui permet de tester si la classe Bonus fonctionne correctement */
    void testRegressionBonus();

private:
    float m_nombrePointsBonusAttaque;       // Le bonus d'attaque que confere la carte
    float m_nombrePointsBonusDefense;       // Le bonus de defense que confere la carte
    float m_nombrePointsBonusExperience;    // Le bonus d'experience et d'or que confere la carte
};

class Defense: public Carte {
public:
    /**@brief Constructeur par defaut de Defense @param non*/
    Defense();
    /**@brief Constructeur par parametre de Defense @param [in] int idCarte @param [in] int coutEnMana @param [in] const std::string& nomCarte
    @param [in] const std::string& descriptionCarte @param [in] bool invulnerable @param [in] bool derniereChance @param [in] float pointsBouclier
    @param [in] float chanceEsquive @return Carte  @warning  Faire attention � l'ordre des param�tres*/
    Defense(unsigned int idCarte, unsigned int coutEnMana, const std::string& nomCarte, const std::string& descriptionCarte,
            bool invulnerable, bool derniereChance, float pointsBouclier, float chanceEsquive);
    ~Defense();
    /**@brief Recupere le type de la carte (m_typeCarte) @param non @return string @warning  Ne modifie pas de valeur*/
    virtual std::string getTypeCarte() const;
    /**@brief Recupere le boleen (m_rendInvulnerable) qui indique si la carte de type Defense donne de l'invulnerabilite @param non @return bool @warning  Ne modifie pas de valeur*/
    bool getRendInvulnerable() const;
    /**@brief Recupere le boleen (m_derniereChance) qui indique si la carte de type Defense donne l'etat de "derniere chance" @param non @return bool @warning  Ne modifie pas de valeur*/
    bool getDerniereChance() const;
    /**@brief Recupere les points de bouclier (m_pointsBouclier) que donne la carte de type Defense @param non @return float @warning  Ne modifie pas de valeur*/
    float getPointsBouclier() const;
    /**@brief Recupere la valeur de la chance d'esquiver (m_chanceEsquive) que donne la carte de type Defense @param non @return float @warning  Ne modifie pas de valeur*/
    float getChanceEsquive() const;
    /**@brief Procedure qui permet de tester si la classe Defense fonctionne correctement */
    void testRegressionDefense();


private:
    bool m_rendInvulnerable;    // Si la carte rend invulnerable
    bool m_derniereChance;      // Si la carte confere derniere chance
    float m_pointsBouclier;     // Points de boucliers conferes par la carte
    float m_chanceEsquive;      // Pourcentage d'esquive confere par la carte
};

class Attaque: public Carte {
public:
    /**@brief Constructeur par defaut d'Attaque @param non*/
    Attaque();
    /**@brief Constructeur par param�tre d'Attaque @param [in] int idCarte
    @param [in] int coutEnMana @param [in] const std::string& nomCarte @param [in] const std::string& descriptionCarte
    @param [in] bool attaqueDirecte @param [in] bool silence @param [in] bool confusion @param [in] bool poison
    @param [in] float pointsDeDegat @param [in] float volDeVie @return Carte  @warning  Faire attention � l'ordre des param�tres*/
    Attaque(unsigned int idCarte, unsigned int coutEnMana, const std::string& nomCarte, const std::string& descriptionCarte,
            bool attaqueDirecte, bool silence, bool confusion, bool poison, float pointsDeDegat, float volDeVie);
    ~Attaque();
    /**@brief Recupere le type de la carte (m_typeCarte) @param non @return string @warning  Ne modifie pas de valeur*/
    virtual std::string getTypeCarte() const;
    /**@brief Recupere le bool�en (m_attaqueDirecte) qui dit si la carte de type Attaque est une attaque directe @param non @return bool @warning  Ne modifie pas de valeur*/
    bool getAttaqueDirecte() const;
    /**@brief Recupere le booleen (m_silence) qui dit si la carte de type Attaque applique l'effet "silence" @param non @return bool @warning  Ne modifie pas de valeur*/
    bool getSilence() const;
    /**@brief Recupere le booleen (m_confusion) qui dit si la carte de type Attaque applique l'effet "Confusion" @param non @return bool @warning  Ne modifie pas de valeur*/
    bool getConfusion() const;
    /**@brief Recupere le booleen (m_poison) qui dit si la carte de type Attaque applique l'effet "Poison" @param non @return bool @warning  Ne modifie pas de valeur*/
    bool getPoison() const;
    /**@brief Recupere la valeur des d�gats (m_pointsDeDegat) de la carte de type Attaque @param non @return float @warning  Ne modifie pas de valeur*/
    float getPointsDeDegat() const;
    /**@brief Recupere la valeur du vol de vie que donne (m_volDeVie) par la carte de type Attaque @param non @return float @warning  Ne modifie pas de valeur*/
    float getVolDeVie() const;
    /**@brief Procedure qui permet de tester si la classe Attaque fonctionne correctement */
    void testRegressionAttaque();

private:
    bool m_attaqueDirecte;  // Si la carte permet une attaque directe (ne prend pas en compte le bouclier)
    bool m_silence;         // Si la carte inflige silence (passe le tour de l'adversaire)
    bool m_confusion;       // Si la carte inflige confusion (l'adversaire risque de se blesser)
    bool m_poison;          // Si la carte inglige poison (degats supplementaires a chaque tour)
    float m_pointsDeDegat;  // Nombre de points de degats de la carte
    float m_volDeVie;       // Si la carte inflige vol de vie (attaque et regenere l'utilisateur) et sa proportion
};

#endif // CARTE_H_INCLUDED
