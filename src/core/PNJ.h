#ifndef PNJ_H
#define PNJ_H

#include <vector>
#include <string>
#include "Point.h"
#include "Dialogue.h"
#include <assert.h>

class PNJ {
public:
    /**@brief Constructeur par defaut de la classe PNJ @param non*/
    PNJ();
    /**@brief Constructeur par parametre de la classe PNJ @param [in] const string& nom @param [in] int x @param [in] int y @warning Attention a l'ordre des parametres*/
    PNJ(int x, int y, unsigned int id, const std::string& nom="");
    /**@brief Destructeur de la classe PNJ @param non*/
    ~PNJ();
    /**@brief Recupere le nom (m_nomPNJ) du PNJ @param non @return string @warning  Ne modifie pas de valeur*/
    std::string getNomPNJ() const;
    /**@brief Recupere la position (m_position) du PNJ @param non @return Point @warning  Ne modifie pas de valeur*/
    Point getPosition() const;
    /**@brief Recupere le Dialogue � l'indice i de m_tableauDialogues du PNJ  @param [in] int i @return Dialogue @warning  Ne modifie pas de valeur ; Retourne un pointeur*/
    Dialogue* getDialogue(int i) const;
    /**@brief Recupere l'indice du dialogue (m_indiceDialogue) du PNJ @param non @return int @warning  Ne modifie pas de valeur*/
    unsigned int getIndiceDialogue() const;
    /**@brief Augmente l'indice de (m_indiceDialogue) du PNJ de 1 pour passer au dialogue suivant @param non @return non*/
    void augmenterIndiceDialogue();
    /**@brief Indique que le PNJ est entrain de parler ou non : m_parle change @param non @return int @warning Attention a la valeur de depart de m_parle*/
    void setParle();
    /**@brief Recupere l'etat de m_parle : renvoie true si le PNJ est en train de parler  @param non @return bool @warning  Ne modifie pas de valeur*/
    bool getParle() const;
    void ajouterDialogue(unsigned int i, Dialogue* d);
    void retirerDialogue(unsigned int i);
    void setDialogue(unsigned int i, Dialogue* d);
    void setNom(const std::string nom);
    void reduireIndiceDialogue();
    bool getADialogue() const;
    unsigned int getIndiceMax() const;
    unsigned int getIdPNJ() const;
    void setPosition(int x, int y);

private:
    unsigned int m_idPNJ;
    std::string m_nomPNJ;                       // Nom du PNJ
    Point m_position;                           // Position du PNJ
    std::vector<Dialogue*> m_tableauDialogues;  // Tableau contenant les Dialogues du PNJ
    unsigned int m_indiceDialogue;              // Indice du dialogue actuel du PNJ
    bool m_parle;                               // Booleen indiquant si le PNJ est en train de parler
    bool m_aDialogue;                           // true si le PNJ possede au moins un dialogue
};

#endif // PNJ_H
