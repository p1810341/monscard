#ifndef TABLEAUCARTES_H
#define TABLEAUCARTES_H

#include <vector>

#include "Carte.h"

class TableauCartes {
public:
    /**@brief Constructeur par defaut de TableauCarte @param non */
    TableauCartes();
    /**@brief Destructeur de TableauCarte @param non */
    ~TableauCartes();
    /**@brief Recupere le nombre de cartes du TableauCartes @param non @return unsigned int @warning  Ne modifie pas de valeur */
    unsigned int getNombreCartes() const;
    /**@brief Recupere la carte d'indice ind dans le TableauCartes @param [in] unsigned int ind @return Carte @warning  Ne modifie pas de valeur et renvoie un pointeur */
    Carte* getCarte(unsigned int ind) const;
    /**@brief Ajoute une nouvelle Carte dans le TableauCartes @param [in] Carte* carte @return void @warning  Prend un pointeur */
    void ajouterCarte(Carte* carte);
    /**@brief Enleve la carte en position ind du TableauCartes et la renvoie @param [in] unsigned int ind @return Carte* @warning  Renvoie un pointeur*/
    Carte* retirerCarte(unsigned int ind);
    /**@brief Indique si une carte d'ID donne est presente dans le TableauCarte @param [in] unsigned int idCarte @return bool */
    bool estCartePresente(unsigned int idCarte) const;
    /**@brief Procedure qui permet de tester si la classe TableauCartes fonctionne correctement */
    void testRegression();

protected:
    unsigned int m_nombreCartes;            // nombre de cartes du tableauCartes
    std::vector<Carte*> m_tableauCartes;    // tableau contenant des pointeurs vers Cartes
};

class Main : public TableauCartes {
public:
    /**@brief Constructeur par defaut de Main @param non */
    Main();
    /**@brief Recupere la taille de base (m_tailleInitiale) de la Main @param non @return unsigned int @warning  Ne modifie pas de valeur */
    unsigned int getTailleInitiale() const;
    void setTailleInitiale(unsigned int taille);
    unsigned int getTailleMaximale() const;
    void setTailleMaximale(unsigned int taille);
    /**@brief Procedure qui permet de tester si la classe Main fonctionne correctement */
    void testRegressionMain();

private:
    unsigned int m_tailleInitiale;  // taille initiale de la main au lancement du combat
    unsigned int m_tailleMaximale;  // taille maximale de la main durant le combat
};

class Deck : public TableauCartes {
public:
    /**@brief Constructeur par defaut du deck @param non */
    Deck();
    /**@brief Constructeur par copie du deck @param [in] Deck* deck @warning Prend un pointeur en parametre */
    Deck(Deck* deck);
    /**@brief Recupere la taille maximale (m_nombreCartesMax) du Deck @param non @return unsigned int @warning  Ne modifie pas de valeur */
    unsigned int getNombreCartesMax()const;
    /**@brief Recupere la taille minimale (m_nombreCartesMin) du Deck @param non @return unsigned int @warning  Ne modifie pas de valeur */
    unsigned int getNombreCartesMin()const;
    /**@brief Procedure qui permet de tester si la classe Deck fonctionne correctement */
    void testRegressionDeck();

private:
    unsigned int m_nombreCartesMax;     // Nombre de cartes maximum que contient le deck
    unsigned int m_nombreCartesMin;     // Nombre de cartes minimum que contient le deck
};
#endif // TABLEAUCARTES_H
