#include "TableauCartes.h"
#include <iostream>
#include <assert.h>

///////////////////////////////////
// TableauCartes
///////////////////////////////////

TableauCartes::TableauCartes() {
    m_nombreCartes = 0;
}

TableauCartes::~TableauCartes() {

}

unsigned int TableauCartes::getNombreCartes() const {
    return m_nombreCartes;
}

Carte* TableauCartes::getCarte(unsigned int ind) const {
    return m_tableauCartes[ind];
}

void TableauCartes::ajouterCarte(Carte* carte) {
    m_tableauCartes.push_back(carte);
    m_nombreCartes++;
}

Carte* TableauCartes::retirerCarte(unsigned int ind) {
    Carte* carte = m_tableauCartes[ind];
    m_tableauCartes.erase(m_tableauCartes.begin() + ind);
    m_nombreCartes--;
    return carte;
}

bool TableauCartes::estCartePresente(unsigned int idCarte) const {
    for(unsigned int i = 0 ; i < m_tableauCartes.size() ; i++) {
        if(m_tableauCartes[i]->getIdCarte() == idCarte) return true;
    }
    return false;
}

void TableauCartes::testRegression() {
    TableauCartes tab;
    Carte* c=new Attaque(1,1,"test","ceci est un test",0,0,0,0,10.0,0.0);
    tab.ajouterCarte(c);
    assert(tab.getCarte(0)->getCout()==c->getCout());
    assert(tab.getCarte(0)->getDescription()==c->getDescription());
    assert(tab.getCarte(0)->getIdCarte()==c->getIdCarte());
    assert(tab.getCarte(0)->getNomCarte()==c->getNomCarte());
    assert(tab.getCarte(0)->getTypeCarte()==c->getTypeCarte());
    assert(tab.getNombreCartes()==1);
    tab.retirerCarte(0);
    assert(tab.getNombreCartes()==0);
    std::cout<<"Test de non regression de TableauCartes....OK"<<std::endl;
    delete c;
}

///////////////////////////////////
// Main
///////////////////////////////////

Main::Main() {
    m_tailleInitiale = 3;
    m_tailleMaximale = 10;
}

unsigned int Main::getTailleInitiale() const {
    return m_tailleInitiale;
}

void Main::setTailleInitiale(unsigned int taille) {
    m_tailleInitiale=taille;
}

unsigned int Main::getTailleMaximale() const {
    return m_tailleMaximale;
}

void Main::setTailleMaximale(unsigned int taille) {
    m_tailleMaximale=taille;
}

void Main::testRegressionMain() {
    Main main;
    assert(main.getTailleInitiale()==3);
    std::cout<<"Test de non regression de Main....OK"<<std::endl;
}

///////////////////////////////////
// Deck
///////////////////////////////////

Deck::Deck() {
    m_nombreCartesMin = 5;
    m_nombreCartesMax = 20;
}

Deck::Deck(Deck* deck) {
    m_nombreCartesMin = 5;
    m_nombreCartesMax = 20;
    for (unsigned int i=0; i<deck->getNombreCartes(); i++)
    {
        m_tableauCartes.push_back(deck->getCarte(i));
    }
    m_nombreCartes=deck->getNombreCartes();
}

unsigned int Deck::getNombreCartesMax() const {
    return m_nombreCartesMax;
}
unsigned int Deck::getNombreCartesMin() const {
    return m_nombreCartesMin;
}

void Deck::testRegressionDeck() {
    Deck* d=new Deck;
    assert(d->getNombreCartesMax()==20);
    assert(d->getNombreCartesMin()==5);
    Carte* c=new Attaque(1,1,"test","ceci est un test",0,0,0,0,10.0,0.0);
    d->ajouterCarte(c);
    Deck* d2= new Deck(d);
    assert(d->getCarte(0)->getCout()==d->getCarte(0)->getCout());
    assert(d->getCarte(0)->getDescription()==d->getCarte(0)->getDescription());
    assert(d->getCarte(0)->getIdCarte()==d->getCarte(0)->getIdCarte());
    assert(d->getCarte(0)->getNomCarte()==d->getCarte(0)->getNomCarte());
    assert(d->getCarte(0)->getTypeCarte()==d->getCarte(0)->getTypeCarte());
    delete d;
    delete d2;
    delete c;
    std::cout<<"test de non regression de Deck....OK"<<std::endl;
}
