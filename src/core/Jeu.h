#ifndef JEU_H
#define JEU_H

#include <vector>
#include <set>

#include "Terrain.h"
#include "Coffre.h"
#include "PNJ.h"
#include "Combat.h"
#include "PointApparition.h"
#include "Quete.h"
#include "PathFinding.h"

#define CHAMPDEVISION 40000

class Jeu {
public:
    // Parametres pour l'affichage : on les laisse en public
    bool m_quitter;                     // Arreter le jeu ; public car on lui aurait mis un get et un set quoiqu'il arrive
    unsigned int m_tailleCase;
    // Gestion menus et fenetres
    bool m_menuPrincipal;               // Si on est au menu principal
    bool m_afficheMenu;                 // Si on affiche le menu
    bool m_afficheQuitter;              // Si on affiche la confirmation pour quitter
    bool m_afficheSauvegarde;           // Si on affiche l'ecran de sauvegarde
    bool m_afficheConfirmSauvegarde;    // Si on affiche la confirmation de sauvegarde
    bool m_afficheSauvegardeOK;         // Si on affiche que la sauvegarde a fonctionne
    bool m_afficheSauvegardeError;      // Si on affiche que la sauvegarde a echoue
    bool m_afficheChargementError;      // Si on affiche que le chargement a echoue
    bool m_afficheChargement;           // Si on affiche l'ecran de chargement
    bool m_afficheInventaire;           // Si on affiche l'inventaire
    bool m_afficheInventaireCoffre;     // Si on affiche l'inventaire d'un coffre
    bool m_afficheInventaireTombe;      // Si on affiche l'inventaire d'une tombe
    bool m_afficheVictoire;             // Si on affiche qu'on a vaincu un ennemi
    bool m_afficheDefaite;              // Si on affiche qu'on a a perdu contre un ennemi
    int m_ligneSelectionnee;            // Ligne du menu selectionnee
    bool m_selectBouton;                // Bouton selectionne (0 = Annuler, 1 = Confirmer)
    bool m_afficheCartes;               // Si on affiche le deck et la reserve
    bool m_afficheDialogue;             // Si on affiche un dialogue
    bool m_fenetreOuverte;              // Si on a une fenetre ouverte (pour autoriser le deplacement etc)
    unsigned int m_niveau;              // Indice du niveau actuel
    std::vector<unsigned int> m_interactionsAction;   // type d'action des elements interactifs (0=coffre 1=maison 2=point d'apparition 3=pnj)
    unsigned int m_tailleDecors;                      // nombre de decors (deuxiemeCouche du terrain)
    unsigned int m_interactionPossible;               // 0 si pas d'interaction, sinon si il y a un Coffre (1), un PNJ (2) ou une tombe (3) avec lequel on peut interagir
    unsigned int m_interactionIndice;                 // indice de la case avec laquelle on veut interagir
    unsigned int m_interactionIndiceObjet;            // indice de l'entite avec laquelle on veut interagir, relatif a son propre tableau (pnj, coffre, ...)
    Point m_interactionTombeCoordonnees;              // coordonnees du centre de la tombe avec laquelle on veut interagir
    bool m_deplacementDiagonale;                      // Si on se deplace en diagonale
    bool m_afficheCombat;                             // Si on affiche que le combat va commencer
    bool m_afficheMonteeNiveau;                       // Si on affiche l'ecran de montee de niveau
    bool m_afficheDebutQuete;                         // Si on affiche la fenetre pour accepter ou refuser une quete
    bool m_afficheFinQuete;                           // Si on affiche la fenetre indiquant qu'on a termine une quete
    unsigned int m_indiceQuete;                       // Indice de la quete avec laquelle on interagit
    unsigned int m_indiceFinQuete;                    // Indice de la quete qu'on vient de finir (on utilise un autre indice que m_indiceQuete car deux fenetres d'information peuvent se suivre, une pour proposer une quete et une de fin)
    bool m_afficheRecueilQuete;                       // Si on affiche la fenetre de recueil des quetes
    bool m_afficheInventairePlein;                    // Si on affiche que l'inventaire est plein
    bool m_afficheValidationCollecte;                 // Si on affiche l'ecran de validation de collecte, demandant si on accepte de donner les objets demandes
    int m_indiceQueteSuivie;                          // Indice de la quete qu'on suit  -1 si aucune
    // scenario
    Dialogue* m_dialogueNarrateur;             // Dialogue du narrateur
    bool m_scenario;                         // Si on a un scenario en cours, on bloque la plupart des actions
    bool m_scenarioNarrateur;                // Si on affiche le dialogue de debut
    bool m_lanceMsqScenario;                 // Si on lance la musique de scenario
    bool m_afficheTutoriel;                  // Si on affiche un tutoriel
    bool m_afficheTutorielQuete;             // Si on affiche le tutoriel du fonctionnement des quetes
    bool m_tutorielQueteDejaAffiche;         // Si on a deja affiche le tutoriel
    bool m_afficheTutorielRefusQuete;        // Si on affiche le tutoriel du refus des quetes
    bool m_tutorielRefusQueteDejaAffiche;    // Si on a deja affiche le tutoriel
    bool m_afficheTutorielRecueil;           // Si on affiche le tutoriel pour le recueil des quetes
    bool m_tutorielRecueilDejaAffiche;       // Si on a deja affiche le tutoriel
    bool m_afficheTutorielInventaire;        // Si on affiche le tutoriel pour l'inventaire
    bool m_tutorielInventaireDejaAffiche;    // Si on a deja affiche le tutoriel
    bool m_debloquerAffichageCartes;         // Si on peut afficher l'editeur de deck
    bool m_debloquerAffichageRecueil;        // Si on peut afficher le recueil
    bool m_debloquerAffichageInventaire;     // Si on peut afficher l'inventaire
    bool m_affichageTransitionNoire;         // Si on fait une transition (assombrissement puis nouvelle scene)
    int m_indiceScenario;                    // Indique ou on en est du scenario
    bool m_afficheTutorielDeck;              // Si on affiche le tutoriel du deck
    bool m_afficheTutorielLancerCombat;      // Si on affiche le tutoriel sur comment lancer un combat
    bool m_afficheTutorielCombat;            // Si on affiche le tutoriel du fonctionnement des combats
    bool m_tutorielCombatDejaAffiche;        // Si on a deja affiche le tutoriel
    bool m_afficheTutorielMain;              // Si on affiche le tutoriel de combat sur la main
    bool m_afficheTutorielMana;              // Si on affiche le tutoriel de combat sur le mana
    bool m_afficheTutorielCarte;             // Si on affiche le tutoriel de combat sur les cartes
    bool m_afficheTutorielLourdeau;          // Si on affiche l'avertissement pour ne pas attaquer le Lourd'eau tout de suite

    /**@brief Constructeur par defaut de Jeu : initialise une partie @param non */
    Jeu();
    /**@brief Destructeur de jeu @param non */
    ~Jeu();
     /**@brief Recupere le Terrain (m_joueur) du Jeu @param non @return Terrain @warning  Ne modifie pas de valeur*/
    Terrain* getTerrain() const;
    /**@brief Recupere le Joueur (m_joueur) du Jeu @param non @return Joueur @warning Renvoie une adresse memoire*/
    Joueur* getJoueur();
    /**@brief Recupere le Joueur (m_joueur) du Jeu @param non @return Joueur @warning  Ne modifie pas de valeur ; Renvoie un pointeur*/
    const Joueur* getJoueurConst() const;
    /**@brief Recupere le nombre d'Ennemi du Jeu @param non @return int @warning Ne modifie pas de valeur*/
    unsigned int getNombreEnnemis() const;
    /**@brief Recupere l'Ennemi a l'indice i de m_tableauEnnemis @param [unsigned int] i @return Ennemi* @warning Ne modifie pas de valeur ; Renvoie un pointeur*/
    Ennemi* getEnnemi(unsigned int i) const;
    Ennemi* getTombe(unsigned int i) const;
    Inventaire* getInventaireTombe(unsigned int i) const;
    /**@brief Recupere le nombre d'Especes du Jeu @param non @return int @warning Ne modifie pas de valeur*/
    unsigned int getNombreEspeces() const;
    /**@brief Recupere l'Espece a l'indice i de m_tableauEspeces @param [in] int i @return Espece* @warning Ne modifie pas de valeur ; Renvoie un pointeur*/
    Espece* getEspece(int i) const;
    PNJ* getPNJ(unsigned int niveau, unsigned int i) const;
    /**@brief Recupere le nombre de PNJ du Jeu @param non @return int @warning Ne modifie pas de valeur*/
    unsigned int getNombrePNJ(unsigned int niveau) const;
    /**@brief Recupere le Coffre du niveau niveau a l'indice i du m_tableauCoffres @param [in] int i @return Coffre @warning Ne modifie pas de valeur*/
    Coffre* getCoffre(unsigned int niveau, unsigned int i) const;
    /**@brief Recupere le nombre de Coffre du niveau @param non @return int @warning Ne modifie pas de valeur*/
    unsigned int getNombreCoffres(unsigned int niveau) const;
    /**@brief Recupere le Tableau de Cartes (m_tableauCartes) du Jeu @param non @return TableauCartes @warning Ne modifie pas de valeur*/
    TableauCartes getTableauCartes() const;
    /**@brief Effectue des actions en reponse a des actions clavier de l'utilisateur @param [in] char touche @param [in] int valeur @return void*/
    void actionClavier(char touche, unsigned int valeur);
    /**@brief Renvoie True si le personnage peut se deplacer dans la direction choisie @param [in] char touche @return bool @warning Ne modifie pas de valeur*/
    bool estDeplacementPossible(const Personnage* personnage, char touche, unsigned int& deplacement) const;
    bool estDeplacementDiagonalPossible(Point depart, Point arrive, void *grille, Point vdeplacement, float distanceEnnemiJoueur) const;
    /**@brief Verifie les collisions entre le joeuur et des elements du terrain (Coffre, Ennemi, PNJ) et appelle les fonctions associes @param non @return void*/
    void collisionObjet(Personnage* personnage);
    /**@brief Sauvegarde la partie dans un fichier et renvoie true en cas de succes @param [in] int fichier @return void*/
    bool sauvegarder(int fichier);
    /**@brief Charge la partie dans un fichier et renvoie true en cas de succes @param [in] int fichier @return void*/
    bool charger(int fichier);
    /**@brief Retourne true si un fichier de sauvegarde est vide @param [in] int numSauvegarde @return bool @warning ne modifie pas de valeur*/
    bool estFichierVide(int numFichier) const;
    /**@brief Recupere la date de la sauvegarde d'un fichier de sauvegarde et la met en forme @param [in] int numSauvegarde @return string @warning ne modifie pas de valeur*/
    std::string getDateSauvegarde(int numSauvegarde) const;
    /**@brief Charge le niveau indique par le numero niveau, avec une position (x, y) pour le joueur  @param [in] int numFichier @return void*/
    void chargerNiveau(unsigned int niveau, int x, int y);
    void chargerMonde(std::string monde);
    unsigned int getHauteurCollisionDecors(unsigned int i);
    void gestionEnnemis(int valeurDeplacement);
    Personnage* getEquipier(unsigned int i);
    void recupererObjet(unsigned int i, unsigned int origineObjet);  // essaye de recuperer l'objet à l'indice i (coffre si origineObjet=0, tombe si origineObjet=1)
    /**@brief Recupere le Combat en cours (m_combat) du Jeu @param non @return Combat @warning renvoie un pointeur*/
    Combat* getCombat() const;
    /**@brief Procedure qui lance un combat entre un Joueur et un Ennemi @param [in] Joueur* joueur, [in] Ennemi* ennemi @return void @warning Prend des pointeurs*/
    void lanceCombat(Ennemi* ennemis[5],unsigned int vitesses[5]);
    /**@brief Procedure qui termine le combat @param (in] bool victoire @return void */
    void finirCombat(bool victoire);
    Personnage* getPersonnage(unsigned int i);
    unsigned int getChampDeVision(unsigned int i);
    unsigned int getNbPersonnages() const;
    unsigned int getNbFamiliers() const;
    unsigned int getAncienNiveauPersonnage(unsigned int i) const;
    unsigned int getAncienPvPersonnage(unsigned int i) const;
    unsigned int getAncienManaPersonnage(unsigned int i) const;
    unsigned int getAncienneAttaquePersonnage(unsigned int i) const;
    unsigned int getAncienneDefensePersonnage(unsigned int i) const;
    unsigned int getGainExperience() const;
    int getIdCarteObtenue() const;
    bool getMonteeNiveau(unsigned int i) const;
    unsigned int getNbQuetes() const;
    Quete* getQuete(unsigned int i) const;
    std::string getNomTerrain(unsigned int niveau) const;
    Objet* getObjet(unsigned int i) const;
    void finDialogueScenario();
    void appliquerTransition();  // appelee quand on est au milieu de la transition (donc l'affichage est totalement noir), permet a jeu de faire les changements necessaires a la scene suivante en fonction du scenario
    void getCollisionsDecors(unsigned int i, unsigned int collisions[4]) const;


private:
    Terrain* m_terrain;                          // Le terrain du niveau actuel
    Joueur *m_joueur;                            // Le joueur
    void *m_grille;                              // grille du pathfinding
    void *m_pathFinding;                  // Le PathFinding pour que les ennemis trouve leurs chemin.
    std::vector<unsigned int*> m_collisionsDecors;  // rectangle de collision des decors
    std::vector<std::vector<PNJ*>> m_tableauPNJ;  // Les PNJ de chaque niveau du jeu
    std::vector<PNJ*> m_tableauPNJJeu;   // Les differents pnj existant dans le jeu, c'est ce vector qui permettra de les supprimer a la fin de l'execution
    std::vector<std::vector<Coffre*>> m_tableauCoffres;      // Les coffres de chaque niveau du jeu
    std::vector<Coffre*> m_tableauCoffresJeu;   // Les differents coffres existant dans le jeu, c'est ce vector qui permettra de les supprimer a la fin de l'execution
    std::vector<Ennemi*> m_tableauEnnemis;      // Les ennemis du niveau actuel
    std::vector<bool> m_tableauChasse;        // Si l'ennemi est en train de chasser le joueur
    std::vector<unsigned int> m_tableauOrientation;  // tableau des orientations des ennemis
    std::vector<Ennemi*> m_tableauTombes;      // Les ennemis morts du niveau actuel
    std::vector<Inventaire*> m_tableauInventairesTombes;   // Les inventaires des ennemis morts
    std::vector<Espece*> m_tableauEspeces;      // Les especes du jeu
    TableauCartes m_tableauCartes;              // Tableau de toutes les cartes du jeu
    std::vector<Objet*> m_tableauObjets;   // Les objets du jeu
    std::vector<Personnage*> m_tableauPersonnages;  // Les personnages du jeu (Personnage et Familier qui peuvent rejoindre l'equipe)
    unsigned int m_nbPersonnages;          // Le nombre total de Personnage dans le jeu
    unsigned int m_nbFamiliers;            // Le nombre total de Familier dans le jeu
    std::string m_datesSauvegardes[6];          // Dates des sauvegardes formatees
    std::vector<PointApparition*> m_tableauPointsApparitions;    // Les points d'apparitions du niveau actuel
    std::vector<std::string> m_niveauNom;           // le nom du fichier du niveau
    std::vector<std::string> m_terrainNom;          // le nom du terrain
    std::vector<std::vector<Point>> m_niveauSorties;  // point : x indice debut sortie, y indice fin sortie
    std::vector<std::vector<Point>> m_niveauDestinationsSorties;  // point : x indice niveau destination, y indice sortie destination
    std::vector<Point> m_niveauTaille;   // point : x tailleX, y tailleX
    std::vector<unsigned int> m_niveauIdRegion;   // l'id de la region dont c'est une version
    Personnage* m_equipe[3];                // tableau des Personnage dans l'equipe du joueur
    Point m_dernierDeplacement[40];          // 40 derniers deplacements du joueur, reproduits en decale par les equipiers
    Combat* m_combat;                           // Combat (NULL si pas de combat en cours)
    int m_idCarteObtenue;                       // Derniere carte obtenue lors d'un combat
    unsigned int m_gainExperience;                       // Dernier gain d'experience lors d'un combat
    unsigned int m_idEnnemi;                             // ID du dernier ennemi touche
    unsigned int m_ancienNiveauPersonnage[4];            // Niveau du personnage avant le combat
    unsigned int m_ancienPvPersonnage[4];                // PV max du personnage avant le combat
    unsigned int m_ancienManaPersonnage[4];              // Mana du personnage avant le combat
    unsigned int m_ancienneAttaquePersonnage[4];         // Attaque du personnage avant le combat
    unsigned int m_ancienneDefensePersonnage[4];         // Defense du personnage avant le combat
    bool m_monteeNiveau[4];         // Si il y a une montee de niveau
    std::vector<Quete*> m_tableauQuetes;    // Les quetes du jeu

    /**@brief Indique si le Joueur peut se déplacer aux coordonee (x,y) @param [in] int y @param [in] int y @return bool @warning Ne modifie pas de valeur*/
    bool estMarchable(int x, int y) const;
    /**@brief Met la date de sauvegarde dans un fichier de sauvegarde @param [in] int numFichier @return bool*/
    bool setDateSauvegarde(int numFichier);
    /**@brief charge tous les coffres et pnj du jeu  @param non @return non*/
    void chargerEntites();
    bool verificationCollisionDecors(unsigned int collisionPersonnage[4], unsigned int collisions[4], unsigned int x, unsigned int y, unsigned int indiceCase, unsigned int deplacement, unsigned int indiceTest) const;
    void supprimerEnnemi(unsigned int i);
    void deplacementEquipiers();
    Quete* initialiserQuete(std::vector<std::string> listeQuetes, unsigned int &i); // permet d'initialiser les quetes, renvoie une quete et ses sous-quetes crees recursivement
    void interactionPnjQuete();
    void gainExperienceJoueurFamilier(unsigned int idPersonnage, unsigned int experience);    // augmente l'experience du joueur si id==0 ou d'un familier si 0<idPersonnage<4 et que m_equipe[idPersonnage-1] est un familier
    void gestionObjetQuete(Objet* objet, bool ajout);  // S'occupe de l'evolution des quetes liees a la collecte d'objet
    void gestionEnnemisQuete(Ennemi* ennemi);   // S'occupe de l'evolution des quetes liees au massacre d'ennemis
    void gestionDeplacementQuete();    // S'occupe de l'evolution des quetes liees au deplacement
    void recupererRecompenseQuete(unsigned int indiceQuete);   // recupere les recompenses d'une quete et la place dans l'inventaire du joueur
    void queteFinieScenario(unsigned int indiceQuete);   // permet de verifier si la fin d'une quete declenche quelque chose dans le scenario
    void positionnerEquipe(int posX, int posY);   // permet de deplacer le joueur et les familiers aux coordonnees donnees
};

#endif // JEU_H
