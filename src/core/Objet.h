#ifndef OBJET_H
#define OBJET_H

#include <string>
#include <vector>

class Objet {
    public:
    /**@brief Constructeur par defaut de Objet @param non*/
    Objet();
    Objet(const Objet* objet);
    Objet(unsigned int id, const std::string nom, const std::string description, unsigned int valeur);
    virtual ~Objet();
    virtual std::string getTypeObjet() const =0;
    unsigned int getIdObjet();
    std::string getNom() const;
    void setNomObjet(const std::string nom);
    std::string getDescriptionObjet() const;
    void setDescriptionObjet(const std::string description);
    unsigned int getValeurObjet() const;
    void setValeurObjet(unsigned int valeur);

    protected:
    unsigned int m_id;   // id de l'objet
    std::string m_nom;   // nom de l'objet
    std::string m_description;   // description de l'objet
    unsigned int m_valeur;   // valeur en or de l'objet
};

class Materiau: public Objet {
    public:
    /**@brief Constructeur par defaut de Materiau @param non*/
    Materiau();
    Materiau(const Materiau* materiau);
    Materiau(unsigned int id, const std::string nom, const std::string description, unsigned int valeur, unsigned int nombreExemplaires, unsigned int nombreMax);
    virtual std::string getTypeObjet() const;
    unsigned int getNombreExemplaires() const;
    void modifierNombreExemplaires(int nombre);
    unsigned int getNombreMax();

    private:
    unsigned int m_nombreExemplaires;
    unsigned int m_nombreMax;
};

class ObjetOrdinaire: public Objet {
    public:
    /**@brief Constructeur par defaut de Objet_Ordinaire @param non*/
    ObjetOrdinaire();
    ObjetOrdinaire(const ObjetOrdinaire* objetOrdinaire);
    ObjetOrdinaire(unsigned int id, const std::string nom, const std::string description, unsigned int valeur, unsigned int pv, unsigned int mana, unsigned int defense, unsigned int attaque);
    virtual ~ObjetOrdinaire();
    virtual std::string getTypeObjet() const =0;
    unsigned int getPvObjet() const;
    void setPvObjet(unsigned int pv);
    unsigned int getManaObjet() const;
    void setManaObjet(unsigned int mana);
    unsigned int getDefenseObjet() const;
    void setDefenseObjet(unsigned int defense);
    unsigned int getAttaqueObjet() const;
    void setAttaqueObjet(unsigned int attaque);

    protected:
    unsigned int m_pv;  // points de vie offerts par l'objet
    unsigned int m_mana;  // mana offert par l'objet
    unsigned int m_defense;  // defense offerte par l'objet
    unsigned int m_attaque;  // attaque offerte par l'objet
};

class Equipement: public ObjetOrdinaire {
    public:
    /**@brief Constructeur par defaut de Equipement @param non*/
    Equipement();
    Equipement(const Equipement* equipement);
    Equipement(unsigned int id, const std::string nom, const std::string description, unsigned int valeur, unsigned int pv, unsigned int mana, unsigned int defense, unsigned int attaque, unsigned int exp,unsigned int emplacement);
    virtual std::string getTypeObjet() const;
    unsigned int getMultiplicateurExp() const;
    unsigned int getEmplacement();

    private:
    unsigned int m_exp;  // pourcentage d'exp gagne en plus
    unsigned int m_emplacement;
};

class Consommable: public ObjetOrdinaire {
    public:
    /**@brief Constructeur par defaut de Consommable @param non*/
    Consommable();
    Consommable(const Consommable* consommable);
    Consommable(unsigned int id, const std::string nom, const std::string description, unsigned int valeur, unsigned int pv, unsigned int mana, unsigned int defense, unsigned int attaque, unsigned int exp);
    virtual std::string getTypeObjet() const;
    unsigned int getExpBonus() const;
    private:
    unsigned int m_exp; // experience offerte par l'objet
};

class ObjetSpecial: public Objet {
    public:
    /**@brief Constructeur par defaut de Special @param non*/
    ObjetSpecial();
    ObjetSpecial(const ObjetSpecial* objetSpecial);
    ObjetSpecial(unsigned int id, const std::string nom, const std::string description, unsigned int valeur);
    virtual std::string getTypeObjet() const;
};

#endif // OBJET_H
