#include "Combat.h"
#include "assert.h"

Combat::Combat(Joueur* joueur, Personnage* equipe[3], Ennemi* ennemi[5], unsigned int vitesse[5]) {
    m_joueur = joueur;
    m_nbEquipier = 0;
    for (unsigned int i=0; i<3; i++) {
        if (equipe[i]!=NULL)
            m_nbEquipier++;
    }
    m_nbEnnemi = 0;
    for (unsigned int i=0; i<5; i++) {
        if (ennemi[i]!=NULL)
            m_nbEnnemi++;
    }
    m_equipe=new Personnage*[m_nbEquipier];
    for (unsigned int i=0; i<m_nbEquipier; i++)
        m_equipe[i]=equipe[i];
    m_ennemi=new Ennemi*[m_nbEnnemi];
    for (unsigned int i=0; i<m_nbEnnemi; i++)
        m_ennemi[i]=ennemi[i];
    m_vivant=new bool[1+m_nbEquipier+m_nbEnnemi];
    m_ordre=new Point[1+m_nbEquipier+m_nbEnnemi];
    m_nbTour = 1;
    //m_logs= new std::vector<std::string>;
    m_tabNomOrdo=new std::string [1+m_nbEquipier+m_nbEnnemi];
    m_tabNomNonOrdo=new std::string [1+m_nbEquipier+m_nbEnnemi];
    m_statutPersonnage=new unsigned int*[1+m_nbEquipier+m_nbEnnemi];
    m_pointsDeViePersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_pointsDeVieMaxPersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_manaPersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_manaMaxPersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_attaquePersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_defensePersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_initiativePersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_bouclierPersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_poisonPersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_esquivePersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_mainPersonnage=new Main[1+m_nbEquipier+m_nbEnnemi];
    m_deckPersonnage=new Deck[1+m_nbEquipier+m_nbEnnemi];
    m_defaussePersonnage=new TableauCartes[1+m_nbEquipier+m_nbEnnemi];
    for (unsigned int i=0; i<1+m_nbEquipier+m_nbEnnemi; i++) {

        m_vivant[i]=true;
        m_manaPersonnage[i]=1;
        m_bouclierPersonnage[i]=0;
        m_poisonPersonnage[i]=0;
        m_esquivePersonnage[i]=0;
        m_statutPersonnage[i]=new unsigned int[6];
        for (unsigned int j=0; j<6; j++)
            m_statutPersonnage[i][j]=0;
    }
    for (unsigned int i=0; i<1+m_nbEquipier+m_nbEnnemi; i++) {
        if(i==0){
            m_tabNomNonOrdo[i]="Joueur";
        }
        else if(i<1+m_nbEquipier){
            m_tabNomNonOrdo[i]=m_equipe[i-1]->getNom();
        }
        else if(i>=1+m_nbEquipier) {
                m_tabNomNonOrdo[i]=m_ennemi[i-(m_nbEquipier+1)]->getNom();

        }
    }
    m_pointsDeViePersonnage[0]=m_joueur->getPointsDeVie();
    for (unsigned int i=1; i<1+m_nbEquipier; i++)
        m_pointsDeViePersonnage[i]=m_equipe[i-1]->getPointsDeVie();
    for (unsigned int i=1+m_nbEquipier; i<1+m_nbEquipier+m_nbEnnemi; i++)
        m_pointsDeViePersonnage[i]=m_ennemi[i-1-m_nbEquipier]->getPointsDeVie();
    m_pointsDeVieMaxPersonnage[0]=m_joueur->getPointsDeVieMax();
    for (unsigned int i=1; i<1+m_nbEquipier; i++)
        m_pointsDeVieMaxPersonnage[i]=m_equipe[i-1]->getPointsDeVieMax();
    for (unsigned int i=1+m_nbEquipier; i<1+m_nbEquipier+m_nbEnnemi; i++)
        m_pointsDeVieMaxPersonnage[i]=m_ennemi[i-1-m_nbEquipier]->getPointsDeVieMax();
    m_manaMaxPersonnage[0]=m_joueur->getPointsDeManaMax();
    for (unsigned int i=1; i<1+m_nbEquipier; i++)
        m_manaMaxPersonnage[i]=m_equipe[i-1]->getPointsDeManaMax();
    for (unsigned int i=1+m_nbEquipier; i<1+m_nbEquipier+m_nbEnnemi; i++)
        m_manaMaxPersonnage[i]=m_ennemi[i-1-m_nbEquipier]->getPointsDeManaMax();
    m_attaquePersonnage[0]=m_joueur->getAttaque();
    for (unsigned int i=1; i<1+m_nbEquipier; i++)
        m_attaquePersonnage[i]=m_equipe[i-1]->getAttaque();
    for (unsigned int i=1+m_nbEquipier; i<1+m_nbEquipier+m_nbEnnemi; i++)
        m_attaquePersonnage[i]=m_ennemi[i-1-m_nbEquipier]->getAttaque();
    m_defensePersonnage[0]=m_joueur->getDefense();
    for (unsigned int i=1; i<1+m_nbEquipier; i++)
        m_defensePersonnage[i]=m_equipe[i-1]->getDefense();
    for (unsigned int i=1+m_nbEquipier; i<1+m_nbEquipier+m_nbEnnemi; i++)
        m_defensePersonnage[i]=m_ennemi[i-1-m_nbEquipier]->getDefense();
    m_initiativePersonnage[0]=m_joueur->getVitesse();
    for (unsigned int i=1; i<1+m_nbEquipier; i++)
        m_initiativePersonnage[i]=m_equipe[i-1]->getVitesse();
    for (unsigned int i=1+m_nbEquipier; i<1+m_nbEquipier+m_nbEnnemi; i++)
        m_initiativePersonnage[i]=m_ennemi[i-1-m_nbEquipier]->getVitesse();
    unsigned int indice;
    Deck deckBaseJoueur(m_joueur->getDeck());
    unsigned int nbCartesJoueur = deckBaseJoueur.getNombreCartes();
    while (nbCartesJoueur>0) {
        indice = rand()%nbCartesJoueur;
        m_deckPersonnage[0].ajouterCarte(deckBaseJoueur.getCarte(indice));
        deckBaseJoueur.retirerCarte(indice);
        nbCartesJoueur--;
    }
    for (unsigned int i=1; i<1+m_nbEquipier; i++) {
        Deck deckBaseEquipier(m_equipe[i-1]->getDeck());
        unsigned int nbCartesEquipier = deckBaseEquipier.getNombreCartes();
        while (nbCartesEquipier>0) {
            indice = rand()%nbCartesEquipier;
            m_deckPersonnage[i].ajouterCarte(deckBaseEquipier.getCarte(indice));
            deckBaseEquipier.retirerCarte(indice);
            nbCartesEquipier--;
        }
    }
    for (unsigned int i=1+m_nbEquipier; i<1+m_nbEquipier+m_nbEnnemi; i++) {
        Deck deckBaseEnnemi(m_ennemi[i-1-m_nbEquipier]->getDeck());
        unsigned int nbCartesEnnemi = deckBaseEnnemi.getNombreCartes();
        while (nbCartesEnnemi>0) {
            indice = rand()%nbCartesEnnemi;
            m_deckPersonnage[i].ajouterCarte(deckBaseEnnemi.getCarte(indice));
            deckBaseEnnemi.retirerCarte(indice);
            nbCartesEnnemi--;
        }
    }
    // Pour l'instant, on laisse la taille par defaut des Main mais elles pourraient etre modifiees plus tard grace a des equipements ou autre

    for (unsigned int i=0; i<m_mainPersonnage[0].getTailleInitiale(); i++) {
        m_mainPersonnage[0].ajouterCarte(m_deckPersonnage[0].retirerCarte(0));
    }
    for (unsigned int i=1; i<1+m_nbEquipier; i++) {
        for (unsigned int j=0; j<m_mainPersonnage[i-1].getTailleInitiale(); j++) {
             m_mainPersonnage[i].ajouterCarte(m_deckPersonnage[i].retirerCarte(0));
        }
    }
    for (unsigned int i=1+m_nbEquipier; i<1+m_nbEquipier+m_nbEnnemi; i++) {
        for (unsigned int j=0; j<m_mainPersonnage[i].getTailleInitiale(); j++) {
             m_mainPersonnage[i].ajouterCarte(m_deckPersonnage[i].retirerCarte(0));
        }
    }
    m_ordre[0].x=0;
    m_ordre[0].y=m_joueur->getVitesse();
    for (unsigned int i=1; i<1+m_nbEquipier; i++) {
        m_ordre[i].x=i;
        m_ordre[i].y=m_equipe[i-1]->getVitesse();
    }
    for (unsigned int i=1+m_nbEquipier; i<1+m_nbEquipier+m_nbEnnemi; i++) {
        m_ordre[i].x=i;
        m_ordre[i].y=vitesse[i-1-m_nbEquipier];
    }
    triPartitionPoint(m_ordre,0,m_nbEquipier+m_nbEnnemi);
    m_ordreActuel=0;
    m_personnageActuel=m_ordre[m_ordreActuel].x;
    m_gainExperience=0;
    for (unsigned int i=0; i<1+m_nbEquipier+m_nbEnnemi; i++) {  // on parcourt m_ordre
       if (m_ordre[i].x==0) {// c'est le joueur
            m_tabNomOrdo[i]=joueur->getNom();
            }
        else if ((unsigned int)m_ordre[i].x<1+m_nbEquipier)  // c'est un equipier
            m_tabNomOrdo[i]=equipe[m_ordre[i].x-1]->getNom();
        else  // c'est un ennemi
            m_tabNomOrdo[i]=ennemi[m_ordre[i].x-1-m_nbEquipier]->getNom();
    }

}

Combat::Combat(const Combat* c) {
    m_joueur = NULL;
    m_nbEquipier = c->m_nbEquipier;
    m_nbEnnemi = c->m_nbEnnemi;
    m_equipe=new Personnage*[m_nbEquipier];
    m_tabNomOrdo=NULL;
    m_tabNomNonOrdo=NULL;
    for (unsigned int i=0; i<m_nbEquipier; i++)
        m_equipe[i]=NULL;
    m_ennemi=new Ennemi*[m_nbEnnemi];
    for (unsigned int i=0; i<m_nbEnnemi; i++)
        m_ennemi[i]=NULL;
    m_vivant=new bool[1+m_nbEquipier+m_nbEnnemi];
    m_ordre=new Point[1+m_nbEquipier+m_nbEnnemi];
    m_nbTour = c->m_nbTour;
    m_statutPersonnage=new unsigned int*[1+m_nbEquipier+m_nbEnnemi];
    m_pointsDeViePersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_pointsDeVieMaxPersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_manaPersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_manaMaxPersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_attaquePersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_defensePersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_initiativePersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_bouclierPersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_poisonPersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_esquivePersonnage=new unsigned int[1+m_nbEquipier+m_nbEnnemi];
    m_mainPersonnage=new Main[1+m_nbEquipier+m_nbEnnemi];
    m_deckPersonnage=new Deck[1+m_nbEquipier+m_nbEnnemi];
    m_defaussePersonnage=new TableauCartes[1+m_nbEquipier+m_nbEnnemi];
    for (unsigned int i=0; i<1+m_nbEquipier+m_nbEnnemi; i++) {
        m_vivant[i]=c->m_vivant[i];
        m_manaPersonnage[i]=c->m_manaPersonnage[i];
        m_bouclierPersonnage[i]=c->m_bouclierPersonnage[i];
        m_poisonPersonnage[i]=c->m_poisonPersonnage[i];
        m_esquivePersonnage[i]=c->m_esquivePersonnage[i];
        m_statutPersonnage[i]=new unsigned int[6];
        for (unsigned int j=0; j<6; j++)
            m_statutPersonnage[i][j]=c->m_statutPersonnage[i][j];
        m_pointsDeViePersonnage[i]=c->m_pointsDeViePersonnage[i];
        m_pointsDeVieMaxPersonnage[i]=c->m_pointsDeVieMaxPersonnage[i];
        m_manaMaxPersonnage[i]=c->m_manaMaxPersonnage[i];
        m_attaquePersonnage[i]=c->m_attaquePersonnage[i];
        m_defensePersonnage[i]=c->m_defensePersonnage[i];
        m_initiativePersonnage[i]=c->m_initiativePersonnage[i];
        Deck deck=c->m_deckPersonnage[i];
        for (unsigned int j=0; j<deck.getNombreCartes(); j++) {
            m_deckPersonnage[i].ajouterCarte(deck.getCarte(j));
        }
        Main main=c->m_mainPersonnage[i];
        for (unsigned int j=0; j<main.getNombreCartes(); j++) {
            m_mainPersonnage[i].ajouterCarte(main.getCarte(j));
        }
        TableauCartes defausse=c->m_defaussePersonnage[i];
        for (unsigned int j=0; j<defausse.getNombreCartes(); j++) {
            m_defaussePersonnage[i].ajouterCarte(defausse.getCarte(j));
        }
        m_ordre[i].x=c->m_ordre[i].x;
        m_ordre[i].y=c->m_ordre[i].y;
        m_ordreActuel=c->m_ordreActuel;
    }
    m_personnageActuel=c->m_personnageActuel;
    m_gainExperience=0;
}

Combat::~Combat() {
    delete m_equipe;
    delete m_ennemi;
    delete [] m_vivant;
    delete [] m_ordre;
    for (unsigned int i=0; i<1+m_nbEquipier+m_nbEnnemi; i++)
        delete [] m_statutPersonnage[i];
    delete [] m_statutPersonnage;
    delete [] m_pointsDeViePersonnage;
    delete [] m_pointsDeVieMaxPersonnage;
    delete [] m_manaPersonnage;
    delete [] m_manaMaxPersonnage;
    delete [] m_attaquePersonnage;
    delete [] m_defensePersonnage;
    delete [] m_initiativePersonnage;
    delete [] m_bouclierPersonnage;
    delete [] m_poisonPersonnage;
    delete [] m_esquivePersonnage;
    delete [] m_mainPersonnage;
    delete [] m_deckPersonnage;
    delete [] m_defaussePersonnage;
    if(m_tabNomOrdo!=NULL)
        delete [] m_tabNomOrdo;
    if(m_tabNomNonOrdo!=NULL)
        delete [] m_tabNomNonOrdo;
}

Joueur* Combat::getJoueur() {
    return m_joueur;
}

unsigned int Combat::getNbEquipier() {
    return m_nbEquipier;
}

Personnage* Combat::getEquipier(unsigned int i) {
    return m_equipe[i];
}

unsigned int Combat::getNbEnnemi() {
    return m_nbEnnemi;
}

Ennemi* Combat::getEnnemi(unsigned int i) {
    return m_ennemi[i];
}

bool Combat::getVivant(unsigned int i) {
    return m_vivant[i];
}

unsigned int Combat::getOrdre(unsigned int i) {
    return m_ordre[i].x;
}

unsigned int Combat::getOrdreActuel() {
    return m_ordreActuel;
}

unsigned int Combat::augmenterOrdreActuel() {
    if (!m_vivant[0])
        return 1;
    else if (!ennemiRestant())
        return 2;
    do {
        m_ordreActuel++;
        if (m_ordreActuel>m_nbEquipier+m_nbEnnemi) {
            finirTour();
            if (!m_vivant[0])
                return 1;
            else if (!ennemiRestant())
                return 2;
        }
        m_personnageActuel=m_ordre[m_ordreActuel].x;
    } while (!m_vivant[m_personnageActuel] || m_statutPersonnage[m_personnageActuel][3]);
    return 0;
}

unsigned int Combat::getNbTour() {
    return m_nbTour;
}

unsigned int Combat::getStatutPersonnage(unsigned int i, unsigned int statut) {
    return m_statutPersonnage[i][statut];
}

unsigned int Combat::getPointsDeViePersonnage(unsigned int i) {
    return m_pointsDeViePersonnage[i];
}

unsigned int Combat::getPointsDeVieMaxPersonnage(unsigned int i) {
    return m_pointsDeVieMaxPersonnage[i];
}

unsigned int Combat::getManaPersonnage(unsigned int i) {
    return m_manaPersonnage[i];
}

unsigned int Combat::getManaMaxPersonnage(unsigned int i) {
    return m_manaMaxPersonnage[i];
}

unsigned int Combat::getAttaquePersonnage(unsigned int i) {
    return m_attaquePersonnage[i];
}

unsigned int Combat::getDefensePersonnage(unsigned int i) {
    return m_defensePersonnage[i];
}

unsigned int Combat::getInitiativePersonnage(unsigned int i) {
    return m_initiativePersonnage[i];
}

unsigned int Combat::getBouclierPersonnage(unsigned int i) {
    return m_bouclierPersonnage[i];
}

unsigned int Combat::getPoisonPersonnage(unsigned int i) {
    return m_poisonPersonnage[i];
}

unsigned int Combat::getEsquivePersonnage(unsigned int i) {
    return m_esquivePersonnage[i];
}

Main Combat::getMainPersonnage(unsigned int i) {
    return m_mainPersonnage[i];
}

Deck Combat::getDeckPersonnage(unsigned int i) {
    return m_deckPersonnage[i];
}

TableauCartes Combat::getDefaussePersonnage(unsigned int i) {
    return m_defaussePersonnage[i];
}

void Combat::piocherCarte(unsigned int i) {
    if (m_deckPersonnage[i].getNombreCartes()==0) {
        unsigned int nbCartesDefausse = m_defaussePersonnage[i].getNombreCartes();
        unsigned int indice;
        while (nbCartesDefausse>0) {
            indice = rand()%nbCartesDefausse;
            m_deckPersonnage[i].ajouterCarte(m_defaussePersonnage[i].getCarte(indice));
            m_defaussePersonnage[i].retirerCarte(indice);
            nbCartesDefausse--;
        }
    }
    if (m_deckPersonnage[i].getNombreCartes()>0) {
        m_mainPersonnage[i].ajouterCarte(m_deckPersonnage[i].getCarte(0));
        m_deckPersonnage[i].retirerCarte(0);
    }
}

unsigned int Combat::getPersonnageActuel(){
    return m_personnageActuel;

}
unsigned int Combat::jouerCarte(Carte* carte, unsigned int positionCarte, unsigned int personnageCible) {

    m_manaPersonnage[m_personnageActuel]-=carte->getCout();
    m_mainPersonnage[m_personnageActuel].retirerCarte(positionCarte);
    m_defaussePersonnage[m_personnageActuel].ajouterCarte(carte);
    m_derniereCible=personnageCible;
    switch (carte->getTypeCarte()[0])
    {
        case 'A': {
                Attaque* carteAttaque=static_cast<Attaque*>(carte);
                // La valeur de retour de la fonction depend de la cible et de ce qu'il s'est passe :
                // 0 : la victime de l'attaque est l'adversaire cibl�, sans effet particulier
                // 1 : l'attaquant se blesse dans sa confusion
                // 2 : l'attaquant se blesse mais est invulnerable
                // 3 : la cible est invulnerable
                // 4 : la cible esquive
                // 5 : la cible est vaincue
                // 6 : l'attaquant est vaincu
                int valeurRetour=0;

                if (m_statutPersonnage[m_personnageActuel][4]>0 && rand()%4==0)  // l'attaquant se blesse dans sa confusion
                {
                    valeurRetour=1;
                    if (m_statutPersonnage[m_personnageActuel][0]==1)  // l'attaquant est invulnerable
                        valeurRetour=2;
                }
                else if (m_statutPersonnage[personnageCible][0]==1)  // la cible est invulnerable
                    valeurRetour=3;
                else if ((unsigned int)rand()%100+1<m_esquivePersonnage[personnageCible])  // la cible esquive
                    valeurRetour=4;
                else // La cible n'est pas invunerable et n'esquive pas
                {
                    float modificateur=(float)(m_attaquePersonnage[m_personnageActuel])/m_defensePersonnage[personnageCible]; // On applique le modificateur d'attaque en fonction de l'attaque et de la defense
                    if (carteAttaque->getSilence())
                        m_statutPersonnage[personnageCible][3]=1;
                    if (carteAttaque->getConfusion())
                        m_statutPersonnage[personnageCible][4]=2;
                    if (carteAttaque->getPoison()) {
                        m_statutPersonnage[personnageCible][5]=3;
                        m_poisonPersonnage[personnageCible]=std::min(m_pointsDeViePersonnage[personnageCible],(unsigned int)(modificateur*carteAttaque->getPointsDeDegat()/2));
                    }
                    if (carteAttaque->getAttaqueDirecte())
                        m_pointsDeViePersonnage[personnageCible]-=std::min(m_pointsDeViePersonnage[personnageCible],(unsigned int)(modificateur*carteAttaque->getPointsDeDegat()));
                    else
                    {
                        unsigned int degats=modificateur*carteAttaque->getPointsDeDegat();
                        if (m_bouclierPersonnage[personnageCible]>0) {
                            if (degats>m_bouclierPersonnage[personnageCible]) {
                                m_bouclierPersonnage[personnageCible]=0;
                                m_pointsDeViePersonnage[personnageCible]-=std::min(m_pointsDeViePersonnage[personnageCible],degats-m_bouclierPersonnage[personnageCible]);
                            }
                            else
                                m_bouclierPersonnage[personnageCible]-=degats;
                        }
                        else
                            m_pointsDeViePersonnage[personnageCible]-=std::min(m_pointsDeViePersonnage[personnageCible],degats);
                    }
                    // On applique le vol de vie uniquement si l'attaquant n'est pas la cible
                    if (carteAttaque->getVolDeVie()>0) {
                        unsigned int vol=std::min(m_pointsDeViePersonnage[personnageCible],(unsigned int)(modificateur*carteAttaque->getVolDeVie()));
                        m_pointsDeViePersonnage[personnageCible]-=vol;
                        m_pointsDeViePersonnage[m_personnageActuel]+=vol;
                        if (m_pointsDeViePersonnage[m_personnageActuel]>m_pointsDeVieMaxPersonnage[m_personnageActuel])
                            m_pointsDeViePersonnage[m_personnageActuel]=m_pointsDeVieMaxPersonnage[m_personnageActuel];
                    }
                    if (m_statutPersonnage[personnageCible][1]==1 && m_pointsDeViePersonnage[personnageCible]==0)
                        m_pointsDeViePersonnage[personnageCible]=1;
                    if (m_statutPersonnage[m_personnageActuel][1]==1 && m_pointsDeViePersonnage[m_personnageActuel]==0)
                        m_pointsDeViePersonnage[m_personnageActuel]=1;
                }
                if (m_pointsDeViePersonnage[personnageCible]==0) {
                    m_vivant[personnageCible]=false;
                    valeurRetour=5;
                }
                if (m_pointsDeViePersonnage[m_personnageActuel]==0) {
                    m_vivant[m_personnageActuel]=false;
                    valeurRetour=6;
                }
                return valeurRetour;
                break;
            }
        case 'B': {
                Bonus* carteBonus=static_cast<Bonus*>(carte);
                m_attaquePersonnage[m_personnageActuel]+=carteBonus->getAttaque();
                m_defensePersonnage[m_personnageActuel]+=carteBonus->getAttaque();
                break;
            }
        case 'D': {
                Defense* carteDefense=static_cast<Defense*>(carte);
                if (carteDefense->getRendInvulnerable()==1)
                    m_statutPersonnage[m_personnageActuel][0]=1;
                if (carteDefense->getDerniereChance()==1)
                    m_statutPersonnage[m_personnageActuel][1]=1;
                if (carteDefense->getChanceEsquive()>0)
                {
                    m_statutPersonnage[m_personnageActuel][2]=3;
                    m_esquivePersonnage[m_personnageActuel]=carteDefense->getChanceEsquive();
                }
                m_bouclierPersonnage[m_personnageActuel]+=carteDefense->getPointsBouclier();
                if (m_bouclierPersonnage[m_personnageActuel]>m_pointsDeVieMaxPersonnage[m_personnageActuel])
                    m_bouclierPersonnage[m_personnageActuel]=m_pointsDeVieMaxPersonnage[m_personnageActuel];
                break;
            }
        case 'S': {
                Soin* carteSoin=static_cast<Soin*>(carte);
                m_pointsDeViePersonnage[personnageCible]+=carteSoin->getPointsDeVie();
                if (m_pointsDeViePersonnage[personnageCible]>m_pointsDeVieMaxPersonnage[personnageCible])
                    m_pointsDeViePersonnage[personnageCible]=m_pointsDeVieMaxPersonnage[personnageCible];
                break;
            }
        default:
            break;
    }
    return 0;
}

void Combat::finirTour() {   // Retourne 0 si tout va bien, 1 si le joueur est vaincu, 2 si tous les ennemis sont vaincus
    m_nbTour++;
    for (unsigned int i=0; i<1+m_nbEquipier+m_nbEnnemi; i++) {
        if (m_poisonPersonnage[i]>=m_pointsDeViePersonnage[i]) {
            m_pointsDeViePersonnage[i]=0;
            m_vivant[i]=false;
        }
        else {
            m_manaPersonnage[i]=std::min(m_nbTour,m_manaMaxPersonnage[i]);
            if (m_statutPersonnage[i][0]==1)
                m_statutPersonnage[i][0]=0;
            if (m_statutPersonnage[i][1]==1)
                m_statutPersonnage[i][1]=0;
            if (m_statutPersonnage[i][2]>0) {
                m_statutPersonnage[i][2]--;
                if (m_statutPersonnage[i][2]==0)
                    m_esquivePersonnage[i]=0;
            }
            m_statutPersonnage[i][3]=0;
            if (m_statutPersonnage[i][4]>0)
                m_statutPersonnage[i][4]--;
            if (m_statutPersonnage[i][5]>0 && rand()%3!=0)
                m_statutPersonnage[i][5]--;
            if (m_statutPersonnage[i][5]==0)
                m_poisonPersonnage[i]=0;
        }
        piocherCarte(i);
    }
    ordonnerJeu();
}

bool Combat::ennemiRestant() {
    for (unsigned int i=1+m_nbEquipier; i<1+m_nbEquipier+m_nbEnnemi; i++) {
        if (m_vivant[i])
            return true;
    }
    return false;
}

void Combat::ordonnerJeu() {
    for (unsigned int i=0; i<1+m_nbEquipier+m_nbEnnemi; i++) {
        m_ordre[i].x=i;
        m_ordre[i].y=m_initiativePersonnage[i];
    }
    triPartitionPoint(m_ordre,0,m_nbEquipier+m_nbEnnemi);
    m_ordreActuel=0;
    m_personnageActuel=m_ordre[m_ordreActuel].x;
}

unsigned int Combat::getGainExperience() {
    return m_gainExperience;
}

Carte* Combat::finirCombat() {
    for (unsigned int i=0; i<m_nbEnnemi; i++) {
        m_gainExperience+=m_ennemi[i]->getExp()+(m_ennemi[i]->getNiveau()-1)*(m_ennemi[i]->getExp()/2);   // Formule de calcul du gain d'experience, x+(n-1)*(x/2) avec x l'experience au niveau 1 et n le niveau
    }
    for (unsigned int i=0; i<m_defaussePersonnage[1+m_nbEquipier].getNombreCartes(); i++) {
        m_deckPersonnage[1+m_nbEquipier].ajouterCarte(m_defaussePersonnage[1+m_nbEquipier].retirerCarte(0));
    }
    for (unsigned int i=0; i<m_mainPersonnage[1+m_nbEquipier].getNombreCartes(); i++) {
        m_deckPersonnage[1+m_nbEquipier].ajouterCarte(m_mainPersonnage[1+m_nbEquipier].retirerCarte(0));
    }
    return m_deckPersonnage[1+m_nbEquipier].getCarte(rand()%m_deckPersonnage[1+m_nbEquipier].getNombreCartes());    // On renvoie la carte gagnee
}

// Partition sur un tableau de Point
void Combat::partitionPoint(Point T[], int pivot, unsigned int debut, unsigned int fin, unsigned int& k1, unsigned int& k2) {
    k1=fin, k2=debut;
    do {
        while (T[k2].y>pivot) {
            k2++;
        }
        while (T[k1].y<pivot) {
            k1--;
        }
        if (k2<=k1) {
            Point p;
            p.x=T[k1].x;
            p.y=T[k1].y;
            T[k1].x=T[k2].x;
            T[k1].y=T[k2].y;
            T[k2].x=p.x;
            T[k2].y=p.y;
            if (k2<fin)
                k2++;
            if (k1>debut)
                k1--;
        }
    } while (k1>=k2);
}

// Algorithme du tri par partition (tri par pivot) sur un tableau de Point
void Combat::triPartitionPoint(Point T[], unsigned int debut, unsigned int fin) {
    unsigned int k1, k2;
    int pivot=T[(debut+fin)/2].y;
    partitionPoint(T,pivot,debut,fin,k1,k2);
    if (debut<k1)
        triPartitionPoint(T,debut,k1);
    if (k2<fin)
        triPartitionPoint(T,k2,fin);
}

int Combat::valeur() {  // On evalue la situation du point de vue de l'ennemi
    int valeurEquipeJoueur=0;
    for (unsigned int i=0; i<1+m_nbEquipier; i++) {
        int valeurJoueur=0;
        valeurJoueur+=m_vivant[i]*(m_pointsDeVieMaxPersonnage[i]*(m_attaquePersonnage[i]+m_defensePersonnage[i])/200*m_esquivePersonnage[i]/100);
        valeurJoueur+=m_pointsDeViePersonnage[i]+m_bouclierPersonnage[i]-m_poisonPersonnage[i]*m_statutPersonnage[i][5];
        valeurJoueur*=1+m_statutPersonnage[i][i]-m_statutPersonnage[i][3]-m_statutPersonnage[i][4];
        if ((float)m_pointsDeViePersonnage[i]/m_pointsDeVieMaxPersonnage[i]<0.1)
            valeurJoueur*=1+m_statutPersonnage[i][1];
        valeurEquipeJoueur+=valeurJoueur;
    }
    int valeurEquipeEnnemi=0;
    for (unsigned int i=1+m_nbEquipier; i<1+m_nbEquipier+m_nbEnnemi; i++) {
        int valeurEnnemi=0;
        valeurEnnemi+=m_vivant[1]*(m_pointsDeVieMaxPersonnage[1]*(m_attaquePersonnage[1]+m_defensePersonnage[1])/200*m_esquivePersonnage[1]/100);
        valeurEnnemi+=m_pointsDeViePersonnage[1]+m_bouclierPersonnage[1]-m_poisonPersonnage[1]*m_statutPersonnage[1][5];
        valeurEnnemi*=1+m_statutPersonnage[1][0]-m_statutPersonnage[1][3]-m_statutPersonnage[1][4];
        if ((float)m_pointsDeViePersonnage[1]/m_pointsDeVieMaxPersonnage[1]<0.1)
            valeurEnnemi*=1+m_statutPersonnage[1][1];
        valeurEquipeEnnemi+=valeurEnnemi;
    }
    int valeurRetour;
    if (m_personnageActuel<1+m_nbEquipier)
        valeurRetour=valeurEquipeJoueur-valeurEquipeEnnemi;
    else
        valeurRetour=valeurEquipeEnnemi-valeurEquipeJoueur;
    if (valeurRetour<-999999)       // On borne la valeur de retour
        valeurRetour=-999999;
    else if (valeurRetour>999999)
        valeurRetour=999999;
    return valeurRetour;
}

void Combat::coupsPossibles(std::vector<Point>& coups) {
    Main m=m_mainPersonnage[m_personnageActuel];
    for (unsigned int i=0; i<m.getNombreCartes(); i++) {
        if (m.getCarte(i)->getCout()<m_manaPersonnage[m_personnageActuel]) {
            Point p;
            p.y=i;
            if (m.getCarte(i)->getTypeCarte()!="Attaque") {
                p.x=m_personnageActuel;
                coups.push_back(p);
            }
            else {
                if (m_personnageActuel<1+m_nbEquipier) {
                    for (unsigned int j=1+m_nbEquipier; j<1+m_nbEquipier+m_nbEnnemi; j++) {
                        if (m_vivant[j]) {
                            p.x=j;
                            coups.push_back(p);
                        }
                    }
                }
                else {
                    for (unsigned int j=0; j<1+m_nbEquipier; j++) {
                        if (m_vivant[j]) {
                            p.x=j;
                            coups.push_back(p);
                        }
                    }
                }
            }
        }
    }
    coups.push_back(Point{-1,-1});
}

Point Combat::meilleurCoup(Point mc, int &valeurMC) {
    if (mc.y==-1) {  // On s'arrete, le coup correspond a la fin du tour
        valeurMC=valeur();    // On recupere la valeur de la situation
        return mc;
    }
    else {
        std::vector<Point> coups;
        coupsPossibles(coups);        // On etablit la liste des coups possibles, y=-1 correspond a une fin de tour
        Point p;
        valeurMC=-999999;   // On met une valeur equivalent a - l'infini, qui ne peut pas etre superieure a une valeur donnee par valeur()
        for (unsigned int k=0; k<coups.size(); k++) {     // On parcourt les coups disponibles
            Combat situation(this);       // On cree une simulation de combat et, si le coup n'est pas une fin de tour, on joue la carte selectionnee dans cette simulation de combat
            if (coups[k].y!=-1)
                situation.jouerCarte(m_mainPersonnage[m_personnageActuel].getCarte(coups[k].y),coups[k].y,coups[k].x);
            int valeur2=valeurMC;
            situation.meilleurCoup(coups[k],valeur2);    // On simule recursivement
            if (valeurMC<valeur2) {     // Si le resultat est plus grand que la valeur actuelle, on prend ce resultat et ce coup comme valeurs de reference du meilleur coup
                p.x=coups[k].x;
                p.y=coups[k].y;
                valeurMC=valeur2;
            }
        }
        return p;  // On renvoie le point donnant la cible et l'indice de la carte a jouer en premier pour atteindre la meilleure situation
    }
}


std::string Combat::getNomOrdo(unsigned int i) const {
    return m_tabNomOrdo[i];
}

std::string Combat::getLogs(unsigned int i) const {
    return m_logs[i];
}
void Combat::setLogs(std::string s){
    m_logs.push_back(s);
}
unsigned int Combat::getTailleLogs() const {
    return m_logs.size();
}

unsigned int Combat::getCible() const{
    return m_derniereCible;
}
std::string Combat::getNomNonOrdo(unsigned int i) const {
    return m_tabNomNonOrdo[i];
}
