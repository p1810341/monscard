#include "PNJ.h"
#include <iostream>

PNJ::PNJ() {
    m_idPNJ=0;
    m_nomPNJ = "";
    m_indiceDialogue = 0;
    m_position.x = 0;
    m_position.y = 0;
    m_parle = false;
    m_aDialogue=false;
}

PNJ::PNJ(int x, int y, unsigned int id, const std::string& nom) {
    m_idPNJ=id;
    m_indiceDialogue = 0;
    m_nomPNJ = nom;
    m_position.x = x;
    m_position.y = y;
    m_aDialogue = false;
    m_parle = false;
}

PNJ::~PNJ() {
    for(unsigned int i = 0 ; i < m_tableauDialogues.size() ; i++) {
        delete m_tableauDialogues[i];
    }
}

std::string PNJ::getNomPNJ() const {
    return m_nomPNJ;
}

Point PNJ::getPosition() const {
    return m_position;
}

Dialogue* PNJ::getDialogue(int i) const {
    return m_tableauDialogues[i];
}

unsigned int PNJ::getIndiceDialogue() const {
    return m_indiceDialogue;
}

void PNJ::augmenterIndiceDialogue() {
    m_indiceDialogue++;
    if(m_indiceDialogue == m_tableauDialogues.size()) {
        m_indiceDialogue--;
    }
}

void PNJ::setParle() {
    m_parle = !m_parle;
}

bool PNJ::getParle() const {
    return m_parle;
}


void PNJ::ajouterDialogue(unsigned int i, Dialogue* d) {
    if (!m_aDialogue)
        m_aDialogue = true;
    m_tableauDialogues.insert(m_tableauDialogues.begin()+i,d);
}

void PNJ::retirerDialogue(unsigned int i) {
    if (i<m_tableauDialogues.size()) {
        delete m_tableauDialogues[i];
        m_tableauDialogues.erase(m_tableauDialogues.begin()+i);
    }
    if (m_tableauDialogues.size()==0)
        m_aDialogue = false;
    if (i==m_tableauDialogues.size() && i>0)
        m_indiceDialogue--;
}

void PNJ::setDialogue(unsigned int i, Dialogue* d) {
    m_tableauDialogues[i]=d;
}

void PNJ::setNom(const std::string nom) {
    m_nomPNJ=nom;
}

void PNJ::reduireIndiceDialogue() {
    if (m_indiceDialogue>0)
        m_indiceDialogue--;
}

bool PNJ::getADialogue() const {
    return m_aDialogue;
}

unsigned int PNJ::getIndiceMax() const {
    return m_tableauDialogues.size()-1;
}

unsigned int PNJ::getIdPNJ() const {
    return m_idPNJ;
}

void PNJ::setPosition(int x, int y) {
    m_position.x=x;
    m_position.y=y;
}
