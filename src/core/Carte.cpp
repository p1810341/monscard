#include "Carte.h"
#include <iostream>
#include<assert.h>

///////////////////////////////////
// Carte
///////////////////////////////////

Carte::Carte() {
    m_idCarte = 0;
    m_coutEnMana = 0;
    m_nomCarte = "";
    m_descriptionCarte = "";
}

Carte::Carte(unsigned int idCarte, unsigned int coutEnMana, const std::string& nomCarte, const std::string& descriptionCarte) :
                m_idCarte(idCarte), m_coutEnMana(coutEnMana), m_nomCarte(nomCarte), m_descriptionCarte(descriptionCarte) {
}

Carte::~Carte() {};

unsigned int Carte::getIdCarte() const {
    return m_idCarte;
}

unsigned int Carte::getCout() const {
    return m_coutEnMana;
}

std::string Carte::getNomCarte() const {
    return m_nomCarte;
}

std::string Carte::getDescription() const {
    return m_descriptionCarte;
}

void Carte::testRegression() {
 Attaque carte(1,10,"test","ceci est une carte test",0,0,0,0,10.0,0.0);
 assert(carte.getIdCarte()==1);
 assert(carte.getCout()==10);
 assert(carte.getNomCarte()=="test");
 assert(carte.getDescription()=="ceci est une carte test");
 assert(carte.getTypeCarte()=="Attaque");
 std::cout<<"Test de non regression de Carte....OK"<<std::endl;
}

///////////////////////////////////
// Soin
///////////////////////////////////

Soin::Soin() {
    m_nombrePointsDeVie = 0;
}

Soin::Soin(unsigned int idCarte, unsigned int coutEnMana, const std::string& nomCarte, const std::string& descriptionCarte,
           float pointsDeVie) :
                Carte(idCarte, coutEnMana, nomCarte, descriptionCarte),
                m_nombrePointsDeVie(pointsDeVie) {
}

Soin::~Soin() {};

std::string Soin::getTypeCarte() const {
    return "Soin";
}

float Soin::getPointsDeVie() const {
    return m_nombrePointsDeVie;
}
void Soin::testRegressionSoin(){
     Soin carte(1,10,"test","ceci est une carte test",20);
     assert(carte.getIdCarte()==1);
     assert(carte.getCout()==10);
     assert(carte.getNomCarte()=="test");
     assert(carte.getDescription()=="ceci est une carte test");
     assert(carte.getTypeCarte()=="Soin");
     assert(carte.getPointsDeVie()==20);
     std::cout<<"Test de non regression de Soin....OK"<<std::endl;
}

///////////////////////////////////
// Bonus
///////////////////////////////////

Bonus::Bonus() {
    m_nombrePointsBonusAttaque = 0;
    m_nombrePointsBonusDefense = 0;
    m_nombrePointsBonusExperience = 0;
}

Bonus::Bonus(unsigned int idCarte, unsigned int coutEnMana, const std::string& nomCarte, const std::string& descriptionCarte,
             float attaque, float defense, float exp) :
                Carte(idCarte, coutEnMana, nomCarte, descriptionCarte),
                m_nombrePointsBonusAttaque(attaque),
                m_nombrePointsBonusDefense(defense),
                m_nombrePointsBonusExperience(exp) {
}

Bonus::~Bonus() {};

std::string Bonus::getTypeCarte() const {
    return "Bonus";
}

float Bonus::getAttaque() const {
    return m_nombrePointsBonusAttaque;
}

float Bonus::getDefense() const {
    return m_nombrePointsBonusDefense;
}

float Bonus::getExp() const {
    return m_nombrePointsBonusExperience;
}
void Bonus::testRegressionBonus() {
    Bonus carte(1,10,"test","ceci est une carte test",10,20,30);
    assert(carte.getIdCarte()==1);
    assert(carte.getCout()==10);
    assert(carte.getNomCarte()=="test");
    assert(carte.getDescription()=="ceci est une carte test");
    assert(carte.getTypeCarte()=="Bonus");
    assert(carte.getAttaque()==10);
    assert(carte.getDefense()==20);
    assert(carte.getExp()==30);
    std::cout<<"Test de non regression de Bonus....OK"<<std::endl;
}

///////////////////////////////////
// Defense
///////////////////////////////////

Defense::Defense() {
    m_rendInvulnerable = false;
    m_derniereChance = false;
    m_pointsBouclier = 0;
    m_chanceEsquive = 0;
}

Defense::Defense(unsigned int idCarte, unsigned int coutEnMana, const std::string& nomCarte, const std::string& descriptionCarte,
                 bool invulnerable, bool derniereChance, float pointsBouclier, float chanceEsquive) :
                    Carte(idCarte, coutEnMana, nomCarte, descriptionCarte),
                    m_rendInvulnerable(invulnerable),
                    m_derniereChance(derniereChance),
                    m_pointsBouclier(pointsBouclier),
                    m_chanceEsquive(chanceEsquive) {
}

Defense::~Defense() {};

std::string Defense::getTypeCarte() const {
    return "D�fense";
}

bool Defense::getRendInvulnerable() const {
    return m_rendInvulnerable;
}

bool Defense::getDerniereChance() const {
    return m_derniereChance;
}

float Defense::getPointsBouclier() const {
    return m_pointsBouclier;
}

float Defense::getChanceEsquive() const {
    return m_chanceEsquive;
}

void Defense::testRegressionDefense() {
    Defense carte(1,10,"test","ceci est une carte test",true,false,50,20);
    assert(carte.getIdCarte()==1);
    assert(carte.getCout()==10);
    assert(carte.getNomCarte()=="test");
    assert(carte.getDescription()=="ceci est une carte test");
    assert(carte.getTypeCarte()=="Defense");
    assert(carte.getRendInvulnerable());
    assert(!carte.getDerniereChance());
    assert(carte.getPointsBouclier()==50);
    assert(carte.getChanceEsquive()==20);
    std::cout<<"Test de non regression de Defense....OK"<<std::endl;
}

///////////////////////////////////
// Attaque
///////////////////////////////////

Attaque::Attaque() {
    m_attaqueDirecte = false;
    m_silence = false;
    m_confusion = false;
    m_poison = false;
    m_pointsDeDegat = 0;
    m_volDeVie = 0;
}

Attaque::Attaque(unsigned int idCarte, unsigned int coutEnMana, const std::string& nomCarte, const std::string& descriptionCarte,
                 bool attaqueDirecte, bool silence, bool confusion, bool poison, float pointsDeDegat, float volDeVie) :
                    Carte(idCarte, coutEnMana, nomCarte, descriptionCarte),
                    m_attaqueDirecte(attaqueDirecte),
                    m_silence(silence),
                    m_confusion(confusion),
                    m_poison(poison),
                    m_pointsDeDegat(pointsDeDegat),
                    m_volDeVie(volDeVie) {
}

std::string Attaque::getTypeCarte() const {
    return "Attaque";
}

Attaque::~Attaque() {};

bool Attaque::getAttaqueDirecte() const {
    return m_attaqueDirecte;
}

bool Attaque::getConfusion() const {
    return m_confusion;
}

bool Attaque::getSilence() const {
    return m_silence;
}

bool Attaque::getPoison() const {
    return m_poison;
}

float Attaque::getPointsDeDegat() const {
    return m_pointsDeDegat;
}

float Attaque::getVolDeVie() const {
    return m_volDeVie;
}

void Attaque::testRegressionAttaque() {
    Attaque carte(1,10,"test","ceci est une carte test",true,false,true,false,20,50);
    assert(carte.getIdCarte()==1);
    assert(carte.getCout()==10);
    assert(carte.getNomCarte()=="test");
    assert(carte.getDescription()=="ceci est une carte test");
    assert(carte.getTypeCarte()=="Attaque");
    assert(carte.getAttaqueDirecte());
    assert(!carte.getSilence());
    assert(carte.getConfusion());
    assert(!carte.getPoison());
    assert(carte.getPointsDeDegat()==20);
    assert(carte.getVolDeVie()==50);
    std::cout<<"Test de non regression de Attaque....OK"<<std::endl;
}
