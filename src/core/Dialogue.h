#ifndef DIALOGUE_H
#define DIALOGUE_H

#include <vector>
#include <string>

class Dialogue {
public:
    /**@brief Constructeur par defaut de Dialogue @param non*/
    Dialogue();
    Dialogue(const std::string& texte, bool fichier);
    /**@brief Charge les phrases du Dialogue via un texte @param [in] const std::string& texte*/
    void setDialogue(const std::string& texte);
    /**@brief Recupere l'indice de la phrase (m_indicePhrase) que le Dialogue doit afficher (phrase en cours) @param non @return unsigned int @warning  Ne modifie pas de valeur*/
    unsigned int getIndicePhrase() const;
    /**@brief Augmente l'indice de la phrase a afficher (m_indicePhrase) @param non @return bool*/
    bool augmenterIndice();  // retourne vrai si il a fait une boucle, faux sinon
    /**@brief Recupere la phrase en cours du dialogue @param non @return std::string @warning Ne modifie pas de valeur*/
    std::string getPhrase(unsigned int i) const;
    /**@brief Procedure qui permet de tester si la classe Dialogue fonctionne correctement */
    void deletePhrase(unsigned int i);
    void ajouterPhrase(unsigned int i, std::string s);
    unsigned int getIndiceMax() const;
    bool reduireIndice();    // retourne vrai si il a fait une boucle, faux sinon
    void setPhrase(unsigned int i, std::string s);

private:
    unsigned int m_indicePhrase;        // Indice de la phrase en cours
    std::vector<std::string> m_phrases; // Tableau contenant les phrases du dialogue
};

#endif // DIALOGUE_H
