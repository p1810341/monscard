#include "Inventaire.h"
#include <iostream>

Inventaire::Inventaire()
{
    m_limite=10;
    //ctor
}

Inventaire::~Inventaire()
{
    for (unsigned int i=0; i<m_tableauObjets.size(); i++)
        delete m_tableauObjets[i];
}

unsigned int Inventaire::getLimite() const{
    return m_limite;
}
void Inventaire::setLimite(unsigned int nouvelleLimite){
    m_limite=nouvelleLimite;
}
unsigned int Inventaire::getOr() const{
    return m_or;
}

void Inventaire::changeOr(int nbOr){
    m_or+=nbOr;
}

Objet* Inventaire::getObjet(unsigned int i){
    if (i<m_tableauObjets.size())
        return m_tableauObjets[i];
    return NULL;
}

Objet* Inventaire::getIemeObjet(unsigned int i){
    unsigned ieme=1;
    if (i>0 && i<=m_nbObjet) {
        for (unsigned int j=0; j<m_limite; j++) {
            if (m_tableauObjets[j]!=NULL) {
                if (i==ieme)
                    return m_tableauObjets[j];
                else
                    ieme++;
            }
        }
    }
    return NULL;
}

bool Inventaire::ajouterObjet(Objet * obj) {
    if(m_nbObjet==m_limite){
        return 0;
    }
    else {
        unsigned int indice=0;
        bool emplacementLibre=false;
        for (unsigned int i=0; i<m_limite; i++) {
            if (i<m_tableauObjets.size() && m_tableauObjets[i]==NULL && !emplacementLibre) {
                indice=i;
                emplacementLibre=true;
            }
        }
        if (emplacementLibre)
            m_tableauObjets[indice]=obj;
        else
            m_tableauObjets.push_back(obj);
        m_nbObjet++;
        return 1;
    }
}

void Inventaire::retirerObjet(unsigned int i) {
    if (i<m_tableauObjets.size() && m_tableauObjets[i]!=NULL) {
        m_tableauObjets[i]=NULL;
        m_nbObjet--;
    }
}

void Inventaire::retirerIemeObjet(unsigned int i) {
    unsigned ieme=1;
    if (i>0 && i<=m_nbObjet) {
        for (unsigned int j=0; j<m_limite; j++) {
            if (m_tableauObjets[j]!=NULL) {
                if (i==ieme) {
                    m_tableauObjets[j]=NULL;
                    m_nbObjet--;
                    return;
                }
                else
                    ieme++;
            }
        }
    }
}

unsigned int Inventaire::getNbObjet() const {
    return m_nbObjet;
}
