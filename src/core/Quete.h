#ifndef QUETE_H
#define QUETE_H

#include <vector>
#include "Inventaire.h"
#include "Point.h"
#include "PNJ.h"

class Quete {
    public:
    /**@brief Constructeur par defaut de Quete @param non*/
    Quete(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible);
    virtual ~Quete();
    std::string getNom() const;
    virtual std::string getType() const = 0;
    char getEtat() const;
    void changeEtat();
    void setEtatRecursif(char etat);
    unsigned int getNbEtapes() const;
    Quete* getEtape(unsigned int i) const;
    unsigned int getOr() const;
    unsigned int getExp() const;
    Inventaire* getRecompenses();
    bool getValidation() const;
    PNJ* getPNJ() const;
    unsigned int getNiveauLancement() const;
    int getRegionCible() const;
    void activerQueteSuivante();
    Quete* getQueteActive() const;

    private:
    std::string m_nom;
    char m_etat;      // 'i' inactive   'a' active   'f' finie   'v' validable   'r' recuperee
    std::vector<Quete*> m_etapes;   // etapes intermediaires de la quete
    unsigned int m_exp;    // xp obtenu en recompense
    Inventaire* m_recompenses;    // objets et or obtenus en recompense
    bool m_validation;  // true s'il faut retourner voir le pnj pour valider la quete
    PNJ* m_pnj;   //  PNJ qui a donne la quete
    unsigned int m_niveauLancement;  // indice du niveau dans lequel est donne la quete
    int m_regionCible;  // indice de la region ou la quete est a faire, -1 si il n'y a pas de contrainte
    Quete* m_queteActive;
};

class QueteCollecte: public Quete {
    public:
    /**@brief Constructeur par defaut de QueteCollecte @param non*/
    QueteCollecte(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible, unsigned int nombreDemande, unsigned int idObjet);
    virtual std::string getType() const;
    unsigned int getNombreDemande() const;
    unsigned int getNombreObtenu() const;
    void changerNombreObtenu(int changement);
    unsigned int getIdObjet() const;
    bool estCollecteFinie() const;

    private:
    unsigned int m_nombreDemande;
    unsigned int m_nombreObtenu;
    unsigned int m_idObjet;
};

class QueteDeplacement: public Quete {
    public:
    /**@brief Constructeur par defaut de QueteDeplacement @param non*/
    QueteDeplacement(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible, Point destination, unsigned int rayon);
    virtual std::string getType() const;
    Point getDestination() const;
    unsigned int getRayon() const;

    private:
    Point m_destination;
    unsigned int m_rayon;
};

class QueteDialogue: public Quete {
    public:
    /**@brief Constructeur par defaut de QueteDialogue @param non*/
    QueteDialogue(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible, PNJ* cible);
    virtual std::string getType() const;
    PNJ* getCible() const;

    private:
    PNJ* m_cible;
};

class QueteMassacre: public Quete {
    public:
    /**@brief Constructeur par defaut de QueteMassacre @param non*/
    QueteMassacre(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible, unsigned int nombreDemande, unsigned int idEspece);
    virtual std::string getType() const;
    unsigned int getNombreDemande() const;
    unsigned int getNombreVaincu() const;
    void augmenterNombreVaincu(unsigned int nombre);
    unsigned int getIdEspece()const ;
    bool estMassacreFini() const;

    private:
    unsigned int m_nombreDemande;
    unsigned int m_nombreVaincu;
    unsigned int m_idEspece;
};

class QueteVengeance: public Quete {
    public:
    /**@brief Constructeur par defaut de QueteVengeance @param non*/
    QueteVengeance(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible, Point coordonnees, unsigned int rayon);
    virtual std::string getType() const;
    Point getCoordonnees() const;
    unsigned int getRayon() const;

    private:
    Point m_coordonnees;
    unsigned int m_rayon;
};

#endif // QUETE_H
