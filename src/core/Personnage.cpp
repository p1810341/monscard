#include "Personnage.h"
#include <iostream>

///////////////////////////////////
// Personnage
///////////////////////////////////

Personnage::Personnage() {
    m_id = 0;
    m_nomPersonnage = "";
    m_pointsDeVie = 0;
    m_pointsDeVieMax = 0;
    m_pointsDeManaMax = 0;
    m_position.x = 0;
    m_position.y = 0;
    m_attaque = 0;
    m_defense = 0;
    m_niveau = 0;
    m_deck = NULL;
    m_vitesse = 0;
}

Personnage::Personnage(unsigned int id, std::string const nom, int x, int y, unsigned int pv, unsigned int mana, unsigned int attaque, unsigned int defense, unsigned int vitesse) {
    m_id = id;
    m_nomPersonnage = nom;
    m_position.x = x;
    m_position.y = y;
    m_pointsDeVie = pv;
    m_pointsDeVieMax = pv;
    m_pointsDeManaMax = mana;
    m_attaque = attaque;
    m_defense = defense;
    m_niveau = 1;
    m_deck = NULL;
    for (unsigned int i=0; i<4; i++)
        m_collisions[i]=0;
    m_vitesse = vitesse;
}

Personnage::~Personnage() {
    if(m_deck != NULL)
        delete m_deck;
}

std::string Personnage::getNom() const {
     return m_nomPersonnage;
}

void Personnage::setNom(const std::string& nom) {
    m_nomPersonnage = nom;
}

void Personnage::setPointsDeVieMax(unsigned int pv) {
    m_pointsDeVieMax=pv;
}

void Personnage::setPointsDeManaMax(unsigned int mana) {
    m_pointsDeManaMax=mana;
}

void Personnage::setAttaque(unsigned int attaque) {
    m_attaque=attaque;
}

void Personnage::setDefense(unsigned int defense) {
    m_defense=defense;
}

unsigned int Personnage::getPointsDeVie() const {
     return m_pointsDeVie;
}

unsigned int Personnage::getPointsDeVieMax() const {
    return m_pointsDeVieMax;
}

unsigned int Personnage::getPointsDeManaMax() const {
    return m_pointsDeManaMax;
}

Point Personnage::getPosition() const {
    return m_position;
}

unsigned int Personnage::getAttaque() const {
    return m_attaque;
}

unsigned int Personnage::getDefense() const {
     return m_defense;
}

unsigned int Personnage::getNiveau() const {
    return m_niveau;
}

Deck* Personnage::getDeck() const {
    return m_deck;
}

void Personnage::deplacerPersonnage(int x, int y) {
    m_position.x += x;
    m_position.y += y;
}

void Personnage::changePointsDeVie(int changement) {
    m_pointsDeVie += changement;
    if(m_pointsDeVie > m_pointsDeVieMax) m_pointsDeVie = m_pointsDeVieMax;
}

void Personnage::setDeck(Deck* deck) {
    if(m_deck != NULL) delete m_deck;
    m_deck = deck;
}

void Personnage::getCollisions(unsigned int collisions[4]) const {
    for (unsigned int i=0; i<4; i++)
        collisions[i]=m_collisions[i];
}

void Personnage::setCollisions(unsigned int collisions[4]) {
    for (unsigned int i=0; i<4; i++)
        m_collisions[i]=collisions[i];
}

unsigned int Personnage::getVitesse() const {
    return m_vitesse;
}

void Personnage::changeVitesse(int changement) {
    m_vitesse=std::max(0,(int)m_vitesse+changement);
}

unsigned int Personnage::getId() const {
    return m_id;
}

///////////////////////////////////
// Ennemi
///////////////////////////////////

Ennemi::Ennemi() : Personnage() {
    m_espece = NULL;
    m_gainExperience=0;
}

Ennemi::~Ennemi() {
    // Joueur a un destructeur
}

Ennemi::Ennemi(int x, int y, Espece* espece, unsigned int pointsDeVieMax, unsigned int pointsDeManaMax, unsigned int attaque, unsigned int defense, unsigned int gainExperience)
                : Personnage(espece->getIdEspece(), espece->getNomEspece(), x, y, pointsDeVieMax, pointsDeManaMax, attaque, defense, espece->getVitesse()) {
    m_espece = espece;
    m_gainExperience = gainExperience;
    unsigned int collisions[4];
    m_espece->getCollisions(collisions);
    for (unsigned int i=0; i<4; i++) {
        m_collisions[i]=collisions[i];
    }
}

Espece* Ennemi::getEspece() const {
    return m_espece;
}

unsigned int Ennemi::getExp() const {
    return m_gainExperience;
}

void Ennemi::setExp(unsigned int exp) {
    m_gainExperience=exp;
}

void Ennemi::setEspece(Espece* e) {
    m_espece=e;
}

///////////////////////////////////
// Joueur
///////////////////////////////////

Joueur::Joueur() : Personnage() {
    m_cartesReserve = new unsigned int[75];
    m_carteObtenable=true;
    m_cartesObtenues = new unsigned int[75];
    for(unsigned int i = 0 ; i < 75 ; i++) {
        m_cartesReserve[i] = 0;
        m_cartesObtenues[i] = 0;
    }
    m_inventaire = new Inventaire();
    for (unsigned int i=0; i<6; i++)
        m_equipement[i] = NULL;
}

Joueur::Joueur(const Joueur *j) {
    m_id = j->m_id;
    m_experience = j->m_experience;
    m_nomPersonnage = j->m_nomPersonnage;
    m_pointsDeVie = j->m_pointsDeVie;
    m_pointsDeVieMax = j->m_pointsDeVieMax;
    m_pointsDeManaMax = j->m_pointsDeManaMax;
    m_position = j->m_position;
    m_attaque = j->m_attaque;
    m_defense = j->m_defense;
    m_niveau = j->m_niveau;
    for (unsigned int i=0; i<j->m_deck->getNombreCartes(); i++) {
        m_deck->ajouterCarte(j->m_deck->getCarte(i));
    }
    for (unsigned int i=0; i<75; i++) {
        m_cartesReserve[i] = j->m_cartesReserve[i];
        m_cartesObtenues[i] = j->m_cartesObtenues[i];
    }
    m_carteObtenable = j->m_carteObtenable;
    m_inventaire = j->m_inventaire;
    for (unsigned int i=0; i<6; i++)
        m_equipement[i]=j->m_equipement[i];
    m_vitesse = j->m_vitesse;
}

Joueur::Joueur(unsigned int id, std::string const nom, int x, int y, unsigned int pv, unsigned int mana, unsigned int attaque, unsigned int defense, unsigned int vitesse) : Personnage(id, nom, x, y, pv, mana, attaque, defense, vitesse) {
    m_experience = 0;
    m_cartesReserve = new unsigned int[75];
    m_carteObtenable=true;
    m_cartesObtenues = new unsigned int[75];
    for (unsigned int i = 0 ; i < 75 ; i++) {
        m_cartesReserve[i] = 0;
        m_cartesObtenues[i] = 0;
    }
    m_inventaire = new Inventaire();
    for (unsigned int i=0; i<6; i++)
        m_equipement[i] = NULL;
}

Joueur::~Joueur() {
    delete [] m_cartesReserve;
    delete [] m_cartesObtenues;
    if (m_deck!=NULL)
        delete m_deck;
    delete m_inventaire;
    for (unsigned int i=0; i<6; i++) {
        if (m_equipement[i]!=NULL)
            delete m_equipement[i];
    }
}

unsigned int* Joueur::getCartesReserve() const {
    return m_cartesReserve;
}

void Joueur::changeCarteReserve(unsigned int id, int i) {
    m_cartesReserve[id] += i;
}

unsigned int* Joueur::getCartesObtenues() const {
    return m_cartesObtenues;
}

void Joueur::changeCarteObtenue(unsigned int id, int i) {
    m_cartesObtenues[id] += i;
}

unsigned int Joueur::getExperience() const {
    return m_experience;
}

void Joueur::augmenteExperience(unsigned int exp) {
    m_experience = m_experience+exp;
    while (m_experience >= m_niveau*m_niveau*25)
    {
        m_experience = m_experience - m_niveau*m_niveau*25;
        augmenteNiveau();
    }
}

unsigned int Joueur::getOr() const {
    return m_inventaire->getOr();
}

void Joueur::augmenteNiveau() {
    m_niveau++;
    m_attaque += 30;
    m_defense += 30;
    m_pointsDeVieMax += 100;
    m_pointsDeManaMax += 1;
    m_pointsDeVie = m_pointsDeVieMax;
}

bool Joueur::getCarteObtenable(){
    return m_carteObtenable;
}

void Joueur::changeCarteObtenable(bool obtenable){
    m_carteObtenable=obtenable;
}

Inventaire* Joueur::getInventaire() const {
    return m_inventaire;
}

void Joueur::setInventaire(Inventaire * inventaire){
    m_inventaire=inventaire;
}

Equipement* Joueur::getEquipement(unsigned int i) const {
    if(i<6){
        return m_equipement[i];
    }
    return NULL;
}

bool Joueur::setEquipement(unsigned int i,Equipement * equipement){
    if(i==equipement->getEmplacement())
        m_equipement[i]=equipement;
    else if(i==4 && equipement->getEmplacement()==3) //si on d�cide de placer un anneau en place 4
        m_equipement[i]=equipement;
    else
        return false;
    return true;
}

///////////////////////////////////
// Joueur
///////////////////////////////////

Familier::Familier() : Personnage() {
    m_experience = 0.0;
}

Familier::Familier(const Familier *f) {
    m_id = f->m_id;
    m_experience = f->m_experience;
    m_nomPersonnage = f->m_nomPersonnage;
    m_pointsDeVie = f->m_pointsDeVie;
    m_pointsDeVieMax = f->m_pointsDeVieMax;
    m_pointsDeManaMax = f->m_pointsDeManaMax;
    m_position = f->m_position;
    m_attaque = f->m_attaque;
    m_defense = f->m_defense;
    m_niveau = f->m_niveau;
    for (unsigned int i=0; i<f->m_deck->getNombreCartes(); i++) {
        m_deck->ajouterCarte(f->m_deck->getCarte(i));
    }
}

Familier::Familier(unsigned int id, std::string const nom, int x, int y, unsigned int pv, unsigned int mana, unsigned int attaque, unsigned int defense, unsigned int vitesse) : Personnage(id, nom, x, y, pv, mana, attaque, defense, vitesse) {
    m_experience = 0;
}

Familier::~Familier() {
    if (m_deck!=NULL)
        delete m_deck;
}

unsigned int Familier::getExperience() const {
    return m_experience;
}

void Familier::augmenteExperience(unsigned int exp) {
    m_experience = m_experience+exp;
    while (m_experience >= m_niveau*m_niveau*15)
    {
        m_experience = m_experience - m_niveau*m_niveau*15;
        augmenteNiveau();
    }
}

void Familier::augmenteNiveau() {
    m_niveau++;
    m_attaque += 10;
    m_defense += 10;
    m_pointsDeVieMax += 30;
    m_pointsDeManaMax += m_niveau%2;
    m_pointsDeVie = m_pointsDeVieMax;
}
