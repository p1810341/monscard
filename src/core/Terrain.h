#ifndef TERRAIN_H
#define TERRAIN_H
#include <string>
#include <vector>

const int MAX_TERRAIN = 100;

class Terrain{
public:
    /**@brief Constructeur par defaut de Terrain @param non */
    Terrain();
    ~Terrain();
    /**@brief Recupere la taille horizontale (m_tailleTerrainX) du Terrain @param non @return int @warning  Ne modifie pas de valeur*/
    unsigned int getTailleX() const;
     /**@brief Recupere la taille verticale (m_tailleTerrainY) du Terrain @param non @return int @warning  Ne modifie pas de valeur*/
    unsigned int getTailleY() const;
    /**@brief Recupere le caractere qui se situe � la position (x, y) du Terrain @param [in] int x @param [in] int y @return char @warning  Ne modifie pas de valeur*/
    unsigned int getPremiereCoucheTerrain(unsigned int i) const;
    unsigned int getDeuxiemeCoucheTerrain(unsigned int i) const;
    /**@brief Charge le Terrain depuis le fichier selectionne @param [in] string& nomFichier @return void @warning  Modifie le tableau carteTerrain */
    void setTerrainDepuisFichier(const std::string& nomFichier);
    /**@brief Retourne True si le joueur peut se deplacer a l'emplacement de coordonnee globale i @param [in] int i @return bool @warning  Ne modifie pas de valeur*/
    bool estMarchable(unsigned int i) const;
    /**@brief Procedure qui permet de tester si la classe Terrain fonctionne correctement */
    void testRegression();
    void setMarchablePremiereCouche(std::vector<bool> m);
    void setMarchableDeuxiemeCouche(std::vector<bool> m);
    std::string getNomRegion() const;
    unsigned int getIdRegion() const;
    void setIdRegion(unsigned int id);

private:
    unsigned int m_tailleTerrainX;                           // Taille horizontale du terrain
    unsigned int m_tailleTerrainY;                           // Taille verticale du terrain
    unsigned int* m_premiereCoucheTerrain;    // Carte du terrain en caracteres
    unsigned int* m_deuxiemeCoucheTerrain;    // Carte du terrain en caracteres
    std::vector<bool> m_marchablePremiereCouche;    // true si on peut marcher dessus
    std::vector<bool> m_marchableDeuxiemeCouche;    // true si on peut marcher dessus
    std::string m_nomRegion;
    int m_idRegion;
};

#endif
