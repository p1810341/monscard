#ifndef PATHFINDING_H
#define PATHFINDING_H

#include "Jeu.h"
#include "math.h"
class Jeu;

class Node {
    public :
        bool m_walkable;
        Point m_positionTerrain;
        int m_gridX;
        int m_gridY;

        int m_gCost;
        int m_hCost;
        int m_fCost;
        Node* m_parent;

        Node(bool walkable, Point positionTerrain, int gridX, int gridY);

        int getfCost() const;
};

class Grid {
    public :
        std::vector<Node*> m_path;

        Grid(Jeu* jeu);
        ~Grid();

        std::vector<Node*> getNeighbours(Node* node);

        Node* getNodePointTerrain(Point position) const;
        Node* getNode(unsigned int x, unsigned int y) const;
        int getTailleX() const;
        int getTailleY() const;
        bool deplacementPossibleEnnemi(Ennemi* ennemi, Point destination) const;
        bool adaptationDestinationEnnemi(Ennemi* ennemi, Point& destination, Joueur* joueur) const;

    private :
        bool estNodeMarchable(Point positionTerrain, unsigned int x, unsigned int y) const;

        Node*** m_grid;
        Jeu* m_jeu;
        int m_tailleX;
        int m_tailleY;

};

class PathFinding {
    public :
        PathFinding(Personnage* entite, Grid* grid);

        bool findPath (Point posDebutCollision, Point posFinCollision, Point posCible);

        void retracePath(Node* start, Node* target);

        unsigned int getDistance(Node* n1, Node* n2);

        Point vecteurDeplacement(int valeurDeplacement, Point positionDeDepart);

    private :

        bool estContenu(std::vector<Node*> liste, Node* node);

        Grid* m_grid;
        Personnage* m_entite;
};

#endif // PATHFINDING_H
