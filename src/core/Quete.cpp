#include "Quete.h"

#include <iostream>

Quete::Quete(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible) {
    m_nom=nom;
    m_etat='i';
    m_etapes=etapes;
    m_exp=exp;
    m_recompenses=recompenses;
    m_validation=validation;
    m_pnj=pnj;
    m_niveauLancement=niveauLancement;
    m_regionCible=regionCible;
    m_queteActive=NULL;
}

Quete::~Quete() {
    for (unsigned int i=0; i<m_etapes.size(); i++)
        delete m_etapes[i];
    delete m_recompenses;
}

std::string Quete::getNom() const {
    return m_nom;
}

char Quete::getEtat() const {
    return m_etat;
}

void Quete::changeEtat() {
    if (m_etat=='i')
        m_etat='a';
    else if (m_etat=='a') {
        if (m_validation)
            m_etat='v';
        else {
            m_etat='f';
            m_queteActive=NULL;
        }
    }
    else if (m_etat=='v') {
        m_etat='f';
    }
    else if (m_etat=='f')
        m_etat='r';
    else if (m_etat=='r')
        m_etat='i';
}

void Quete::setEtatRecursif(char etat) {
    m_etat=etat;
    for (unsigned int i=0; i<m_etapes.size(); i++)
        m_etapes[i]->setEtatRecursif(etat);
}

void Quete::activerQueteSuivante() {
    if (m_etat=='i') {
        changeEtat();
        m_queteActive=this;
        if (m_etapes.size()>0) {
            m_etapes[0]->activerQueteSuivante();
            m_queteActive=m_etapes[0]->getQueteActive();
        }
    }
    else if (m_etat=='a' || m_etat=='v') {
        for (unsigned int i=0; i<m_etapes.size(); i++) {
            if (m_etapes[i]->getEtat()!='f') {
                m_etapes[i]->activerQueteSuivante();  // permet d'avancer dans la progression de l'etape actuelle
                m_queteActive=m_etapes[i]->getQueteActive();
                if (m_etapes[i]->getEtat()=='f')
                    activerQueteSuivante();   // permet d'activer la quete suivante
                return;
            }
        }
        // si toutes les sous-quetes sont finies, on fini celle-ci
        changeEtat();
    }
}

unsigned int Quete::getNbEtapes() const {
    return m_etapes.size();
}

Quete* Quete::getEtape(unsigned int i) const {
    return m_etapes[i];
}

unsigned int Quete::getOr() const {
    return m_recompenses->getOr();
}

unsigned int Quete::getExp() const {
    return m_exp;
}

Inventaire* Quete::getRecompenses() {
    return m_recompenses;
}

bool Quete::getValidation() const {
    return m_validation;
}

PNJ* Quete::getPNJ() const {
    return m_pnj;
}

unsigned int Quete::getNiveauLancement() const {
    return m_niveauLancement;
}

int Quete::getRegionCible() const {
    return m_regionCible;
}

Quete* Quete::getQueteActive() const {
    return m_queteActive;
}

QueteCollecte::QueteCollecte(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible, unsigned int nombreDemande, unsigned int idObjet)
                                : Quete(nom, etapes, exp, recompenses, validation, pnj, niveauLancement, regionCible) {
    m_nombreDemande=nombreDemande;
    m_nombreObtenu=0;
    m_idObjet=idObjet;
}

std::string QueteCollecte::getType() const {
    return "Collecte";
}

unsigned int QueteCollecte::getNombreDemande() const {
    return m_nombreDemande;
}

unsigned int QueteCollecte::getNombreObtenu() const {
    return m_nombreObtenu;
}

void QueteCollecte::changerNombreObtenu(int changement) {
    if (changement<0 && changement+m_nombreObtenu<0)
        changement=m_nombreObtenu;
    m_nombreObtenu+=changement;
}

unsigned int QueteCollecte::getIdObjet() const {
    return m_idObjet;
}

bool QueteCollecte::estCollecteFinie() const {
    return (m_nombreObtenu>=m_nombreDemande);
}

QueteDeplacement::QueteDeplacement(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible, Point destination, unsigned int rayon)
                                        : Quete(nom, etapes, exp, recompenses, validation, pnj, niveauLancement, regionCible) {
    m_destination=destination;
    m_rayon=rayon;
}

std::string QueteDeplacement::getType() const {
    return "D\u00E9placement";
}

Point QueteDeplacement::getDestination() const {
    return m_destination;
}

unsigned int QueteDeplacement::getRayon() const {
    return m_rayon;
}

QueteDialogue::QueteDialogue(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible, PNJ* cible)
                                    : Quete(nom, etapes, exp, recompenses, validation, pnj, niveauLancement, regionCible) {
    m_cible=cible;
}

std::string QueteDialogue::getType() const {
    return "Dialogue";
}

PNJ* QueteDialogue::getCible() const {
    return m_cible;
}

QueteMassacre::QueteMassacre(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible, unsigned int nombreDemande, unsigned int idEspece)
                                    : Quete(nom, etapes, exp, recompenses, validation, pnj, niveauLancement, regionCible) {
    m_nombreDemande=nombreDemande;
    m_nombreVaincu=0;
    m_idEspece=idEspece;
}

std::string QueteMassacre::getType() const {
    return "Massacre";
}

unsigned int QueteMassacre::getNombreDemande() const {
    return m_nombreDemande;
}

unsigned int QueteMassacre::getNombreVaincu() const {
    return m_nombreVaincu;
}

void QueteMassacre::augmenterNombreVaincu(unsigned int changement) {
    m_nombreVaincu+=changement;
}

unsigned int QueteMassacre::getIdEspece() const {
    return m_idEspece;
}

bool QueteMassacre::estMassacreFini() const {
    return (m_nombreVaincu>=m_nombreDemande);
}

QueteVengeance::QueteVengeance(const std::string nom, std::vector<Quete*> etapes, unsigned int exp, Inventaire* recompenses, bool validation, PNJ* pnj, unsigned int niveauLancement, int regionCible, Point coordonnees, unsigned int rayon)
                                    : Quete(nom, etapes, exp, recompenses, validation, pnj, niveauLancement, regionCible) {
    m_coordonnees=coordonnees;
    m_rayon=rayon;
}

std::string QueteVengeance::getType() const {
    return "Vengeance";
}

Point QueteVengeance::getCoordonnees() const {
    return m_coordonnees;
}

unsigned int QueteVengeance::getRayon() const {
    return m_rayon;
}
