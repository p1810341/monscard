#ifndef COFFRE_H
#define COFFRE_H
#include <assert.h>
#include "Point.h"
#include <vector>
#include "Objet.h"
#include "Inventaire.h"

class Coffre {
public:
    /**@brief Constructeur par defaut de Coffre, lui associant un nombre aleatoire d'or et de potions @param non*/
    Coffre();
    /**@brief Constructeur par parametre de position de Coffre, lui associant un nombre aleatoire d'or et de potions, @param [in] int x @param [in] int y @return Coffre */
    Coffre(int x, int y);
    ~Coffre();
    /**@brief Recupere l'or (m_or) a l'interieur du coffre @param non @return int @warning  Ne modifie pas de valeur*/
    unsigned int getOr() const;
    /**@brief Recupere le booleen (m_estOuvert) qui indique si le coffre est ouvert ou non @param non @return bool @warning  Ne modifie pas de valeur*/
    bool getOuvert() const;
    /**@brief Recupere le point de coordonees du coffre(m_position) @param non @return Point @warning  Ne modifie pas de valeur*/
    Point getPosition() const;
    /**@brief Change le booleen m_estOuvert (ouvre ou ferme le coffre) @param non @return non @warning Peut ouvrir et fermer le coffre, attention a son etat initial **/
    void setOuvert();
    Inventaire* getButin();
    Objet * getObjet(unsigned int i);
    unsigned int getNbButin() const;
    void retirerObjet(unsigned int i);
    /**@brief Procedure qui permet de tester si la classe Coffre fonctionne correctement */
    void testRegression();

private:
    bool m_estOuvert;           // Indique si le coffre est ouvert (true = ouvert)
    Point m_position;           // Position du coffre
    Inventaire* m_butin;   // Objets contenus dans le coffre
};
#endif // Coffre.h

