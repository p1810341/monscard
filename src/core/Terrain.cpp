#include "Terrain.h"
#include <iostream>
#include <cassert>
#include <fstream>

Terrain::Terrain() {
    m_tailleTerrainX = 0;
    m_tailleTerrainY = 0;
    m_premiereCoucheTerrain=NULL;
    m_deuxiemeCoucheTerrain=NULL;
    m_nomRegion="";
    m_idRegion=0;
}

Terrain::~Terrain() {
    if (m_premiereCoucheTerrain!=NULL)
        delete [] m_premiereCoucheTerrain;
    if (m_deuxiemeCoucheTerrain!=NULL)
        delete [] m_deuxiemeCoucheTerrain;
}

unsigned int Terrain::getTailleX() const {
    return m_tailleTerrainX;
}

unsigned int Terrain::getTailleY() const {
    return m_tailleTerrainY;
}

unsigned int Terrain::getPremiereCoucheTerrain(unsigned int i) const {
    return m_premiereCoucheTerrain[i];
}

unsigned int Terrain::getDeuxiemeCoucheTerrain(unsigned int i) const {
    return m_deuxiemeCoucheTerrain[i];
}

void Terrain::setTerrainDepuisFichier(const std::string& nomFichier) {
    if (m_premiereCoucheTerrain!=NULL) {
        delete [] m_premiereCoucheTerrain;
        m_premiereCoucheTerrain=NULL;
    }
    if (m_deuxiemeCoucheTerrain!=NULL) {
        delete [] m_deuxiemeCoucheTerrain;
        m_deuxiemeCoucheTerrain=NULL;
    }
    std::ifstream fichier(nomFichier.c_str());
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            std::string parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"Terrain::setTerrainDepuisFichier : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier.is_open());

            }
        }
    }
    if(fichier.is_open()) {
        std::string nom;
        getline(fichier,nom);
        m_nomRegion=nom;
        unsigned int x, y;
        fichier>>x;
        fichier>>y;
        assert(x <= MAX_TERRAIN);
        assert(y <= MAX_TERRAIN);
        m_tailleTerrainX = x;
        m_tailleTerrainY = y;
        m_premiereCoucheTerrain=new unsigned int[x*y];
        m_deuxiemeCoucheTerrain=new unsigned int[x*y];
        for(unsigned int j = 0 ; j < y ; j++) {
            for(unsigned int i = 0 ; i < x ; i++) {
                fichier >> m_premiereCoucheTerrain[i+j*x] >> m_deuxiemeCoucheTerrain[i+j*x];
            }
        }
        fichier.close();
    }
}

bool Terrain::estMarchable(unsigned int i) const {
    if (m_marchablePremiereCouche[m_premiereCoucheTerrain[i]] && m_marchableDeuxiemeCouche[m_deuxiemeCoucheTerrain[i]])
        return true;
    return false;
}

void Terrain::setMarchablePremiereCouche(std::vector<bool> m) {
    m_marchablePremiereCouche=m;
}

void Terrain::setMarchableDeuxiemeCouche(std::vector<bool> m) {
    m_marchableDeuxiemeCouche=m;
}

std::string Terrain::getNomRegion() const {
    return m_nomRegion;
}

unsigned int Terrain::getIdRegion() const {
    return m_idRegion;
}

void Terrain::setIdRegion(unsigned int id) {
    m_idRegion=id;
}
