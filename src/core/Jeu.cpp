#include "Jeu.h"
#include<time.h>
#include<iostream>
#include <string>
#include <fstream>
#include <cassert>
#include <ctime>
#include <math.h>

#define M_PI           3.14159265358979323846

Jeu::Jeu() {
    srand(time(NULL));

    // On ajoute toutes les cartes
    std::string nomFichier = "data/cartes/cartes.txt";
    std::ifstream cartes(nomFichier.c_str());
    if(!cartes.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        cartes.open(parent.c_str());
        if(!cartes.is_open()) {
            parent = std::string("../") + parent;
            cartes.open(parent.c_str());
            if(!cartes.is_open()) {
                std::cout<<"Jeu::Jeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(cartes.is_open());
            }
        }
    }
    if (cartes.is_open()) {
        std::vector<std::string> listeCartes;
        std::string parametre;
        unsigned int i=0,j=0;
        while (std::getline(cartes,parametre,';')) {
            listeCartes.push_back(parametre);
        }
        while (i<listeCartes.size()) {
            std::string type=listeCartes[i];
            unsigned int coutMana=std::stoi(listeCartes[++i]);
            std::string nom=listeCartes[++i];
            std::string description=listeCartes[++i];
            if (type=="Attaque") {
                bool attaqueDirecte=std::stoi(listeCartes[++i]);
                bool silence=std::stoi(listeCartes[++i]);
                bool confusion=std::stoi(listeCartes[++i]);
                bool poison=std::stoi(listeCartes[++i]);
                unsigned int degats=std::stoi(listeCartes[++i]);
                unsigned int volDeVie=std::stoi(listeCartes[++i]);
                m_tableauCartes.ajouterCarte(new Attaque(j,coutMana,nom,description,attaqueDirecte,silence,confusion,poison,degats,volDeVie));
            }
            else if (type=="Bonus") {
                unsigned int attaque=std::stoi(listeCartes[++i]);
                unsigned int defense=std::stoi(listeCartes[++i]);
                unsigned int experience=std::stoi(listeCartes[++i]);
                m_tableauCartes.ajouterCarte(new Bonus(j,coutMana,nom,description,attaque,defense,experience));
            }
            else if (type=="D\u00E9fense") {
                bool invulnerable=std::stoi(listeCartes[++i]);
                bool derniereChance=std::stoi(listeCartes[++i]);
                unsigned int bouclier=std::stoi(listeCartes[++i]);
                unsigned int esquive=std::stoi(listeCartes[++i]);
                m_tableauCartes.ajouterCarte(new Defense(j,coutMana,nom,description,invulnerable,derniereChance,bouclier,esquive));

            }
            else if (type=="Soin") {
                unsigned int pv=std::stoi(listeCartes[++i]);
                m_tableauCartes.ajouterCarte(new Soin(j,coutMana,nom,description,pv));
            }
            i++;
            j++;
        }
        cartes.close();
    }

    nomFichier = "data/objets/objets.txt";
    std::ifstream objets(nomFichier.c_str());
    if(!objets.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        objets.open(parent.c_str());
        if(!objets.is_open()) {
            parent = std::string("../") + parent;
            objets.open(parent.c_str());
            if(!objets.is_open()) {
                std::cout<<"Jeu::Jeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(objets.is_open());
            }
        }
    }
    if (objets.is_open()) {
        std::vector<std::string> listeObjets;
        std::string parametre;
        unsigned int i=0,j=0;
        while (std::getline(objets,parametre,';')) {
            listeObjets.push_back(parametre);
        }
        while (i<listeObjets.size()) {
            std::string type=listeObjets[i];
            std::string nom=listeObjets[++i];
            std::string description=listeObjets[++i];
            unsigned int valeur=std::stoi(listeObjets[++i]);
            if (type=="Mat\u00E9riau") {
                unsigned int nbMax=std::stoi(listeObjets[++i]);
                m_tableauObjets.push_back(new Materiau(j,nom,description,valeur,1,nbMax));
            }
            else if (type=="\u00C9quipement") {
                unsigned int pv=std::stoi(listeObjets[++i]);
                unsigned int mana=std::stoi(listeObjets[++i]);
                unsigned int defense=std::stoi(listeObjets[++i]);
                unsigned int attaque=std::stoi(listeObjets[++i]);
                unsigned int exp=std::stoi(listeObjets[++i]);
                unsigned int emplacement=std::stoi(listeObjets[++i]);
                m_tableauObjets.push_back(new Equipement(j,nom,description,valeur,pv,mana,defense,attaque,exp,emplacement));
            }
            else if (type=="Consommable") {
                unsigned int pv=std::stoi(listeObjets[++i]);
                unsigned int mana=std::stoi(listeObjets[++i]);
                unsigned int defense=std::stoi(listeObjets[++i]);
                unsigned int attaque=std::stoi(listeObjets[++i]);
                unsigned int exp=std::stoi(listeObjets[++i]);
                m_tableauObjets.push_back(new Consommable(j,nom,description,valeur,pv,mana,defense,attaque,exp));

            }
            else if (type=="Sp\u00E9cial") {
                m_tableauObjets.push_back(new ObjetSpecial(j,nom,description,valeur));
            }
            i++;
            j++;
        }
        objets.close();
    }
    // On cree les differentes especes
    nomFichier = "data/ennemis/ennemis.txt";
    std::ifstream ennemis(nomFichier.c_str());
    if(!ennemis.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        ennemis.open(parent.c_str());
        if(!ennemis.is_open()) {
            parent = std::string("../") + parent;
            ennemis.open(parent.c_str());
            if(!ennemis.is_open()) {
                std::cout<<"Jeu::Jeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(ennemis.is_open());
            }
        }
    }
    if (ennemis.is_open()) {
        std::vector<std::string> listeEnnemis;
        std::string parametre;
        unsigned int i=0,j=0;
        while (std::getline(ennemis,parametre,';')) {
            listeEnnemis.push_back(parametre);
        }
        while (i<listeEnnemis.size()) {
            std::string nomEspece=listeEnnemis[i];
            unsigned int butinOr=std::stoi(listeEnnemis[++i]);
            unsigned int vitesse=std::stoi(listeEnnemis[++i]);
            Espece* espece=new Espece(j,nomEspece,butinOr,vitesse);
            m_tableauEspeces.push_back(espece);
            unsigned int nbObjet=std::stoi(listeEnnemis[++i]);
            for (unsigned int k=0; k<nbObjet; k++) {
                unsigned int idObjet=std::stoi(listeEnnemis[++i]);
                Objet* objet=m_tableauObjets[idObjet];
                std::string type=m_tableauObjets[idObjet]->getTypeObjet();
                unsigned int chance;
                if (type=="Mat\u00E9riau") {
                    unsigned int nbExemplaire=std::stoi(listeEnnemis[++i]);
                    Materiau* m=static_cast<Materiau*>(objet);
                    Materiau* materiau=new Materiau(m);
                    materiau->modifierNombreExemplaires(-materiau->getNombreExemplaires()+nbExemplaire);
                    chance=std::stoi(listeEnnemis[++i]);
                    espece->ajouterButin(materiau,chance);
                }
                else if (type=="\u00C9quipement") {
                    Equipement* e=static_cast<Equipement*>(objet);
                    Equipement* equipement=new Equipement(e);
                    chance=std::stoi(listeEnnemis[++i]);
                    espece->ajouterButin(equipement,chance);
                }
                else if (type=="Consommable") {
                    Consommable* c=static_cast<Consommable*>(objet);
                    Consommable* consommable=new Consommable(c);
                    chance=std::stoi(listeEnnemis[++i]);
                    espece->ajouterButin(consommable,chance);

                }
                else if (type=="Sp\u00E9cial") {
                    ObjetSpecial* s=static_cast<ObjetSpecial*>(objet);
                    ObjetSpecial* objetSpecial=new ObjetSpecial(s);
                    chance=std::stoi(listeEnnemis[++i]);
                    espece->ajouterButin(objetSpecial,chance);
                }
            }
            i++;
            j++;
        }
        ennemis.close();
    }

    // Divers parametres
    m_quitter = false;  // On ne quitte pas le jeu
    m_tailleCase = 10000;

    // Gestion menus et fenetres
    m_menuPrincipal = true;     // On est sur le menu principal au debut
    m_afficheMenu = false;
    m_afficheQuitter = false;
    m_afficheSauvegarde = false;
    m_afficheConfirmSauvegarde = false;
    m_afficheSauvegardeOK = false;
    m_afficheSauvegardeError = false;
    m_afficheChargementError = false;
    m_afficheChargement = false;
    m_afficheInventaire = false;
    m_afficheInventaireCoffre = false;
    m_afficheInventaireTombe = false;
    m_afficheVictoire = false;
    m_afficheDefaite = false;
    m_ligneSelectionnee = 0;    // On selectionne la premiere ligne par defaut
    m_selectBouton = true;     // On selectionne le bouton "OK" par defaut
    m_afficheDialogue = false;
    m_afficheCartes = false;
    m_fenetreOuverte = true;
    m_niveau=0;
    m_interactionPossible = 0;
    m_deplacementDiagonale = false;
    m_afficheCombat = false;
    m_afficheMonteeNiveau = false;
    m_afficheDebutQuete = false;
    m_afficheFinQuete = false;
    m_afficheRecueilQuete = false;
    m_afficheInventairePlein = false;
    m_afficheValidationCollecte = false;
    m_indiceQueteSuivie = -1;

    // scenario
    m_dialogueNarrateur = new Dialogue("data/dialogues/DialogueDebut.txt", true);
    m_scenario = false;
    m_scenarioNarrateur = false;
    m_afficheTutoriel = false;
    m_afficheTutorielQuete = false;
    m_tutorielQueteDejaAffiche = false;
    m_afficheTutorielRefusQuete = false;
    m_tutorielRefusQueteDejaAffiche = false;
    m_afficheTutorielRecueil = false;
    m_tutorielRecueilDejaAffiche = false;
    m_afficheTutorielInventaire = false;
    m_tutorielInventaireDejaAffiche = false;
    m_debloquerAffichageCartes = false;
    m_debloquerAffichageRecueil = false;
    m_debloquerAffichageInventaire = false;
    m_affichageTransitionNoire = false;
    m_indiceScenario = 0;
    m_afficheTutorielDeck = false;
    m_afficheTutorielLancerCombat = false;
    m_tutorielCombatDejaAffiche = false;
    m_afficheTutorielMain = false;
    m_afficheTutorielMana = false;
    m_afficheTutorielCarte = false;

    // On charge les sauvegardes existantes
    for(int i = 0 ; i < 6 ; i++) setDateSauvegarde(i);

    m_terrain=new Terrain();
    // On recupere les informations sur les decors du jeu, sans s'occuper des images
    // Terrain
    nomFichier = "data/decors/premiereCouche.txt";
    std::ifstream fichier(nomFichier.c_str());
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"Jeu::Jeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier.is_open());
            }
        }
    }
    if (fichier.is_open()) {
        std::string ligne;
        std::vector<bool> vectorMarchable;
        bool marchable;
        unsigned int i=0;
        while (!fichier.eof()) {
            fichier >> ligne >> marchable;
            vectorMarchable.push_back(marchable);
            i++;
        }
        m_terrain->setMarchablePremiereCouche(vectorMarchable);
        fichier.close();
    }
    // Decors
    nomFichier = "data/decors/deuxiemeCouche1.txt";
    std::ifstream fichier2(nomFichier.c_str());
    if(!fichier2.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier2.open(parent.c_str());
        if(!fichier2.is_open()) {
            parent = std::string("../") + parent;
            fichier2.open(parent.c_str());
            if(!fichier2.is_open()) {
                std::cout<<"Jeu::Jeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier2.is_open());
            }
        }
    }
    if (fichier2.is_open()) {
        std::vector<bool> vectorMarchable;
        bool marchable;
        vectorMarchable.push_back(1);  // pas de decor
        vectorMarchable.push_back(0);  // faux vide (utilise par maison etc)
        unsigned int* c;
        for (unsigned int i=0; i<2; i++) {
            c=new unsigned int[4]{0,0,50,50};
            c[0]=c[0]*m_tailleCase/50;
            c[1]=c[1]*m_tailleCase/50;
            c[2]=c[2]*m_tailleCase/50-1;
            c[3]=c[3]*m_tailleCase/50-1;
            m_collisionsDecors.push_back(c);
        }
        std::string ligne;
        unsigned int i=0;
        while (!fichier2.eof()) {
            c=new unsigned int[4]{0,0,0,0};
            fichier2 >> ligne >> marchable;
            vectorMarchable.push_back(marchable);
            if (!marchable) {
                fichier2 >> c[0] >> c[1] >> c[2] >> c[3];
                c[0]=c[0]*m_tailleCase/50;
                c[1]=c[1]*m_tailleCase/50;
                c[2]=c[2]*m_tailleCase/50-1;
                c[3]=c[3]*m_tailleCase/50-1;
            }
            m_collisionsDecors.push_back(c);
            i++;
        }
        m_tailleDecors=i;
        // Interactions et PNJ
        nomFichier = "data/interactions/deuxiemeCouche2.txt";
        std::ifstream fichier3(nomFichier.c_str());
        if(!fichier3.is_open()) {
            std::string parent = std::string("../") + nomFichier;
            fichier3.open(parent.c_str());
            if(!fichier3.is_open()) {
                parent = std::string("../") + parent;
                fichier3.open(parent.c_str());
                if(!fichier3.is_open()) {
                    std::cout<<"Jeu::Jeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                    assert(fichier3.is_open());
                }
            }
        }
        if (fichier3.is_open()) {
            unsigned int action;
            while (!fichier3.eof()) {
                c=new unsigned int[4]{0,0,0,0};
                fichier3 >> ligne >> marchable;
                Point p;
                vectorMarchable.push_back(marchable);
                if (!marchable) {
                    fichier3 >> c[0] >> c[1] >> c[2] >> c[3];
                    c[0]=c[0]*m_tailleCase/50;
                    c[1]=c[1]*m_tailleCase/50;
                    c[2]=c[2]*m_tailleCase/50-1;
                    c[3]=c[3]*m_tailleCase/50-1;
                }
                m_collisionsDecors.push_back(c);
                fichier3 >> action;
                if (action==1) {
                    c[0]=0;
                    c[1]=0;
                    c[2]=m_tailleCase-1;
                    c[3]=m_tailleCase-1;
                    fichier3 >> p.x >> p.y;
                }
                m_interactionsAction.push_back(action);
                i++;
            }
            fichier3.close();
        }
        fichier2.close();
        m_terrain->setMarchableDeuxiemeCouche(vectorMarchable);
    }
    // Ennemis
    nomFichier="data/ennemis/nomsImagesEnnemis.txt";
    std::ifstream nomsImagesEnnemis (nomFichier.c_str());
    if(!nomsImagesEnnemis.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        nomsImagesEnnemis.open(parent);
        if(!nomsImagesEnnemis.is_open()) {
            parent = std::string("../") + parent;
            nomsImagesEnnemis.open(parent);
            if(!nomsImagesEnnemis.is_open()) {
                std::cout << "Jeu::Jeu : Erreur dans l'ouverture du fichier "<< nomFichier << std::endl;
                assert(nomsImagesEnnemis.is_open());
            }
        }
    }
    if (nomsImagesEnnemis.is_open()) {
        std::string nomImage;
        unsigned int c[4], i=0;
        while (!nomsImagesEnnemis.eof()) {
            nomsImagesEnnemis >> nomImage >> c[0] >> c[1] >> c[2] >> c[3];
            c[0]=c[0]*m_tailleCase/50;
            c[1]=c[1]*m_tailleCase/50;
            c[2]=c[2]*m_tailleCase/50-1;
            c[3]=c[3]*m_tailleCase/50-1;
            m_tableauEspeces[i]->setCollisions(c);
            i++;
        }
        nomsImagesEnnemis.close();
    }

    m_nbPersonnages=0;
    m_nbFamiliers=0;
    // On cree les differentes personnages
    nomFichier = "data/personnages/personnages.txt";
    std::ifstream personnages(nomFichier.c_str());
    if(!personnages.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        personnages.open(parent.c_str());
        if(!personnages.is_open()) {
            parent = std::string("../") + parent;
            personnages.open(parent.c_str());
            if(!personnages.is_open()) {
                std::cout<<"Jeu::Jeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(personnages.is_open());
            }
        }
    }
    if (personnages.is_open()) {
        std::vector<std::string> listePersonnages;
        std::string parametre;
        unsigned int i=0,j=0;
        while (std::getline(personnages,parametre,';')) {
            listePersonnages.push_back(parametre);
        }
        while (i<listePersonnages.size()) {
            unsigned int type=std::stoi(listePersonnages[i]);
            std::string nomPersonnage=listePersonnages[++i];
            unsigned int pv=std::stoi(listePersonnages[++i]);
            unsigned int mana=std::stoi(listePersonnages[++i]);
            unsigned int attaque=std::stoi(listePersonnages[++i]);
            unsigned int defense=std::stoi(listePersonnages[++i]);
            unsigned int vitesse=std::stoi(listePersonnages[++i]);
            Personnage* p;
            switch (type) {
                case 0:  // Joueur
                    p=new Joueur(j,nomPersonnage,0,0,pv,mana,attaque,defense,vitesse);
                    m_joueur=static_cast<Joueur*>(p);
                    break;
                case 1:  // Personnage
                    p=new Personnage(j,nomPersonnage,0,0,pv,mana,attaque,defense,vitesse);
                    m_nbPersonnages++;
                    break;
                case 2:  // Familier
                    p=new Familier(j,nomPersonnage,0,0,pv,mana,attaque,defense,vitesse);
                    m_nbFamiliers++;
                    break;
            }
            Deck* deck = new Deck;
            for (unsigned int k=0; k<5; k++) {
                unsigned int idCarte=std::stoi(listePersonnages[++i]);
                deck->ajouterCarte(m_tableauCartes.getCarte(idCarte));
                if (type==0)  // Si c'est le joueur on modifie les cartes obtenues
                    m_joueur->changeCarteObtenue(idCarte,1);
            }
            p->setDeck(deck);
            m_tableauPersonnages.push_back(p);
            i++;
            j++;
        }
        personnages.close();
    }
    nomFichier="data/personnages/nomsImagesPersonnages.txt";
    std::ifstream nomsImagesPersonnages (nomFichier.c_str());
    if(!nomsImagesPersonnages.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        nomsImagesPersonnages.open(parent);
        if(!nomsImagesPersonnages.is_open()) {
            parent = std::string("../") + parent;
            nomsImagesPersonnages.open(parent);
            if(!nomsImagesPersonnages.is_open()) {
                std::cout << "Jeu::Jeu : Erreur dans l'ouverture du fichier "<< nomFichier << std::endl;
                assert(nomsImagesPersonnages.is_open());
            }
        }
    }
    if (nomsImagesPersonnages.is_open()) {
        std::string nomImage;
        unsigned int c[4], i=0;
        while (!nomsImagesPersonnages.eof()) {
            nomsImagesPersonnages >> nomImage >> c[0] >> c[1] >> c[2] >> c[3];
            c[0]=c[0]*m_tailleCase/50;
            c[1]=c[1]*m_tailleCase/50;
            c[2]=c[2]*m_tailleCase/50-1;
            c[3]=c[3]*m_tailleCase/50-1;
            m_tableauPersonnages[i]->setCollisions(c);
            i++;
        }
        nomsImagesPersonnages.close();
    }

    //Equipement par defaut
    Equipement* e=static_cast<Equipement*>(m_tableauObjets[2]);
    m_joueur->setEquipement(0,e);
    e=static_cast<Equipement*>(m_tableauObjets[3]);
    m_joueur->setEquipement(2,e);

    // Initialisation de l'equipe
    for (unsigned int i=0; i<3; i++)
        m_equipe[i]=NULL;

    chargerMonde("Tutoriel");
    chargerEntites();

    // Initialisation des quetes
    nomFichier = "data/quetes/quetes.txt";
    std::ifstream quetes(nomFichier.c_str());
    if(!quetes.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        quetes.open(parent.c_str());
        if(!quetes.is_open()) {
            parent = std::string("../") + parent;
            quetes.open(parent.c_str());
            if(!quetes.is_open()) {
                std::cout<<"Jeu::Jeu : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(quetes.is_open());
            }
        }
    }
    if (quetes.is_open()) {
        std::vector<std::string> listeQuetes;
        std::string parametre;
        unsigned int i=0;
        while (std::getline(quetes,parametre,';')) {
            listeQuetes.push_back(parametre);
        }
        while (i<listeQuetes.size()) {
            Quete* quete=initialiserQuete(listeQuetes,i);
            i++;
            m_tableauQuetes.push_back(quete);
        }
        quetes.close();
    }

    m_grille=NULL;
    m_pathFinding=NULL;
    chargerNiveau(0,m_tailleCase*8,m_tailleCase*4);

    for (unsigned int i=0; i<40; i++) {
        m_dernierDeplacement[i].x=0;
        m_dernierDeplacement[i].y=0;
    }
    m_combat=NULL;

    for (unsigned int i=0; i<4; i++) {
        m_monteeNiveau[i]=0;
        m_ancienNiveauPersonnage[i]=1;
    }
}

Jeu::~Jeu() {
    delete m_terrain;
    for(unsigned int i = 0 ; i < m_tableauEspeces.size() ; i++) {
        delete m_tableauEspeces[i];
    }
    for(unsigned int i = 0 ; i < m_tableauPNJJeu.size() ; i++) {
        delete m_tableauPNJJeu[i];
    }
    for (unsigned int i=0; i<m_tableauPointsApparitions.size(); i++)
        delete m_tableauPointsApparitions[i];
    for (unsigned int i=0; i<m_tableauCoffresJeu.size(); i++) {
        delete m_tableauCoffresJeu[i];
    }
    for (unsigned int i=0; i<m_collisionsDecors.size(); i++)
        delete m_collisionsDecors[i];
    for (unsigned int i=0; i<m_tableauCartes.getNombreCartes(); i++) {
        delete m_tableauCartes.getCarte(i);
    }
    for (unsigned int i=0; i<m_tableauObjets.size(); i++) {
        delete m_tableauObjets[i];
    }
    for (unsigned int i=0; i<m_tableauPersonnages.size(); i++)
        delete m_tableauPersonnages[i];
    if (m_combat!=NULL)
        delete m_combat;
    for (unsigned int i=0; i<m_tableauQuetes.size(); i++)
        delete m_tableauQuetes[i];
    for (unsigned int i=0; i<m_tableauInventairesTombes.size(); i++)
        delete m_tableauInventairesTombes[i];
    delete m_dialogueNarrateur;
}

Terrain* Jeu::getTerrain() const {
    return m_terrain;
}

Joueur* Jeu::getJoueur() {
    return m_joueur;
}

const Joueur* Jeu::getJoueurConst() const {
    return m_joueur;
}

Ennemi* Jeu::getEnnemi(unsigned int i) const {
    return m_tableauEnnemis[i];
}

Ennemi* Jeu::getTombe(unsigned int i) const {
    return m_tableauTombes[i];
}

Inventaire* Jeu::getInventaireTombe(unsigned int i) const {
    return m_tableauInventairesTombes[i];
}

unsigned int Jeu::getNombreEnnemis() const {
    return m_tableauEnnemis.size();
}

unsigned int Jeu::getNombreEspeces() const {
    return m_tableauEspeces.size();
}

Espece* Jeu::getEspece(int i) const {
    return m_tableauEspeces[i];
}

PNJ* Jeu::getPNJ(unsigned int niveau, unsigned int i) const {
    return m_tableauPNJ[niveau][i];
}

unsigned int Jeu::getNombrePNJ(unsigned int niveau) const {
    return m_tableauPNJ[niveau].size();
}

Coffre* Jeu::getCoffre(unsigned int niveau, unsigned int i) const {
    return m_tableauCoffres[niveau][i];
}

unsigned int Jeu::getNombreCoffres(unsigned int niveau) const {
    return m_tableauCoffres[niveau].size();
}

TableauCartes Jeu::getTableauCartes() const {
    return m_tableauCartes;
}

void Jeu::actionClavier(char touche, unsigned int valeur) { // b = bas, h = haut, g = gauche, d = droite, p = echap, v = espace, e = interaction touche e

    // Echap : touche du menu
    if(touche == 'p') {
        if (m_scenarioNarrateur) {
            m_affichageTransitionNoire=true;
            m_afficheDialogue=false;
            if (m_afficheDefaite)
                m_quitter=true;
            else
                m_indiceScenario++;
        }
        // Echap permet de fermer certaines fenetres
        else if(m_afficheChargementError)
            m_afficheChargementError = false;
        else if(m_afficheSauvegardeOK) {
            m_afficheSauvegardeOK = false;
            m_fenetreOuverte = false;
        }
        else if(m_afficheSauvegardeError) {
            m_afficheSauvegardeError = false;
            m_fenetreOuverte = false;
        }
        else if(m_afficheConfirmSauvegarde) {}
        else if (m_afficheFinQuete && !m_afficheVictoire && !m_afficheMonteeNiveau) { // A faire avant les inventaires car peut etre superpose a leur fenetre
            if (!m_afficheDebutQuete) {
                m_afficheFinQuete = false;
                if (m_indiceQueteSuivie==(int)m_indiceFinQuete)
                    m_indiceQueteSuivie=-1;
                if (!m_afficheInventaire && !m_afficheRecueilQuete)
                    m_fenetreOuverte = false;
                queteFinieScenario(m_indiceFinQuete);
            }
        }
        else if (m_afficheInventaire) {
            if (m_afficheInventaireCoffre || m_afficheInventaireTombe)
                actionClavier('e',0);
            else {
                m_afficheInventaire = false;
                m_fenetreOuverte = false;
            }
        }
        else if (m_afficheCombat) {
            m_afficheCombat = false;
            if (m_tutorielCombatDejaAffiche)
                m_fenetreOuverte = false;
            else {
                m_afficheTutoriel = true;
                m_fenetreOuverte = true;
                m_afficheTutorielCombat = true;
                m_tutorielCombatDejaAffiche = true;
            }
        }
        else if(m_afficheMonteeNiveau) {
            bool trouve = false;
            if (!m_afficheFinQuete)
                m_fenetreOuverte = false;
            m_afficheMonteeNiveau = false;
            for (unsigned int i=0; i<4; i++) {   // on parcourt le tableau
                if (m_monteeNiveau[i]) {
                    if (!trouve) {  // si c'est le premier trouve, on le met a faux
                        m_monteeNiveau[i] = false;
                        trouve = true;
                    }
                    else {          // sinon cela veut dire qu'il y a encore une montee de niveau a faire, on laisse les fenetres ouvertes
                        m_fenetreOuverte = true;
                        m_afficheMonteeNiveau = true;
                    }
                }
            }
        }
        else if(m_afficheVictoire) {
            m_afficheVictoire = false;
            if (!m_afficheFinQuete)
                m_fenetreOuverte = false;
            m_afficheMonteeNiveau = false;
            for (unsigned int i=0; i<4; i++) {   // on parcourt le tableau et on s'arrete au premier vrai
                if (m_monteeNiveau[i]) {
                    m_afficheMonteeNiveau = true;
                    m_fenetreOuverte = true;
                    break;
                }
            }
        }
        else if (m_afficheInventairePlein)   // a fermer avant le recueil de quete
            m_afficheInventairePlein = false;
        else if (m_afficheRecueilQuete) {
            m_afficheRecueilQuete = false;
            m_fenetreOuverte = false;
        }
        else if (m_afficheCartes) {
            m_afficheCartes = false;
            m_fenetreOuverte = false;
        }
        else if(m_afficheSauvegarde) {
                m_afficheSauvegarde = false;
                m_ligneSelectionnee = 1;
        }
        else if(m_afficheChargement) {
                m_afficheChargement = false;
                if(m_afficheMenu) m_ligneSelectionnee = 2;
                else m_ligneSelectionnee = 1;
        }
        else if(m_afficheQuitter) m_afficheQuitter = false;
        // Permet d'afficher le menu si on est en jeu et si on affiche pas de dialogue ni un combat
        else if(!m_menuPrincipal && !m_afficheDialogue && m_combat==NULL && !m_afficheTutoriel) {
            m_afficheMenu=!m_afficheMenu;
            m_fenetreOuverte=!m_fenetreOuverte;
            m_ligneSelectionnee = 0;
        }
    }
    // Espace : touche de validation dans un menu
    else if(touche == 'v') {
        // Ferme les fenetres de messages
        if(m_afficheChargementError)
        {
            m_afficheChargementError = false;
            m_fenetreOuverte = false;
        }
        else if(m_afficheSauvegardeError)
        {
            m_afficheSauvegardeError = false;
            m_fenetreOuverte = false;
        }
        else if(m_afficheSauvegardeOK) {
            m_afficheSauvegardeOK = false;
            m_fenetreOuverte = false;
            m_ligneSelectionnee = 0;
        }

        // Valide dans les fenetres modales
        else if(m_afficheQuitter) {     // Menu de confirmation de quitter
            if(m_selectBouton) m_quitter = true;   // On veut quitter
            else {
                m_afficheQuitter = false;   // On ferme la fenetre
                m_selectBouton = true;     // On remet le bouton sur "oui" par defaut
            }
        }
        else if(m_afficheConfirmSauvegarde) {   // Menu de confirmation de sauvegarde
            m_afficheConfirmSauvegarde = false;
            if(m_selectBouton) {
                bool res = sauvegarder(m_ligneSelectionnee);
                assert(res);
                if(res) {
                    m_afficheSauvegardeOK = true;
                    m_afficheSauvegarde = false;
                }
                else
                    m_afficheSauvegardeError = true;
                m_afficheMenu = false;
            }
            else m_selectBouton = true; // On remet le bouton sur "oui" par defaut
            m_ligneSelectionnee = 0;
        }

        // Valide dans la sauvegarde
        else if(m_afficheSauvegarde) m_afficheConfirmSauvegarde = true;

        // Valide dans le chargement
        else if(m_afficheChargement) {
            bool res = charger(m_ligneSelectionnee);
            if(res) {
                m_menuPrincipal = false;
                m_afficheChargement = false;
                m_fenetreOuverte = false;
            }
            else
                m_afficheChargementError = true;
            m_afficheMenu = false;
        }

        // Valide dans le menu principal
        else if(m_menuPrincipal) {
            switch(m_ligneSelectionnee) {
                case 0 :  // Nouvelle partie
                    m_lanceMsqScenario = true;
                    m_scenario=true;
                    m_indiceScenario=1;
                    m_affichageTransitionNoire=true;
                    break;
                case 1 : {
                        m_afficheChargement = true;
                        m_ligneSelectionnee = 0;
                    }    // On charge une partie
                    break;
                case 2 : m_afficheQuitter = true;  // On quitte
                    break;
                default: break;
            }
        }

        // Valide dans le menu
        else if(m_afficheMenu) {
            switch(m_ligneSelectionnee) {
                case 0 : m_afficheInventaire = true; // Inventaire
                    m_afficheMenu = false;
                    m_ligneSelectionnee = 0;
                    break;
                case 1 : {
                        if (m_indiceScenario>=14) {
                            m_afficheSauvegarde = true; // Sauvegarde
                            m_ligneSelectionnee = 0;
                        }
                    }
                    break;
                case 2 : {
                        m_afficheChargement = true;    // Charger
                        m_ligneSelectionnee = 0;
                    }
                    break;
                case 3 : m_afficheQuitter = true;   // Quitter
                    break;
                default:break;
            }
        }

        else if (m_afficheCombat) {
            m_afficheCombat = false;
            if (m_tutorielCombatDejaAffiche)
                m_fenetreOuverte = false;
            else {
                m_afficheTutoriel = true;
                m_fenetreOuverte = true;
                m_afficheTutorielCombat = true;
                m_tutorielCombatDejaAffiche = true;
            }
        }
        else if(m_afficheMonteeNiveau) {
            bool trouve = false;
            if (!m_afficheFinQuete)
                m_fenetreOuverte = false;
            m_afficheMonteeNiveau = false;
            for (unsigned int i=0; i<4; i++) {   // on parcourt le tableau
                if (m_monteeNiveau[i]) {
                    if (!trouve) {  // si c'est le premier trouve, on le met a faux
                        m_monteeNiveau[i] = false;
                        trouve = true;
                    }
                    else {          // sinon cela veut dire qu'il y a encore une montee de niveau a faire, on laisse les fenetres ouvertes
                        m_fenetreOuverte = true;
                        m_afficheMonteeNiveau = true;
                    }
                }
            }
        }
        else if(m_afficheVictoire) {
            m_afficheVictoire = false;
            if (!m_afficheFinQuete)
                m_fenetreOuverte = false;
            m_afficheMonteeNiveau = false;
            for (unsigned int i=0; i<4; i++) {   // on parcourt le tableau et on s'arrete au premier vrai
                if (m_monteeNiveau[i]) {
                    m_afficheMonteeNiveau = true;
                    m_fenetreOuverte = true;
                    break;
                }
            }
        }
        else if (m_afficheRecueilQuete) {
            if (m_tableauQuetes[valeur]->getEtat()=='f') {
                if (m_joueur->getInventaire()->getLimite()-m_joueur->getInventaire()->getNbObjet()>m_tableauQuetes[valeur]->getRecompenses()->getNbObjet())
                    recupererRecompenseQuete(valeur);
                else
                    m_afficheInventairePlein = true;
            }
            else if (m_tableauQuetes[valeur]->getEtat()=='a' || m_tableauQuetes[valeur]->getEtat()=='v')
                m_indiceQueteSuivie=valeur;
        }
        else if (m_afficheDebutQuete) {
            m_afficheDebutQuete = false;
            if (!m_afficheFinQuete)
                m_fenetreOuverte = false;
            if (m_selectBouton) {
                m_tableauQuetes[m_indiceQuete]->activerQueteSuivante(); // activation de la quete
                if (m_tableauQuetes[m_indiceQuete]->getQueteActive()->getType()=="Collecte") {
                    QueteCollecte* queteCollecte=static_cast<QueteCollecte*>(m_tableauQuetes[m_indiceQuete]->getQueteActive());
                    if (queteCollecte->getValidation()) {
                        Inventaire* inventaire=m_joueur->getInventaire();
                        for (unsigned int i=0; i<inventaire->getNbObjet(); i++) {
                            if (inventaire->getIemeObjet(i+1)->getIdObjet()==queteCollecte->getIdObjet())
                                gestionObjetQuete(inventaire->getIemeObjet(i+1), true);
                        }
                    }
                }
                if (!m_tutorielRecueilDejaAffiche) {  // premiere fois qu'on commence une quete, on affiche le tutoriel concernant le recueil de quetes
                    m_fenetreOuverte = true;
                    m_afficheTutoriel = true;
                    m_afficheTutorielRecueil = true;
                    m_tutorielRecueilDejaAffiche = true;
                }
            }
            else { // si on a refuse la quete on la desactive
                m_tableauQuetes[m_indiceQuete]->setEtatRecursif('r');
                if (!m_tutorielRefusQueteDejaAffiche) {  // premiere fois qu'on refuse une quete, on affiche le tutoriel
                    m_fenetreOuverte = true;
                    m_afficheTutoriel = true;
                    m_afficheTutorielRefusQuete = true;
                    m_tutorielRefusQueteDejaAffiche = true;
                }
                queteFinieScenario(m_indiceQuete);
            }
        }
        else if (m_afficheValidationCollecte) {
            m_afficheValidationCollecte = false;
            m_fenetreOuverte = false;
            if (m_selectBouton) {
                QueteCollecte* queteCollecte=static_cast<QueteCollecte*>(m_tableauQuetes[m_indiceQuete]->getQueteActive());
                Inventaire* inventaire=m_joueur->getInventaire();
                for (unsigned int i=0; i<inventaire->getNbObjet(); i++) {
                    if (inventaire->getIemeObjet(i+1)->getIdObjet()==queteCollecte->getIdObjet()) {
                        gestionObjetQuete(inventaire->getIemeObjet(i+1), false);
                        inventaire->retirerIemeObjet(i+1);
                    }
                }
                m_tableauQuetes[m_indiceQuete]->activerQueteSuivante();  // la quete est validee on la finit
                if (m_tableauQuetes[m_indiceQuete]->getEtat()=='f') {
                    m_afficheFinQuete=true;
                    m_fenetreOuverte=true;
                }
            }
        }
        else if (m_afficheTutoriel) {
            m_afficheTutorielQuete = false;
            m_afficheTutorielLancerCombat = false;
            m_afficheTutorielCarte = false;
            m_afficheTutorielLourdeau = false;
            m_afficheTutoriel = false;
            m_fenetreOuverte = false;
            if (m_afficheTutorielRefusQuete) {
                m_afficheTutorielRefusQuete=false;
                if (!m_tutorielRecueilDejaAffiche) {  // premiere fois qu'on commence une quete, on affiche le tutoriel concernant le recueil de quetes
                    m_fenetreOuverte = true;
                    m_afficheTutoriel = true;
                    m_afficheTutorielRecueil = true;
                    m_tutorielRecueilDejaAffiche = true;
                }
            }
            else if (m_afficheTutorielRecueil) {
                m_afficheTutorielRecueil = false;
                m_debloquerAffichageRecueil = true;  // on autorise l'acces au recueil de quetes
            }
            else if (m_afficheTutorielInventaire) {
                m_afficheTutorielInventaire = false;
                m_debloquerAffichageInventaire = true;  // a l'inventaire
            }
            else if (m_afficheTutorielDeck) {
                m_afficheTutorielDeck = false;
                m_debloquerAffichageCartes = true;  // a l'editeur de deck
                m_afficheTutoriel = true;
                m_fenetreOuverte = true;
                m_afficheTutorielLancerCombat = true;
            }
            else if (m_afficheTutorielCombat) {
                m_afficheTutorielCombat = false;
                m_afficheTutoriel = true;
                m_fenetreOuverte = true;
                m_afficheTutorielMain = true;
            }
            else if (m_afficheTutorielMain) {
                m_afficheTutorielMain = false;
                m_afficheTutoriel = true;
                m_fenetreOuverte = true;
                m_afficheTutorielMana = true;
            }
            else if (m_afficheTutorielMana) {
                m_afficheTutorielMana = false;
                m_afficheTutoriel = true;
                m_fenetreOuverte = true;
                m_afficheTutorielCarte = true;
            }
        }
    }

    else if(touche == 'h' && !m_fenetreOuverte) {    // Touche "haut" (z)
        // Si on est dans le menu principal
        if((m_menuPrincipal) && !m_afficheChargement) {
            m_ligneSelectionnee--;
            if(m_ligneSelectionnee < 0) m_ligneSelectionnee = 2;    // On fait un tour
        }
        // Si on est dans le menu ou la defaite
        else if(m_afficheMenu && !m_afficheSauvegarde && !m_afficheChargement && !m_afficheInventaire) {
            m_ligneSelectionnee--;
            if(m_ligneSelectionnee < 0) m_ligneSelectionnee = 3;    // On fait un tour
        }
        // Si on est dans la sauvegarde ou le chargement
        else if(m_afficheChargement || m_afficheSauvegarde) {
            m_ligneSelectionnee--;
            if(m_ligneSelectionnee < 0) m_ligneSelectionnee = 5;    // On fait un tour
        }
        // Si on est dans l'inventaire
        else if(m_afficheInventaire) {
            m_ligneSelectionnee--;
            if(m_ligneSelectionnee < 0) m_ligneSelectionnee = 6;    // On fait un tour
        }
        // Si on n'a pas une autre fenetre ouverte on deplace le personnage
        else if (!m_menuPrincipal && !m_afficheMenu) {
            valeur=std::min(valeur,m_tailleCase);
            if(estDeplacementPossible(m_joueur, 'h',valeur)) {
                m_joueur->deplacerPersonnage(0, -valeur);
                m_dernierDeplacement[0].x=0;
                m_dernierDeplacement[0].y=-valeur;
                if (!m_deplacementDiagonale) {
                    for (unsigned int i=39; i>0; i--) {
                        m_dernierDeplacement[i].x=m_dernierDeplacement[i-1].x;
                        m_dernierDeplacement[i].y=m_dernierDeplacement[i-1].y;
                    }
                    deplacementEquipiers();
                }
                collisionObjet(m_joueur);
            }
            else
                m_deplacementDiagonale=false;
        }
    }

    else if(touche == 'b' && !m_fenetreOuverte) {    // Touche "bas" (s)
        // Si on est dans le menu principal ou la defaite
        if((m_menuPrincipal) && !m_afficheChargement) {
            m_ligneSelectionnee++;
            if(m_ligneSelectionnee > 2) m_ligneSelectionnee = 0;    // On fait un tour
        }
        // Si on est dans le menu
        else if(m_afficheMenu  && !m_afficheSauvegarde && !m_afficheChargement && !m_afficheInventaire) {
            m_ligneSelectionnee++;
            if(m_ligneSelectionnee > 3) m_ligneSelectionnee = 0;    // On fait un tour
        }
        // Si on est dans la sauvegarde ou le chargement
        else if(m_afficheChargement || m_afficheSauvegarde) {
            m_ligneSelectionnee++;
            if(m_ligneSelectionnee > 5) m_ligneSelectionnee = 0;    // On fait un tour
        }
        else if(m_afficheInventaire) {
            m_ligneSelectionnee++;
            if(m_ligneSelectionnee > 6) m_ligneSelectionnee = 0; // On fait un tour
        }
        // Si on n'a pas une autre fenetre ouverte on deplace le personnage
        else if (!m_menuPrincipal && !m_afficheMenu) {
            valeur=std::min(valeur,m_tailleCase);
            if(estDeplacementPossible(m_joueur, 'b',valeur)) {
                m_joueur->deplacerPersonnage(0, valeur);
                m_dernierDeplacement[0].x=0;
                m_dernierDeplacement[0].y=valeur;
                if (!m_deplacementDiagonale) {
                    for (unsigned int i=39; i>0; i--) {
                        m_dernierDeplacement[i].x=m_dernierDeplacement[i-1].x;
                        m_dernierDeplacement[i].y=m_dernierDeplacement[i-1].y;
                    }
                    deplacementEquipiers();
                }
                collisionObjet(m_joueur);
            }
            else
                m_deplacementDiagonale=false;
        }
    }

    else if(touche == 'g' || touche == 'd') {    // Touche "gauche" (g) ou "droite" (d)
        // Si on est dans une confirmation
        if(m_afficheQuitter || m_afficheConfirmSauvegarde)
            m_selectBouton = !m_selectBouton;    // On inverse "OK" ou "Annuler"
        // Si on n'a pas une autre fenetre ouverte on deplace le personnage
        else if(!m_menuPrincipal && !m_afficheMenu) {
            valeur=std::min(valeur,m_tailleCase);
            if(estDeplacementPossible(m_joueur, touche, valeur)) {
                if(touche == 'g') {
                    m_joueur->deplacerPersonnage(-valeur, 0);
                    m_dernierDeplacement[0].x=-valeur;
                }
                else {
                    m_joueur->deplacerPersonnage(valeur, 0);
                    m_dernierDeplacement[0].x=valeur;
                }
                if (m_deplacementDiagonale)
                    m_deplacementDiagonale=false;
                else
                    m_dernierDeplacement[0].y=0;
                for (unsigned int i=39; i>0; i--) {
                    m_dernierDeplacement[i].x=m_dernierDeplacement[i-1].x;
                    m_dernierDeplacement[i].y=m_dernierDeplacement[i-1].y;
                }
                deplacementEquipiers();
                collisionObjet(m_joueur);
            }
            else if (m_deplacementDiagonale) {
                m_dernierDeplacement[0].x=0;
                for (unsigned int i=39; i>0; i--) {
                    m_dernierDeplacement[i].x=m_dernierDeplacement[i-1].x;
                    m_dernierDeplacement[i].y=m_dernierDeplacement[i-1].y;
                }
                deplacementEquipiers();
                m_deplacementDiagonale=false;
            }
        }
    }
    else if (touche == 'e' ) {    // Touche e d'interaction tpco tpinv
        if (!m_menuPrincipal && !m_afficheMenu && !m_afficheFinQuete && !m_afficheDialogue) {
            if (m_interactionPossible==1 && (m_afficheInventaireCoffre || !m_fenetreOuverte)) {  // Coffre
                if (valeur == 0) {
                    if (!m_afficheInventaireCoffre || m_tableauCoffres[m_niveau][m_interactionIndiceObjet]->getNbButin()>0)  // soit le coffre n'est pas encore ouvert et on l'ouvre, soit il l'est et a encore des objets et on le referme
                        m_tableauCoffres[m_niveau][m_interactionIndiceObjet]->setOuvert();
                    m_afficheInventaireCoffre = !m_afficheInventaireCoffre;
                    m_afficheInventaire = !m_afficheInventaire;
                    m_fenetreOuverte = !m_fenetreOuverte;
                    if (m_afficheInventaireCoffre == false && !m_tutorielInventaireDejaAffiche) { // premiere fois qu'on ferme un coffre, on affiche le tutoriel concernant l'inventaire
                        m_fenetreOuverte = true;
                        m_afficheTutoriel = true;
                        m_afficheTutorielInventaire = true;
                        m_tutorielInventaireDejaAffiche = true;
                    }
                }
                else {
                    // l'equipement d'objets est a coder
                }
            }
            else if (m_interactionPossible==2 && !m_fenetreOuverte) { // PNJ
                interactionPnjQuete();
                m_tableauPNJ[m_niveau][m_interactionIndiceObjet]->setParle();
                m_afficheDialogue = true;
            }
            else if (m_interactionPossible==3 && (m_afficheInventaireTombe || !m_fenetreOuverte)) { // tombe
                if (m_afficheInventaireTombe && m_tableauInventairesTombes[m_interactionIndiceObjet]->getNbObjet()==0) {
                    m_tableauTombes[m_interactionIndiceObjet]=NULL;
                    delete m_tableauInventairesTombes[m_interactionIndiceObjet];
                    m_tableauInventairesTombes[m_interactionIndiceObjet]=NULL;
                    m_interactionPossible=0;
                }
                m_afficheInventaireTombe = !m_afficheInventaireTombe;
                m_afficheInventaire = !m_afficheInventaire;
                m_fenetreOuverte = !m_fenetreOuverte;
            }
        }
    }
    else if (touche == 'i' && (m_afficheInventaire || !m_fenetreOuverte) && m_debloquerAffichageInventaire && !m_afficheDialogue) {     // Touche i pour l'inventaire
        if (!m_afficheInventaireCoffre && !m_afficheFinQuete) {
            m_afficheInventaire = !m_afficheInventaire;
            m_fenetreOuverte = !m_fenetreOuverte;
        }
        else
            actionClavier('e', 0);
    }
    else if (touche == 'r' && (m_afficheRecueilQuete || !m_fenetreOuverte) && m_debloquerAffichageRecueil && !m_afficheDialogue) {  // Touche r pour le recueil de quetes
        m_afficheRecueilQuete = !m_afficheRecueilQuete;
        m_fenetreOuverte = !m_fenetreOuverte;
    }
    else if (touche == 'c' && (m_afficheCartes || !m_fenetreOuverte) && m_debloquerAffichageCartes && !m_afficheDialogue) {  // Touche c pour l'editeur de deck
        m_afficheCartes = !m_afficheCartes;
        m_fenetreOuverte = !m_fenetreOuverte;
    }
}

bool Jeu::estMarchable(int x, int y) const {
    return (m_terrain->estMarchable(x+y*m_terrain->getTailleX()));
}

bool Jeu::verificationCollisionDecors(unsigned int collisionPersonnage[4], unsigned int collision[4], unsigned int x, unsigned int y, unsigned int indiceCase, unsigned int deplacement, unsigned int indiceTest) const {
    unsigned int modulo, modulo2;
    switch (indiceTest) {
        case 0:
            modulo=x%m_tailleCase;
            modulo2=modulo+collisionPersonnage[2];
            return ((y-deplacement)%m_tailleCase<=collision[1]+collision[3] && (
                        (modulo>=collision[0] && modulo<=collision[0]+collision[2]) || (collision[0]>=modulo && collision[0]<=modulo2)
                        || (modulo2>=collision[0] && modulo2<=collision[0]+collision[2]) || (collision[0]+collision[2]>=modulo && collision[0]+collision[2]<=modulo2)));
            break;
        case 1: // deplacement bas sur 1 case de large
            modulo=x%m_tailleCase;
            modulo2=modulo+collisionPersonnage[2];
            return ((y+deplacement)%m_tailleCase>=collision[1] && (
                        (modulo>=collision[0] && modulo<=collision[0]+collision[2]) || (collision[0]>=modulo && collision[0]<=modulo2)
                        || (modulo2>=collision[0] && modulo2<=collision[0]+collision[2]) || (collision[0]+collision[2]>=modulo && collision[0]+collision[2]<=modulo2)));
            break;
        case 2: // deplacement gauche sur 1 case de haut
            modulo=y%m_tailleCase;
            modulo2=modulo+collisionPersonnage[3];
            return ((x-deplacement)%m_tailleCase<=collision[0]+collision[2] && (
                        (modulo>=collision[1] && modulo<=collision[1]+collision[3]) || (collision[1]>=modulo && collision[1]<=modulo2)
                        || (modulo<=collision[1] && modulo>=collision[1]+collision[3]) || (collision[1]+collision[3]>=modulo && collision[1]+collision[3]<=modulo2)));
            break;
        case 3: // deplacement droite sur 1 case de haut
            modulo=y%m_tailleCase;
            modulo2=modulo+collisionPersonnage[3];
            return ((x+deplacement)%m_tailleCase>=collision[0] && (
                        (modulo>=collision[1] && modulo<=collision[1]+collision[3]) || (collision[1]>=modulo && collision[1]<=modulo2)
                        || (modulo<=collision[1] && modulo>=collision[1]+collision[3]) || (collision[1]+collision[3]>=modulo && collision[1]+collision[3]<=modulo2)));
            break;
    }
    return false;
}

bool Jeu::estDeplacementPossible(const Personnage* personnage, char touche, unsigned int& deplacement) const {
    unsigned int collisionPersonnage[4];
    personnage->getCollisions(collisionPersonnage);
    unsigned int x = (unsigned int) (personnage->getPosition().x+collisionPersonnage[0]);
    unsigned int y = (unsigned int) (personnage->getPosition().y+collisionPersonnage[1]);
    unsigned int i = x/m_tailleCase;
    unsigned int j = y/m_tailleCase;
    unsigned int collision[4];
    unsigned int deplacementPossible=deplacement;
    switch(touche) {
        case 'h' :
            if (estMarchable(i,j)) {
                if ((x+collisionPersonnage[2])/m_tailleCase > i && !estMarchable(i+1,j)) {  // collision sur 2 cases de largeur et deuxieme case avec collision
                    for (unsigned int k=0; k<4; k++)
                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+j*m_terrain->getTailleX())][k];
                    if (y%m_tailleCase>collision[1]+collision[3] && verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,0))
                        deplacement=y-(j*m_tailleCase+collision[1]+collision[3])-1;
                }
                if ((y-deplacement)/m_tailleCase<y/m_tailleCase) {  // si on change de case on verifie la collision de la case suivante
                    if (estMarchable(i, j-1)) {
                        if ((x+collisionPersonnage[2])/m_tailleCase > i) {  // collision sur 2 cases de largeur
                            if (!estMarchable(i+1, j-1)) {
                                for (unsigned int k=0; k<4; k++)
                                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j-1)*m_terrain->getTailleX())][k];
                                if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,0))
                                    deplacement=y-((j-1)*m_tailleCase+collision[1]+collision[3])-1 ;
                            }
                        }
                    }
                    else {
                        for (unsigned int k=0; k<4; k++)
                            collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+(j-1)*m_terrain->getTailleX())][k];
                        if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,0))
                            deplacementPossible=y-((j-1)*m_tailleCase+collision[1]+collision[3])-1 ;
                        if ((x+collisionPersonnage[2])/m_tailleCase > i) {  // collision sur 2 cases de largeur
                            if (!estMarchable(i+1, j-1)) {
                                for (unsigned int k=0; k<4; k++)
                                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j-1)*m_terrain->getTailleX())][k];
                                if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,0))
                                    deplacementPossible=std::min(y-((j-1)*m_tailleCase+collision[1]+collision[3])-1, deplacementPossible) ;
                            }
                        }
                        deplacement=deplacementPossible;
                    }
                }
            }
            else {
                if ((x+collisionPersonnage[2])/m_tailleCase > i && !estMarchable(i+1,j)) {  // collision sur 2 cases de largeur et deuxieme case avec collision
                    for (unsigned int k=0; k<4; k++)
                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+j*m_terrain->getTailleX())][k];
                    if (y%m_tailleCase>collision[1]+collision[3] && verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,0))
                        deplacement=y-(j*m_tailleCase+collision[1]+collision[3])-1;
                }
                for (unsigned int k=0; k<4; k++)
                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+j*m_terrain->getTailleX())][k];
                if (y%m_tailleCase>collision[1]+collision[3] && verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,0))
                    deplacement=y-(j*m_tailleCase+collision[1]+collision[3])-1;
                else {
                    if ((y-deplacement)/m_tailleCase<y/m_tailleCase) {  // si on change de case on verifie la collision de la case suivante
                        if (estMarchable(i, j-1)) {
                            if ((x+collisionPersonnage[2])/m_tailleCase > i) {  // collision sur 2 cases de largeur
                                if (!estMarchable(i+1, j-1)) {
                                    for (unsigned int k=0; k<4; k++)
                                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j-1)*m_terrain->getTailleX())][k];
                                    if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,0))
                                        deplacement=y-((j-1)*m_tailleCase+collision[1]+collision[3])-1 ;
                                }
                            }
                        }
                        else {
                            for (unsigned int k=0; k<4; k++)
                                collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+(j-1)*m_terrain->getTailleX())][k];
                            if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,0))
                                deplacementPossible=y-((j-1)*m_tailleCase+collision[1]+collision[3])-1 ;
                            if ((x+collisionPersonnage[2])/m_tailleCase > i) {  // collision sur 2 cases de largeur
                                if (!estMarchable(i+1, j-1)) {
                                    for (unsigned int k=0; k<4; k++)
                                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j-1)*m_terrain->getTailleX())][k];
                                    if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,0))
                                        deplacementPossible=std::min(y-((j-1)*m_tailleCase+collision[1]+collision[3])-1, deplacementPossible) ;
                                }
                            }
                            deplacement=deplacementPossible;
                        }
                    }
                }
            }
            if (y<deplacement)
                deplacement=y;
            return deplacement;
            break;
        case 'b' :
            y+=collisionPersonnage[3];
            j=y/m_tailleCase;
            if (estMarchable(i,j)) {
                if ((x+collisionPersonnage[2])/m_tailleCase > i && !estMarchable(i+1,j)) {  // collision sur 2 cases de largeur et deuxieme case avec collision
                    for (unsigned int k=0; k<4; k++)
                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+j*m_terrain->getTailleX())][k];
                    if (y%m_tailleCase<collision[1] && verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,1))
                        deplacement=(j*m_tailleCase+collision[1])-y-1 ;
                }
                if ((y+deplacement)/m_tailleCase>y/m_tailleCase) {  // si on change de case on verifie la collision de la case suivante
                    if (estMarchable(i, j+1)) {
                        if ((x+collisionPersonnage[2])/m_tailleCase > i) {  // collision sur 2 cases de largeur
                            if (!estMarchable(i+1, j+1)) {
                                for (unsigned int k=0; k<4; k++)
                                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j+1)*m_terrain->getTailleX())][k];
                                if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,1))
                                    deplacement=((j+1)*m_tailleCase+collision[1])-y-1 ;
                            }
                        }
                    }
                    else {
                        for (unsigned int k=0; k<4; k++)
                            collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+(j+1)*m_terrain->getTailleX())][k];
                        if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,1))
                            deplacementPossible=((j+1)*m_tailleCase+collision[1])-y-1 ;
                        if ((x+collisionPersonnage[2])/m_tailleCase > i) {  // collision sur 2 cases de largeur
                            if (!estMarchable(i+1, j+1)) {
                                for (unsigned int k=0; k<4; k++)
                                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j+1)*m_terrain->getTailleX())][k];
                                if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,1))
                                    deplacementPossible=std::min(((j+1)*m_tailleCase+collision[1])-y-1, deplacementPossible) ;
                            }
                        }
                        deplacement=deplacementPossible;
                    }
                }
            }
            else {
                if ((x+collisionPersonnage[2])/m_tailleCase > i && !estMarchable(i+1,j)) {  // collision sur 2 cases de largeur et deuxieme case avec collision
                    for (unsigned int k=0; k<4; k++)
                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+j*m_terrain->getTailleX())][k];
                    if (y%m_tailleCase<collision[1] && verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,1))
                        deplacement=(j*m_tailleCase+collision[1])-y-1 ;
                }
                for (unsigned int k=0; k<4; k++)
                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+j*m_terrain->getTailleX())][k];
                if (y%m_tailleCase<collision[1] && verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,1))
                    deplacement=(j*m_tailleCase+collision[1])-y-1;
                else {
                    if ((y+deplacement)/m_tailleCase>y/m_tailleCase) {  // si on change de case on verifie la collision de la case suivante
                        if (estMarchable(i, j+1)) {
                            if ((x+collisionPersonnage[2])/m_tailleCase > i) {  // collision sur 2 cases de largeur
                                if (!estMarchable(i+1, j+1)) {
                                    for (unsigned int k=0; k<4; k++)
                                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j+1)*m_terrain->getTailleX())][k];
                                    if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,1))
                                        deplacement=((j+1)*m_tailleCase+collision[1])-y-1 ;
                                }
                            }
                        }
                        else {
                            for (unsigned int k=0; k<4; k++)
                                collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+(j+1)*m_terrain->getTailleX())][k];
                            if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,1))
                                deplacementPossible=((j+1)*m_tailleCase+collision[1])-y-1 ;
                            if ((x+collisionPersonnage[2])/m_tailleCase > i) {  // collision sur 2 cases de largeur
                                if (!estMarchable(i+1, j+1)) {
                                    for (unsigned int k=0; k<4; k++)
                                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j+1)*m_terrain->getTailleX())][k];
                                    if (verificationCollisionDecors(collisionPersonnage,collision,x,y,i,deplacement,1))
                                        deplacementPossible=std::min(((j-1)*m_tailleCase+collision[1])-y-1, deplacementPossible) ;
                                }
                            }
                            deplacement=deplacementPossible;
                        }
                    }
                }
            }
            if (y+deplacement>=m_terrain->getTailleY()*m_tailleCase)
                deplacement=m_terrain->getTailleY()*m_tailleCase-1-y;
            return deplacement;
            break;
        case 'g' :
            if (estMarchable(i,j)) {
                if ((y+collisionPersonnage[3])/m_tailleCase > j && !estMarchable(i,j+1)) {  // collision sur 2 cases de haut et deuxieme case avec collision
                    for (unsigned int k=0; k<4; k++)
                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+(j+1)*m_terrain->getTailleX())][k];
                    if (x%m_tailleCase>collision[0]+collision[2] && verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,2))
                        deplacement=x-(i*m_tailleCase+collision[0]+collision[2])-1 ;
                }
                if ((x-deplacement)/m_tailleCase<x/m_tailleCase) {  // si on change de case on verifie la collision de la case suivante
                    if (estMarchable(i-1, j)) {
                        if ((y+collisionPersonnage[3])/m_tailleCase > j) {  // collision sur 2 cases de hauteur
                            if (!estMarchable(i-1, j+1)) {
                                for (unsigned int k=0; k<4; k++)
                                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i-1+(j+1)*m_terrain->getTailleX())][k];
                                if (verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,2))
                                    deplacement=x-((i-1)*m_tailleCase+collision[0]+collision[2])-1 ;
                            }
                        }
                    }
                    else {
                        for (unsigned int k=0; k<4; k++)
                            collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i-1+j*m_terrain->getTailleX())][k];
                        if (verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,2))
                            deplacementPossible=x-((i-1)*m_tailleCase+collision[0]+collision[2])-1 ;
                        if ((y+collisionPersonnage[3])/m_tailleCase > j) {  // collision sur 2 cases de hauteur
                            if (!estMarchable(i-1, j+1)) {
                               for (unsigned int k=0; k<4; k++)
                                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i-1+(j+1)*m_terrain->getTailleX())][k];
                                if (verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,2))
                                    deplacementPossible=std::min(x-((i-1)*m_tailleCase+collision[0]+collision[2])-1,deplacementPossible) ;
                            }
                        }
                        deplacement=deplacementPossible;
                    }
                }
            }
            else {
                if ((y+collisionPersonnage[3])/m_tailleCase > j && !estMarchable(i,j+1)) {  // collision sur 2 cases de haut et deuxieme case avec collision
                    for (unsigned int k=0; k<4; k++)
                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+(j+1)*m_terrain->getTailleX())][k];
                    if (x%m_tailleCase>collision[0]+collision[2] && verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,2))
                        deplacement=x-(i*m_tailleCase+collision[0]+collision[2])-1 ;
                }
                for (unsigned int k=0; k<4; k++)
                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+j*m_terrain->getTailleX())][k];
                if (x%m_tailleCase>collision[0]+collision[2] && verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,2))
                    deplacement=x-(i*m_tailleCase+collision[0]+collision[2])-1;
                else {
                    if ((x-deplacement)/m_tailleCase<x/m_tailleCase) {  // si on change de case on verifie la collision de la case suivante
                        if (estMarchable(i-1, j)) {
                            if ((y+collisionPersonnage[3])/m_tailleCase > j) {  // collision sur 2 cases de hauteur
                                if (!estMarchable(i-1, j+1)) {
                                    for (unsigned int k=0; k<4; k++)
                                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i-1+(j+1)*m_terrain->getTailleX())][k];
                                    if (verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,2))
                                        deplacement=x-((i-1)*m_tailleCase+collision[0]+collision[2])-1 ;
                                }
                            }
                        }
                        else {
                            for (unsigned int k=0; k<4; k++)
                                collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i-1+j*m_terrain->getTailleX())][k];
                            if (verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,2))
                                deplacementPossible=x-((i-1)*m_tailleCase+collision[0]+collision[2])-1 ;
                            if ((y+collisionPersonnage[3])/m_tailleCase > j) {  // collision sur 2 cases de hauteur
                                if (!estMarchable(i-1, j+1)) {
                                    for (unsigned int k=0; k<4; k++)
                                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i-1+(j+1)*m_terrain->getTailleX())][k];
                                    if (verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,2))
                                        deplacementPossible=std::min(x-((i-1)*m_tailleCase+collision[0]+collision[2])-1,deplacementPossible) ;
                                }
                            }
                            deplacement=deplacementPossible;
                        }
                    }
                }
            }
            if (x<deplacement)
                deplacement=x;
            return deplacement;
            break;
        case 'd' :
            x+=collisionPersonnage[2];
            i=x/m_tailleCase;
            if (estMarchable(i,j)) {
                if ((y+collisionPersonnage[3])/m_tailleCase > j && !estMarchable(i,j+1)) {  // collision sur 2 cases de haut et deuxieme case avec collision
                    for (unsigned int k=0; k<4; k++)
                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+(j+1)*m_terrain->getTailleX())][k];
                    if (x%m_tailleCase<collision[0] && verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,3))
                        deplacement=(i*m_tailleCase+collision[0])-x-1 ;
                }
                if ((x+deplacement)/m_tailleCase>x/m_tailleCase) {  // si on change de case on verifie la collision de la case suivante
                    if (estMarchable(i+1, j)) {
                        if ((y+collisionPersonnage[3])/m_tailleCase > j) {  // collision sur 2 cases de hauteur
                            if (!estMarchable(i+1, j+1)) {
                                for (unsigned int k=0; k<4; k++)
                                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j+1)*m_terrain->getTailleX())][k];
                                if (verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,3))
                                    deplacement=((i+1)*m_tailleCase+collision[0])-x-1 ;
                            }
                        }
                    }
                    else {
                        for (unsigned int k=0; k<4; k++)
                            collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+j*m_terrain->getTailleX())][k];
                        if (verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,3))
                            deplacementPossible=((i+1)*m_tailleCase+collision[0])-x-1 ;
                        if ((y+collisionPersonnage[3])/m_tailleCase > j) {  // collision sur 2 cases de hauteur
                            if (!estMarchable(i+1, j+1)) {
                                for (unsigned int k=0; k<4; k++)
                                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j+1)*m_terrain->getTailleX())][k];
                                if (verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,3))
                                    deplacementPossible=std::min(((i+1)*m_tailleCase+collision[0])-x+1,deplacementPossible) ;
                            }
                        }
                        deplacement=deplacementPossible;
                    }
                }
            }
            else {
                if ((y+collisionPersonnage[3])/m_tailleCase > j && !estMarchable(i,j+1)) {  // collision sur 2 cases de haut et deuxieme case avec collision
                    for (unsigned int k=0; k<4; k++)
                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+(j+1)*m_terrain->getTailleX())][k];
                    if (x%m_tailleCase<collision[0] && verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,3))
                        deplacement=(i*m_tailleCase+collision[0])-x-1;
                }
                for (unsigned int k=0; k<4; k++)
                    collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+j*m_terrain->getTailleX())][k];
                if (x%m_tailleCase<collision[0] && verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,3))
                    deplacement=(i*m_tailleCase+collision[0])-x-1;
                else {
                    if ((x+deplacement)/m_tailleCase>x/m_tailleCase) {  // si on change de case on verifie la collision de la case suivante
                        if (estMarchable(i+1, j)) {
                            if ((y+collisionPersonnage[3])/m_tailleCase > j) {  // collision sur 2 cases de hauteur
                                if (!estMarchable(i+1, j+1)) {
                                    for (unsigned int k=0; k<4; k++)
                                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j+1)*m_terrain->getTailleX())][k];
                                    if (verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,3))
                                        deplacement=((i+1)*m_tailleCase+collision[0])-x-1 ;
                                }
                            }
                        }
                        else {
                            for (unsigned int k=0; k<4; k++)
                                collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+j*m_terrain->getTailleX())][k];
                            if (verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,3))
                                deplacementPossible=((i+1)*m_tailleCase+collision[0])-x-1 ;
                            if ((y+collisionPersonnage[3])/m_tailleCase > j) {  // collision sur 2 cases de hauteur
                                if (!estMarchable(i+1, j+1)) {
                                    for (unsigned int k=0; k<4; k++)
                                        collision[k]=m_collisionsDecors[m_terrain->getDeuxiemeCoucheTerrain(i+1+(j+1)*m_terrain->getTailleX())][k];
                                    if (x%m_tailleCase<collision[0] && verificationCollisionDecors(collisionPersonnage,collision,x,y,j,deplacement,3))
                                        deplacementPossible=std::min(((i+1)*m_tailleCase+collision[0])-x-1,deplacementPossible) ;
                                }
                            }
                            deplacement=deplacementPossible;
                        }
                    }
                }
            }
            if (x+deplacement>=m_terrain->getTailleX()*m_tailleCase)
                deplacement=m_terrain->getTailleX()*m_tailleCase-1-x;
            return deplacement;
            break;
    }
    return false;
}

bool Jeu::estDeplacementDiagonalPossible(Point depart, Point arrive, void *grille, Point vdeplacement, float distanceEnnemiJoueur) const { //tpdiago
    bool cheminLibre = true;
    float distanceParcouru = 0;

    while (distanceParcouru <= distanceEnnemiJoueur && cheminLibre) {
        depart.x+=vdeplacement.x;
        depart.y+=vdeplacement.y;
        if (static_cast<Grid *>(grille)->getNode(depart.x/1000, depart.y/1000)->m_walkable){
                distanceParcouru += sqrt(pow(vdeplacement.x, 2.0) +
                                 pow(vdeplacement.y, 2.0));
            }
        else {
            cheminLibre = false;


        }
    }

    return cheminLibre;
}

void Jeu::deplacementEquipiers() {
    for (unsigned int i=0; i<3; i++) {
        if (m_equipe[i]!=NULL) {
            unsigned int collisionFamilier[4];
            m_equipe[i]->getCollisions(collisionFamilier);
            unsigned int collisionJoueur[4];
            m_joueur->getCollisions(collisionJoueur);
            Point posFamilier=m_equipe[i]->getPosition();
            posFamilier.x=((posFamilier.x+collisionFamilier[0])+(posFamilier.x+collisionFamilier[0]+collisionFamilier[2]))/2;
            posFamilier.y=((posFamilier.y+collisionFamilier[1])+(posFamilier.y+collisionFamilier[1]+collisionFamilier[3]))/2;
            Point posJoueur=m_joueur->getPosition();
            posJoueur.x=((posJoueur.x+collisionJoueur[0])+(posJoueur.x+collisionJoueur[0]+collisionJoueur[2]))/2;
            posJoueur.y=((posJoueur.y+collisionJoueur[1])+(posJoueur.y+collisionJoueur[1]+collisionJoueur[3]))/2;
            m_equipe[i]->deplacerPersonnage(m_dernierDeplacement[19+i*10].x, m_dernierDeplacement[19+i*10].y);
        }
    }
}

void Jeu::collisionObjet(Personnage* personnage) {
    unsigned int collisionPersonnage[4];
    personnage->getCollisions(collisionPersonnage);
    int x = m_joueur->getPosition().x+collisionPersonnage[0];
    int y = m_joueur->getPosition().y+collisionPersonnage[1];
    int x2=x+collisionPersonnage[2];
    int y2=y+collisionPersonnage[3];
    // "Collision" point de sortie
    for(unsigned int i = 0 ; i < m_niveauSorties[m_niveau].size() ; i++) {
        Point p=m_niveauSorties[m_niveau][i];
        unsigned int tailleX=m_terrain->getTailleX();
        if ((unsigned int)x>=(p.x%tailleX)*m_tailleCase && (unsigned int)x<(p.y%tailleX+1)*m_tailleCase && (unsigned int)y>=(p.x/tailleX)*m_tailleCase && (unsigned int)y<(p.y/tailleX+1)*m_tailleCase) {
            Point destination=m_niveauDestinationsSorties[m_niveau][i];
            if (destination.x!=-1) {
                Point sortieDestination=m_niveauSorties[destination.x][destination.y];
                Point positionDestination;
                unsigned int tailleXDestination=m_niveauTaille[destination.x].x;
                if (sortieDestination.x/tailleXDestination==0 || sortieDestination.x/(int)tailleXDestination==m_niveauTaille[destination.x].y) {  // sortie nord/sud
                    positionDestination.x=(sortieDestination.x%tailleXDestination+(sortieDestination.y-sortieDestination.x)/2)*m_tailleCase;
                    if (sortieDestination.x/tailleXDestination==0) // sortie nord
                        positionDestination.y=(sortieDestination.x/tailleXDestination+1)*m_tailleCase;
                    else  // sortie sud
                        positionDestination.y=(sortieDestination.x/tailleXDestination-1)*m_tailleCase;
                }
                else {    // sortie ouest/est
                    if (sortieDestination.x%tailleXDestination==0)  // sortie ouest
                        positionDestination.x=(sortieDestination.x%tailleXDestination+1)*m_tailleCase;
                    else // sortie est
                        positionDestination.x=(sortieDestination.x%tailleXDestination-1)*m_tailleCase;
                    positionDestination.y=((unsigned int)(sortieDestination.x/tailleXDestination)+(sortieDestination.y-sortieDestination.x)/tailleXDestination/2)*m_tailleCase;
                }
                chargerNiveau(destination.x, positionDestination.x, positionDestination.y);
            }
        }
    }
    if (!m_afficheInventaireCoffre)
        m_interactionPossible=0;
    // Collision Coffre
    unsigned int indiceCollision;
    unsigned int rayon=m_tailleCase/2;
    for(unsigned int i = 0 ; i < m_tableauCoffres[m_niveau].size() ; i++) {
        unsigned int CoffrePosX = m_tableauCoffres[m_niveau][i]->getPosition().x;
        unsigned int CoffrePosY = m_tableauCoffres[m_niveau][i]->getPosition().y;
        indiceCollision=m_terrain->getDeuxiemeCoucheTerrain(CoffrePosX+CoffrePosY*m_terrain->getTailleX());
        int CoffrePosX1 = CoffrePosX*m_tailleCase+m_collisionsDecors[indiceCollision][0];
        int CoffrePosY1 = CoffrePosY*m_tailleCase+m_collisionsDecors[indiceCollision][1];
        int CoffrePosX2 = CoffrePosX1+m_collisionsDecors[indiceCollision][2];
        int CoffrePosY2 = CoffrePosY1+m_collisionsDecors[indiceCollision][3];
        if ((abs(CoffrePosX1 - x) <= rayon || abs(CoffrePosX1 - x2) <= rayon || abs(CoffrePosX2 - x) <= rayon || abs(CoffrePosX2 - x2) <= rayon) &&
            ((abs(CoffrePosY1 - y) <= rayon || abs(CoffrePosY1 - y2) <= rayon || abs(CoffrePosY2 - y) <= rayon || abs(CoffrePosY2 - y2) <= rayon))) {
            if(!m_tableauCoffres[m_niveau][i]->getOuvert()) {
                m_interactionPossible=1;
                m_interactionIndice=m_tableauCoffres[m_niveau][i]->getPosition().x+m_tableauCoffres[m_niveau][i]->getPosition().y*m_terrain->getTailleX();
                m_interactionIndiceObjet=i;
                i=m_tableauCoffres[m_niveau].size();
            }
        }
    }
    // Collision PNJ
    for(unsigned int i = 0 ; i < m_tableauPNJ[m_niveau].size() ; i++) {
        unsigned int PNJPosX = m_tableauPNJ[m_niveau][i]->getPosition().x;
        unsigned int PNJPosY = m_tableauPNJ[m_niveau][i]->getPosition().y;
        indiceCollision=m_terrain->getDeuxiemeCoucheTerrain(PNJPosX+PNJPosY*m_terrain->getTailleX());
        int PNJPosX1 = PNJPosX*m_tailleCase+m_collisionsDecors[indiceCollision][0];
        int PNJPosY1 = PNJPosY*m_tailleCase+m_collisionsDecors[indiceCollision][1];
        int PNJPosX2 = PNJPosX1+m_collisionsDecors[indiceCollision][2];
        int PNJPosY2 = PNJPosY1+m_collisionsDecors[indiceCollision][3];
        if ((abs(PNJPosX1 - x) <= rayon || abs(PNJPosX1 - x2) <= rayon || abs(PNJPosX2 - x) <= rayon || abs(PNJPosX2 - x2) <= rayon) &&
            ((abs(PNJPosY1 - y) <= rayon || abs(PNJPosY1 - y2) <= rayon || abs(PNJPosY2 - y) <= rayon || abs(PNJPosY2 - y2) <= rayon))) {
            if (m_tableauPNJ[m_niveau][i]->getADialogue()) {
                m_interactionPossible=2;
                m_interactionIndice=m_tableauPNJ[m_niveau][i]->getPosition().x+m_tableauPNJ[m_niveau][i]->getPosition().y*m_terrain->getTailleX();
                m_interactionIndiceObjet=i;
                i=m_tableauPNJ[m_niveau].size();
            }
        }
    }
    // Collision Ennemi
    for(unsigned int i = 0 ; i < m_tableauEnnemis.size() ; i++) {
        Ennemi* ennemi=m_tableauEnnemis[i];
        if (ennemi != NULL) {
            unsigned int collisionEnnemi[4];
            ennemi->getCollisions(collisionEnnemi);
            int EnnemiPosX1 = ennemi->getPosition().x+collisionEnnemi[0];
            int EnnemiPosY1 = ennemi->getPosition().y+collisionEnnemi[1];
            int EnnemiPosX2 = EnnemiPosX1+collisionEnnemi[2];
            int EnnemiPosY2 = EnnemiPosY1+collisionEnnemi[3];
            if ((abs(EnnemiPosX1 - x) <= rayon || abs(EnnemiPosX1 - x2) <= rayon || abs(EnnemiPosX2 - x) <= rayon || abs(EnnemiPosX2 - x2) <= rayon) &&
                ((abs(EnnemiPosY1 - y) <= rayon || abs(EnnemiPosY1 - y2) <= rayon || abs(EnnemiPosY2 - y) <= rayon || abs(EnnemiPosY2 - y2) <= rayon))) {   // il y a collision
                Ennemi* ennemisProches[5];    //  on cree le tableau qui va contenir les ennemis suffisamment proches pour entrer dans le combat
                ennemisProches[0]=ennemi;     // on y ajoute l'ennemi avec lequel on est rentre en collision
                for (unsigned int j=1; j<5; j++)   // on met a NULL les autres
                    ennemisProches[j]=NULL;
                unsigned int nombreEnnemisProches=1;   // on a deja un ennemi sur les 5 possibles
                unsigned int tableauVitesses[5];    // on cree le tableau qui va contenir les vitesses modifiees pour le combat, definissant l'ordre de jeu au premier tour
                tableauVitesses[0]=ennemisProches[0]->getVitesse();    // pour l'ennemi avec lequel on est rentre en collision, il n'y a pas de bonus ou malus particulier
                for (unsigned int j=1; j<5; j++)
                    tableauVitesses[j]=0;
                char direction;        // On recupere la direction vers laquelle est tourne le joueur pour verifier quels monstres se situent dans son dos
                if (m_dernierDeplacement[0].x<0)
                    direction='g';
                else if (m_dernierDeplacement[0].x==0 && m_dernierDeplacement[0].y<0)
                    direction='h';
                else if (m_dernierDeplacement[0].x==0 && m_dernierDeplacement[0].y>0)
                    direction='b';
                else
                    direction='d';
                unsigned int bonusDos=50;   // Bonus de vitesse si le monstre est dans le dos du joueur
                unsigned int rayonMax=3*m_tailleCase;    // on definit le rayon max de recherche
                unsigned int rayonActuel=0.5*m_tailleCase;     // on definit le rayon actuel qui augmentera de 0.5*m_tailleCase en 0.5*m_tailleCase
                if (m_terrain->getIdRegion()!=0) {
                    while (nombreEnnemisProches<5 && rayonActuel<rayonMax) {   // tant qu'on a pas assez d'ennemi et qu'on a pas atteint le rayon maximum de recherche
                        bool dejaPris;   // pour verifier qu'un ennemi n'a pas deja ete ajoute au tableau
                        for(unsigned int j = 0 ; j < m_tableauEnnemis.size() ; j++) {
                            ennemi=m_tableauEnnemis[j];
                            dejaPris=false;
                            if (ennemi!=NULL && nombreEnnemisProches<5) {
                                for (unsigned int k=0; k<5; k++) {
                                    if (ennemisProches[k]==m_tableauEnnemis[j])   // on verifie si l'ennemi est deja pris
                                        dejaPris=true;
                                }
                                if (!dejaPris) {   // s'il n'est pas deja pris, on verifie s'il est assez proche
                                    ennemi->getCollisions(collisionEnnemi);
                                    EnnemiPosX1 = ennemi->getPosition().x+collisionEnnemi[0];
                                    EnnemiPosY1 = ennemi->getPosition().y+collisionEnnemi[1];
                                    EnnemiPosX2 = EnnemiPosX1+collisionEnnemi[2];
                                    EnnemiPosY2 = EnnemiPosY1+collisionEnnemi[3];
                                    if ((abs(EnnemiPosX1 - x) <= rayonActuel || abs(EnnemiPosX1 - x2) <= rayonActuel || abs(EnnemiPosX2 - x) <= rayonActuel || abs(EnnemiPosX2 - x2) <= rayonActuel) &&
                                        ((abs(EnnemiPosY1 - y) <= rayonActuel || abs(EnnemiPosY1 - y2) <= rayonActuel || abs(EnnemiPosY2 - y) <= rayonActuel || abs(EnnemiPosY2 - y2) <= rayonActuel))) {  // s'il est assez proche on l'ajoute au tableau
                                        ennemisProches[nombreEnnemisProches]=ennemi;
                                        tableauVitesses[nombreEnnemisProches]=std::max((int)(ennemi->getVitesse()-10*(rayonActuel/(0.5*m_tailleCase)-1)),0);  // on donne un malus de 10 de vitesse pour chaque tranche du rayon de recherche
                                        switch (direction) {
                                            case 'h':
                                                if (EnnemiPosY1>y2)
                                                    tableauVitesses[nombreEnnemisProches]+=bonusDos;
                                                break;
                                            case 'b':
                                                if (EnnemiPosY2<y)
                                                    tableauVitesses[nombreEnnemisProches]+=bonusDos;
                                                break;
                                            case 'g':
                                                if (EnnemiPosX1>x2)
                                                    tableauVitesses[nombreEnnemisProches]+=bonusDos;
                                                break;
                                            case 'd':
                                                if (EnnemiPosX2<x)
                                                    tableauVitesses[nombreEnnemisProches]+=bonusDos;
                                                break;
                                        }
                                        nombreEnnemisProches++;
                                    }
                                }
                            }
                        }
                        rayonActuel+=0.5*m_tailleCase;  // on augmente le rayon de recherche
                    }
                }
                lanceCombat(ennemisProches,tableauVitesses);
            }
        }
    }
    // Collision Tombe
    for(unsigned int i = 0 ; i < m_tableauTombes.size() ; i++) {
        Ennemi* ennemi=m_tableauTombes[i];
        if (ennemi != NULL) {
            unsigned int collisionEnnemi[4];
            ennemi->getCollisions(collisionEnnemi);
            int EnnemiPosX1 = ennemi->getPosition().x+collisionEnnemi[0];
            int EnnemiPosY1 = ennemi->getPosition().y+collisionEnnemi[1];
            int EnnemiPosX2 = EnnemiPosX1+collisionEnnemi[2];
            int EnnemiPosY2 = EnnemiPosY1+collisionEnnemi[3];
            if ((abs(EnnemiPosX1 - x) <= rayon || abs(EnnemiPosX1 - x2) <= rayon || abs(EnnemiPosX2 - x) <= rayon || abs(EnnemiPosX2 - x2) <= rayon) &&
                ((abs(EnnemiPosY1 - y) <= rayon || abs(EnnemiPosY1 - y2) <= rayon || abs(EnnemiPosY2 - y) <= rayon || abs(EnnemiPosY2 - y2) <= rayon))) {   // il y a collision
                m_interactionPossible=3;
                m_interactionTombeCoordonnees.x=EnnemiPosX1/m_tailleCase;  // on recupere les coordonnees pour afficher le bouton d'interaction
                m_interactionTombeCoordonnees.y=EnnemiPosY1/m_tailleCase;
                m_interactionIndiceObjet=i;
                break;  // on quitte la boucle for
            }
        }
    }
    // On actualise les quetes pour les quetes de deplacement
    gestionDeplacementQuete();
}

bool Jeu::sauvegarder(int numFichier) {
    // Recuperer date et heure
    time_t temps = time(0);
    tm *ltm = localtime(&temps);
    std::string nomFichier = "data/sauvegardes/sauvegarde";
    std::string extension = ".txt";
    nomFichier = nomFichier + std::to_string(numFichier) + extension;
    std::ofstream fichier(nomFichier.c_str());
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"Jeu::sauvegarder : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier.is_open());
                return false;
            }
        }
    }
    if(fichier.is_open()) {
        // Date et heure
        fichier << ltm->tm_mday << " " << ltm->tm_mon << " " << ltm->tm_year << " ";
        fichier << ltm->tm_hour << " " << ltm->tm_min << std::endl;

        // Infos monde
        for (unsigned int i=0; i<m_tableauCoffres.size(); i++) {
            for (unsigned int j=0; j<m_tableauCoffres[i].size(); j++) {
                Coffre* coffre=m_tableauCoffres[i][j];
                fichier << coffre->getOuvert() << " ";
                fichier << coffre->getOr() << " ";
                fichier << coffre->getButin()->getLimite() << " ";
                for (unsigned int k=0; k<coffre->getButin()->getLimite(); k++) {
                    Objet* objet=coffre->getObjet(k);
                    if (objet!=NULL) {
                        fichier << objet->getIdObjet() << " ";
                        if (objet->getTypeObjet()=="Mat\u00E9riau")
                            fichier << static_cast<Materiau*>(objet)->getNombreExemplaires() << " ";
                    }
                    else
                        fichier << "-1 ";
                }
                fichier << std::endl;
            }
            fichier << std::endl;
        }
        fichier << std::endl;

        // Infos niveau
        fichier << m_niveau << std::endl;

        // Infos ennemis ?

        // Infos joueur
        fichier << m_joueur->getPosition().x <<" "<<m_joueur->getPosition().y<<" ";
        fichier << int(m_joueur->getPointsDeVie()) << " " << int(m_joueur->getExperience()) << " " << m_joueur->getNiveau() << std::endl;

        // Inventaire du joueur
        Inventaire* inventaire=m_joueur->getInventaire();
        fichier << inventaire->getOr() << " " << inventaire->getLimite() << " ";
        for (unsigned int i=0; i<inventaire->getLimite(); i++) {
            Objet* objet=inventaire->getObjet(i);
            if (objet!=NULL) {
                fichier << objet->getIdObjet() << " ";
                if (objet->getTypeObjet()=="Mat\u00E9riau")
                    fichier << static_cast<Materiau*>(objet)->getNombreExemplaires() << " ";
            }
            else
                fichier << "-1 ";
        }
        fichier << std::endl;
;
        // Cartes obtenues
        unsigned int* cartesObtenues = m_joueur->getCartesObtenues();
        for(unsigned int i = 0 ; i < 75 ; i++) {
            fichier << cartesObtenues[i] << " ";
        }
        fichier << std::endl;

        // Deck
        unsigned int nbCartesDeck = m_joueur->getDeck()->getNombreCartes();
        fichier << nbCartesDeck << " ";
        for(unsigned int i = 0 ; i < nbCartesDeck ; i++) {
            fichier << m_joueur->getDeck()->getCarte(i)->getIdCarte() << " ";
        }
        fichier << std::endl;

        // Infos equipe
        for (unsigned int i=0; i<3; i++) {
            if (m_equipe[i]!=NULL) {
                Personnage* personnage=m_equipe[i];
                fichier << personnage->getId() << " ";
                if (personnage->getId()>m_nbPersonnages)
                    fichier << personnage->getPointsDeVie() << " " << static_cast<Familier*>(personnage)->getExperience() << " " << personnage->getNiveau() << " ";
            }
            else
                fichier << "-1 ";
        }

        // Infos quetes ?

        fichier.close();
    }
    setDateSauvegarde(numFichier);
    return true;
}

bool Jeu::charger(int numFichier) {
    if(numFichier == -1) {  // Nouvelle partie
        chargerNiveau(0, 150, 150);
        return true;
    }
    std::string nomFichier = "data/sauvegardes/sauvegarde";
    std::string extension = ".txt";
    nomFichier = nomFichier + std::to_string(numFichier) + extension;
    std::ifstream fichier(nomFichier.c_str());
    std::string ligne;
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"Jeu::charger : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier.is_open());
                return false;
            }
        }
    }

    if(fichier.is_open()) {
        // On ne se preoccupe pas de la date
        getline(fichier, ligne);
        if(ligne[0] == '0') {
            return false;      // Fichier vide
        }

        else {      // Fichier avec sauvegarde
            // passer le tutoriel
            m_tutorielQueteDejaAffiche=true;
            m_tutorielRefusQueteDejaAffiche=true;
            m_tutorielRecueilDejaAffiche=true;
            m_tutorielInventaireDejaAffiche=true;
            m_debloquerAffichageCartes=true;
            m_debloquerAffichageRecueil=true;
            m_debloquerAffichageInventaire=true;
            m_indiceScenario=14;
            m_tutorielCombatDejaAffiche=true;

            int positionX, positionY, Or, PV, XP, niveauJoueur;
            unsigned int nbObjet;

            // Coffres
            for (unsigned int i=0; i<m_tableauCoffres.size(); i++) {
                for (unsigned int j=0; j<m_tableauCoffres[i].size(); j++) {
                    bool ouvert;
                    fichier >> ouvert;
                    Coffre* coffre=m_tableauCoffres[i][j];
                    if (coffre->getOuvert()!=ouvert)
                        coffre->setOuvert();
                    fichier >> Or;
                    Inventaire* inventaire=coffre->getButin();
                    inventaire->changeOr(-inventaire->getOr()+Or);
                    for (unsigned int k=0; k<inventaire->getLimite(); k++) {
                        Objet* objet=inventaire->getObjet(k);
                        inventaire->retirerObjet(k);
                        delete objet;
                    }
                    fichier >> nbObjet;
                    int id;
                    for (unsigned int k=0; k<nbObjet; k++) {
                        fichier >> id;
                        if (id!=-1) {
                            std::string type=m_tableauObjets[id]->getTypeObjet();
                            if (type=="Mat\u00E9riau") {
                                unsigned int nbExemplaire;
                                fichier >> nbExemplaire;
                                Materiau* m=static_cast<Materiau*>(m_tableauObjets[id]);
                                Materiau* materiau=new Materiau(m);
                                materiau->modifierNombreExemplaires(-materiau->getNombreExemplaires()+nbExemplaire);
                                inventaire->ajouterObjet(materiau);
                            }
                            else if (type=="\u00C9quipement") {
                                Equipement* e=static_cast<Equipement*>(m_tableauObjets[id]);
                                Equipement* equipement=new Equipement(e);
                                inventaire->ajouterObjet(equipement);
                            }
                            else if (type=="Consommable") {
                                Consommable* c=static_cast<Consommable*>(m_tableauObjets[id]);
                                Consommable* consommable=new Consommable(c);
                                inventaire->ajouterObjet(consommable);
                            }
                            else if (type=="Sp\u00E9cial") {
                                ObjetSpecial* s=static_cast<ObjetSpecial*>(m_tableauObjets[id]);
                                ObjetSpecial* special=new ObjetSpecial(s);
                                inventaire->ajouterObjet(special);
                            }
                        }
                        else
                            inventaire->ajouterObjet(NULL);
                    }
                }
            }

            // Niveau
            fichier >> m_niveau;

            // Ennemis ?


            // Joueur
            fichier >> positionX >> positionY >> PV >> XP >> niveauJoueur;
            for(int i = 0 ; i < niveauJoueur - 1; i++) {
                m_joueur->augmenteNiveau();
            }
            m_joueur->changePointsDeVie(-(m_joueur->getPointsDeVieMax() - PV));
            m_joueur->augmenteExperience(XP-m_joueur->getExperience());


            // Inventaire
            Inventaire* inventaire=m_joueur->getInventaire();
            for (unsigned int i=0; i<inventaire->getLimite(); i++) {
                Objet* objet=inventaire->getObjet(i);
                inventaire->retirerObjet(i);
                delete objet;
            }

            fichier >> Or >> nbObjet;
            inventaire->changeOr(-inventaire->getOr()+Or);
            int id;
            for (unsigned int i=0; i<nbObjet; i++) {
                fichier >> id;
                if (id!=-1) {
                    std::string type=m_tableauObjets[id]->getTypeObjet();
                    if (type=="Mat\u00E9riau") {
                        unsigned int nbExemplaire;
                        fichier >> nbExemplaire;
                        Materiau* m=static_cast<Materiau*>(m_tableauObjets[id]);
                        Materiau* materiau=new Materiau(m);
                        materiau->modifierNombreExemplaires(-materiau->getNombreExemplaires()+nbExemplaire);
                        inventaire->ajouterObjet(materiau);
                    }
                    else if (type=="\u00C9quipement") {
                        Equipement* e=static_cast<Equipement*>(m_tableauObjets[id]);
                        Equipement* equipement=new Equipement(e);
                        inventaire->ajouterObjet(equipement);
                    }
                    else if (type=="Consommable") {
                        Consommable* c=static_cast<Consommable*>(m_tableauObjets[id]);
                        Consommable* consommable=new Consommable(c);
                        inventaire->ajouterObjet(consommable);
                    }
                    else if (type=="Sp\u00E9cial") {
                        ObjetSpecial* s=static_cast<ObjetSpecial*>(m_tableauObjets[id]);
                        ObjetSpecial* special=new ObjetSpecial(s);
                        inventaire->ajouterObjet(special);
                    }
                }
            }


            // Deck
            unsigned int nbCartes;
            for (unsigned int i = 0 ; i < 75 ; i++) {
                fichier >> nbCartes;
                m_joueur->changeCarteObtenue(i,-m_joueur->getCartesObtenues()[i]+nbCartes);
            }

            fichier >> nbCartes;
            Deck* deck=m_joueur->getDeck();
            unsigned int nbCartesDeck=deck->getNombreCartes();
            for (unsigned int i=0; i<nbCartesDeck; i++)
                deck->retirerCarte(0);
            for(unsigned int i = 0 ; i < nbCartes ; i++) {
                unsigned int idCarte;
                fichier >> idCarte;
                deck->ajouterCarte(m_tableauCartes.getCarte(idCarte));
            }

            // Equipe
            for (unsigned int i=0; i<3; i++) {
                int id;
                fichier >> id;
                if (id==-1)
                    m_equipe[i]=NULL;
                else {
                    if ((unsigned int)id<m_nbPersonnages)
                        m_equipe[i]=m_tableauPersonnages[i+1];
                    else {
                        m_equipe[i]=m_tableauPersonnages[i+1];
                        // on ne peut pas remettre les familiers a 0, il faudrait recree le tableau de personnages, mais c'est fait dans le constructeur uniquement... encore quelque chose a revoir
                    }
                }
            }

            // Quetes ?

            chargerNiveau(m_niveau, positionX, positionY);
        }

        m_afficheMenu = false;
        m_menuPrincipal = false;
        fichier.close();

    }
    return true;
}

bool Jeu::estFichierVide(int numFichier) const {
    std::string nomFichier = "data/sauvegardes/sauvegarde";
    std::string extension = ".txt";
    nomFichier = nomFichier + std::to_string(numFichier) + extension;
    std::ifstream fichier(nomFichier.c_str());
    int premier;
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"Jeu::estFichierVide : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier.is_open());
                return true;    // Si le fichier s'est mal ouvert on dit qu'il etait vide
            }
        }
    }
    if(fichier.is_open()) {
        // On recupere le premier caractere (0 = fichier vide)
        fichier >> premier;
        if(premier == 0) return true;   // Le fichier est vide
        else return false;
        fichier.close();
    }
    return true;    // Si le fichier s'est mal ouvert on dit qu'il etait vide
}

bool Jeu::setDateSauvegarde(int numFichier) {   // renvoie false si echec
    std::string nomFichier = "data/sauvegardes/sauvegarde";
    std::string extension = ".txt";
    nomFichier = nomFichier + std::to_string(numFichier) + extension;
    std::ifstream fichier(nomFichier.c_str());
    int jour, mois, annee, heure, minutes;
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"Jeu::setDateSauvegarde : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier.is_open());
                return false;
            }
        }
    }
    if(fichier.is_open()) {
        // On recupere le premier caractere (0 = fichier vide)
        fichier >> jour;
        if(jour == 0) {
            m_datesSauvegardes[numFichier] = "0";
            return true;
        }
        else {
            fichier >> mois >> annee >> heure >> minutes;
            std::string date = "Sauvegard\u00E9e le ";
            date += std::to_string(jour) + "/";
            if(mois+1 < 10) date += "0";
            date += std::to_string(mois+1) + "/" + std::to_string(1900 + annee);
            date += " \u00E0 ";
            if(heure < 10) date += "0";
            date += std::to_string(heure) + "h";
            if(minutes < 10) date += "0";
            date += std::to_string(minutes);
            m_datesSauvegardes[numFichier] = date;
            return true;
        }
        fichier.close();
        return true;
    }
    return false;
}

std::string Jeu::getDateSauvegarde(int numSauvegarde) const {
    return m_datesSauvegardes[numSauvegarde];
}

void Jeu::chargerEntites() {
    // On charge tous les coffres et pnj de tous les niveaux
    for(unsigned int i = 0 ; i < m_niveauNom.size() ; i++) {   // Nombre de niveaux
        // On charge le terrain
        std::string nomFichier = "data/niveaux/";
        nomFichier += m_niveauNom[i] + ".txt";
        m_terrain->setTerrainDepuisFichier(nomFichier);
        // On charge le nom du terrain
        m_terrainNom[i]=m_terrain->getNomRegion();
        m_terrain->setIdRegion(m_niveauIdRegion[i]);
        std::ifstream fichier(nomFichier.c_str());
        if(!fichier.is_open()) {
            std::string parent = std::string("../") + nomFichier;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                parent = std::string("../") + parent;
                fichier.open(parent.c_str());
                if(!fichier.is_open()) {
                    std::cout<<"Jeu::chargerCoffres : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                    assert(fichier.is_open());
                }
            }
        }
        if (fichier.is_open()) {
            int x, y;
            unsigned int nbPNJ, nbPointApparition, nbDialogue, nbEnnemi, nbPhrase, id;
            bool booleen;
            std::string phrase;
            for (unsigned int j=0; j<m_terrain->getTailleY()+3; j++)  // on saute les lignes d'initialisation du terrain
            {
                getline(fichier,phrase);
            }
            // on initialise tous les PNJ du jeu
            fichier >> nbPNJ;
            for (unsigned int j=0; j<nbPNJ; j++) {
                getline(fichier,phrase);
                fichier >> id;
                bool pnjDejaInitialise=false;
                // On parcourt tous les PNJ du jeu
                for (unsigned int indiceNiveau=0; indiceNiveau<m_tableauPNJ.size(); indiceNiveau++) {
                    for (unsigned int indicePNJ=0; indicePNJ<m_tableauPNJ[indiceNiveau].size(); indicePNJ++) {
                        if (m_tableauPNJ[indiceNiveau][indicePNJ]->getIdPNJ()==id) {  // si le PNJ existe deja (grace a son id) on se contente d'ajouter son pointeur au tableau
                            pnjDejaInitialise=true;
                            m_tableauPNJ[i].push_back(m_tableauPNJ[indiceNiveau][indicePNJ]);
                            indiceNiveau=m_tableauPNJ.size()-1;  // on quitte la double boucle
                            indicePNJ=m_tableauPNJ[indiceNiveau].size()-1;
                        }
                    }
                }
                getline(fichier,phrase); // passer a la ligne suivant
                getline(fichier,phrase); // On recupere le nom : cela permettra d'afficher le nom du pnj qu'on doit aller voir pour une quete. On considere donc que le nom est toujours le meme pour un meme pnj
                if (!pnjDejaInitialise) { // sinon, on cree un nouveau PNJ en indiquant seulement son ID. Les autres informations seront chargees lors de chargerNiveau()
                    PNJ* pnj=new PNJ(0,0,id,phrase);
                    m_tableauPNJ[i].push_back(pnj);
                    m_tableauPNJJeu.push_back(pnj);
                }
                fichier >> x >> x;
                fichier >> booleen >> nbDialogue;
                if (booleen) {
                    for (unsigned int k=0; k<nbDialogue; k++) {
                        fichier >> nbPhrase;
                        getline(fichier,phrase);  // sauter une ligne
                        for (unsigned int l=0; l<nbPhrase; l++) {
                            getline(fichier,phrase);
                        }
                    }
                }
            }
            fichier >> nbPointApparition;
            for (unsigned int j=0; j<nbPointApparition; j++) {  // on saute les lignes d'initialisation des points d'apparition
                fichier >> x >> x;
                fichier >> x >> x >> nbEnnemi;
                for (unsigned int k=0; k<nbEnnemi; k++) {
                    fichier >> x;
                    getline(fichier, phrase); // sauter une ligne
                    getline(fichier, phrase); // recuperation du nom
                    fichier >> x >> x >> x >> x >> x;
                }
            }
            if (m_terrain->getIdRegion()==i) {  // N'est pas une version d'un autre niveau
                unsigned int nbCoffre, nbObjets, nbExemplaire;
                fichier >> nbCoffre;
                for (unsigned int j=0; j<nbCoffre; j++) {  // Initialisation des coffres
                    fichier >> x >> y;
                    Coffre* coffre=new Coffre(x,y);
                    Inventaire* inventaire=coffre->getButin();
                    fichier >> nbObjets;
                    for (unsigned int k=0; k<nbObjets; k++) {
                        fichier >> id;
                        std::string type=m_tableauObjets[id]->getTypeObjet();
                        if (type=="Mat\u00E9riau") {
                            fichier >> nbExemplaire;
                            Materiau* m=static_cast<Materiau*>(m_tableauObjets[id]);
                            Materiau* materiau=new Materiau(m);
                            materiau->modifierNombreExemplaires(-materiau->getNombreExemplaires()+nbExemplaire);
                            inventaire->ajouterObjet(materiau);
                        }
                        else if (type=="\u00C9quipement") {
                            Equipement* e=static_cast<Equipement*>(m_tableauObjets[id]);
                            Equipement* equipement=new Equipement(e);
                            inventaire->ajouterObjet(equipement);
                        }
                        else if (type=="Consommable") {
                            Consommable* c=static_cast<Consommable*>(m_tableauObjets[id]);
                            Consommable* consommable=new Consommable(c);
                            inventaire->ajouterObjet(consommable);
                        }
                        else if (type=="Sp\u00E9cial") {
                            ObjetSpecial* s=static_cast<ObjetSpecial*>(m_tableauObjets[id]);
                            ObjetSpecial* special=new ObjetSpecial(s);
                            inventaire->ajouterObjet(special);
                        }
                    }
                    m_tableauCoffres[i].push_back(coffre);
                    m_tableauCoffresJeu.push_back(coffre);
                }
            }
            fichier.close();
        }
        if (m_terrain->getIdRegion()!=i)  // Est une version d'un autre niveau
            m_tableauCoffres[i]=m_tableauCoffres[m_terrain->getIdRegion()];
    }
}

void Jeu::positionnerEquipe(int posX, int posY) {
    // On place le joueur
    int x = posX - m_joueur->getPosition().x;
    int y = posY - m_joueur->getPosition().y;
    m_joueur->deplacerPersonnage(x, y);
    // On place les familiers et on efface les derniers mouvements
    for (unsigned int i=0; i<3; i++) {
        if (m_equipe[i]!=NULL)
        {
            int x = posX - m_equipe[i]->getPosition().x;
            int y = posY - m_equipe[i]->getPosition().y;
            m_equipe[i]->deplacerPersonnage(x, y);
        }
    }
    for (unsigned int i=0; i<40; i++) {
        m_dernierDeplacement[i].x=0;
        m_dernierDeplacement[i].y=0;
    }
}

void Jeu::chargerNiveau(unsigned int niveau, int posX, int posY) {//tppf
    m_niveau=niveau;

    // On charge le terrain
    std::string nomFichier = "data/niveaux/";
    nomFichier += m_niveauNom[niveau] + ".txt";
    m_terrain->setTerrainDepuisFichier(nomFichier);
    if (m_grille!=NULL)
        delete static_cast<Grid *>(m_grille);
    m_grille = new Grid (this);
    if (m_pathFinding!=NULL)
        delete static_cast<PathFinding*>(m_pathFinding);
    m_pathFinding= new PathFinding(m_joueur, static_cast<Grid *>(m_grille));
    m_terrain->setIdRegion(m_niveauIdRegion[niveau]);

    positionnerEquipe(posX, posY);

    // On charge le niveau depuis un fichier (joueur, PNJ, dialogues, points de sorties)
    std::ifstream fichier(nomFichier.c_str());
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"Jeu::chargerNiveau : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier.is_open());
            }
        }
    }
    if (fichier.is_open()) {
        int x, y;
        unsigned int nbPnj, id, nbDialogue, nbPhrase;
        bool aDialogue;
        std::string nom, phrase;
        for (unsigned int i=0; i<m_terrain->getTailleY()+3; i++)  // on saute les lignes d'initialisation du terrain
        {
            getline(fichier,phrase);
        }
        fichier >> nbPnj;
        for (unsigned int i=0; i<nbPnj; i++) {
            PNJ* pnj=m_tableauPNJ[m_niveau][i];
            getline(fichier,nom);  // sauter une ligne
            fichier >> id;
            getline(fichier,nom);  // passer a la ligne suivante
            getline(fichier,nom);  // recuperation nom
            fichier >> x >> y;
            while (pnj->getADialogue()) { // on retire tous les dialogues du pnj
                pnj->retirerDialogue(0);
                pnj->reduireIndiceDialogue();
            }
            pnj->setNom(nom);
            pnj->setPosition(x,y);
            fichier >> aDialogue >> nbDialogue;
            if (aDialogue) {
                for (unsigned int j=0; j<nbDialogue; j++) {
                    Dialogue* d=new Dialogue();
                    fichier >> nbPhrase;
                    getline(fichier,phrase);  // sauter une ligne
                    for (unsigned int k=0; k<nbPhrase; k++) {
                        getline(fichier,phrase);
                        d->ajouterPhrase(k,phrase);
                    }
                    pnj->ajouterDialogue(j,d);
                }
            }
            m_tableauPNJ[m_niveau].push_back(pnj);
        }
        std::vector<PointApparition*>().swap(m_tableauPointsApparitions);
        std::vector<Ennemi*>().swap(m_tableauEnnemis);
        std::vector<Ennemi*>().swap(m_tableauTombes);
        std::vector<Inventaire*>().swap(m_tableauInventairesTombes);
        std::vector<unsigned int>().swap(m_tableauOrientation);
        std::vector<bool>().swap(m_tableauChasse);
        unsigned int nbPoint, temps, nbEnnemi, pv, mana, att, def, exp;
        bool garde;
        fichier >> nbPoint;
        for (unsigned int i=0; i<nbPoint; i++) {
            fichier >> x >> y;
            PointApparition* point=new PointApparition(x,y);
            fichier >> garde >> temps >> nbEnnemi;
            point->setGarde(garde);
            point->setTempsReapparition(temps);
            for (unsigned int j=0; j<nbEnnemi; j++) {
                fichier >> id;
                getline(fichier, nom); // sauter une ligne
                getline(fichier, nom); // recuperation du nom
                fichier >> pv >> mana >> att >> def >> exp;
                Ennemi* e = new Ennemi(x,y,m_tableauEspeces[id],pv,mana,att,def,exp);
                e->setNom(nom);
                point->ajouterEnnemi(e);
                m_tableauOrientation.push_back(0);
                m_tableauChasse.push_back(false);
            }
            m_tableauPointsApparitions.push_back(point);
            m_tableauEnnemis.push_back(NULL);
            m_tableauTombes.push_back(NULL);
            m_tableauInventairesTombes.push_back(NULL);
        }
        fichier.close();
    }
}

void Jeu::chargerMonde(std::string monde) {
    std::string nomFichier = "data/niveaux/";
    nomFichier = nomFichier + monde + ".txt";
    std::ifstream fichier(nomFichier.c_str());
    if(!fichier.is_open()) {
        std::string parent = std::string("../") + nomFichier;
        fichier.open(parent.c_str());
        if(!fichier.is_open()) {
            parent = std::string("../") + parent;
            fichier.open(parent.c_str());
            if(!fichier.is_open()) {
                std::cout<<"Jeu::chargerMonde : Erreur dans l'ouverture du fichier " << nomFichier <<std::endl;
                assert(fichier.is_open());
            }
        }
    }
    if (fichier.is_open()) {
        unsigned int nb, version, nbSorties;
        fichier >> nb;
        std::string nom;
        Point p, p2;
        for (unsigned int i=0; i<nb; i++) {
            fichier >> nom >> version >> p.x >> p.y >> p2.x >> p2.y >> nbSorties;
            m_niveauNom.push_back(nom);
            m_niveauIdRegion.push_back(version);
            std::vector<Point> sorties, destinations;
            Point sortie, destination;
            for (unsigned int j=0; j<nbSorties; j++) {
                fichier >> sortie.x >> sortie.y >> destination.x >> destination.y;
                sorties.push_back(sortie);
                destinations.push_back(destination);
            }
            m_niveauSorties.push_back(sorties);
            m_niveauDestinationsSorties.push_back(destinations);
            m_niveauTaille.push_back(p2);
            std::vector<Coffre*> coffres;
            m_tableauCoffres.push_back(coffres);
            std::vector<PNJ*> pnj;
            m_tableauPNJ.push_back(pnj);
            std::string nomTerrain="";
            m_terrainNom.push_back(nomTerrain);
        }
        fichier.close();
    }
}

unsigned int Jeu::getHauteurCollisionDecors(unsigned int i) {
    return m_collisionsDecors[i][1];
}

void Jeu::getCollisionsDecors(unsigned int i, unsigned int collisions[4]) const {
    for (unsigned int j=0; j<4; j++)
        collisions[j]=m_collisionsDecors[i][j];
}

void Jeu::gestionEnnemis(int valeurDeplacement) {
    Ennemi* ennemi=NULL;

    for (unsigned int i=0; i<m_tableauPointsApparitions.size(); i++) {
        ennemi=m_tableauPointsApparitions[i]->apparition();
        if (ennemi!=NULL) {
            // Ajoute le deck de l'ennemi
            Deck* deckEnnemi = new Deck;
            for (unsigned int j = 0; j < deckEnnemi->getNombreCartesMax(); j++)
            {
                // Selection aleatoire entre les 5 cartes possible pour chaque espece
                unsigned int idCarte=ennemi->getEspece()->getIdEspece()*5 + rand()%5;
                deckEnnemi->ajouterCarte(m_tableauCartes.getCarte(idCarte));
            }
            ennemi->setDeck(deckEnnemi);
            ennemi->deplacerPersonnage(-ennemi->getPosition().x+m_tableauPointsApparitions[i]->getPosition().x*m_tailleCase,-ennemi->getPosition().y+m_tableauPointsApparitions[i]->getPosition().y*m_tailleCase);
            if (m_interactionPossible==3 && m_interactionIndiceObjet==i) {
                actionClavier('e',0);
                m_interactionPossible=0;
                collisionObjet(m_joueur);
            }
            m_tableauEnnemis[i]=ennemi;
            m_tableauTombes[i]=NULL;
        }
    }

    for (unsigned int i=0; i<m_tableauEnnemis.size(); i++) {
        if (m_tableauEnnemis[i]!=NULL && !m_tableauPointsApparitions[i]->getGarde()) {
            ennemi=m_tableauEnnemis[i];
            float visionX = cos(m_tableauOrientation[i]*M_PI/180);
            float visionY = sin(m_tableauOrientation[i]*M_PI/180);
            float distanceEnnemiJoueur = sqrt(pow(m_joueur->getPosition().x - ennemi->getPosition().x, 2.0) +
                               pow(m_joueur->getPosition().y - ennemi->getPosition().y, 2.0)); //distance du joueur

            Point vJoueur;
                vJoueur.x = m_joueur->getPosition().x - ennemi->getPosition().x; //direction du joueur
                vJoueur.y = m_joueur->getPosition().y - ennemi->getPosition().y;
            if (distanceEnnemiJoueur < CHAMPDEVISION)
                m_tableauChasse[i]=true;
            else if (distanceEnnemiJoueur>3*CHAMPDEVISION)
                m_tableauChasse[i]=false;
            if (m_tableauChasse[i]) {
                unsigned int nbChasse=0;
                unsigned int numeroFormation=0;
                for (unsigned int j=0; j<m_tableauChasse.size(); j++) {
                    if (m_tableauChasse[j]) {
                        nbChasse++;
                        if (i<j)
                            numeroFormation++;
                    }
                }
                Point posJoueur=m_joueur->getPosition();
                unsigned int collisionJoueur[4];
                m_joueur->getCollisions(collisionJoueur);
                posJoueur.x+=collisionJoueur[0];
                posJoueur.y+=collisionJoueur[1];
                Point destination=posJoueur;
                Point posEnnemi=ennemi->getPosition();
                double tempsJoueur=0;
                double tempsEnnemi=0;
                unsigned int collisionEnnemi[4];
                ennemi->getCollisions(collisionEnnemi);
                Point debutCollision{posEnnemi.x+(int)collisionEnnemi[0],posEnnemi.y+(int)collisionEnnemi[1]};
                Point finCollision{debutCollision.x+(int)collisionEnnemi[2], debutCollision.y+(int)collisionEnnemi[3]};
                Grid* grille=static_cast<Grid*>(m_grille);
                bool continuerAnticipation=true;
                bool formation=false;
                if (nbChasse>1)
                    formation=true;
                if ((vJoueur.x*visionX + vJoueur.y*visionY)
                    / (sqrt(pow(vJoueur.x, 2) + pow(vJoueur.y, 2))
                    * (sqrt(pow(visionX, 2) + pow(visionY, 2))))> 0) {   // verification que le joueur est dans le champ de vision de 180�

                    m_tableauOrientation[i]=(std::acos((vJoueur.x) / (sqrt(pow(vJoueur.x, 2) + pow(vJoueur.y, 2)))) * 180 / M_PI);
                    if (vJoueur.y<0) {
                        m_tableauOrientation[i]= 360 - m_tableauOrientation[i];
                    }
                    Point ancienneDestination;
                    if ((m_dernierDeplacement[0].x != 0 || m_dernierDeplacement[0].y!=0) && distanceEnnemiJoueur > CHAMPDEVISION) {
                        Point direction;
                        bool continuerFormation=true;
                        if (nbChasse==2) {
                            direction.y=0;
                            if (numeroFormation==0) {
                                direction.x=-1000;
                            }
                            else {
                                direction.x=1000;
                            }
                        }
                        else if (nbChasse==3) {
                            if (numeroFormation==0) {
                                direction.x=0;
                                direction.y=-1000;
                            }
                            else if (numeroFormation==1) {
                                direction.x=1000;
                                direction.y=500;
                            }
                            else {
                                direction.x=-1000;
                                direction.y=500;
                            }
                        }
                        else if (nbChasse==4) {
                            if (numeroFormation==0) {
                                direction.x=0;
                                direction.y=-1000;
                            }
                            else if (numeroFormation==1) {
                                direction.x=1000;
                                direction.y=0;
                            }
                            else if (numeroFormation==2) {
                                direction.x=0;
                                direction.y=1000;
                            }
                            else {
                                direction.x=-1000;
                                direction.y=0;
                            }
                        }
                        else if (nbChasse==5) {
                            if (numeroFormation==0) {
                                direction.x=0;
                                direction.y=-1000;
                            }
                            else if (numeroFormation==1) {
                                direction.x=951;
                                direction.y=-309;
                            }
                            else if (numeroFormation==2) {
                                direction.x=615;
                                direction.y=788;
                            }
                            else if (numeroFormation==3) {
                                direction.x=-615;
                                direction.y=788;
                            }
                            else {
                                direction.x=-951;
                                direction.y=-309;
                            }
                        }
                        if (formation) {
                            float distanceDestinationJoueur=0;
                            do {
                                ancienneDestination=destination;
                                destination.x += direction.x;
                                destination.y += direction.y;
                                if (!grille->deplacementPossibleEnnemi(ennemi, destination)) {
                                    destination=ancienneDestination;
                                    continuerFormation=false;
                                }
                                distanceDestinationJoueur = sqrt(pow(m_joueur->getPosition().x - destination.x, 2.0) + pow(m_joueur->getPosition().y - destination.y, 2.0));
                            } while (distanceDestinationJoueur<CHAMPDEVISION && continuerFormation);
                            if (distanceDestinationJoueur<CHAMPDEVISION/2)
                                formation=false;
                        }
                        if (!formation) {
                            destination=posJoueur;
                            do {
                                ancienneDestination=destination;
                                destination.x += m_dernierDeplacement[0].x;
                                destination.y += m_dernierDeplacement[0].y;

                                // on verifie si les nodes de destination du joueur sont marchables (plus simple qu'avec estDeplacementPossible car on n'a pas besoin d'indiquer la touche de deplacement)
                                for (int k=grille->getNodePointTerrain(destination)->m_gridX; k<grille->getTailleX() && k<grille->getNodePointTerrain({destination.x+(int)collisionJoueur[2], destination.y})->m_gridX; k++) {
                                    for (int j=grille->getNodePointTerrain(destination)->m_gridY; j<grille->getTailleY() && j<grille->getNodePointTerrain({destination.x, destination.y+(int)collisionJoueur[3]})->m_gridY; j++) {
                                        if (!grille->getNode(k,j)->m_walkable) {
                                            destination=ancienneDestination;  // on recupere la destination precedente, vu que la suivante n'est pas accessible, et on quitte la boucle
                                            continuerAnticipation=false;
                                        }
                                    }
                                }

                                // on verifie ensuite si l'ennemi peut acceder au joueur a cette destination
                                if (!grille->deplacementPossibleEnnemi(ennemi, destination)) { // en prenant une zone plus large mais mettant en contact leurs zones de collision
                                    if (!grille->adaptationDestinationEnnemi(ennemi, destination, m_joueur)) {
                                         m_tableauChasse[i]=false;
                                    }
                                    continuerAnticipation=false;
                                }
                                tempsJoueur=std::sqrt(std::pow(destination.x-posJoueur.x, 2)+std::pow(destination.y-posJoueur.y, 2))/m_joueur->getVitesse();
                                tempsEnnemi=std::sqrt(std::pow(destination.x-posEnnemi.x, 2)+std::pow(destination.y-posEnnemi.y, 2))/ennemi->getVitesse();
                            } while (continuerAnticipation && tempsJoueur<=tempsEnnemi);  // on boucle tant qu'on prend plus de temps que le joueur, qu'on n'a pas abandonne la chasse ou que le joueur est tombe sur un obstacle
                        }
                    }
                    if (m_tableauChasse[i]) { // si chasse alors on se deplace vers destination
                        if (static_cast<PathFinding*>(m_pathFinding)->findPath(debutCollision, finCollision, destination)) {
                            Point vdeplacement = static_cast<PathFinding *>(m_pathFinding)->vecteurDeplacement(valeurDeplacement, debutCollision);
                            if (vdeplacement.x+vdeplacement.y != 0)
								ennemi->deplacerPersonnage((float)(valeurDeplacement * ennemi->getVitesse() / 100) * (float)(vdeplacement.x)  / (float)((abs(vdeplacement.x)+abs(vdeplacement.y))), //  composante x du mouvement a faire (x / (x+y))
                                                           (float)(valeurDeplacement * ennemi->getVitesse() / 100) * (float)(vdeplacement.y)  / (float)((abs(vdeplacement.x)+abs(vdeplacement.y)))); // composante y du mouvement a faire (y / (x+y))
                        }
                        else
                            m_tableauChasse[i]=false;
                    }
                }
            }
        }
    }
    collisionObjet(m_joueur);
}

unsigned int Jeu::getChampDeVision(unsigned int i) {
    return m_tableauOrientation[i];
}

void Jeu::supprimerEnnemi(unsigned int i) {
    // Creation de l'inventaire de l'ennemi mort
    Inventaire* butin=new Inventaire();
    Espece* especeEnnemiMort=m_tableauEnnemis[i]->getEspece();
    for (unsigned int indiceObjet=0; indiceObjet<especeEnnemiMort->getTableauButins().size(); indiceObjet++) {  // on parcourt la liste des butins
        if (rand()%100<especeEnnemiMort->getChanceButin()[indiceObjet]) {  // on fait un tirage aleatoire grace aux probabilites d'obtenir ce butin
            butin->ajouterObjet(especeEnnemiMort->getTableauButins()[indiceObjet]);
        }
    }
    if (butin->getNbObjet()>0) { // On ajoute une tombe uniquement si il y a quelque chose a recuperer
        m_tableauInventairesTombes[i]=butin;
        m_tableauTombes[i]=m_tableauEnnemis[i];
    }
    m_tableauEnnemis[i]=NULL;
    m_tableauChasse[i]=false;
    m_tableauPointsApparitions[i]->defaite();
    collisionObjet(m_joueur);
}

Personnage* Jeu::getEquipier(unsigned int i) {
    if (i<3)
        return m_equipe[i];
    return NULL;
}

void Jeu::recupererObjet(unsigned int i, unsigned int origineObjet) {
    Objet* objet;
    Coffre* coffre;
    Inventaire* tombe;
    if (origineObjet==0) { // l'objet vient d'un coffre
        coffre=m_tableauCoffres[m_niveau][m_interactionIndiceObjet];
        objet=coffre->getObjet(i);
    }
    else if (origineObjet==1) {  // tombe
        tombe=m_tableauInventairesTombes[m_interactionIndiceObjet];
        objet=tombe->getObjet(i);std::cout<<objet->getNom();
    }
    Inventaire* inventaire=m_joueur->getInventaire();std::cout<<inventaire->getNbObjet()<<" "<<inventaire->getLimite();
    if (objet!=NULL && inventaire->getNbObjet()<inventaire->getLimite()) {std::cout<<"ça serait ok";
        inventaire->ajouterObjet(objet);
        if (origineObjet==0) // l'objet vient d'un coffre
            coffre->retirerObjet(i);
        else if (origineObjet==1)  // tombe
            tombe->retirerObjet(i);
        // gestion des quetes, verification pour les quetes de collecte
        gestionObjetQuete(objet, true);
    }
}

Combat* Jeu::getCombat() const {
    return m_combat;
}

void Jeu::lanceCombat(Ennemi* ennemis[5], unsigned int vitesses[5]) {
    if (m_debloquerAffichageCartes) {
        if (m_niveau==4 && ennemis[0]->getEspece()==m_tableauEspeces[10] && m_tableauQuetes[4]->getEtape(0)->getEtat()!='f') {
            m_afficheTutoriel=true;
            m_fenetreOuverte=true;
            m_afficheTutorielLourdeau=true;
        }
        else {
            m_combat=new Combat(m_joueur, m_equipe, ennemis, vitesses);
            m_afficheCombat=true;
            m_fenetreOuverte=true;
        }
    }
    else {
        delete m_dialogueNarrateur;
        m_dialogueNarrateur=new Dialogue("data/dialogues/DialogueDefaite.txt", true);
        m_lanceMsqScenario=true;
        m_afficheDialogue=true;
        m_scenarioNarrateur=true;
        m_scenario=true;
        m_afficheDefaite=true;
    }
}

void Jeu::finirCombat(bool victoire) {
    // On finir le combat
    Carte* carteGagnee = m_combat->finirCombat();
    if(victoire) {
        // On recupere la carte gagnee
        m_idCarteObtenue = carteGagnee->getIdCarte();
        // On rajoute cette carte aux cartes obtenues si le joueur en a moins de 3 exemplaires
        if (m_joueur->getCartesObtenues()[m_idCarteObtenue]<3)
        {
            m_joueur->changeCarteReserve(m_idCarteObtenue, 1);
            m_joueur->changeCarteObtenue(m_idCarteObtenue, 1);
            if (m_joueur->getCartesObtenues()[m_idCarteObtenue]==3)
            {
                unsigned int i=0;
                bool obtenable=false;
                while (!obtenable || i<74)
                {
                    if (m_joueur->getCartesObtenues()[i]<3)
                        obtenable=true;
                    i++;
                }
                m_joueur->changeCarteObtenable(m_idCarteObtenue);
            }
        }
        else m_idCarteObtenue=-1;
        // On prend les degats
        unsigned int pv = m_combat->getPointsDeViePersonnage(0);
        m_joueur->changePointsDeVie(pv - m_joueur->getPointsDeVie());
        for (unsigned int i=0; i<m_combat->getNbEquipier(); i++) {
            pv=m_combat->getPointsDeViePersonnage(i+1);
            m_equipe[i]->changePointsDeVie(pv - m_equipe[i]->getPointsDeVie());
        }
        // On recupere l'XP
        m_gainExperience = m_combat->getGainExperience();
        unsigned int diviseur=1;
        for (unsigned int i=0; i<m_combat->getNbEquipier(); i++) {
            if (m_equipe[i]->getId()>m_nbPersonnages)
                diviseur++;
        }
        for (unsigned int i=0; i<4; i++)
            gainExperienceJoueurFamilier(i,m_gainExperience/diviseur);
        // On supprime les ennemis
        for (unsigned int i=0; i<m_combat->getNbEnnemi(); i++) {
            for (unsigned int j=0; j<m_tableauEnnemis.size(); j++) {
                if (m_combat->getEnnemi(i)==m_tableauEnnemis[j])
                    supprimerEnnemi(j);
            }
            // On actualise les quetes pour les quetes de massacre
            gestionEnnemisQuete(m_combat->getEnnemi(i));
        }
        // On affiche le menu de victoire
        m_afficheVictoire = true;
    }
    else {
        m_ligneSelectionnee = 0;
        m_afficheDefaite = true;
        m_idCarteObtenue = -1;
        delete m_dialogueNarrateur;
        m_dialogueNarrateur=new Dialogue("data/dialogues/DialogueDefaite.txt", true);
        m_lanceMsqScenario=true;
        m_afficheDialogue=true;
        m_scenarioNarrateur=true;
        m_scenario=true;
    }
    m_fenetreOuverte = true;
    delete m_combat;
    m_combat=NULL;
}

Personnage* Jeu::getPersonnage(unsigned int i) {
    if (i<m_tableauPersonnages.size())
        return m_tableauPersonnages[i];
    return NULL;
}

unsigned int Jeu::getNbPersonnages() const {
    return m_nbPersonnages;
}

unsigned int Jeu::getNbFamiliers() const {
    return m_nbFamiliers;
}

unsigned int Jeu::getAncienNiveauPersonnage(unsigned int i) const {
    return m_ancienNiveauPersonnage[i];
}

unsigned int Jeu::getAncienPvPersonnage(unsigned int i) const {
    return m_ancienPvPersonnage[i];
}

unsigned int Jeu::getAncienManaPersonnage(unsigned int i) const {
    return m_ancienManaPersonnage[i];
}

unsigned int Jeu::getAncienneAttaquePersonnage(unsigned int i) const {
    return m_ancienneAttaquePersonnage[i];
}

unsigned int Jeu::getAncienneDefensePersonnage(unsigned int i) const {
    return m_ancienneDefensePersonnage[i];
}

unsigned int Jeu::getGainExperience() const {
    return m_gainExperience;
}

int Jeu::getIdCarteObtenue() const {
    return m_idCarteObtenue;
}

bool Jeu::getMonteeNiveau(unsigned int i) const {
    return m_monteeNiveau[i];
}

Quete* Jeu::initialiserQuete(std::vector<std::string> listeQuetes, unsigned int &i) {
    std::string typeQuete=listeQuetes[i];
    std::string nomQuete=listeQuetes[++i];
    unsigned int nbEtapes=std::stoi(listeQuetes[++i]);
    std::vector<Quete*> etapes;
    unsigned int gainExperience=std::stoi(listeQuetes[++i]);
    unsigned int gainOr=std::stoi(listeQuetes[++i]);
    unsigned int nbObjet=std::stoi(listeQuetes[++i]);
    Inventaire* recompenses=new Inventaire();
    recompenses->changeOr(gainOr);
    for (unsigned int k=0; k<nbObjet; k++) {
        unsigned int idObjet=std::stoi(listeQuetes[++i]);
        Objet* objet=m_tableauObjets[idObjet];
        std::string typeObjet=m_tableauObjets[idObjet]->getTypeObjet();
        if (typeObjet=="Mat\u00E9riau") {
            unsigned int nbExemplaire=std::stoi(listeQuetes[++i]);
            Materiau* m=static_cast<Materiau*>(objet);
            Materiau* materiau=new Materiau(m);
            materiau->modifierNombreExemplaires(-materiau->getNombreExemplaires()+nbExemplaire);
            recompenses->ajouterObjet(materiau);
        }
        else if (typeObjet=="\u00C9quipement") {
            Equipement* e=static_cast<Equipement*>(objet);
            Equipement* equipement=new Equipement(e);
            recompenses->ajouterObjet(equipement);
        }
        else if (typeObjet=="Consommable") {
            Consommable* c=static_cast<Consommable*>(objet);
            Consommable* consommable=new Consommable(c);
            recompenses->ajouterObjet(consommable);
        }
        else if (typeObjet=="Sp\u00E9cial") {
            ObjetSpecial* s=static_cast<ObjetSpecial*>(objet);
            ObjetSpecial* objetSpecial=new ObjetSpecial(s);
            recompenses->ajouterObjet(objetSpecial);
        }
    }
    bool validation=std::stoi(listeQuetes[++i]);
    unsigned int idPnjLanceur=std::stoi(listeQuetes[++i]);
    unsigned int idNiveauLancement=std::stoi(listeQuetes[++i]);
    int idRegion=std::stoi(listeQuetes[++i]);
    PNJ* pnj;
    // on parcourt tous les pnj du jeu
    for (unsigned int indiceNiveau=0; indiceNiveau<m_tableauPNJ.size(); indiceNiveau++) {
        for (unsigned int indicePNJ=0; indicePNJ<m_tableauPNJ[indiceNiveau].size(); indicePNJ++) {
            if (m_tableauPNJ[indiceNiveau][indicePNJ]->getIdPNJ()==idPnjLanceur) {  // si le pnj a l'id qu'on recherche
                pnj=m_tableauPNJ[indiceNiveau][indicePNJ];
                indiceNiveau=m_tableauPNJ.size()-1;  // on quitte la double boucle
                indicePNJ=m_tableauPNJ[indiceNiveau].size()-1;
            }
        }
    }
    Quete* quete;
    // Parametres possibles
    unsigned int nombreDemande;
    unsigned int idObjet;
    Point destination;
    unsigned int rayon;
    PNJ* cible;
    unsigned int idEspece;
    // on recupere les parametres propres au type de quete
    if (typeQuete=="Collecte") {
        nombreDemande=std::stoi(listeQuetes[++i]);
        idObjet=std::stoi(listeQuetes[++i]);
    }
    else if (typeQuete=="D\u00E9placement") {
        destination.x=std::stoi(listeQuetes[++i]);
        destination.y=std::stoi(listeQuetes[++i]);
        rayon=std::stoi(listeQuetes[++i]);
    }
    else if (typeQuete=="Dialogue") {
        unsigned int idPnjCible=std::stoi(listeQuetes[++i]);
        // on parcourt tous les pnj du jeu
        for (unsigned int indiceNiveau=0; indiceNiveau<m_tableauPNJ.size(); indiceNiveau++) {
            for (unsigned int indicePNJ=0; indicePNJ<m_tableauPNJ[indiceNiveau].size(); indicePNJ++) {
                if (m_tableauPNJ[indiceNiveau][indicePNJ]->getIdPNJ()==idPnjCible) {  // si le pnj a l'id qu'on recherche
                    cible=m_tableauPNJ[indiceNiveau][indicePNJ];
                    indiceNiveau=m_tableauPNJ.size()-1;   // on quitte la double boucle
                    indicePNJ=m_tableauPNJ[indiceNiveau].size()-1;
                }
            }
        }
    }
    else if (typeQuete=="Massacre") {
        nombreDemande=std::stoi(listeQuetes[++i]);
        idEspece=std::stoi(listeQuetes[++i]);
    }
    // On ajoute recursivement les sous-quetes
    for (unsigned int k=0; k<nbEtapes; k++) {
        etapes.push_back(initialiserQuete(listeQuetes,++i));
    }
    // maintenant que les sous-quetes sont ajoutees a etapes, on peut creer la quete principale
    if (typeQuete=="Collecte") {
        QueteCollecte* queteCollecte=new QueteCollecte(nomQuete, etapes, gainExperience, recompenses, validation, pnj, idNiveauLancement, idRegion, nombreDemande, idObjet);
        quete=queteCollecte;
    }
    else if (typeQuete=="D\u00E9placement") {
        QueteDeplacement* queteDeplacement=new QueteDeplacement(nomQuete, etapes, gainExperience, recompenses, validation, pnj, idNiveauLancement, idRegion, destination, rayon);
        quete=queteDeplacement;
    }
    else if (typeQuete=="Dialogue") {
        QueteDialogue* queteDialogue=new QueteDialogue(nomQuete, etapes, gainExperience, recompenses, validation, pnj, idNiveauLancement, idRegion, cible);
        quete=queteDialogue;
    }
    else if (typeQuete=="Massacre") {
        QueteMassacre* queteMassacre=new QueteMassacre(nomQuete, etapes, gainExperience, recompenses, validation, pnj, idNiveauLancement, idRegion, nombreDemande, idEspece);
        quete=queteMassacre;
    }
    return quete;
}

unsigned int Jeu::getNbQuetes() const {
    return m_tableauQuetes.size();
}

Quete* Jeu::getQuete(unsigned int i) const {
    return m_tableauQuetes[i];
}

void Jeu::interactionPnjQuete() {
    for (unsigned int i=0; i<m_tableauQuetes.size(); i++) {
        if (m_tableauQuetes[i]->getEtat()=='i') {  // la quete n'a pas ete commencee (inactive)
            if (m_tableauQuetes[i]->getNiveauLancement()==m_niveau && m_tableauPNJ[m_niveau][m_interactionIndiceObjet]==m_tableauQuetes[i]->getPNJ()) { // on verifie si le PNJ a qui on veut parler est celui qui peut nous donner cette quete
                m_afficheDebutQuete=true;
                m_indiceQuete=i;
            }
        }
        else if (m_tableauQuetes[i]->getEtat()=='a' || m_tableauQuetes[i]->getEtat()=='v') {  // la quete est en cours (active ou attente de validation)
            if (m_tableauQuetes[i]->getQueteActive()->getType()=="Collecte" && m_tableauQuetes[i]->getQueteActive()->getEtat()=='v') {  // une quete de collecte a valider aupres d'un PNJ
                QueteCollecte* queteCollecte=static_cast<QueteCollecte*>(m_tableauQuetes[i]->getQueteActive());  // si c'est une quete de collecte on cast en QueteCollecte
                if (queteCollecte->getPNJ()==m_tableauPNJ[m_niveau][m_interactionIndiceObjet]) {  // On verifie que le PNJ est celui qui nous a donne la quete
                    m_afficheValidationCollecte = true;
                    m_indiceQuete=i;
                }
            }
            if (m_tableauQuetes[i]->getQueteActive()->getType()=="D\u00E9placement" && m_tableauQuetes[i]->getQueteActive()->getEtat()=='v') {
                QueteDeplacement* queteDeplacement=static_cast<QueteDeplacement*>(m_tableauQuetes[i]->getQueteActive());
                if (queteDeplacement->getPNJ()==m_tableauPNJ[m_niveau][m_interactionIndiceObjet]) {
                    queteDeplacement->changeEtat();
                    m_tableauQuetes[i]->activerQueteSuivante();
                }
            }
            else if (m_tableauQuetes[i]->getQueteActive()->getType()=="Dialogue") {
                QueteDialogue* queteDialogue=static_cast<QueteDialogue*>(m_tableauQuetes[i]->getQueteActive());
                if (queteDialogue->getEtat()=='a') {
                    if (queteDialogue->getCible()==m_tableauPNJ[m_niveau][m_interactionIndiceObjet] && (queteDialogue->getRegionCible()==-1 || (unsigned int)queteDialogue->getRegionCible()==m_terrain->getIdRegion())) {  // On verifie que le PNJ est celui auquel on doit parler
                        queteDialogue->changeEtat();
                        if (!queteDialogue->getValidation())
                            m_tableauQuetes[i]->activerQueteSuivante();
                    }
                }
                else if (queteDialogue->getEtat()=='v') {
                    if (queteDialogue->getPNJ()==m_tableauPNJ[m_niveau][m_interactionIndiceObjet]) {
                        queteDialogue->changeEtat();
                        m_tableauQuetes[i]->activerQueteSuivante();
                    }
                }
            }
            else if (m_tableauQuetes[i]->getQueteActive()->getType()=="Massacre" && m_tableauQuetes[i]->getQueteActive()->getEtat()=='v') {
                QueteMassacre* queteMassacre=static_cast<QueteMassacre*>(m_tableauQuetes[i]->getQueteActive());
                if (queteMassacre->getPNJ()==m_tableauPNJ[m_niveau][m_interactionIndiceObjet]) {
                    queteMassacre->changeEtat();
                    m_tableauQuetes[i]->activerQueteSuivante();
                }
            }
            if (m_tableauQuetes[i]->getEtat()=='f') {  // l'ensemble de la quete est finie
                m_afficheFinQuete=true;
                m_indiceFinQuete=i;
            }
        }
    }
}

void Jeu::gainExperienceJoueurFamilier(unsigned int idPersonnage, unsigned int experience) {
    if (idPersonnage==0) {
        m_ancienNiveauPersonnage[0] = m_joueur->getNiveau();
        m_ancienPvPersonnage[0] = m_joueur->getPointsDeVieMax();
        m_ancienManaPersonnage[0] = m_joueur->getPointsDeManaMax();
        m_ancienneAttaquePersonnage[0] = m_joueur->getAttaque();
        m_ancienneDefensePersonnage[0] = m_joueur->getDefense();
        m_joueur->augmenteExperience(experience);
        if (m_ancienNiveauPersonnage[0]!=m_joueur->getNiveau())
            m_monteeNiveau[0]=1;
    }
    else if (m_equipe[idPersonnage-1]!=NULL) {
        if (m_equipe[idPersonnage-1]->getId()>m_nbPersonnages) {
            Familier* familier=static_cast<Familier*>(m_equipe[idPersonnage-1]);
            m_ancienNiveauPersonnage[idPersonnage] = familier->getNiveau();
            m_ancienPvPersonnage[idPersonnage] = familier->getPointsDeVieMax();
            m_ancienManaPersonnage[idPersonnage] = familier->getPointsDeManaMax();
            m_ancienneAttaquePersonnage[idPersonnage] = familier->getAttaque();
            m_ancienneDefensePersonnage[idPersonnage] = familier->getDefense();
            familier->augmenteExperience(experience);
            if (m_ancienNiveauPersonnage[idPersonnage]!=familier->getNiveau())
                m_monteeNiveau[idPersonnage]=1;
        }
    }
}

std::string Jeu::getNomTerrain(unsigned int niveau) const {
    return m_terrainNom[niveau];
}

void Jeu::gestionObjetQuete(Objet* objet, bool ajout) {
    for (unsigned int i=0; i<m_tableauQuetes.size(); i++) {
        if (m_tableauQuetes[i]->getEtat()=='a' || m_tableauQuetes[i]->getEtat()=='v') {  // la quete est en cours (active)
            if (m_tableauQuetes[i]->getQueteActive()->getType()=="Collecte") {
                QueteCollecte* queteCollecte=static_cast<QueteCollecte*>(m_tableauQuetes[i]->getQueteActive());  // si c'est une quete de collecte on cast en QueteCollecte
                if (queteCollecte->getIdObjet()==objet->getIdObjet() && (queteCollecte->getRegionCible()==-1 || (unsigned int)queteCollecte->getRegionCible()==m_terrain->getIdRegion())) {  // On verifie que c'est l'objet qu'on doit collecter
                    std::string typeObjet=objet->getTypeObjet();
                    if (typeObjet=="Mat\u00E9riau") {
                        Materiau* materiau=static_cast<Materiau*>(objet);
                        unsigned int nbMateriau=materiau->getNombreExemplaires();
                        if (ajout)
                            queteCollecte->changerNombreObtenu(nbMateriau);
                        else if (queteCollecte->getValidation())  // on ne retire que si on doit ramener les objets (quete de collecte avec validation)
                            queteCollecte->changerNombreObtenu(-nbMateriau);
                    }
                    else {
                        if (ajout)
                            queteCollecte->changerNombreObtenu(1);
                        else if (queteCollecte->getValidation() && !queteCollecte->estCollecteFinie())
                            queteCollecte->changerNombreObtenu(-1);
                    }
                    if (queteCollecte->estCollecteFinie()) {
                        queteCollecte->changeEtat();
                        if (queteCollecte->getEtat()=='f')
                            m_tableauQuetes[i]->activerQueteSuivante();
                    }
                }
            }
            if (m_tableauQuetes[i]->getEtat()=='f') {
                m_afficheFinQuete=true;
                m_indiceFinQuete=i;
            }
        }
    }
}

void Jeu::gestionEnnemisQuete(Ennemi* ennemi) {
    for (unsigned int i=0; i<m_tableauQuetes.size(); i++) {
        if (m_tableauQuetes[i]->getEtat()=='a') {  // la quete est en cours (active)
            if (m_tableauQuetes[i]->getQueteActive()->getType()=="Massacre") {
                QueteMassacre* queteMassacre=static_cast<QueteMassacre*>(m_tableauQuetes[i]->getQueteActive());  // si c'est une quete de massacre on cast en QueteMassacre
                if (queteMassacre->getIdEspece()==ennemi->getId() && (queteMassacre->getRegionCible()==-1 || (unsigned int)queteMassacre->getRegionCible()==m_terrain->getIdRegion())) {  // On verifie que c'est l'objet qu'on doit collecter
                    queteMassacre->augmenterNombreVaincu(1);
                    if (queteMassacre->estMassacreFini()) {
                        queteMassacre->changeEtat();
                        if (queteMassacre->getEtat()=='f')
                            m_tableauQuetes[i]->activerQueteSuivante();
                    }
                }
            }
            if (m_tableauQuetes[i]->getEtat()=='f') {
                m_afficheFinQuete=true;
                m_indiceFinQuete=i;
            }
        }
    }
}

void Jeu::gestionDeplacementQuete() {
    for (unsigned int i=0; i<m_tableauQuetes.size(); i++) {
        if (m_tableauQuetes[i]->getEtat()=='a') {  // la quete est en cours (active)
            if (m_tableauQuetes[i]->getQueteActive()->getType()=="D\u00E9placement" && m_tableauQuetes[i]->getQueteActive()->getEtat()=='a') {
                QueteDeplacement* queteDeplacement=static_cast<QueteDeplacement*>(m_tableauQuetes[i]->getQueteActive());  // si c'est une quete de deplacement on cast en QueteDeplacement
                if (queteDeplacement->getRegionCible()==-1 || (unsigned int)queteDeplacement->getRegionCible()==m_terrain->getIdRegion()) {  // On verifie que c'est le niveau ou on doit se rendre
                    // On verifie qu'on est dans le rayon cible
                    if (std::sqrt(std::pow(queteDeplacement->getDestination().x-(m_joueur->getPosition().x/m_tailleCase),2)+std::pow(queteDeplacement->getDestination().y-(m_joueur->getPosition().y/m_tailleCase),2))<=queteDeplacement->getRayon()) {
                        queteDeplacement->changeEtat();
                        if (queteDeplacement->getEtat()=='f')
                            m_tableauQuetes[i]->activerQueteSuivante();
                    }
                }
            }
            if (m_tableauQuetes[i]->getEtat()=='f') {
                m_afficheFinQuete=true;
                m_fenetreOuverte=true;
                m_indiceFinQuete=i;
            }
        }
    }
}

Objet* Jeu::getObjet(unsigned int i) const {
    return m_tableauObjets[i];
}

void Jeu::recupererRecompenseQuete(unsigned int indiceQuete) {
    Inventaire* recompensesQuete=m_tableauQuetes[indiceQuete]->getRecompenses();
    Inventaire* inventaireJoueur=m_joueur->getInventaire();
    for (unsigned int i=0; i<recompensesQuete->getNbObjet(); i++) {
        inventaireJoueur->ajouterObjet(recompensesQuete->getIemeObjet(i+1));
        gestionObjetQuete(recompensesQuete->getIemeObjet(i+1), true);
    }
    inventaireJoueur->changeOr(recompensesQuete->getOr());
    gainExperienceJoueurFamilier(0, m_tableauQuetes[indiceQuete]->getExp());
    if (m_monteeNiveau[0]) {
        m_afficheRecueilQuete = false;
        m_afficheMonteeNiveau = true;
    }
    m_tableauQuetes[indiceQuete]->changeEtat();  // on fini la quete
}

void Jeu::queteFinieScenario(unsigned int indiceQuete) {
    if (m_indiceScenario==2) {
        // on verifie si on a fini les 4 premieres quetes du jeu afin de debloquer la suite du scenario
        for (unsigned int i=0; i<4; i++) {
            if (!(m_tableauQuetes[i]->getEtat()=='f' || m_tableauQuetes[i]->getEtat()=='r'))
                return;
        }
        if (m_fenetreOuverte)  // si on a une fenetre ouverte, comme l'inventaire ou le recueil de quetes, on la ferme
            actionClavier('p',0);
        m_affichageTransitionNoire=true;
        m_indiceScenario=3;
    }
    else if (m_indiceScenario==12) {
        if (m_tableauQuetes[4]->getEtat()=='f') {
            m_affichageTransitionNoire=true;
            m_indiceScenario=13;
        }
    }

}

void Jeu::finDialogueScenario() {
    if (m_niveau==0 && m_interactionIndiceObjet==0) { // on a fini de parler au conseiller au debut du jeu
        if (!m_tutorielQueteDejaAffiche) {  // premiere fois qu'on fini ce dialogue, on affiche le tutoriel concernant les quetes
            m_fenetreOuverte = true;
            m_afficheTutoriel = true;
            m_afficheTutorielQuete = true;
            m_tutorielQueteDejaAffiche = true;
            m_menuPrincipal = false;
        }
        else if (m_indiceScenario==3) { // fin du dialogue scenarise Conseiller1, on fait passer la nuit
            m_affichageTransitionNoire=true;
            m_indiceScenario=4;
        }
    }
    if (m_niveau==3) {// on a fini de parler au conseiller au moment ou il meurt
        m_affichageTransitionNoire=true;
        m_indiceScenario=12;
    }
}

void Jeu::appliquerTransition() {
    switch (m_indiceScenario) {
        case 1: {  // narration du debut de jeu, on active le dialogue du narrateur et on ferme le menu principal
            m_afficheDialogue=true;
            m_scenarioNarrateur=true;
            m_menuPrincipal=false;
            m_fenetreOuverte=false;
            break;
        }
        case 2: {  // fin du scenario de debut, lancement du dialogue avec le Conseiller
            m_scenarioNarrateur=false;
            m_scenario=false;
            m_lanceMsqScenario=false;
            m_afficheDialogue=true;
            m_interactionPossible=2;
            m_interactionIndiceObjet=0;  // on parle au pnj conseiller, mis intentionnellement en premier
            m_interactionIndice=129;  // case du pnj (x:9 y:4)
            m_tableauQuetes[0]->activerQueteSuivante();  // On active la premiere quete principale
            break;
        }
        case 3: {  // On deplace le joueur et on active un dialogue scenarise, le Conseiller remercie la joueuse et lui souhaite une bonne nuit
            positionnerEquipe(8*m_tailleCase, 4*m_tailleCase);
            Dialogue* dialogueScenarise= new Dialogue("data/dialogues/Conseiller1.txt", true);
            m_tableauPNJ[0][0]->ajouterDialogue(m_tableauPNJ[0][0]->getIndiceMax()+1, dialogueScenarise);
            m_tableauPNJ[0][0]->augmenterIndiceDialogue();
            m_afficheDialogue=true;
            m_interactionIndiceObjet=0;  // on parle au pnj conseiller, mis intentionnellement en premier
            m_interactionIndice=129;  // case du pnj (x:9 y:4)
            break;
        }
        case 4: {  // Fin du dialogue, transition pour la nuit, on declenche le dialogue du narrateur
            delete m_dialogueNarrateur;
            m_dialogueNarrateur=new Dialogue("data/dialogues/Narrateur1.txt", true);
            m_lanceMsqScenario=true;
            m_afficheDialogue=true;
            m_scenarioNarrateur=true;
            m_scenario=true;
            break;
        }
        case 5: {  // Debut de l'animation de l'attaque du village par les mosntres, on desactive la narration et on charge le niveau suivant
            m_scenarioNarrateur=false;
            m_afficheDialogue=false;
            chargerNiveau(2, 8*m_tailleCase, 4*m_tailleCase);
            break;
        }
        case 6: {  // On fait apparaitre un Lourd'eau
            m_tableauEnnemis.push_back(new Ennemi(0, 8*m_tailleCase, m_tableauEspeces[10], 100, 5, 70, 90, 20));
            // on fait comme si les ennemis provenaient de PointApparition
            m_tableauChasse.push_back(0);
            m_tableauOrientation.push_back(0);
            m_tableauPointsApparitions.push_back(new PointApparition());
            m_tableauTombes.push_back(NULL);
            m_tableauInventairesTombes.push_back(NULL);
            break;
        }
        case 7: {  // on change de niveau pour faire disparaitre l'obstacle et on remet le Lourd'eau
            chargerNiveau(3, 8*m_tailleCase, 4*m_tailleCase);
            m_tableauEnnemis.push_back(new Ennemi(0, 8*m_tailleCase, m_tableauEspeces[10], 100, 5, 70, 90, 20));
            // on fait comme si les ennemis provenaient de PointApparition
            for (unsigned int i=0; i<4; i++) {
                m_tableauChasse.push_back(0);
                m_tableauOrientation.push_back(0);
                m_tableauPointsApparitions.push_back(new PointApparition());
                m_tableauTombes.push_back(NULL);
                m_tableauInventairesTombes.push_back(NULL);
            }
            break;
        }
        case 8: {  // on deplace le Lourd'eau et on fait apparaitre un premier Vaut-rien
            m_tableauEnnemis[0]->deplacerPersonnage(2*m_tailleCase, 0);
            m_tableauEnnemis.push_back(new Ennemi(0, 8*m_tailleCase, m_tableauEspeces[13], 70, 5, 50, 50, 5));
            break;
        }
        case 9: {  // on continue les deplacements et on ajoute deux Vaut-rien
            m_tableauEnnemis[0]->deplacerPersonnage(2*m_tailleCase, 0);
            m_tableauEnnemis[1]->deplacerPersonnage(3*m_tailleCase, -m_tailleCase);
            m_tableauEnnemis.push_back(new Ennemi(0, 8*m_tailleCase, m_tableauEspeces[13], 70, 5, 50, 50, 5));
            m_tableauEnnemis.push_back(new Ennemi(m_tailleCase, 8*m_tailleCase, m_tableauEspeces[13], 70, 5, 50, 50, 5));
            break;
        }
        case 10: {  // derniers deplacements
            m_tableauEnnemis[1]->deplacerPersonnage(5*m_tailleCase, 0);
            m_tableauEnnemis[2]->deplacerPersonnage(4*m_tailleCase, 1*m_tailleCase);
            m_tableauEnnemis[3]->deplacerPersonnage(3*m_tailleCase, 3*m_tailleCase);
            break;
        }
        case 11: {  // on parle au Conseiller qui nous demande notre aide
            m_scenario=false;
            m_afficheDialogue=true;
            m_interactionPossible=2;
            m_interactionIndiceObjet=0;  // on parle au pnj conseiller, mis intentionnellement en premier
            m_interactionIndice=129;  // case du pnj (x:9 y:4)
            break;
        }
        case 12: {  // on charge le niveau suivant ou le conseiller est mort et on active l'editeur de deck, on remet les ennemis
            m_lanceMsqScenario=false;
            // on active la quete de massacre
            m_tableauQuetes[4]->activerQueteSuivante();
            chargerNiveau(4, 8*m_tailleCase, 4*m_tailleCase);
            m_tableauEnnemis.push_back(new Ennemi(4*m_tailleCase, 8*m_tailleCase, m_tableauEspeces[10], 100, 5, 70, 90, 20));
            m_tableauEnnemis.push_back(new Ennemi(8*m_tailleCase, 7*m_tailleCase, m_tableauEspeces[13], 70, 5, 50, 50, 5));
            m_tableauEnnemis.push_back(new Ennemi(4*m_tailleCase, 9*m_tailleCase, m_tableauEspeces[13], 70, 5, 50, 50, 5));
            m_tableauEnnemis.push_back(new Ennemi(4*m_tailleCase, 11*m_tailleCase, m_tableauEspeces[13], 70, 5, 50, 50, 5));
            // on fait comme si les ennemis provenaient de PointApparition
            for (unsigned int i=0; i<4; i++) {
                m_tableauChasse.push_back(0);
                m_tableauOrientation.push_back(0);
                m_tableauPointsApparitions.push_back(new PointApparition());
                m_tableauTombes.push_back(NULL);
                m_tableauInventairesTombes.push_back(NULL);
                Deck* deckEnnemi = new Deck;
                for (unsigned int j = 0; j < deckEnnemi->getNombreCartesMax(); j++)
                {
                    // Selection aleatoire entre les 5 cartes possible pour chaque espece
                    unsigned int idCarte=m_tableauEnnemis[i]->getEspece()->getIdEspece()*5 + rand()%5;
                    deckEnnemi->ajouterCarte(m_tableauCartes.getCarte(idCarte));
                }
                m_tableauEnnemis[i]->setDeck(deckEnnemi);
            }
            m_afficheTutoriel=true;
            m_afficheTutorielDeck=true;
            break;
        }
        case 13: {  // fin du tuto
            delete m_dialogueNarrateur;
            m_dialogueNarrateur=new Dialogue("data/dialogues/DialogueFin.txt", true);
            m_lanceMsqScenario=true;
            m_afficheDialogue=true;
            m_scenarioNarrateur=true;
            m_scenario=true;
            break;
        }
        case 14: { // on permet de jouer normalement et on ajoute les familiers
            m_scenario=false;
            m_lanceMsqScenario=false;
            m_afficheDialogue=false;
            m_scenarioNarrateur=false;
            m_equipe[0]=m_tableauPersonnages[1];
            m_equipe[1]=m_tableauPersonnages[2];
            m_equipe[2]=m_tableauPersonnages[3];
            chargerNiveau(5, m_joueur->getPosition().x, m_joueur->getPosition().y);
        }
        default :
            break;
    }
}
