#ifndef INVENTAIRE_H
#define INVENTAIRE_H

#include "Objet.h"

class Inventaire
{
    public:
        Inventaire();
        ~Inventaire();
        unsigned int getLimite() const;
        void setLimite(unsigned int nouvelleLimite);
        Objet* getObjet(unsigned int i);
        Objet* getIemeObjet(unsigned int i);
        /**@brief Recupere l'or (m_or) @param non @return unsigned int @warning Ne modifie pas de valeur*/
        unsigned int getOr() const;
        /**@brief Change l'or du  : change m_or de nbOr @param [in] int nbOr @return non */
        void changeOr(int nbOr);
        void setObjet(unsigned int i);
        bool ajouterObjet(Objet * obj);
        void retirerObjet(unsigned int i);
        void retirerIemeObjet(unsigned int i);
        unsigned int getNbObjet() const;

    private:
        unsigned int m_limite;
        unsigned int m_nbObjet=0;
        unsigned int m_or=0;
        std::vector<Objet*> m_tableauObjets;
};

#endif // INVENTAIRE_H
