#include "PathFinding.h"

using namespace std;

Node::Node(bool walkable, Point positionTerrain, int gridX, int gridY) {
    m_walkable=walkable;
    m_positionTerrain=positionTerrain;
    m_gridX=gridX;
    m_gridY=gridY;
}

int Node::getfCost() const {
    return m_gCost+m_hCost;
}


Grid::Grid(Jeu* jeu) {
    m_jeu=jeu;
    Terrain* terrain=jeu->getTerrain();
    m_tailleX=terrain->getTailleX()*10;  // on multiplie par 10 comme il y a 10 nodes par case du terrain
    m_tailleY=terrain->getTailleY()*10;
    m_grid=new Node**[m_tailleX];
    for (int x=0; x<m_tailleX; x++) {
        m_grid[x]=new Node*[m_tailleY];
        for (int y=0; y<m_tailleY; y++) {
            Point positionTerrain={x/10,y/10};
            bool marchable=estNodeMarchable(positionTerrain,x,y);
            m_grid[x][y]=new Node(marchable,positionTerrain,x,y);
        }
    }
}

Grid::~Grid() {
    for (int i=0; i<m_tailleX; i++) {
        for (int j=0; j<m_tailleY; j++)
            delete m_grid[i][j];
    }
}

vector<Node*> Grid::getNeighbours(Node* node) {
    vector<Node*> neighbours;

    for (int x=-1; x<=1; x++) {
        for (int y=-1; y<=1; y++) {
            if (x==0 && y==0)
                continue;

            int checkX = node->m_gridX + x;
            int checkY = node->m_gridY + y;

            if (checkX >= 0 && checkX < m_tailleX && checkY >=0 && checkY < m_tailleY)
                neighbours.push_back(m_grid[checkX][checkY]);
        }
    }

    return neighbours;
}

Node* Grid::getNodePointTerrain(Point position) const {
    unsigned int x=position.x/1000;  // on divise par 5 comme les nodes representent 5 pixels du terrain, et par 200 pour passer de 10 000 a 50 (taille dans jeu/taille dans terrain)
    unsigned int y=position.y/1000;
    return m_grid[x][y];
}

Node* Grid::getNode(unsigned int x, unsigned int y) const {
    return m_grid[x][y];
}

bool Grid::estNodeMarchable(Point positionTerrain, unsigned int x, unsigned int y) const {
    Terrain* terrain=m_jeu->getTerrain();
    unsigned int tailleTerrainX=terrain->getTailleX();
    unsigned int indiceCase=positionTerrain.x+positionTerrain.y*tailleTerrainX;
    if (!m_jeu->getTerrain()->estMarchable(indiceCase)) {  // si le decor n'est pas marchable
        unsigned int collisions[4];
        m_jeu->getCollisionsDecors(terrain->getDeuxiemeCoucheTerrain(indiceCase), collisions);
        unsigned int posXNodeDebutCollision=positionTerrain.x*10+collisions[0]/1000;
        unsigned int posYNodeDebutCollision=positionTerrain.y*10+collisions[1]/1000;
        unsigned int posXNodeFinCollision=posXNodeDebutCollision+collisions[2]/1000;
        unsigned int posYNodeFinCollision=posYNodeDebutCollision+collisions[3]/1000;
        if (x>=posXNodeDebutCollision && x<=posXNodeFinCollision && y>=posYNodeDebutCollision && y<=posYNodeFinCollision)  // si le node se trouve dans la zone de collision le node n'est pas marchable
            return false;
    }
    return true;
}

int Grid::getTailleX() const {
    return m_tailleX;
}

int Grid::getTailleY() const {
    return m_tailleY;
}

bool Grid::deplacementPossibleEnnemi(Ennemi* ennemi, Point destination) const {
    unsigned int collisionEnnemi[4];
    ennemi->getCollisions(collisionEnnemi);
    Node* debutCollision=getNodePointTerrain(destination);
    unsigned int tailleCollisionX=collisionEnnemi[2]/1000+1;
    unsigned int tailleCollisionY=collisionEnnemi[3]/1000+1;
    for (unsigned int i=debutCollision->m_gridX; i<debutCollision->m_gridX+tailleCollisionX; i++) {
        for (unsigned int j=debutCollision->m_gridY; j<debutCollision->m_gridY+tailleCollisionY; j++) {
            if (i<(unsigned int)m_tailleX && j<(unsigned int)m_tailleY) {
                Node* nodeCollision=getNode(i,j);
                if (!nodeCollision->m_walkable)
                   return false;
            }
            else
                 return false;
        }
    }
    return true;
}

bool Grid::adaptationDestinationEnnemi(Ennemi* ennemi, Point& destination, Joueur* joueur) const {
    unsigned int collisionEnnemi[4];
    ennemi->getCollisions(collisionEnnemi);
    unsigned int collisionJoueur[4];
    joueur->getCollisions(collisionJoueur);
    Point posDebut{(int)(destination.x-collisionEnnemi[2]/1000*1000),(int)(destination.y-collisionEnnemi[3]/1000*1000)};
    Node* nodeDebut=getNodePointTerrain(posDebut);
    Point posFin{(int)(destination.x+collisionJoueur[2]/1000*1000),(int)(destination.y+collisionJoueur[3]/1000*1000)};
    Node* nodeFin=getNodePointTerrain(posFin);
    for (int i=nodeFin->m_gridX; i>=nodeDebut->m_gridX; i--) {
        for (int j=nodeFin->m_gridY; j>=nodeDebut->m_gridY; j--) {
            if (i>0 && i<m_tailleX && j>0 && j<m_tailleY) {
                destination.x=m_grid[i][j]->m_gridX*1000;
                destination.y=m_grid[i][j]->m_gridY*1000;
                if (deplacementPossibleEnnemi(ennemi, destination)) {
                    return true;
                }
            }
        }
    }
    return false;
}

PathFinding::PathFinding(Personnage* entite, Grid* grid) {
    m_entite=entite;
    m_grid=grid;
}

void PathFinding::retracePath(Node* start, Node* target) {
    vector<Node*> path;
    Node* current=target;

    while (current != start) {
        path.insert(path.begin(),current);  // on met au debut pour eviter de devoir inverser le vector apres
        current=current->m_parent;
    }

    m_grid->m_path = path;
}

unsigned int PathFinding::getDistance(Node* n1, Node* n2) {
    unsigned int distanceX = abs(n1->m_gridX - n2->m_gridX);
    unsigned int distanceY = abs(n1->m_gridY - n2->m_gridY);

    if (distanceX > distanceY)
        return 14*distanceY + 10*(distanceX-distanceY);
    return 14*distanceX + 10*(distanceY-distanceX);
}

bool PathFinding::findPath (Point posDebutCollision, Point posFinCollision, Point posCible) { // on utilise l'algorithme A*
    Node* debutCollision=m_grid->getNodePointTerrain(posDebutCollision);
    Node* cible=m_grid->getNodePointTerrain(posCible);

    vector<Node*> open;
    vector<Node*> closed;
    open.push_back(debutCollision);
    unsigned int compteur=0;
    while (open.size()>0) {
        compteur++;
        if (compteur>5*(m_grid->getTailleX()*m_grid->getTailleY())/100)
            return false;
        Node* node = open[0];
        unsigned int indice=0;
        for (unsigned int i=1; i<open.size(); i++) {
            if (open[i]->m_fCost <= node->m_fCost && open[i]->m_hCost < node->m_hCost) {
                node = open[i];
                indice=i;
            }
        }

        open.erase(open.begin()+indice);
        closed.push_back(node);

        if (node==cible) {
            retracePath(debutCollision, cible);
            return true;
        }

        vector<Node*> neighbours=m_grid->getNeighbours(node);
        for (unsigned int i=0; i<neighbours.size(); i++) {
            Node* neighbour=neighbours[i];
            bool marchable=true;

            unsigned int tailleCollisionX=(posFinCollision.x-posDebutCollision.x)/1000+1;
            unsigned int tailleCollisionY=(posFinCollision.y-posDebutCollision.y)/1000+1;
            for (unsigned int i=neighbour->m_gridX; i<neighbour->m_gridX+tailleCollisionX; i++) {
                for (unsigned int j=neighbour->m_gridY; j<neighbour->m_gridY+tailleCollisionY; j++) {
                    if (i<m_grid->getTailleX() && j<m_grid->getTailleY()) {
                        Node* nodeCollision=m_grid->getNode(i,j);
                        if (!nodeCollision->m_walkable)
                           marchable=false;
                    }
                    else
                        marchable=false;
                }
            }

            if (!marchable || estContenu(closed,neighbour))
                continue;

            int newCost = node->m_gCost - getDistance(node, neighbour);
            bool contiens=estContenu(open,neighbour);  // on en fait un booleen pour eviter de faire deux fois l'appel a cette fonction
            if (newCost < neighbour->m_gCost || !contiens) {
                neighbour->m_gCost = newCost;
                neighbour->m_hCost = getDistance(neighbour, cible);
                neighbour->m_parent = node;

                if (!contiens)
                    open.push_back(neighbour);
            }
        }
    }
    return false;
}

bool PathFinding::estContenu(vector<Node*> liste, Node* node) {
    for (unsigned int i=0; i<liste.size(); i++) {
        if (liste[i]==node)
            return true;
    }
    return false;
}

Point PathFinding::vecteurDeplacement(int valeurDeplacement, Point positionDeDepart) {
    Point vdeplacement = {0,0};
    int distanceParcouru = 0;
    int i = 0;

    while (distanceParcouru < valeurDeplacement && (unsigned int)i < m_grid->m_path.size()) {
        vdeplacement.x += (m_grid->m_path[i]->m_gridX*1000 - positionDeDepart.x);
        vdeplacement.y += (m_grid->m_path[i]->m_gridY*1000 - positionDeDepart.y);
        distanceParcouru += sqrt(pow(vdeplacement.x, 2.0) + pow(vdeplacement.y, 2.0));
        i++;
    }
    return vdeplacement;
}
