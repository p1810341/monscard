#ifndef ESPECE_H
#define ESPECE_H

#include<string>
#include <assert.h>
#include "Objet.h"
#include <vector>

class Espece
{
public:
    /**@brief Constructeur par defaut d'espece @param non */
    Espece();
    /**@brief Constructeur par parametre d'Espece @param [in] unsigned int id @param [in] string nom @param [in] float gainExp @return Espece  @warning  Faire attention � l'ordre des parametres*/
    Espece(unsigned int id, const std::string nom, unsigned int butinOr, unsigned int vitesse);
    ~Espece();
    /**@brief Recupere l'id de l'Espece (m_idEspece) @param non @return unsigned int @warning  Ne modifie pas de valeur*/
    unsigned int getIdEspece() const;
    /**@brief Recupere le nom de l'Espece (m_nomEspece) @param non @return string @warning  Ne modifie pas de valeur*/
    std::string getNomEspece() const;
    std::vector<Objet*> getTableauButins() const;
    void ajouterButin(Objet* butin, float chance);
    std::vector<float> getChanceButin() const;
    void getCollisions(unsigned int collisions[4]) const;
    void setCollisions(unsigned int collisions[4]);
    unsigned int getButinOr() const;
    unsigned int getVitesse() const;

private:
    unsigned int m_idEspece;    // id de l'espece
    std::string m_nomEspece;    // nom de l'espece
    std::vector<Objet*> m_tableauButins;    // butins de l'espece
    std::vector<float> m_tableauChanceButins;    // pourcentages de chance d'obtenir les butins
    unsigned int m_collisions[4];    // rectangle de collision de l'espece
    unsigned int m_butinOr;      // quantite d'or obtenue en battant cette espece
    unsigned int m_vitesse;      // vitesse de l'espece
};

#endif // ESPECE_H
