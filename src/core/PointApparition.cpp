#include "PointApparition.h"

PointApparition::PointApparition() {
    m_position.x=0;
    m_position.y=0;
    m_garde=true;
    m_temps=0;
    m_tempsReapparition=0;
}

PointApparition::PointApparition(unsigned int x, unsigned int y) {
    m_position.x=x;
    m_position.y=y;
    m_garde=true;
    m_temps=0;
    m_tempsReapparition=0;
}

PointApparition::~PointApparition() {
    for (unsigned int i=0; i<m_listeEnnemis.size(); i++)
        delete m_listeEnnemis[i];
}

Point PointApparition::getPosition() const {
    return m_position;
}

Ennemi* PointApparition::getEnnemi(unsigned int i) const {
    return m_listeEnnemis[i];
}

unsigned int PointApparition::getTailleListe() const {
    return m_listeEnnemis.size();
}

bool PointApparition::getGarde() const {
    return m_garde;
}

void PointApparition::setGarde(bool garde) {
    m_garde=garde;
}

unsigned int PointApparition::getTempsReapparition() const {
    return m_tempsReapparition;
}

void PointApparition::setTempsReapparition(unsigned int tempsSecondes) {
    m_tempsReapparition=tempsSecondes;
}

void PointApparition::ajouterEnnemi(Ennemi* ennemi) {
    m_listeEnnemis.push_back(ennemi);
}

void PointApparition::supprimerEnnemi(unsigned int i) {
    delete m_listeEnnemis[i];
    m_listeEnnemis.erase(m_listeEnnemis.begin()+i);
}

void PointApparition::defaite() {
    m_temps = time(NULL);
    m_vivant = false;
}

bool PointApparition::apparitionPossible() {
    if (time(NULL)- m_temps >= m_tempsReapparition)
        return true;
    return false;
}

Ennemi* PointApparition::apparition() {
    if (m_vivant == false && apparitionPossible()) {
        m_vivant = true;
        return m_listeEnnemis[rand() % m_listeEnnemis.size()];
    }
    return NULL;
}
