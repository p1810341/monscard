#include <fstream>
#include <cassert>
#include <iostream>
#include "Dialogue.h"
#include <sstream>

Dialogue::Dialogue() {
    m_indicePhrase = 0;
}

Dialogue::Dialogue(const std::string& texte, bool fichier) {
    m_indicePhrase = 0;
    if (fichier) {
        std::ifstream fichierDialogue(texte.c_str());
        if(!fichierDialogue.is_open()) {
            std::string parent = std::string("../") + texte;
            fichierDialogue.open(parent.c_str());
            if(!fichierDialogue.is_open()) {
                std::string parent = std::string("../") + parent;
                fichierDialogue.open(parent.c_str());
                if(!fichierDialogue.is_open()) {
                    std::cout<<"Dialogue::Dialogue : Erreur dans l'ouverture du fichier " << texte <<std::endl;
                    assert(fichierDialogue.is_open());
                }
            }
        }
        if(fichierDialogue.is_open()) {
            std::string ligne;
            while (std::getline(fichierDialogue, ligne))
            {
                m_phrases.push_back(ligne);
            }
            fichierDialogue.close();
        }
    }
    else {
        if (texte=="")
            m_phrases.push_back(texte);
        else {
            std::string ligne;
            std::istringstream fichier;
            fichier.str(texte);
            while (std::getline(fichier, ligne))
            {
                m_phrases.push_back(ligne);
            }
        }
    }
}

void Dialogue::setDialogue(const std::string& texte) {
    m_indicePhrase = 0;
    std::string ligne;
    std::istringstream fichier;
    fichier.str(texte);
    while (std::getline(fichier, ligne))
    {
        m_phrases.push_back(ligne);
    }
}

unsigned int Dialogue::getIndicePhrase() const {
    return m_indicePhrase;
}

bool Dialogue::augmenterIndice() {
    m_indicePhrase++;
    if(m_indicePhrase == m_phrases.size()) {
        m_indicePhrase = 0;
        return true;
    }
    return false;
}

std::string Dialogue::getPhrase(unsigned int i) const{
    return m_phrases[i];
}

void Dialogue::deletePhrase(unsigned int i) {
    if (i<m_phrases.size())
        m_phrases.erase(m_phrases.begin()+i);
    if (i==m_phrases.size())
        m_indicePhrase--;
}

void Dialogue::ajouterPhrase(unsigned int i, std::string s) {
    if (i>m_phrases.size())
        i=m_phrases.size();
    m_phrases.insert(m_phrases.begin()+i,s);
}

unsigned int Dialogue::getIndiceMax() const {
    return m_phrases.size()-1;
}

bool Dialogue::reduireIndice() {
    if (m_indicePhrase==0)
        return true;
    else
        m_indicePhrase--;
    return false;
}

void Dialogue::setPhrase(unsigned int i, std::string s) {
    if (i>m_phrases.size())
        i=m_phrases.size()-1;
    m_phrases[i]=s;
}
