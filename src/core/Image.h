#ifndef IMAGE_H
#define IMAGE_H
#include "SDL.h"
#include "SDL_image.h"

class Image
{
    private:
    SDL_Surface * surface;
    SDL_Texture * texture;

    public:
    Image();
    ~Image();
    void loadFromFile(const char* filename, SDL_Renderer * renderer);
    void loadFromCurrentSurface (SDL_Renderer * renderer);
    void setSurface(SDL_Surface * surf);
    SDL_Surface * getSurface() const;
    SDL_Texture * getTexture() const;
    void libereSurface();
    void libereTexture();
    void draw(SDL_Renderer* renderer, int x, int y, int w=-1, int h=-1, unsigned int transparence=255);
    void drawRotate(SDL_Renderer* renderer, int x, int y, int angle, int w=-1, int h=-1, unsigned int transparence=255);
};

#endif // IMAGE_H
