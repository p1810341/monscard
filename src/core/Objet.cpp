#include "Objet.h"
//Objet
Objet::Objet(){}
Objet::Objet(unsigned int id, const std::string nom, const std::string description, unsigned int valeur){
    m_id=id;
    m_nom=nom;
    m_description=description;
    m_valeur=valeur;
}

Objet::Objet(const Objet* objet) {
    m_id=objet->m_id;
    m_nom=objet->m_nom;
    m_description=objet->m_description;
    m_valeur=objet->m_valeur;
}

Objet::~Objet(){

}
unsigned int Objet::getIdObjet(){
    return m_id;
}
std::string Objet::getNom() const{
    return m_nom;
}
void Objet::setNomObjet(const std::string nom){
    m_nom=nom;
}

std::string Objet::getDescriptionObjet() const{
    return m_description;
}

void Objet::setDescriptionObjet(const std::string description){
    m_description=description;
}

unsigned int Objet::getValeurObjet() const{
    return m_valeur;
}

void Objet::setValeurObjet(unsigned int valeur){
    m_valeur=valeur;
}

//Materiau
Materiau::Materiau(unsigned int id, const std::string nom, const std::string description, unsigned int valeur, unsigned int nombreExemplaires, unsigned int nombreMax) : Objet(id, nom, description, valeur) {
    m_nombreExemplaires=nombreExemplaires;
    m_nombreMax=nombreMax;
}

Materiau::Materiau(const Materiau* materiau) : Objet(materiau) {
    m_nombreExemplaires=materiau->m_nombreExemplaires;
    m_nombreMax=materiau->m_nombreMax;
}

std::string Materiau::getTypeObjet() const{
    return "Mat\u00E9riau";
}

unsigned int Materiau::getNombreExemplaires() const{
    return m_nombreExemplaires;
}

void Materiau::modifierNombreExemplaires(int nombre){
    m_nombreExemplaires+=nombre;
}

unsigned int Materiau::getNombreMax(){
    return m_nombreMax;
}

//ObjetOrdinaire
ObjetOrdinaire::ObjetOrdinaire(){
}

ObjetOrdinaire::ObjetOrdinaire(unsigned int id, const std::string nom, const std::string description, unsigned int valeur, unsigned int pv, unsigned int mana, unsigned int defense, unsigned int attaque) : Objet(id, nom, description, valeur) {
    m_pv=pv;
    m_mana=mana;
    m_defense=defense;
    m_attaque=attaque;
}

ObjetOrdinaire::ObjetOrdinaire(const ObjetOrdinaire* objetOrdinaire) : Objet(objetOrdinaire) {
    m_pv=objetOrdinaire->m_pv;
    m_mana=objetOrdinaire->m_mana;
    m_defense=objetOrdinaire->m_defense;
    m_attaque=objetOrdinaire->m_attaque;
}

ObjetOrdinaire::~ObjetOrdinaire(){
}
unsigned int ObjetOrdinaire::getPvObjet() const{
    return m_pv;
}
void ObjetOrdinaire::setPvObjet(unsigned int pv){
    m_pv=pv;
}
unsigned int ObjetOrdinaire::getManaObjet() const{
    return m_mana;
}
void ObjetOrdinaire::setManaObjet(unsigned int mana){
    m_mana=mana;
}
unsigned int ObjetOrdinaire::getDefenseObjet() const{
    return m_defense;
}
void ObjetOrdinaire::setDefenseObjet(unsigned int defense){
    m_defense=defense;
}
unsigned int ObjetOrdinaire::getAttaqueObjet() const{
    return m_attaque;
}
void ObjetOrdinaire::setAttaqueObjet(unsigned int attaque){
    m_attaque=attaque;
}

//Equipement
Equipement::Equipement(unsigned int id, const std::string nom, const std::string description, unsigned int valeur, unsigned int pv, unsigned int mana, unsigned int defense, unsigned int attaque, unsigned int exp,unsigned int emplacement) : ObjetOrdinaire(id, nom, description, valeur, pv, mana, defense, attaque) {
    m_exp=exp;
    if(emplacement<6)
        m_emplacement=emplacement;
}

Equipement::Equipement(const Equipement* equipement) : ObjetOrdinaire(equipement) {
    m_exp=equipement->m_exp;
}

std::string Equipement::getTypeObjet() const{
    return "\u00C9quipement";
}

unsigned int Equipement::getMultiplicateurExp() const{
    return m_exp;
}

unsigned int Equipement::getEmplacement(){
    return m_emplacement;
}
//Consommable
Consommable::Consommable(unsigned int id, const std::string nom, const std::string description, unsigned int valeur, unsigned int pv, unsigned int mana, unsigned int defense, unsigned int attaque, unsigned int exp) : ObjetOrdinaire(id, nom, description, valeur, pv, mana, defense, attaque) {
    m_exp=exp;
}

Consommable::Consommable(const Consommable* consommable) : ObjetOrdinaire(consommable) {
    m_exp=consommable->m_exp;
}

std::string Consommable::getTypeObjet() const{
    return "Consommable";
}
unsigned int Consommable::getExpBonus() const{
    return m_exp;
}

//ObjetSpecial
ObjetSpecial::ObjetSpecial(unsigned int id, const std::string nom, const std::string description, unsigned int valeur) : Objet(id, nom, description, valeur) {
}

ObjetSpecial::ObjetSpecial(const ObjetSpecial* objetSpecial) : Objet(objetSpecial) {
}

std::string ObjetSpecial::getTypeObjet() const{
    return "Sp\u00E9cial";
}
