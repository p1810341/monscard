#include "Espece.h"
#include <iostream>

Espece::Espece() {
    m_idEspece = 0;
    m_nomEspece = "";
    for (unsigned int i=0; i<4; i++)
        m_collisions[i]=0;
    m_butinOr=0;
    m_vitesse=0;
}

Espece::Espece(unsigned int id, const std::string nom, unsigned int butinOr, unsigned int vitesse) {
    m_idEspece=id;
    m_nomEspece=nom;
    for (unsigned int i=0; i<4; i++)
        m_collisions[i]=0;
    m_butinOr=butinOr;
    m_vitesse=vitesse;
}

Espece::~Espece() {
    for (unsigned int i=0; i<m_tableauButins.size(); i++) {
        delete m_tableauButins[i];
    }
}

unsigned int Espece::getIdEspece() const {
    return m_idEspece;
}

std::string Espece::getNomEspece() const {
    return m_nomEspece;
}

std::vector<Objet*> Espece::getTableauButins() const {
    return m_tableauButins;
}

void Espece::ajouterButin(Objet* butin, float chance) {
    m_tableauButins.push_back(butin);
    m_tableauChanceButins.push_back(chance);
}

std::vector<float> Espece::getChanceButin() const {
    return m_tableauChanceButins;
}

void Espece::getCollisions(unsigned int collisions[4]) const {
    for (unsigned int i=0; i<4; i++)
        collisions[i]=m_collisions[i];
}

void Espece::setCollisions(unsigned int collisions[4]) {
    for (unsigned int i=0; i<4; i++)
        m_collisions[i]=collisions[i];
}

unsigned int Espece::getButinOr() const {
    return m_butinOr;
}

unsigned int Espece::getVitesse() const {
    return m_vitesse;
}
