#ifndef POINTAPPARITION_H
#define POINTAPPARITION_H

#include "Personnage.h"
#include <vector>
#include <ctime>

class PointApparition
{
    public:
    PointApparition();
    PointApparition(unsigned int x, unsigned int y);
    ~PointApparition();

    Point getPosition() const;
    unsigned int getTailleListe() const;
    Ennemi* getEnnemi(unsigned int i) const;
    Ennemi* getEnnemiAleatoire() const;
    bool getGarde() const;
    void setGarde(bool garde);
    unsigned int getTempsReapparition() const;
    void setTempsReapparition(unsigned int tempsSecondes);
    void ajouterEnnemi(Ennemi* ennemi);
    void supprimerEnnemi(unsigned int i);
    void defaite();
    bool apparitionPossible();
    Ennemi* apparition();

    private:
    Point m_position;
    std::vector<Ennemi*> m_listeEnnemis;    // liste des ennemis qui peuvent apparaitre
    bool m_garde;  // true si l'ennemi reste a son point d'apparition sans se deplacer
    std::time_t m_temps;    // le temps donne au moment ou est mort l'ennemi
    unsigned int m_tempsReapparition;   // temps de reapparition en secondes
    bool m_vivant;
};

#endif // POINTAPPARITION_H
