#include "Coffre.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>

Coffre::Coffre() {
    m_position.x = 0;
    m_position.y = 0;
    m_estOuvert = false;
    m_butin=new Inventaire();
}

Coffre::Coffre(int x, int y) {
    m_position.x = x;
    m_position.y = y;
    m_estOuvert = false;
    m_butin=new Inventaire();
}

Coffre::~Coffre() {
    delete m_butin;
}

unsigned int Coffre::getOr() const {
   return m_butin->getOr();
}

Point Coffre::getPosition() const {
    return m_position;
}

bool Coffre::getOuvert() const {
    return m_estOuvert;
}

void Coffre::setOuvert() {
    m_estOuvert = !m_estOuvert;
}

Inventaire* Coffre::getButin() {
    return m_butin;
}
Objet * Coffre::getObjet(unsigned int i){
    return m_butin->getObjet(i);
}

unsigned int Coffre::getNbButin() const {
    return m_butin->getNbObjet();
}

void Coffre::retirerObjet(unsigned int i) {
    m_butin->retirerObjet(i);
}

void Coffre::testRegression() {
    Coffre c(20,30);
    assert(!c.getOuvert());
    assert(c.m_position.x==20);
    assert(c.m_position.y==30);
    assert(c.getPosition().x==20);
    assert(c.getPosition().y==30);
    std::cout<<"Test de non regression de coffre....OK "<<std::endl;
}
