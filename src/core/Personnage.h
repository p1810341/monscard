#ifndef PERSONNAGE_H
#define PERSONNAGE_H

#include <string>
#include <assert.h>
#include <stdlib.h>
#include "Point.h"
#include "Espece.h"
#include "TableauCartes.h"
#include "Inventaire.h"

#include <iostream>
class Personnage {
public:
    /**@brief Constructeur par defaut de la classe Personnage @param non*/
    Personnage();
    /**@brief Constructeur par parametres de position de la classe Personnage @param [in] int x @param [in] int y*/
    Personnage(unsigned int id, std::string const nom, int x, int y, unsigned int pv, unsigned int mana, unsigned int attaque, unsigned int defense, unsigned int vitesse);
    /**@brief Destructeur de la classe Personnage @param non*/
    ~Personnage();
    /**@brief Recupere le nom du personnage @param non  @return std::string @warning  Ne modifie pas de valeur*/
    std::string getNom() const;
    /**@brief Affecte un nom a un personnage @param [in] const std::string& nom @return non*/
    void setNom(const std::string& nom);
    void setPointsDeVieMax(unsigned int pv);
    void setPointsDeManaMax(unsigned int mana);
    void setAttaque(unsigned int attaque);
    void setDefense(unsigned int defense);
    /**@brief Recupere les points de vie du Personnage (m_pointsDeVie) @param non @return float @warning  Ne modifie pas de valeur*/
    unsigned int getPointsDeVie() const;
    /**@brief Recupere les points de vie maximum du Personnage (m_pointsDeVieMax) @param non @return float @warning  Ne modifie pas de valeur*/
    unsigned int getPointsDeVieMax() const;
    /**@brief Recupere les points de mana maximum du Personnage (m_pointsDeManaMax) @param non @return unsigned int @warning  Ne modifie pas de valeur*/
    unsigned int getPointsDeManaMax() const;
    /**@brief Recupere la position du Personnage @param non @return Point @warning  Ne modifie pas de valeur*/
    Point getPosition() const;
    /**@brief Recupere les points d'attaque du Personnage (m_attaque) @param non @return float @warning Ne modifie pas de valeur*/
    unsigned int getAttaque() const;
    /**@brief Recupere les points de defense du Personnage (m_defense) @param non @return float @warning Ne modifie pas de valeur*/
    unsigned int getDefense() const;
    /**@brief Recupere le niveau du Personnage (m_niveau) @param non @return unsigned int @warning Ne modifie pas de valeur*/
    unsigned int getNiveau() const;
    /**@brief Recupere le Deck du Personnage (m_deck) @param non @return Deck @warning Ne modifie pas de valeur*/
    Deck* getDeck() const;
    /**@brief Deplace le personnage horizontalement et verticalement : modifie les coordonnees x et y de la position du Personnage @param [in] int x @param [in] int y @return non */
    void deplacerPersonnage(int x, int y);
    /**@brief Change les points de vie du Personnage selon un changement indique en parametre @param [in] float changement @return non */
    void changePointsDeVie(int changement);
    /**@brief Affecte et remplace le deck du Personnage : remplace m_deck par deck @param [in] Deck* deck @return non @warning deck est un pointeur*/
    void setDeck(Deck* deck);
    void getCollisions(unsigned int collisions[4]) const;
    void setCollisions(unsigned int collisions[4]);
    unsigned int getVitesse() const;
    void changeVitesse(int changement);
    unsigned int getId() const;

protected:
    unsigned int m_id;              // Id du personnage
    std::string m_nomPersonnage;    // Nom du personnage
    unsigned int m_pointsDeVie;            // Points de vie actuels du personnage
    unsigned int m_pointsDeVieMax;         // Points de vie maximum du personnage (m_pointsDeVie = m_pointsDeVieMax si le personnage a toute sa vie)
    unsigned int m_pointsDeManaMax; // Points de mana maximums dont le personnage pourra beneficier en combat
    Point m_position;               // Position du personnage
    unsigned int m_attaque;                // Attaque du personnage
    unsigned int m_defense;                // Defense du personnage
    unsigned int m_niveau;          // Niveau du personnage
    Deck* m_deck;                   // Deck du personnage
    unsigned int m_collisions[4];    // Rectangle de collision du personnage
    unsigned int m_vitesse;          // Vitesse du personnage
};


class Ennemi : public Personnage {
public:
    /**@brief Constructeur par defaut d'Ennemi @param non*/
    Ennemi();
    /**@brief Constructeur par parametre d'Ennemi @param [in] int x @param [in] int y @param [in] const Espece& espece @param [in] float pointsDeVieMax
    @param [in] unsigned int pointsDeManaMax @param [in] float attaque @param [in] float defense @return non @warning Attention a l'ordre de parametres*/
    Ennemi(int x, int y, Espece* espece, unsigned int pointsDeVieMax, unsigned int pointsDeManaMax, unsigned int attaque, unsigned int defense, unsigned int gainExperience);
    /**@brief Destructeur par defaut d'Ennemi @param non*/
    ~Ennemi();
    /**@brief Recupere l'Espece (m_espece) de l'Ennemi @param non @return Espece @warning Ne modifie pas de valeur*/
    Espece* getEspece() const;
    /**@brief Recupere l'experience que donne de l'Ennemi a l'issue d'un combat @param non @return float @warning Ne modifie pas de valeur*/
    unsigned int getExp() const;
    void setExp(unsigned int exp);
    void setEspece(Espece* e);

private:
    Espece* m_espece;
    unsigned int m_gainExperience;
};

class Joueur : public Personnage {
public:
    /**@brief Constructeur par defaut de la classe Joueur @param non*/
    Joueur();
    /**@brief Constructeur par copie de la classe Joueur @param [in] const Joueur* j*/
    Joueur(const Joueur* j);
    /**@brief Constructeur par parametre de position du Joueur @param [in] int x @param [in] int y*/
    Joueur(unsigned int id, std::string const nom, int x, int y, unsigned int pv, unsigned int mana, unsigned int attaque, unsigned int defense, unsigned int vitesse);
    /**@brief Destructeur par defaut d'Ennemi @param non*/
    ~Joueur();
    /**@brief Recupere l'experience (m_experience) du Joueur @param non @return float @warning Ne modifie pas de valeur*/
    unsigned int getExperience() const;
    /**@brief Recupere le tableau de cartes obtenues (m_cartesObtenues) : l'indice est l'ID de la carte et le int contenu est le nombre d'exemplaire @param non
    @return unsigned int* @warning Ne modifie pas de valeur ; retourne un pointeur*/
    unsigned int* getCartesReserve() const;
    /**@brief Change le nombre obtenues d'une carte du Joueur : l'id est l'id de la carte dont on veut changer le nombre et i le changement a effectuer
    @param [in] unsigned int id @param [in] int i @return non*/
    void changeCarteReserve(unsigned int id, int i);
    /**@brief Augmente l'experience du joueur : augmente m_experience par exp @param [in] float exp @return non*/
    void augmenteExperience(unsigned int exp);
    /**@brief Augmente le niveau du Joueur de 1 et les statistiques associees Joueur ; rend toute sa vie au joueur @param non @return non */
    void augmenteNiveau();
    bool getCarteObtenable();
    void changeCarteObtenable(bool obtenable);
    unsigned int* getCartesObtenues() const;
    void changeCarteObtenue(unsigned int id, int i);
    Inventaire* getInventaire() const;
    void setInventaire(Inventaire * inventaire);
    Equipement* getEquipement(unsigned int i) const;
    bool setEquipement(unsigned int i,Equipement * equipement);
    unsigned int getOr() const;

private:
    unsigned int m_experience;              // Points d'experience du joueur
    unsigned int* m_cartesReserve;   // Tableau de cartes dans la reserve du joueur : pour chacune des cartes du jeu, indique combien le joueur en possede
    unsigned int* m_cartesObtenues;   // Tableau de cartes obtenues en comptant le deck
    bool m_carteObtenable;           // True si parmi les cartes debloquees, au moins une n'est pas possedee en trois exemplaires
    Inventaire* m_inventaire;       // Inventaire du joueur
    Equipement* m_equipement[6];    // Chapeau, collier, cape, anneau1, anneau2,bottes
};

class Familier : public Personnage {
public:
    Familier();
    Familier(const Familier* j);
    Familier(unsigned int id, std::string const nom, int x, int y, unsigned int pv, unsigned int mana, unsigned int attaque, unsigned int defense, unsigned int vitesse);
    ~Familier();
    unsigned int getExperience() const;
    void augmenteExperience(unsigned int exp);
    void augmenteNiveau();


private:
    unsigned int m_experience;              // Points d'experience du joueur
};

#endif // PERSONNAGE_H
