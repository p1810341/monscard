#ifndef POINT_H
#define POINT_H

// Permet de definir les positions des personnages et objets
struct Point{
    int x, y;
};

#endif
