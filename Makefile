CORE =core/Carte.cpp core/Coffre.cpp core/Combat.cpp core/Dialogue.cpp core/Espece.cpp core/Image.cpp core/Inventaire.cpp core/Jeu.cpp core/Objet.cpp core/PathFinding.cpp core/Personnage.cpp core/PNJ.cpp core/PointApparition.cpp core/Quete.cpp core/TableauCartes.cpp core/Terrain.cpp 

SRCS_SDL = $(CORE)  sdl2/main_sdl.cpp sdl2/sdlJeu.cpp
DEBUG_TARGET_SDL = debug_sdl
FINAL_TARGET_SDL = main_sdl
#DEFINE_SDL = -DJEU_SDL

SRCS_EDITEUR = $(CORE)  sdl2/main_editeur.cpp sdl2/sdlEditeur.cpp
DEBUG_TARGET_EDITEUR = debug_editeur
FINAL_TARGET_EDITEUR = main_editeur
#DEFINE_SDL = -DJEU_EDITEUR

ifeq ($(OS),Windows_NT)
	INCLUDE_DIR_SDL = 	-Iextern/SDL2_mingw-cb20/SDL2-2.0.12/x86_64-w64-mingw32/include/SDL2 \
						-Iextern/SDL2_mingw-cb20/SDL2_ttf-2.0.15/x86_64-w64-mingw32/include/SDL2 \
						-Iextern/SDL2_mingw-cb20/SDL2_image-2.0.5/x86_64-w64-mingw32/include/SDL2 \
						-Iextern/SDL2_mingw-cb20/SDL2_mixer-2.0.4/x86_64-w64-mingw32/include/SDL2

	LIBS_SDL = -Lextern \
			-Lextern/SDL2_mingw-cb20/SDL2-2.0.12/x86_64-w64-mingw32/lib \
			-Lextern/SDL2_mingw-cb20/SDL2_ttf-2.0.15/x86_64-w64-mingw32/lib \
			-Lextern/SDL2_mingw-cb20/SDL2_image-2.0.5/x86_64-w64-mingw32/lib \
			-Lextern/SDL2_mingw-cb20/SDL2_mixer-2.0.4/x86_64-w64-mingw32/lib \
			-lmingw32 -lSDL2main -lSDL2.dll -lSDL2_ttf.dll -lSDL2_image.dll -lSDL2_mixer.dll

else
	INCLUDE_DIR_SDL = -I/usr/include/SDL2
	LIBS_SDL = -lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_mixer
endif

CC				= g++
LD 				= g++	
LDFLAGS				=
CPPFLAGS 			= -Wall -std=c++11
OBJ_DIR 			= obj
SRC_DIR 			= src
BIN_DIR 			= bin
INCLUDE_DIR			= -Isrc -Isrc/core -Isrc/sdl2

DebugSDL: CPPFLAGS += -ggdb
DebugSDL: make_dir $(BIN_DIR)/$(DEBUG_TARGET_SDL)
DebugEditeur: CPPFLAGS += -ggdb
DebugEditeur: make_dir $(BIN_DIR)/$(DEBUG_TARGET_EDITEUR)

ReleaseSDL: CPPFLAGS += -O2 -s
ReleaseSDL: make_dir $(BIN_DIR)/$(FINAL_TARGET_SDL)
ReleaseEditeur: CPPFLAGS += -O2 -s
ReleaseEditeur: make_dir $(BIN_DIR)/$(FINAL_TARGET_EDITEUR)

make_dir:
ifeq ($(OS),Windows_NT)
	if not exist $(OBJ_DIR) mkdir $(OBJ_DIR) $(OBJ_DIR)\sdl2 $(OBJ_DIR)\core
else
	test -d $(OBJ_DIR) || mkdir -p $(OBJ_DIR) $(OBJ_DIR)/sdl2 $(OBJ_DIR)/core
endif

$(BIN_DIR)/$(DEBUG_TARGET_SDL): $(SRCS_SDL:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) $+ -o $@ $(LDFLAGS) $(LIBS_SDL)

$(BIN_DIR)/$(DEBUG_TARGET_EDITEUR): $(SRCS_EDITEUR:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) $+ -o $@ $(LDFLAGS) $(LIBS_SDL)
	
$(BIN_DIR)/$(DEBUG_TARGET_MAIN): $(SRCS_MAIN:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) $+ -o $@ $(LDFLAGS)
	
$(BIN_DIR)/$(FINAL_TARGET_EDITEUR): $(SRCS_EDITEUR:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) $+ -o $@ $(LDFLAGS) $(LIBS_SDL)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) -c $(CPPFLAGS) $(INCLUDE_DIR_SDL) $(INCLUDE_DIR) $< -o $@

clean:
ifeq ($(OS),Windows_NT)
	del /f $(OBJ_DIR)\sdl2\*.o $(OBJ_DIR)\core\*.o $(BIN_DIR)\$(FINAL_TARGET_SDL).exe $(BIN_DIR)\$(FINAL_TARGET_EDITEUR).exe
else
	rm -rf $(OBJ_DIR) $(BIN_DIR)/$(FINAL_TARGET_SDL) $(BIN_DIR)/$(FINAL_TARGET_EDITEUR)
endif

doc: $(SRCS_SDL:%.cpp=$(OBJ_DIR)/%.o) $(SRCS_EDITEUR:%.cpp=$(OBJ_DIR)/%.o)
	doxygen doc/monscard.doxy
	firefox doc/html/index.html

